<h4>Una nueva agencia!</h4>

<p>Datos de la agencia:</p>

<ul>
    <li><b>ID:</b> {{ $agency->id }}</li>
    <li><b>Usuario:</b> {{ $agency->usuario }}</li>
    <li><b>Correo:</b> {{ $agency->correo }}</li>
    <li><b>RUC:</b> {{ $agency->ruc }}</li>
    <li><b>Razón Social:</b> {{ $agency->razon_social }}</li>
    <li><b>Nombre Comercial:</b> {{ $agency->nombre_comercial }}</li>
    <li><b>Nombre Responsable:</b> {{ $agency->nombres }}</li>
    <li><b>Teléfono:</b> {{ $agency->telefono }}</li>
    <li><b>Celular:</b> {{ $agency->celular }}</li>
    <li><b>Dirección:</b> {{ $agency->direccion_1 }}</li>
    <li><b>Dirección (Opcional):</b> {{ $agency->direccion_2 }}</li>
</ul>

<p><a href="{{ url('dashboard') }}" target="_blank">Activar Agencia</a></p>

<p>Gracias.</p>


