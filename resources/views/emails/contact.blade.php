<h4>Un nuevo contacto!</h4>

<p>Datos del contacto:</p>

<ul>
    <li><b>ID:</b> {{ $contact->id }}</li>
    <li><b>Nombres:</b> {{ $contact->nombres }}</li>
    <li><b>Apellidos:</b> {{ $contact->apellidos }}</li>
    <li><b>Teléfono:</b> {{ $contact->telefono }}</li>
    <li><b>Correo:</b> {{ $contact->correo }}</li>
    <li><b>Consulta:</b> {{ $contact->consulta }}</li>
</ul>
