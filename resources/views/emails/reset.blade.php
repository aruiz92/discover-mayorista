<h4>Discover Mayorista!</h4>

<p>Sus nuevos datos de acceso a nuestra plataforma son las siguientes:</p>

<ul>
    <li><b>Correo:</b> {{ $user->correo }}</li>
    <li><b>Contraseña:</b> {{ $contrasena }}</li>
</ul>

<p><a href="{{ url('ingreso-agencias') }}" target="_blank">Ingresar</a></p>

<p>Gracias.</p>
