<h4>Un nuevo suscriptor!</h4>

<p>Datos del suscriptor:</p>

<ul>
    <li><b>ID:</b> {{ $subscriber->id }}</li>
    <li><b>Nombres:</b> {{ $subscriber->nombres }}</li>
    <li><b>Apellidos:</b> {{ $subscriber->apellidos }}</li>
    <li><b>Fecha de nacimiento:</b> {{ $subscriber->fecnac }}</li>
    <li><b>Correo:</b> {{ $subscriber->correo }}</li>
    <li><b>Teléfono:</b> {{ $subscriber->telefono }}</li>
    <li><b>Dirección:</b> {{ $subscriber->direccion }}</li>
</ul>
