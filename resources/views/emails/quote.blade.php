<h4>Cotización #{{ $quote->id }}</h4>
<hr>
<strong>Agencia:</strong> {{ $quote->agency->nombre_comercial }}<br>
<hr>
<strong>Cliente:</strong> {{ $quote->nombres }}<br>
<strong>Correo:</strong> {{ $quote->correo }}<br>
<hr>
<strong>Paquete:</strong> {{ $quote->package->nombre }}<br>
<strong>URL:</strong> <a href="{{ url('package/'.$quote->package->id.'-'.str_slug($quote->package->nombre)) }}" target="_blank">Ver</a> <br>
<hr>
<strong>Mensaje:</strong>
<p>{{ $quote->mensaje }}</p>
