<h4>Cotización #{{ $quote->id }}</h4>
<hr>
<strong>Agencia:</strong> {{ $quote->agency_user->agency->nombre_comercial }}<br>
<strong>Agente:</strong> {{ $quote->agency_user->nombre }}<br>
<strong>Correo:</strong> {{ $quote->agency_user->correo }}<br>
<hr>
<strong>Paquete:</strong> {{ $quote->package->nombre }}<br>
<strong>URL:</strong> <a href="{{ url('package/'.$quote->package->id.'-'.str_slug($quote->package->nombre)) }}" target="_blank">Ver</a> <br>
<hr>
<strong>Mensaje:</strong>
<p>{{ $quote->mensaje }}</p>
