@extends('frontend.layout.layout-internal') @section('title', 'Paquete') @section('description', 'Discover')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-513.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/paquete.custom.css')}}" />
@stop

@section('custom-script')

@stop

@section('style')

@stop

@section('body-style')
post-template-default single single-post postid-513 single-format-standard categoria-id-8 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text
@stop


@section('content')

    @include('frontend.partials.header')

    <div id="sliders-container"></div>

    <div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
        <div class="fusion-page-title-row">
            <div class="fusion-page-title-wrapper">

                <div class="fusion-page-title-secondary">
                    <div class="fusion-breadcrumbs"><span><a href="{{ route('home') }}"><span>Home</span></a>
                        </span><span class="fusion-breadcrumb-sep">/</span><span><a href=""><span>Perú</span></a>
                        </span><span class="fusion-breadcrumb-sep">/</span><span><a href="{{ route('mundo-vacaciones') }}"><span>Vacaciones</span></a>
                        </span><span class="fusion-breadcrumb-sep">/</span><span class="breadcrumb-leaf">CANCÚN / RIVIERA MAYA</span>
                    </div>
                </div>
                <div class="fusion-page-title-captions">
                    <h1 class="entry-title">Vacaciones</h1>
                </div>
            </div>
        </div>
    </div>

    <div id="main" role="main" class="clearfix " style="">
        <div class="fusion-row" style="">

            <div id="content" style="width: 100%;">

                <article id="post-513" class="post post-513 type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">

                    <div class="fusion-flexslider flexslider fusion-flexslider-loading post-slideshow fusion-post-slideshow">
                        <ul class="slides">
                            <li>
                                <a href="{{ url('paquetes/cancun-collage-2.jpg')}}" data-rel="iLightbox[gallery513]" title="" data-title="CancúnCollage2" data-caption="" aria-label="CancúnCollage2">
                                    <span class="screen-reader-text">View Larger Image</span>
                                    <img width="275" height="420" src="{{ url('cancun-collage-2.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(max-width: 800px) 100vw, 1300px" />
                                </a>
                            </li>
                        </ul>
                    </div>

                    <h2 class="entry-title fusion-post-title">CANCÚN / RIVIERA MAYA</h2>

                    <div class="post-content">
                        <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
                            <div class="fusion-builder-row fusion-row ">
                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth fusion-column-first 3_5" style='margin-top:0px;margin-bottom:20px;width:60%;width:calc(60% - ( ( 4% ) * 0.6 ) );margin-right: 4%;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                        <span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none">
                                            <img src="{{ url('images/Mailing-Bloqueos-Fiestas-Patrias-Cun-Faranda_.jpg') }}" width="800" height="1202" alt="" title="Mailing Bloqueos Fiestas Patrias Cun-Faranda_" class="img-responsive wp-image-1145" srcset="" sizes="(max-width: 800px) 100vw, 1300px" />
                                        </span>
                                        <div class="fusion-clearfix"></div>
                                    </div>
                                </div>

                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_2_5  fusion-two-fifth fusion-column-last contenido-vacaciones 2_5" style='margin-top:0px;margin-bottom:20px;width:40%;width:calc(40% - ( ( 4% ) * 0.4 ) );'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                        <h3 class="titulo-paquete">CANCÚN / RIVIERA MAYA</h3>
                                        <h4 id="Fecha-paq" class="fecha-inicio-fin">
                                            <strong>6 <span class="span-dias"> días</span></strong> <strong>5 <span class="span-noches"> noches</span></strong>
                                        </h4>
                                        <p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur
                                            incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>
                                        <a href="#" target="_blank" class="boton-1" download>Descargar Información Detallada</a>
                                            <div class="fusion-sep-clear"></div>
                                            <div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:5px;margin-bottom:5px;width:100%;max-width:100%;"></div>
                                            <div class="fusion-button-wrapper">
                                                <style type="text/css" scoped="scoped">
                                                    .fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{width:auto;}
                                                </style>
                                                <a class="fusion-button button-flat fusion-button-round button-large button-default button-1 boton-2" target="_self" title="Solicitar Cotización" href="#" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar"><span class="fusion-button-text">Solicitar Cotización</span></a>
                                            </div>
                                            <div class="fusion-sep-clear"></div>
                                            <div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:15px;margin-bottom:15px;width:100%;max-width:100%;"></div>

                                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                            <div class="addthis_inline_share_toolbox"></div>
                                            <div class="fusion-modal modal fade modal-1 Form_Cotizar max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true" id="form_cotizar_paq">
        <style type="text/css">
            .modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}
        </style>
        <div class="modal-dialog modal-lg">
            <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Solicita una cotización</h3>
                </div>
                <div class="modal-body">
                    <p>Estas solicitando cotización de:</p>
                    <h3>CANCÚN / RIVIERA MAYA</h3>
                    <h4> &#8211; </h4>
                    <div role="form" class="wpcf7" id="wpcf7-f162-p513-o1" lang="es-ES" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <form action="/discover/peru/vacaciones-peru/cancun-riviera-maya/#wpcf7-f162-p513-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="162" />
                                <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p513-o1" />
                                <input type="hidden" name="_wpcf7_container_post" value="513" />
                            </div>
                            <p>
                                <label> Nombre (requerido)<br />
                                    <span class="wpcf7-form-control-wrap Nombre"><input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span>
                                </label>
                            </p>
                            <p>
                                <label> Tu correo electrónico (requerido)<br />
                                <span class="wpcf7-form-control-wrap Email">
                                    <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span>
                                </label>
                            </p>
                            <p>
                                <label> Asunto<br />
                                    <span class="wpcf7-form-control-wrap Asunto"><input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" /></span>
                                </label>
                            </p>
                            <p>
                                <label> Mensaje<br />
                                    <span class="wpcf7-form-control-wrap Mensaje"><textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span>
                                </label>
                            </p>
                            <p>
                                <span class="wpcf7-form-control-wrap Producto"><input type="hidden" name="Producto" value="CANCÚN / RIVIERA MAYA" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" /></span>
                                <span class="wpcf7-form-control-wrap URL"><input type="hidden" name="URL" value="{{ route('paquete') }}" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden"
                                        aria-invalid="false" /></span>
                            </p>
                            <p style="text-align:right">
                                <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                            </p>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="fusion-button button-default button-medium button default medium" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:35px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:20px;'>
                                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                            <div class="fusion-tabs fusion-tabs-1 classic tabs-post-interno horizontal-tabs">
                                                <style type="text/css">
                                                    .fusion-tabs.fusion-tabs-1 .nav-tabs li a{border-top-color:#ebeaea;background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{border-right-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#ffffff;border-top-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}
                                                </style>
                                                <div class="nav">
                                                    <ul class="nav-tabs nav-justified">
                                                        <li class="active">
                                                            <a class="tab-link" data-toggle="tab" id="fusion-tab-sinboletosaéreos" href="#tab-505270cc30833e12e56">
                                                                <h4 class="fusion-tab-heading">Sin Boletos Aéreos</h4>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="tab-link" data-toggle="tab" id="fusion-tab-conboletosaéreos" href="#tab-7430b7e76c58d1ba01a">
                                                                <h4 class="fusion-tab-heading">Con Boletos Aéreos</h4>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="tab-link" data-toggle="tab" id="fusion-tab-itinerario" href="#tab-6507278a7682a48e962">
                                                                <h4 class="fusion-tab-heading">Itinerario</h4>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="tab-link" data-toggle="tab" id="fusion-tab-recomendaciones" href="#tab-701402fb54c05e60222">
                                                                <h4 class="fusion-tab-heading">Recomendaciones</h4>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a class="tab-link" data-toggle="tab" id="fusion-tab-informacióndepagos" href="#tab-a3271d0a0f95e3ac9d1">
                                                                <h4 class="fusion-tab-heading">Información de pagos</h4>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="tab-content">
                <div class="nav fusion-mobile-tab-nav">
                    <ul class="nav-tabs nav-justified">
                        <li class="active">
                            <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-sinboletosaéreos" href="#tab-505270cc30833e12e56">
                                <h4 class="fusion-tab-heading">Sin Boletos Aéreos</h4>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade in active" id="tab-505270cc30833e12e56">
                    <p><strong>Incluye:</strong></p>
                    <ul>
                        <li>Veniam melius nusquam mei ad.</li>
                        <li>Mea ea decore impedit voluptaria.</li>
                        <li>Mei eu dolorum explicari. Ridens utroque instructior eu eum.</li>
                        <li>Qui viderer feugait id. Nam no ignota abhorreant.</li>
                        <li>Hinc brute eu sea, atqui iracundia sadipscing ei cum.</li>
                        <li>Assum elitr dolorem cu sea, vis id esse putant praesent.</li>
                    </ul>
                </div>
                <div class="nav fusion-mobile-tab-nav">
                    <ul class="nav-tabs nav-justified">
                        <li>
                            <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-conboletosaéreos" href="#tab-7430b7e76c58d1ba01a">
                                <h4 class="fusion-tab-heading">Con Boletos Aéreos</h4>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="tab-7430b7e76c58d1ba01a">
                    <p><strong>Incluye:</strong></p>
                    <ul>
                        <li>Boletos aéreos Lima / Panamá / Cancún / Panamá / Lima vía Copa Airlines</li>
                        <li>IGV TKT, Queues $236, DY$15.00, XT$162.22, HW$30.74</li>
                        <li>05 Noches de alojamiento</li>
                        <li>Sistema de alimentación TODO INCLUIDO</li>
                        <li>Impuestos hoteleros y aéreos</li>
                    </ul>
                </div>
                <div class="nav fusion-mobile-tab-nav">
                    <ul class="nav-tabs nav-justified">
                        <li>
                            <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-itinerario" href="#tab-6507278a7682a48e962">
                                <h4 class="fusion-tab-heading">Itinerario</h4>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="tab-6507278a7682a48e962">
                    <table>
                        <tbody>
                            <tr>
                                <td><strong>FECHA</strong></td>
                                <td><strong>VUELOS</strong></td>
                                <td colspan="2"><strong>RUTA</strong></td>
                                <td><strong>SALIDA</strong></td>
                                <td><strong>LLEGADA</strong></td>
                            </tr>
                            <tr>
                                <td>25 JUL</td>
                                <td>CM 492</td>
                                <td>LIMA</td>
                                <td>PANAMÁ</td>
                                <td>04:49</td>
                                <td>08:30</td>
                            </tr>
                            <tr>
                                <td>25 JUL</td>
                                <td>CM 270</td>
                                <td>PANAMÁ</td>
                                <td>CANCÚN</td>
                                <td>12:50</td>
                                <td>15:30</td>
                            </tr>
                            <tr>
                                <td>30 JUL</td>
                                <td>CM 354</td>
                                <td>CANCÚN</td>
                                <td>PANAMÁ</td>
                                <td>08:02</td>
                                <td>10:36</td>
                            </tr>
                            <tr>
                                <td>30 JUL</td>
                                <td>CM 131</td>
                                <td>PANAMÁ</td>
                                <td>LIMA</td>
                                <td>11:47</td>
                                <td>15:26</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="nav fusion-mobile-tab-nav">
                    <ul class="nav-tabs nav-justified">
                        <li>
                            <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-recomendaciones" href="#tab-701402fb54c05e60222">
                                <h4 class="fusion-tab-heading">Recomendaciones</h4>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="tab-701402fb54c05e60222">
                    <p>Recomendaciones</p>
                </div>
                <div class="nav fusion-mobile-tab-nav">
                    <ul class="nav-tabs nav-justified">
                        <li>
                            <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-informacióndepagos" href="#tab-a3271d0a0f95e3ac9d1">
                                <h4 class="fusion-tab-heading">Información de pagos</h4>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane fade" id="tab-a3271d0a0f95e3ac9d1">
                    <p><strong>NOMBRE DE CUENTA:   DISCOVER MAYORISTA DE TURISMO SAC<br /></strong></p>
                    <p>BCP DOLARES: 193-2321123-1-96</p>
                    <p>BCP SOLES: 193-35475836-0-76</p>
                    <p>SCOTIABANK DOLARES: 000-4717806</p>
                    <p>SCOTIABANK SOLES: 036-7471430</p>
                    <p><strong> </strong></p>
                    <p><strong>Pagos con tarjeta de crédito:</strong><br /> Considerar el 5% adicional</p>
                    <ul>
                        <li>Visa (cualquier tarjeta asociada a visa, débito o crédito en dólares).</li>
                        <li>Mastercard</li>
                        <li>American Express</li>
                        <li>Diners Club</li>
                        <li>Discover</li>
                        <li>Union Pay</li>
                        <li>Ripley</li>
                        <li>CMR Falabella</li>
                        <li>OH!</li>
                        <li>CENCOSUD</li>
                    </ul>
                    <p>Todas las cobranzas de Agencias en Lima deben de pasar obligatoriamente sus tarjetas vía POS. Para esto el cliente deberá presentar obligatoriamente un documento de identidad (DNI/Pasaporte/Carnet de Extranjería,
                        etc.) al momento de realizar la transacción, lo cual confirmará la pertenencia de la tarjeta a procesar.</p>
                </div>
                                                </div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="fusion-meta-info">
                            <div class="fusion-meta-info-wrapper">

                                <span class="vcard rich-snippet-hidden">
                                <span class="fn">
                                    <a href="{{ route('mayorista') }}" rel="author">Discover Mayorista</a></span>
                                </span>

                                <span class="updated rich-snippet-hidden">2018-01-28T21:49:07+00:00       </span>
                                <span>mayo 25, 2017</span><span class="fusion-inline-sep">|</span>
                            </div>
                        </div>
                        <div class="related-posts single-related-posts">
                            <h3 class="title-heading-left title-post-relacionados">
                                Otros Bloqueos Vigentes </h3>
                            <div class="fusion-carousel fusion-carousel-title-below-image" data-imagesize="fixed" data-metacontent="yes" data-autoplay="no" data-touchscroll="no" data-columns="3" data-itemmargin="44px" data-itemwidth="180" data-touchscroll="yes" data-scrollitems="1">
                                <div class="fusion-carousel-positioner">
                                    <div class="max-width-1030">
                                        <ul class="fusion-carousel-holder">
                                            <li class="fusion-carousel-item">
                                                <div class="fusion-carousel-item-wrapper">

                                                    <div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                                        <a href="{{ route('paquete') }}">
                                                            <img src="{{ url('images/Lima-Tradicional-500x383.jpg') }}" srcset="" width="500" height="383" alt="¡Últimos Espacios!!! Iguazú  Fiestas Patrias" />
                                                        </a>

                                                    </div>
                                                    <div class="contenedor-info-related-post">
                                                        <h4 class="fusion-carousel-title">
                                                            <a href="{{ route('paquete') }}" _self>¡Últimos Espacios!!! Iguazú  Fiestas Patrias</a>
                                                        </h4>
                                                        <div class="fusion-carousel-meta info-adicional">
                                                            <span class="fusion-date" style="display:none">mayo 25, 2017</span>

                                                            <span class="fusion-inline-sep" style="display:none">|</span>
                                                            <div class="info-adicional-content"> 6D - 5N<br />$ 835 </div>
                                                            <a class="leer-mas-related-post" href="{{ route('paquete') }}" _self>Leer más</a>
                                                        </div>
                                                        <!-- fusion-carousel-meta -->
                                                    </div>
                                                </div>
                                                <!-- fusion-carousel-item-wrapper -->
                                            </li>
                                            <li class="fusion-carousel-item">
                                                <div class="fusion-carousel-item-wrapper">
                                                    <div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                                        <a href="{{ route('paquete') }}">
                                                            <img src="{{ url('images/Lima-Tradicional-500x383.jpg') }}" srcset="" width="500" height="383" alt="Los Mejores Conciertos en Las Vegas" />
                                                        </a>
                                                    </div>
                                                    <div class="contenedor-info-related-post">
                                                        <h4 class="fusion-carousel-title">
                                                            <a href="{{ route('paquete') }}" _self>Los Mejores Conciertos en Las Vegas</a>
                                                        </h4>

                                                        <div class="fusion-carousel-meta info-adicional">
                                                            <span class="fusion-date" style="display:none">mayo 25, 2017</span>

                                                            <span class="fusion-inline-sep" style="display:none">|</span>
                                                            <div class="info-adicional-content"> 4D - 3N<br />$ 459 </div>
                                                            <a class="leer-mas-related-post" href="{{ route('paquete') }}" _self>Leer más</a>
                                                        </div>
                                                        <!-- fusion-carousel-meta -->
                                                    </div>
                                                </div>
                                                <!-- fusion-carousel-item-wrapper -->
                                            </li>
                                            <li class="fusion-carousel-item">
                                                <div class="fusion-carousel-item-wrapper">

                                                    <div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                                        <a href="{{ route('paquete') }}">
                                                            <img src="{{ url('images/Lima-Tradicional-500x383.jpg') }}" srcset="" width="500" height="383" alt="Colombia es Realismo Mágico" />
                                                        </a>
                                                    </div>
                                                    <div class="contenedor-info-related-post">
                                                        <h4 class="fusion-carousel-title">
                                                            <a href="{{ route('paquete') }}" _self>Colombia es Realismo Mágico</a>
                                                        </h4>

                                                        <div class="fusion-carousel-meta info-adicional">
                                                            <span class="fusion-date" style="display:none">mayo 25, 2017</span>

                                                            <span class="fusion-inline-sep" style="display:none">|</span>
                                                            <div class="info-adicional-content"> 4D - 3N<br />$ 445 </div>
                                                            <a class="leer-mas-related-post" href="{{ route('paquete') }}" _self>Leer más</a>
                                                        </div>
                                                        <!-- fusion-carousel-meta -->
                                                    </div>
                                                </div>
                                                <!-- fusion-carousel-item-wrapper -->
                                            </li>
                                            <li class="fusion-carousel-item">
                                                <div class="fusion-carousel-item-wrapper">

                                                    <div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                                        <a href="{{ route('paquete') }}">
                                                            <img src="{{ url('images/Lima.jpg') }}" srcset="" width="500" height="383" alt="Caribbean Pride Chic Punta Cana" />
                                                        </a>
                                                    </div>
                                                    <div class="contenedor-info-related-post">
                                                        <h4 class="fusion-carousel-title">
                                                            <a href="{{ route('paquete') }}" _self>Caribbean Pride Chic Punta Cana</a>
                                                        </h4>

                                                        <div class="fusion-carousel-meta info-adicional">
                                                            <span class="fusion-date" style="display:none">mayo 25, 2017</span>

                                                            <span class="fusion-inline-sep" style="display:none">|</span>
                                                            <div class="info-adicional-content"> 8D - 7N<br />$ 1699 </div>
                                                            <a class="leer-mas-related-post" href="{{ route('paquete') }}" _self>Leer más</a>
                                                        </div>
                                                        <!-- fusion-carousel-meta -->
                                                    </div>
                                                </div>
                                                <!-- fusion-carousel-item-wrapper -->
                                            </li>
                                            <li class="fusion-carousel-item">
                                                <div class="fusion-carousel-item-wrapper">

                                                    <div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                                        <a href="{{ route('paquete') }}">
                                                            <img src="{{ url('images/Lima-Tradicional-500x383.jpg') }}" srcset="" width="500" height="383" alt="Vacaciones de Lujo por Fiestas Patrias  The Grand At Moon Palace" />
                                                        </a>
                                                    </div>
                                                    <div class="contenedor-info-related-post">
                                                        <h4 class="fusion-carousel-title">
                                                            <a href="{{ route('paquete') }}" _self>Vacaciones de Lujo por Fiestas Patrias  The Grand At Moon Palace</a>
                                                        </h4>

                                                        <div class="fusion-carousel-meta info-adicional">
                                                            <span class="fusion-date" style="display:none">mayo 25, 2017</span>

                                                            <span class="fusion-inline-sep" style="display:none">|</span>
                                                            <div class="info-adicional-content"> 6D - 5N<br />$ 1769 </div>
                                                            <a class="leer-mas-related-post" href="{{ route('paquete') }}" _self>Leer más</a>
                                                        </div>
                                                        <!-- fusion-carousel-meta -->
                                                    </div>
                                                </div>
                                                <!-- fusion-carousel-item-wrapper -->
                                            </li>
                                        </ul>
                                        <!-- fusion-carousel-holder -->
                                    </div>
                                    <div class="fusion-carousel-nav">
                                        <span class="fusion-nav-prev"></span>
                                        <span class="fusion-nav-next"></span>
                                    </div>

                                </div>
                                <!-- fusion-carousel-positioner -->
                            </div>
                            <!-- fusion-carousel -->
                        </div>
                        <!-- related-posts -->
                    </article>
                </div>

            </div>
        <!-- fusion-row -->
        </div>
        <!-- #main -->
@stop

@section('script')

<script type='text/javascript' src="{{ url('js/2b853892b45b6c7c62e273b1eba75fcb.js') }}"></script>

@stop
