@extends('frontend.layout.layout-internal') @section('title', 'Ingreso de agentes') @section('description', 'Discover') 

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-419.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/ingreso-agentes.custom.css')}}" />
@stop

@section('custom-script')
<script type='text/javascript' src="{{ url('js/contact-script.js') }}"></script>
@stop

@section('style')

@stop

@section('body-style')
page-template-default page page-id-419 top-parent-419 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text
@stop


@section('content')

    @include('frontend.partials.header')

        
<div id="sliders-container"></div>
            
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
    <div class="fusion-page-title-row">
        <div class="fusion-page-title-wrapper">
            <div class="fusion-page-title-secondary">
                <div class="fusion-breadcrumbs">
                    <span><a href="{{ route('home') }}"><span>Home</span></a></span><span class="fusion-breadcrumb-sep">/</span><span class="breadcrumb-leaf">Acceder</span>
                </div>                    
            </div>
            <div class="fusion-page-title-captions">
                <h1 class="entry-title">Sesión de Agentes</h1>
            </div>
        </div>
    </div>
</div>

<div id="main" role="main" class="clearfix " style="">
    <div class="fusion-row" style="">
        <div id="content" style="width: 100%;">
            <div id="post-419" class="post-419 page type-page status-publish hentry">

                <span class="entry-title rich-snippet-hidden">    Acceder        </span>

                <span class="vcard rich-snippet-hidden">
                    <span class="fn">
                        <a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>            
                    </span>
                </span>

                <span class="updated rich-snippet-hidden">2017-06-04T14:49:00+00:00</span>
                                                                                            
                <div class="post-content">
                    <p>
                        <p class="private visitor-content visitor-only">
                            <div  class="fusion-fullwidth fullwidth-box fusion-blend-mode user-not-login nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:0px;width:48%; margin-right: 4%;'>
                                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                            <div id="wpmem_login">
                                                <a name="login"></a>
                                                <form action="#" method="POST" id="" class="form">
                                                    <fieldset>
                                                        <legend>Usuarios Existentes Entrar</legend>
                                                        <label for="log">Nombre de usuario</label>
                                                        <div class="div_text"><input name="log" type="text" id="log" value="" class="username" /></div>
                                                        <label for="pwd">Contraseña</label>
                                                        <div class="div_text">
                                                            <input name="pwd" type="password" id="pwd" class="password" />
                                                        </div>
                                                        <input name="redirect_to" type="hidden" value="#" />
                                                        <input name="a" type="hidden" value="login" />
                                                        <div class="button_div"><input name="rememberme" type="checkbox" id="rememberme" value="forever" />&nbsp;<label for="rememberme">Recuérdame</label>&nbsp;&nbsp;<input type="submit" name="Submit" value="Acceder" class="buttons" /></div>
                                                        <div align="right" class="link-text">¿Olvidó su clave?&nbsp;<a href="http://www.proyectoswebtilia.info/discover/perfil/?a=pwdreset">Pulse para reiniciar</a></div>
                                                        <div align="right" class="link-text">¿Nuevo usuario?&nbsp;<a href="{{ route('registro-agentes') }}">Haga clic aquí para registrarse</a></div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="fusion-clearfix"></div>
                                        </div>
                                    </div>
                                    <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last invitacion-a-registro 1_2"  style='margin-top:0px;margin-bottom:0px;width:48%'>
                                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                            <div class="fusion-title title fusion-sep-none fusion-title-size-three title-h3 color-verde" style="margin-top:0px;margin-bottom:31px;">
                                                <h3 class="title-heading-left">¿Deseas formar parte de nuestra gran red de agentes de viajes?</h3>
                                            </div>
                                            <p>Registra tu empresa en nuestra web y llega a todos nuestros usuarios alrededor del mundo.</p>
                                            <div class="fusion-button-wrapper"><style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{background: #01a13d;}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #005494;}.fusion-button.button-1{width:auto;}</style><a class="fusion-button button-flat fusion-button-square button-large button-custom button-1 boton-verde" target="_self" title="Registrarse" href="{{ route('registro-agentes') }}"><span class="fusion-button-text">Registrarse</span></a></div>
                                            <div class="fusion-clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </p>
                    </p>
                </div>
            </div>
        </div>
    </div>  <!-- fusion-row -->
</div>  <!-- #main -->

@stop


@section('script')
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
@stop
