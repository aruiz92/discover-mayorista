@extends('frontend.layout.layout-internal') @section('title', 'Registro de agentes') @section('description', 'Discover')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-421.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/registro-agentes.custom.css')}}" />
@stop

@section('custom-script')
<script src="{{ url('js/bloqueo.custom.js')}}"></script>
@stop

@section('style')

@stop

@section('body-style')
page-template-default page page-id-421 top-parent-421 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop

@section('content')

@include('frontend.partials.header')

<div id="sliders-container"></div>
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
	<div class="fusion-page-title-row">
		<div class="fusion-page-title-wrapper">
			<div class="fusion-page-title-secondary">
				<div class="fusion-breadcrumbs">
					<span>
						<a href="{{ route('home') }}">
							<span>Home</span>
						</a>
					</span>
					<span class="fusion-breadcrumb-sep">/</span>
					<span class="breadcrumb-leaf">Registrarse</span>
				</div>
			</div>
			<div class="fusion-page-title-captions">
				<h1 class="entry-title">Registrarse</h1>
			</div>
		</div>
	</div>
</div>
<div id="main" role="main" class="clearfix " style="">
	<div class="fusion-row" style="">
		<div id="content" style="width: 100%;">
			<div id="post-421" class="post-421 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">
Registrarse		</span>
				<span class="vcard rich-snippet-hidden">
					<span class="fn">
						<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
					</span>
				</span>
				<span class="updated rich-snippet-hidden">
2017-06-04T19:10:51+00:00		</span>
				<div class="post-content">
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-first 1_5"  style='margin-top:0px;margin-bottom:0px;width:16.8%; margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth invitacion-a-registro 3_5"  style='margin-top:0px;margin-bottom:0px;width:58.4%; margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-three title-h3 color-verde" style="margin-top:0px;margin-bottom:31px;">
										<h3 class="title-heading-center">Registra a tu Agente</h3>
									</div>
									<div class="fusion-sep-clear"></div>
									<div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:10px;margin-bottom:10px;width:100%;max-width:100%;"></div>
									<p style="text-align: center;">Siempre brindamos los mejores paquetes de viajes a precios increíbles.

										<br />
Llena este formulario y se parte de nuestros agentes

									</p>
									<div id="wpmem_reg">
										<a name="register"></a>
										<form name="form" method="post" action="#" id="" class="form">
											<fieldset>
												<legend>Registro de Nuevo Usuario</legend>
												<label for="user_login" class="text">Escoja un Nombre de Usuario

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="user_login" type="text" id="user_login" value="" class="textbox" required  />
												</div>
												<label for="ruc" class="text">RUC

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="ruc" type="text" id="ruc" value="" class="textbox" required  />
												</div>
												<label for="razonsocial" class="text">Razón Social

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="razonsocial" type="text" id="razonsocial" value="" class="textbox" required  />
												</div>
												<label for="nombre_comercial" class="text">Nombre Comercial

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="nombre_comercial" type="text" id="nombre_comercial" value="" class="textbox" required  />
												</div>
												<label for="phone1" class="text">Teléfono

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="phone1" type="text" id="phone1" value="" class="textbox" required  />
												</div>
												<label for="celular" class="text">Celular</label>
												<div class="div_text">
													<input name="celular" type="text" id="celular" value="" class="textbox" />
												</div>
												<label for="addr1" class="text">Dirección (línea 1)

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="addr1" type="text" id="addr1" value="" class="textbox" required  />
												</div>
												<label for="addr2" class="text">Dirección (línea 2)</label>
												<div class="div_text">
													<input name="addr2" type="text" id="addr2" value="" class="textbox" />
												</div>
												<label for="city" class="text">Ciudad

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="city" type="text" id="city" value="" class="textbox" required  />
												</div>
												<label for="distrito" class="text">Distrito

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="distrito" type="text" id="distrito" value="" class="textbox" required  />
												</div>
												<label for="thestate" class="text">Provincia

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="thestate" type="text" id="thestate" value="" class="textbox" required  />
												</div>
												<label for="country" class="text">País

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="country" type="text" id="country" value="" class="textbox" required  />
												</div>
												<label for="first_name" class="text">Nombre

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="first_name" type="text" id="first_name" value="" class="textbox" required  />
												</div>
												<label for="last_name" class="text">Apellidos

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="last_name" type="text" id="last_name" value="" class="textbox" required  />
												</div>
												<label for="nombre_contacto" class="text">Nombre del contacto rersponsable

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="nombre_contacto" type="text" id="nombre_contacto" value="" class="textbox" required  />
												</div>
												<label for="user_email" class="text">Email

													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="user_email" type="email" id="user_email" value="" class="textbox" required  />
												</div>
												<label class="text" for="captcha">Input the code:</label>
												<div class="div_text">
													<input id="captcha_code" name="captcha_code" size="4" type="text" />
													<input id="captcha_prefix" name="captcha_prefix" type="hidden" value="640365384" />
													<img src="{{ url('images/imagen-captcha.png') }}" alt="captcha" width="72" height="30" />
												</div>
												<input name="a" type="hidden" value="register" />
												<input name="wpmem_reg_page" type="hidden" value="#" />
												<div class="button_div">
													<input name="submit" type="submit" value="Registrarse" class="buttons" />
												</div>
												<div class="req-text">
													<span class="req">*</span>Campo Obligatorio

												</div>
											</fieldset>
										</form>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-last 1_5"  style='margin-top:0px;margin-bottom:0px;width:16.8%'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->


@stop


@section('script')
<script type='text/javascript' src="{{ url('js/7fe5606794b62066a68cdda469435491.js') }}"></script>
@stop
