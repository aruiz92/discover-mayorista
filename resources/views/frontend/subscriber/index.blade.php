@extends('frontend.layout.layout-internal') @section('title', 'Registro de suscriptores') @section('description', 'Discover')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-365.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/registro-suscriptores.custom.css')}}" />
@stop

@section('style')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<link rel="stylesheet" href="{{ url('css/bootstrap-select.css') }}"/>
@stop

@section('custom-script')
<script src="{{ url('js/bloqueo.custom.js')}}"></script>
@stop

@section('body-style')
page-template page-template-100-width page-template-100-width-php page page-id-365 top-parent-365 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop

@section('content')

@include('frontend/partials/header')

<div id="sliders-container"></div>

<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
    <div class="fusion-page-title-row">
        <div class="fusion-page-title-wrapper">
            <div class="fusion-page-title-secondary">
                <div class="fusion-breadcrumbs">
                    <span><a href="{{ route('home') }}"><span>Home</span></a></span>
                    <span class="fusion-breadcrumb-sep">/</span>
                    <span class="breadcrumb-leaf">Registro de Suscriptores</span>
                </div>
            </div>
            <div class="fusion-page-title-captions">
                <h1 class="entry-title">Registro de Suscriptores</h1>
            </div>
        </div>
    </div>
</div>

<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
    <div class="fusion-row" style="max-width:100%;">
        <div id="content" class="full-width">
            <div id="post-365" class="post-365 page type-page status-publish hentry">
                <span class="entry-title rich-snippet-hidden">
                    Registro de Suscriptores
                </span>
                <span class="vcard rich-snippet-hidden">
                    <span class="fn">
                        <a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
                    </span>
                </span>

                <span class="updated rich-snippet-hidden">2017-05-21T20:37:25+00:00        </span>
                <div class="post-content">
                    <div id="Suscribe_Form" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style="background-color: rgba(255,255,255,0);background-image: url('{{ url('images/fondo_registro_turista.jpg') }}');background-position: center bottom;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;">
                    	<div class="fusion-builder-row fusion-row ">

	                    	<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-first 1_5"  style='margin-top:0px;margin-bottom:0px;width:16.8%; margin-right: 4%;'>
		                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
		                            <div class="fusion-clearfix"></div>
		                        </div>
	                    	</div>

		                    <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth 3_5"  style='margin-top:0px;margin-bottom:0px;width:58.4%; margin-right: 4%;'>
                            <div class="frmSuccess text-center hidden">
                              <h3>Gracias! Se ha suscrito correctamente.</h3>
                            </div>
		                        <div class="fusion-column-wrapper wrapper-subscriber" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
		                            <h3 style="text-align: center;">¡Hola!</h3>
		                            <p style="text-align: center;">Sabemos que te gusta viajar, por eso te ofrecemos promociones especiales.<br />Llena tus datos a continuación y disfruta de grandes sorpresas</p>
		                            <div role="form" class="wpcf7" id="wpcf7-f364-p365-o1" lang="es-ES" dir="ltr">
		                                <div class="screen-reader-response"></div>
		                                <form id="frmSuscriptores" action="{{ url('api/subscriber') }}" method="post" class="wpcf7-form" novalidate="novalidate">
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Nombres:</div>
		                                        <div class="col-sm-8">
		                                            <span class="wpcf7-form-control-wrap Nombres"><input type="text" name="nombres" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span>
		                                        </div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Apellidos:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Apellidos"><input type="text" name="apellidos" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Fecha de nacimiento:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Fecha_Nacimiento"><input type="date" name="fecnac" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="1992-01-11" /></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Teléfono:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Telefono"><input type="text" name="telefono" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Dirección:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Direccion"><input type="text" name="direccion" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>

                                        <div class="fila-cf7">
                                          <div class="col-sm-4">Correo:</div>
                                          <div class="col-sm-8"><span class="wpcf7-form-control-wrap Email"><input type="email" name="correo" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span></div>
                                          <div class="fusion-sep-clear"></div>
                                        </div>

                                        <div class="fila-cf7">
		                                        <div class="col-sm-4">País:</div>
		                                        <div class="col-sm-8">
                                              <span class="wpcf7-form-control-wrap Direccion">
                                                <select id="selectPais" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="pais">
                                                  <option value="">Seleccionar</option>
                                                  @foreach($countries as $country)
                                                  <option value="{{ $country->id }}">{{ $country->nombre }}</option>
                                                  @endforeach
                                                </select>
                                              </span>
                                            </div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>

                                        <div class="fila-cf7 group-provincia">
		                                        <div class="col-sm-4">Provincia:</div>
		                                        <div class="col-sm-8">
                                              <span class="wpcf7-form-control-wrap Direccion">
                                                <select id="selectProvincia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="provincia">
                                                  <option value="">Seleccionar</option>
                                                </select>
                                              </span>
                                            </div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>

                                        <div class="fila-cf7 group-distrito">
		                                        <div class="col-sm-4">Distrito:</div>
		                                        <div class="col-sm-8">
                                              <span class="wpcf7-form-control-wrap Direccion">
                                                <select id="selectDistrito" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="distrito">
                                                  <option value="">Seleccionar</option>
                                                </select>
                                              </span>
                                            </div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>

                                        <!-- <div class="fila-cf7 group-agencia">
		                                        <div class="col-sm-4">Agencias:</div>
		                                        <div class="col-sm-8">
                                              <span class="wpcf7-form-control-wrap Direccion">
                                                <select id="selectAgencia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="agencia">
                                                  <option value="">Seleccionar</option>
                                                </select>
                                              </span>
                                            </div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div> -->

		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Destino de Viaje Preferencial:</div>
		                                        <div class="col-sm-8">
                                              <span class="wpcf7-form-control-wrap Destinos">
                                                <select name="Destinos[]" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required selectpicker show-menu-arrow form-control" id="maxOption2m" aria-required="true" aria-invalid="false" multiple="multiple">
                                                  <option value="Destino1">Destino1</option>
                                                  <option value="Destino2">Destino2</option>
                                                  <option value="Destino3">Destino3</option>
                                                  <option value="Destino4">Destino4</option>
                                                </select>
                                              </span>
                                            </div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4 fila-newsletter-subs">¿Te gustaría recibir información de nuestros paquetes y promociones?:</div>
		                                        <div class="col-sm-8">
                                              <span class="wpcf7-form-control-wrap Recibir_Newsletter">
                                                <span class="wpcf7-form-control wpcf7-radio">
                                                  <span class="wpcf7-list-item first">
                                                    <label>
                                                      <input type="radio" name="newsletter" value="1" checked="checked" />
                                                      <span class="wpcf7-list-item-label">Si</span>
                                                    </label>
                                                  </span>
                                                  <span class="wpcf7-list-item last">
                                                    <label>
                                                      <input type="radio" name="newsletter" value="0" />
                                                      <span class="wpcf7-list-item-label">No</span>
                                                    </label>
                                                  </span>
                                                </span>
                                              </span>
                                            </div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <p style="text-align:center">
                                          <input id="btnSubmit" type="submit" value="Suscribirse" class="wpcf7-form-control wpcf7-submit" />
                                        </p>
		                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
		                                </form>
		                            </div>
		                            <div class="fusion-clearfix"></div>
		                        </div>
		                    </div>
		                    <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-last 1_5"  style='margin-top:0px;margin-bottom:0px;width:16.8%'>
		                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
		                            <div class="fusion-clearfix"></div>
		                        </div>
		                    </div>
                		</div>
            		</div>
        		</div>
    		</div>
		</div>
	</div>  <!-- fusion-row -->
</div>  <!-- #main -->

@stop

@section('script')
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  jQuery.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
    }
  });

  jQuery('#selectPais').change(function () {
    var value = jQuery(this).val();

    if (value == 1) {
      jQuery('.group-provincia').addClass('d-block');
      var form = {
        method: 'GET',
        action: '{{ url("api/province") }}/' + value,
        data: {}
      }

      ajaxLocation(form, 'selectProvincia');

    } else {
      jQuery('.group-provincia').removeClass('d-block');
      jQuery('.group-distrito').removeClass('d-block');
    }
  });

  jQuery('#selectProvincia').change(function () {
    var value = jQuery(this).val();
    jQuery('.group-distrito').addClass('d-block');

    var form = {
      method: 'GET',
      action: '{{ url("api/district") }}/' + value,
      data: {}
    }

    ajaxLocation(form, 'selectDistrito');
  });

  // jQuery('#selectDistrito').change(function () {
  //   var value = jQuery(this).val();
  //   jQuery('.group-agencia').addClass('d-block');

  //   var form = {
  //     method: 'GET',
  //     action: '{{ url("api/agencies/district/") }}/' + value,
  //     data: {}
  //   }

  //   ajaxLocation(form, 'selectAgencia');
  // });

  jQuery('#frmSuscriptores').submit(function( event ) {
    var action = jQuery(this).attr('action');
    var method = jQuery(this).attr('method');
    var data = jQuery(this).serialize();

    jQuery('#btnSubmit').val('Suscribiendo...');

    jQuery.ajax({
      method: method,
      url: action,
      data: data,
      success: function (response) {
        removeClassError();
        clearForm();
        jQuery('.group-provincia').removeClass('d-block');
        jQuery('.group-distrito').removeClass('d-block');
        jQuery('.group-agencia').addClass('d-block');

        jQuery('.wrapper-subscriber').hide();
        jQuery('.frmSuccess').removeClass('hidden');
      }
    })
    .fail(function(error) {
      var status = error.status;
      var data = error.responseJSON;

      jQuery('#btnSubmit').val('Suscribirse');

      if (status === 422) {
        removeClassError();
        jQuery.each(data, function(key, value) {
          var input = jQuery('#frmSuscriptores *[name="' + key + '"]').addClass('has-error');
        });
      }
    });

    event.preventDefault();
  });

  function clearForm() {
    document.getElementById('frmSuscriptores').reset();
  }

  function removeClassError () {
    jQuery('#frmSuscriptores input, #frmSuscriptores select').removeClass('has-error');
  }

  function removeOption (select) {
    jQuery('#' + select).empty();
    jQuery('#' + select).append(jQuery('<option>', {
      value: "",
      text : 'Seleccionar'
    }));
  }

  function ajaxLocation (form, select) {
    removeOption(select);

    jQuery.ajax({
      method: form.method,
      url: form.action,
      data: form.data,
      success: function (response) {
        jQuery.each(response, function (key, item) {
          if (select === 'selectAgencia') {
            jQuery('#' + select).append(jQuery('<option>', {
              value: item.id,
              text : item.nombre_comercial
            }));
          } else {
            jQuery('#' + select).append(jQuery('<option>', {
              value: item.id,
              text : item.nombre
            }));
          }
        });
      }
    });
  }
</script>
@stop
