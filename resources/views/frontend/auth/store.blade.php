@extends('frontend.layout.layout-internal') @section('title', 'Registro de agentes') @section('description', 'Discover')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-421.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/registro-agentes.custom.css')}}" />
@stop

@section('custom-script')
<script src="{{ url('js/bloqueo.custom.js')}}"></script>
@stop

@section('style')

@stop

@section('body-style')
page-template-default page page-id-421 top-parent-421 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop

@section('content')

@include('frontend.partials.header')

<div id="sliders-container"></div>
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
	<div class="fusion-page-title-row">
		<div class="fusion-page-title-wrapper">
			<div class="fusion-page-title-secondary">
				<div class="fusion-breadcrumbs">
					<span>
						<a href="{{ route('home') }}">
							<span>Home</span>
						</a>
					</span>
					<span class="fusion-breadcrumb-sep">/</span>
					<span class="breadcrumb-leaf">Registrarse</span>
				</div>
			</div>
			<div class="fusion-page-title-captions">
				<h1 class="entry-title">Registrarse</h1>
			</div>
		</div>
	</div>
</div>
<div id="main" role="main" class="clearfix " style="">
	<div class="fusion-row" style="">
		<div id="content" style="width: 100%;">
			<div class="frmSuccess hidden">
				<h3>¡Gracias!</h3>
				<p>Hemos registrado correctamente tu agencia. Un administrador validará tu registro y te enviará un correo con los datos de acceso a tu cuenta.</p>
			</div>
			<div id="post-421" class="post-421 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">
Registrarse		</span>
				<span class="vcard rich-snippet-hidden">
					<span class="fn">
						<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
					</span>
				</span>
				<span class="updated rich-snippet-hidden">
2017-06-04T19:10:51+00:00		</span>
				<div class="post-content">
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-first 1_5"  style='margin-top:0px;margin-bottom:0px;width:16.8%; margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth invitacion-a-registro 3_5"  style='margin-top:0px;margin-bottom:0px;width:58.4%; margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-three title-h3 color-verde" style="margin-top:0px;margin-bottom:31px;">
										<h3 class="title-heading-center">Registra tu Agencia</h3>
									</div>
									<div class="fusion-sep-clear"></div>
									<div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:10px;margin-bottom:10px;width:100%;max-width:100%;"></div>
									<p style="text-align: center;">Siempre brindamos los mejores paquetes de viajes a precios increíbles.<br />Llena este formulario y se parte de nuestros agencias asociadas.</p>
									<div id="wpmem_reg">
										<a name="register"></a>
										<form id="frmAgency" method="post" action="{{ url('api/agency') }}" class="form" enctype="multipart/form-data">
											<fieldset>
												<legend>Registro de Nuevo Usuario</legend>
												<label for="user_login" class="text">Escoja un Nombre de Usuario
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="usuario" type="text" class="textbox"   />
													<small class="msgField"></small>
												</div>
												<label for="ruc" class="text">RUC
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="ruc" type="text" class="textbox"   />
													<small class="msgField"></small>
												</div>
												<label for="razonsocial" class="text">Razón Social
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="razon_social" type="text" value="" class="textbox"   />
													<small class="msgField"></small>
												</div>
												<label for="nombre_comercial" class="text">Nombre Comercial
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="nombre_comercial" type="text" value="" class="textbox"   />
													<small class="msgField"></small>
												</div>
												<label for="phone1" class="text">Teléfono
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="telefono" type="text" id="telefono" value="" class="textbox"   />
													<small class="msgField"></small>
												</div>
												<label for="celular" class="text">Celular</label>
												<div class="div_text">
													<input name="celular" type="text" value="" class="textbox" />
												</div>
												<label for="addr1" class="text">Dirección (línea 1)
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="direccion_1" type="text" value="" class="textbox"   />
													<small class="msgField"></small>
												</div>
												<label for="addr2" class="text">Dirección (línea 2)</label>
												<div class="div_text">
													<input name="direccion_2" type="text" value="" class="textbox" />
													<small class="msgField"></small>
												</div>
												<!-- <label for="city" class="text">Ciudad
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="city" type="text" id="city" value="" class="textbox" required  />
												</div> -->
												<label for="distrito" class="text">País
													<span class="req">*</span>
												</label>
												<div class="div_text f-arial">
													<select class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="pais">
														<option value="{{ $country->id }}">{{ $country->nombre }}</option>
													</select>
													<small class="msgField"></small>
												</div>
												<label for="thestate" class="text">Provincia
													<span class="req">*</span>
												</label>
												<div class="div_text f-arial">
													<select id="selectProvincia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="provincia">
														<option value="">Seleccionar</option>
														@foreach($provinces as $province)
															<option value="{{ $province->id }}">{{ $province->nombre }}</option>
														@endforeach
													</select>
													<small class="msgField"></small>
												</div>
												<div class="group-distrito">
													<div class="div-wrapper-field">
														<label for="thestate" class="text">Distrito
															<span class="req">*</span>
														</label>
														<div class="div_text f-arial">
															<select id="selectDistrito" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="distrito">
																<option value="">Seleccionar</option>
															</select>
															<small class="msgField"></small>
														</div>
													</div>
												</div>
												<!-- <label for="first_name" class="text">Nombre
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="first_name" type="text" value="" class="textbox"   />
												</div>
												<label for="last_name" class="text">Apellidos
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="apellidos" type="text" value="" class="textbox"   />
												</div> -->
												<label for="nombre_contacto" class="text">Nombre del contacto responsable
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="nombres" type="text" value="" class="textbox"   />
													<small class="msgField"></small>
												</div>
												<label for="user_email" class="text">Correo
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input name="correo" type="email" value="" class="textbox"   />
													<small class="msgField"></small>
												</div>
												<label for="user_email" class="text">Logo
													<span class="req">*</span>
												</label>
												<div class="div_text">
													<input type="file" name="logo" value="">
													<small class="msgField"></small>
													<!-- <input name="correo" type="email" id="correo" value="" class="textbox" required  /> -->
												</div>
												<!-- <label class="text" for="captcha">Input the code:</label>
												<div class="div_text">
													<input id="captcha_code" name="captcha_code" size="4" type="text" />
													<input id="captcha_prefix" name="captcha_prefix" type="hidden" value="640365384" />
													<img src="{{ url('images/imagen-captcha.png') }}" alt="captcha" width="72" height="30" />
												</div> -->
												<input name="a" type="hidden" value="register" />
												<input name="wpmem_reg_page" type="hidden" value="#" />
												<div class="button_div">
													<input id="btnSubmit" name="submit" type="submit" value="Registrarse" class="buttons" />
												</div>
												<div class="req-text">
													<span class="req">*</span>Campo Obligatorio
												</div>
											</fieldset>
										</form>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-last 1_5"  style='margin-top:0px;margin-bottom:0px;width:16.8%'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->
@stop

@section('script')
<script type='text/javascript' src="{{ url('js/7fe5606794b62066a68cdda469435491.js') }}"></script>
<script type="text/javascript">
jQuery.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
	}
});

jQuery('#selectProvincia').change(function () {
	var value = jQuery(this).val();
	jQuery('.group-distrito').addClass('d-block');

	var form = {
		method: 'GET',
		action: '{{ url("api/district") }}/' + value,
		data: {}
	}

	ajaxLocation(form, 'selectDistrito');
});

jQuery('#frmAgency').submit(function(event) {
	var action = jQuery(this).attr('action');
	var method = jQuery(this).attr('method');
	var data = new FormData(jQuery(this)[0]);
	// Attach file
  data.append('logo', jQuery('input[type=file]')[0].files[0]);

	console.log(jQuery('input[type=file]')[0].files[0]);

	jQuery('#btnSubmit').prop('disabled', true);
	jQuery('#btnSubmit').val('Registrando...');

	jQuery.ajax({
		method: method,
		url: action,
		data: data,
		processData: false,
  	contentType: false,
		success: function (response) {
			console.log(response);
			removeClassError();
			clearForm();

			jQuery('#btnSubmit').prop('disabled', false);
			jQuery('.group-distrito').removeClass('d-block');
			jQuery('#post-421').hide();
			jQuery('.frmSuccess').removeClass('hidden');
		}
	})
	.fail(function(error) {
		var status = error.status;
		var data = error.responseJSON;

		jQuery('#btnSubmit').prop('disabled', false);
		jQuery('#btnSubmit').val('Registrarse');

		if (status === 422) {
			removeClassError();
			jQuery.each(data, function(key, value) {
				var input = jQuery('#frmAgency *[name="' + key + '"]').addClass('has-error');
				var msg;

				if (key === 'pais' || key === 'provincia' || key === 'distrito') {
						var dd = jQuery(input).parent()[0];
						msg = jQuery(dd).parent()[0].children[1];
				} else {
						msg = jQuery(input).parent()[0].children[1];
				}

				jQuery(msg).text(value[0]);
			});
		}
	});

	event.preventDefault();
});

function clearForm() {
	document.getElementById('frmAgency').reset();
}

function removeClassError () {
	jQuery('#frmAgency input, #frmAgency select').removeClass('has-error');
}

function removeOption (select) {
	jQuery('#' + select).empty();
	jQuery('#' + select).append(jQuery('<option>', {
		value: "",
		text : 'Seleccionar'
	}));
}

function ajaxLocation (form, select) {
	removeOption(select);

	jQuery.ajax({
		method: form.method,
		url: form.action,
		data: form.data,
		success: function (response) {
			jQuery.each(response, function (key, item) {
				jQuery('#' + select).append(jQuery('<option>', {
					value: item.id,
					text : item.nombre
				}));
			});
		}
	});
}
</script>
@stop
