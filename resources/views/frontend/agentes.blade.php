@extends('frontend.layout.layout-internal') @section('title', 'Agentes') @section('description', 'Discover') 


@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-1184.css')}}" />
@stop


@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/hm_custom_js-custom.css')}}" />
@stop


@section('custom-script')
<script src="{{ url('js/bloqueo.custom.js')}}"></script>
@stop


@section('style')

@stop

@section('body-style')
page-template page-template-100-width page-template-100-width-php page page-id-738 page-parent top-parent-738 fusion-image-hovers page-agentes fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop

@section('content')

@include('frontend.partials.header')

<div id="sliders-container"></div>
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
	<div class="fusion-page-title-row">
		<div class="fusion-page-title-wrapper">
			<div class="fusion-page-title-secondary">
				<div class="fusion-breadcrumbs">
					<span>
						<a href="{{ route('home') }}">
							<span>Home</span>
						</a>
					</span>
					<span class="fusion-breadcrumb-sep">/</span>
					<span class="breadcrumb-leaf">Agentes</span>
				</div>
			</div>
			<div class="fusion-page-title-captions">
				<h1 class="entry-title">Agentes Asociados</h1>
			</div>
		</div>
	</div>
</div>
<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
	<div class="fusion-row" style="max-width:100%;">
		<div id="content" class="full-width">
			<div id="post-738" class="post-738 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">Agentes</span>
				<span class="vcard rich-snippet-hidden">
					<span class="fn">
						<a href="{{ route('mayorista') }}" title="Entradas de Discover" rel="author">
							Discover Mayorista
						</a>
					</span>
				</span>
				<span class="updated rich-snippet-hidden">2017-06-07T01:49:30+00:00</span>
				<div class="post-content">
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6  fusion-one-sixth fusion-column-first 1_6"  style='margin-top:0px;margin-bottom:0px;width:16.66%;width:calc(16.66% - ( ( 4% ) * 0.1666 ) );margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<p>Filtro de búsqueda &gt;</p>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_5_6  fusion-five-sixth fusion-column-last 5_6"  style='margin-top:0px;margin-bottom:0px;width:83.33%;width:calc(83.33% - ( ( 4% ) * 0.8333 ) );'>
								<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6  fusion-one-sixth fusion-column-first 1_6"  style='margin-top:0px;margin-bottom:20px;width:16.66%;width:calc(16.66% - ( ( 4% ) * 0.1666 ) );margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<!-- Page-list plugin v.5.1 wordpress.org/plugins/page-list/ -->
									<ul class="page-list ">
										<li class="page_item page-item-836 page_item_has_children">
											<a href="http://www.proyectoswebtilia.info/discover/agentes/lima/">Lima</a>
											<ul class='children'>
												<li class="page_item page-item-844">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/lima/ate/">Ate</a>
												</li>
												<li class="page_item page-item-846">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/lima/barranco/">Barranco</a>
												</li>
												<li class="page_item page-item-848">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/lima/brena/">Breña</a>
												</li>
												<li class="page_item page-item-850">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/lima/cercado/">Cercado</a>
												</li>
												<li class="page_item page-item-852">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/lima/comas/">Comas</a>
												</li>
												<li class="page_item page-item-855">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/lima/miraflores/">Miraflores</a>
												</li>
												<li class="page_item page-item-858">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/lima/san-borja/">San Borja</a>
												</li>
											</ul>
										</li>
										<li class="page_item page-item-842 page_item_has_children">
											<a href="http://www.proyectoswebtilia.info/discover/agentes/provincias/">Provincias</a>
											<ul class='children'>
												<li class="page_item page-item-860">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/provincias/arequipa/">Arequipa</a>
												</li>
												<li class="page_item page-item-865">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/provincias/cuzco/">Cuzco</a>
												</li>
												<li class="page_item page-item-869">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/provincias/huancayo/">Huancayo</a>
												</li>
												<li class="page_item page-item-863">
													<a href="http://www.proyectoswebtilia.info/discover/agentes/provincias/trujillo/">Trujillo</a>
												</li>
											</ul>
										</li>
									</ul>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_5_6  fusion-five-sixth fusion-column-last 5_6"  style='margin-top:0px;margin-bottom:20px;width:83.33%;width:calc(83.33% - ( ( 4% ) * 0.8333 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<style type="text/css">.fusion-portfolio-wrapper#fusion-portfolio-1 .fusion-portfolio-content{  text-align: left; }</style>
									<div class="fusion-recent-works fusion-portfolio fusion-portfolio-1 fusion-portfolio-grid-with-text fusion-portfolio-paging-load-more-button fusion-portfolio fusion-portfolio-four fusion-portfolio-unboxed fusion-portfolio-text" data-id="-rw-1" data-columns="four">
										<style type="text/css">.fusion-portfolio-1 .fusion-portfolio-wrapper .fusion-col-spacing{padding:10px;}</style>
										<div class="fusion-portfolio-wrapper" id="fusion-portfolio-1" data-picturesize="fixed" data-pages="2" style="margin:-10px;">
											<article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
												<div class="fusion-portfolio-content-wrapper">
													<span class="vcard rich-snippet-hidden">
														<span class="fn">
															<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
														</span>
													</span>
													<span class="updated rich-snippet-hidden">2017-06-05T00:57:56+00:00</span>
													<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
														<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-10/">
															<img width="240" height="160" src="{{ url('images/240x160.jpg') }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
														</a>
													</div>
													<div class="fusion-portfolio-content">
														<h2 class="entry-title fusion-post-title">
															<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-10/">Agencia 10</a>
														</h2>
														<div class="fusion-post-content">
															<p>
																teléf. 333.4536<br />info@worldtour.com
															</p>
														</div>
													</div>
												</div>
											</article>
											<article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
												<div class="fusion-portfolio-content-wrapper">
													<span class="vcard rich-snippet-hidden">
														<span class="fn">
															<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
														</span>
													</span>
													<span class="updated rich-snippet-hidden">2017-06-05T00:57:42+00:00</span>
													<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
														<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-9/">
															<img width="240" height="160" src="{{ url('images/240x160.jpg') }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
														</a>
													</div>
													<div class="fusion-portfolio-content">
														<h2 class="entry-title fusion-post-title">
															<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-9/">Agencia 9</a>
														</h2>
														<div class="fusion-post-content">
															<p>
																teléf. 333.4536<br />info@worldtour.com
															</p>
														</div>
													</div>
												</div>
											</article>
											<article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
												<div class="fusion-portfolio-content-wrapper">
													<span class="vcard rich-snippet-hidden">
														<span class="fn">
															<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
														</span>
													</span>
													<span class="updated rich-snippet-hidden">2017-06-05T00:57:27+00:00</span>
													<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
														<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-8/">
															<img width="240" height="160" src="{{ url('images/240x160.jpg') }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
														</a>
													</div>
													<div class="fusion-portfolio-content">
														<h2 class="entry-title fusion-post-title">
															<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-8/">Agencia 8</a>
														</h2>
														<div class="fusion-post-content">
															<p>
																teléf. 333.4536<br />info@worldtour.com
															</p>
														</div>
													</div>
												</div>
											</article>
											<article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
												<div class="fusion-portfolio-content-wrapper">
													<span class="vcard rich-snippet-hidden">
														<span class="fn">
															<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
														</span>
													</span>
													<span class="updated rich-snippet-hidden">2017-06-05T00:57:12+00:00</span>
													<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
														<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-7/">
															<img width="240" height="160" src="{{ url('images/240x160.jpg') }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
														</a>
													</div>
													<div class="fusion-portfolio-content">
														<h2 class="entry-title fusion-post-title">
															<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-7/">Agencia 7</a>
														</h2>
														<div class="fusion-post-content">
															<p>
																teléf. 333.4536<br />info@worldtour.com
															</p>
														</div>
													</div>
												</div>
											</article>
											<article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
												<div class="fusion-portfolio-content-wrapper">
													<span class="vcard rich-snippet-hidden">
														<span class="fn">
															<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
														</span>
													</span>
													<span class="updated rich-snippet-hidden">2017-06-05T00:53:08+00:00</span>
													<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
														<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-6/">
															<img width="240" height="160" src="{{ url('images/240x160.jpg') }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
														</a>
													</div>
													<div class="fusion-portfolio-content">
														<h2 class="entry-title fusion-post-title">
															<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-6/">Agencia 6</a>
														</h2>
														<div class="fusion-post-content">
															<p>
																teléf. 333.4536<br />info@worldtour.com
															</p>
														</div>
													</div>
												</div>
											</article>
											<article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
												<div class="fusion-portfolio-content-wrapper">
													<span class="vcard rich-snippet-hidden">
														<span class="fn">
															<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
														</span>
													</span>
													<span class="updated rich-snippet-hidden">2017-06-05T00:45:12+00:00</span>
													<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
														<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-5/">
															<img width="240" height="160" src="{{ url('images/240x160.jpg') }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
														</a>
													</div>
													<div class="fusion-portfolio-content">
														<h2 class="entry-title fusion-post-title">
															<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-5/">Agencia 5</a>
														</h2>
														<div class="fusion-post-content">
															<p>
																teléf. 333.4536<br />info@worldtour.com
															</p>
														</div>
													</div>
												</div>
											</article>
											<article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
												<div class="fusion-portfolio-content-wrapper">
													<span class="vcard rich-snippet-hidden">
														<span class="fn">
															<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
														</span>
													</span>
													<span class="updated rich-snippet-hidden">2017-06-01T19:10:55+00:00</span>
													<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
														<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-4/">
															<img width="240" height="160" src="{{ url('images/240x160.jpg') }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
														</a>
													</div>
													<div class="fusion-portfolio-content">
														<h2 class="entry-title fusion-post-title">
															<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-4/">Agencia 4</a>
														</h2>
														<div class="fusion-post-content">
															<p>
																teléf. 333.4536<br />info@worldtour.com
															</p>
														</div>
													</div>
												</div>
											</article>
											<article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
												<div class="fusion-portfolio-content-wrapper">
													<span class="vcard rich-snippet-hidden">
														<span class="fn">
															<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
														</span>
													</span>
													<span class="updated rich-snippet-hidden">2017-06-01T19:08:45+00:00</span>
													<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
														<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-3/">
															<img width="240" height="160" src="{{ url('images/240x160.jpg') }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
														</a>
													</div>
													<div class="fusion-portfolio-content">
														<h2 class="entry-title fusion-post-title">
															<a href="http://www.proyectoswebtilia.info/discover/portfolio-items/agencia-3/">Agencia 3</a>
														</h2>
														<div class="fusion-post-content">
															<p>
																teléf. 333.4536<br />info@worldtour.com
															</p>
														</div>
													</div>
												</div>
											</article>
										</div>
										<div class="fusion-load-more-button fusion-portfolio-button fusion-clearfix">Leer más</div>
										<div class="fusion-infinite-scroll-trigger"></div>
										<div class='pagination infinite-scroll clearfix' style="display:none;">
											<span class="current">1</span>
											<a href="http://www.proyectoswebtilia.info/discover/agentes/page/2/" class="inactive">2</a>
											<a class="pagination-next" href="http://www.proyectoswebtilia.info/discover/agentes/page/2/">
												<span class="page-text">Next</span>
												<span class="page-next"></span>
											</a>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div id="registrate-calltoaction" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-image: url("{{ url('images/Banner-registo-agentes.png') }}");background-position: left center;background-repeat: no-repeat;padding-top:155px;padding-bottom:10px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-spacing-no 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-size-two font-script title-h2 color-verde" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-left">¡Sé parte de nuestros agentes asociados!</h2>
									</div>
									<p>
										<strong>Llena nuestro formulario y únete a la familia DISCOVER MAYORISTA</strong>
									</p>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6  fusion-one-sixth fusion-spacing-no 1_6"  style='margin-top:0px;margin-bottom:20px;width:16.66%;width:calc(16.66% - ( ( 0 + 0 ) * 0.1666 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-column-content-centered">
										<div class="fusion-column-content">
											<div class="fusion-button-wrapper fusion-aligncenter">
												<style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{background: #01a13d;}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #0260a0;}.fusion-button.button-1{width:auto;}</style>
												<a class="fusion-button button-flat fusion-button-square button-large button-custom button-1" target="_self" title="Registrate" href="{{ route('registro-agentes') }}">
													<span class="fusion-button-text">Registrate</span>
												</a>
											</div>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last fusion-spacing-no 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 0 + 0 ) * 0.3333 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->			
			
@stop

@section('script')
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
@stop
