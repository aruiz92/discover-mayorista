@extends('frontend.layout.layout-internal') @section('title', 'Blog') @section('description', 'Discover') 

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-40.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/blog.custom.css')}}" />
@stop

@section('custom-script')
<script type='text/javascript' src="{{ url('js/contact-script.js') }}"></script>
@stop

@section('style')

@stop

@section('body-style')
page-template page-template-100-width page-template-100-width-php page page-id-330 top-parent-330 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop


@section('content')

	@include('frontend.partials.header')
					
<div id="sliders-container"></div>
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
	<div class="fusion-page-title-row">
		<div class="fusion-page-title-wrapper">
			<div class="fusion-page-title-secondary">
				<div class="fusion-breadcrumbs">
					<span>
						<a href="{{ route('home') }}">
							<span>Home</span>
						</a>
					</span>
					<span class="fusion-breadcrumb-sep">/</span>
					<span>
						<span>Blog</span>
					</span>
					<span class="fusion-breadcrumb-sep">/</span>
				</div>
			</div>
			<div class="fusion-page-title-captions">
				<h1 class="entry-title">Blog</h1>
			</div>
		</div>
	</div>
</div>
<div id="main" role="main" class="clearfix " style="">
	<div class="fusion-row" style="">
		<div id="content" class="full-width" style="width: 100%;">
			<div id="posts-container" class="fusion-blog-archive fusion-blog-layout-grid-wrapper fusion-clearfix">
				<div class="fusion-blog-layout-grid fusion-blog-layout-grid-3 isotope fusion-blog-pagination " data-pages="1">
					<article id="post-199" class="fusion-post-grid  post fusion-clearfix post-199 type-post status-publish format-standard has-post-thumbnail hentry category-blog categoria-id-5">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ route('paquete') }}">
												<img width="600" height="400" src="{{ url('images/600x400-400x267.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ route('paquete') }}">Increíbles lugares</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
											2017-05-19T01:01:13+00:00		
										</span>
										<span>mayo 18, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>Lorem ipsum dolor sit amet, an movet percipit instructior mea. Id nibh omnium intellegam vix.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ route('paquete') }}" class="fusion-read-more">
										Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>

					<article id="post-1" class="fusion-post-grid  post fusion-clearfix post-1 type-post status-publish format-standard has-post-thumbnail hentry category-blog categoria-id-5">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ route('paquete') }}">
												<img width="600" height="400" src="{{ url('images/600x400-400x267.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ route('paquete') }}">Lugares Increibles</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
											2017-05-19T01:01:21+00:00	
										</span>
										<span>abril 22, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>
											Lorem ipsum dolor sit amet, an movet percipit instructior mea. Id nibh omnium intellegam vix.
										</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ route('paquete') }}" class="fusion-read-more">
											Read More
										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>

					<article id="post-1" class="fusion-post-grid  post fusion-clearfix post-1 type-post status-publish format-standard has-post-thumbnail hentry category-blog categoria-id-5">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ route('paquete') }}">
												<img width="600" height="400" src="{{ url('images/600x400-400x267.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ route('paquete') }}">Lugares Increibles</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
											2017-05-19T01:01:21+00:00	
										</span>
										<span>abril 22, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>
											Lorem ipsum dolor sit amet, an movet percipit instructior mea. Id nibh omnium intellegam vix.
										</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ route('paquete') }}" class="fusion-read-more">
											Read More
										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
<!-- fusion-row -->
</div>
<!-- #main -->	

@stop

@section('script')
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
@stop
