@extends('frontend.layout.layout-mundo-vacaciones') @section('title', 'Mundo - Vacaciones') @section('description', 'Discover') 


@section('content')

@include('frontend.partials.vacaciones-header')
@include('frontend.partials.vacaciones-slider')


<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
    <div class="fusion-row" style="max-width:100%;">
        <div id="content" class="full-width">
            <div id="post-563" class="post-563 page type-page status-publish hentry">
                <span class="entry-title rich-snippet-hidden">Familiar</span>
                <span class="vcard rich-snippet-hidden">
                    <span class="fn">
                        <a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>          
                    </span>
                </span>
                <span class="updated rich-snippet-hidden">2017-05-28T02:04:42+00:00 </span>

                <div class="post-content">
                    <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first fusion-spacing-no 1_3" style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 0 + 0 ) * 0.3333 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-spacing-no icon-sobre-slider 1_3" style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 0 + 0 ) * 0.3333 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="background-image: url('{{ url('images/fondo-titulo.png') }}');background-position:center top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                    data-bg-url="{{ url('images/fondo-titulo.png') }}">
                                    <div class="imageframe-align-center">
                                        <span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none">
                                            <img src="{{ url('images/icon-title-vacaciones.png') }}" width="68" height="56" alt="Paquetes Vacacionales" title="Paquetes Vacacionales" class="img-responsive wp-image-589"/>
                                        </span>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last fusion-spacing-no 1_3" style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 0 + 0 ) * 0.3333 ) );'>
                                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>

                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-spacing-no titulo-estilo-2 font-script 1_2" style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-two line-height-normal" style="margin-top:15px;margin-bottom:31px;">
                                        <h2 class="title-heading-center">Paquetes<br /> Vacacionales </h2>
                                    </div>

                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );'>
                                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:30px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:0px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-spacing-no 1_2" style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">

                                    <!-- Page-list plugin v.5.1 wordpress.org/plugins/page-list/ -->
                                    <ul class="page-list menu-estilo-1">
                                        <li class="page_item page-item-563 current_page_item"><a href="#">Familiar</a></li>
                                        <li class="page_item page-item-615"><a href="#">Parejas</a></li>
                                        <li class="page_item page-item-617"><a href="#">Solteros</a></li>
                                    </ul>
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>

                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:0px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );'>
                                <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="Vacaciones_Div" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:0px;'>
                                <div class="fusion-column-wrapper" style="background-color:#ffffff;padding: 50px 0px 10px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-grid-wrapper fusion-blog-infinite paquetes-carrusel sin-carrusel">
                                        <style type="text/css">
                                            .fusion-blog-shortcode-1 .fusion-blog-layout-grid .fusion-post-grid{padding:7.5px;}.fusion-blog-shortcode-1 .fusion-posts-container{margin-left: -7.5px !important; margin-right:-7.5px !important;}
                                        </style>
                                        <div class="fusion-posts-container fusion-posts-container-infinite fusion-posts-container-load-more fusion-blog-layout-grid fusion-blog-layout-grid-4 isotope" data-pages="3" data-grid-col-space="15" style="margin: -7.5px -7.5px 0;height:500px;">
                                            <article id="post-1186" class="fusion-post-grid post-1186 post type-post status-publish format-standard has-post-thumbnail hentry category-familiar-vacaciones-mundo category-vacaciones-mundo categoria-id-20 categoria-id-13">
                                                <div class="fusion-post-wrapper">
                                                    <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                                                        <ul class="slides">
                                                            <li>
                                                                <div class="fusion-image-wrapper" aria-haspopup="true">
                                                                    <a href="{{ route('paquete') }}">
                                                                        <img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="fusion-post-content-wrapper">
                                                        <div class="fusion-post-content post-content">
                                                            <h2 class="blog-shortcode-post-title entry-title"><a href="{{ route('paquete') }}">Euroferta I</a></h2>
                                                            <div class="extracto-carousel"></div>
                                                            <div class="dias-noches-carrusel">18D 17N</div>
                                                            <div class="precio-usd">$3499</div>
                                                            <div class="div-leermas-2">
                                                                <a href="{{ route('paquete') }}" class="link-leermas">Leer más</a>
                                                            </div>
                                                            <a class="fusion-modal-text-link boton-3" data-toggle="modal"
                                                                data-target=".fusion-modal.Form_Cotizar_1186" href="#">Cotizar</a>
                                                            <div class="fusion-modal modal fade modal-1 Form_Cotizar_1186 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1"
                                                                aria-hidden="true">
                                                                <style type="text/css">
                                                                    .modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}
                                                                </style>
                                                                <div class="modal-dialog modal-lg">
                <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                    <div class="modal-header">
                        <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Cotizar</h3>
                    </div>
                    <div class="modal-body">
                        <h3>Euroferta I</h3>
                        <h4 class="sub_titulo_post_form"></h4>
                        <div role="form" class="wpcf7" id="wpcf7-f162-p1186-o1" lang="es-ES" dir="ltr">
                            <div class="screen-reader-response"></div>
                            <form action="/discover/peru/vacaciones/familiar/#wpcf7-f162-p1186-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="162" />
                                    <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                    <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p1186-o1" />
                                    <input type="hidden" name="_wpcf7_container_post" value="1186" />
                                </div>
                                <p>
                                    <label>
                                        Nombre (requerido)<br />
                                        <span class="wpcf7-form-control-wrap Nombre">
                                            <input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                                        </span>
                                    </label>
                                </p>
                                <p>
                                    <label>
                                        Tu correo electrónico (requerido)<br />
                                        <span class="wpcf7-form-control-wrap Email">
                                            <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                                        </span>
                                    </label>
                                </p>
                                <p>
                                    <label>
                                        Asunto<br />
                                        <span class="wpcf7-form-control-wrap Asunto">
                                            <input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
                                        </span>
                                    </label>
                                </p>
                                <p>
                                    <label>
                                        Mensaje<br />
                                        <span class="wpcf7-form-control-wrap Mensaje">
                                            <textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                                        </span> 
                                    </label>
                                </p>
                                <p>
                                    <span class="wpcf7-form-control-wrap Producto">
                                        <input type="hidden" name="Producto" value="Euroferta I" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                    </span>
                                    <span class="wpcf7-form-control-wrap URL">
                                        <input type="hidden" name="URL" value="#" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                    </span>
                                </p>
                                <p style="text-align:right">
                                    <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                                </p>
                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                            </form>
                        </div>
                    </div>
                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fusion-clearfix"></div>
                                                </div>
                                            </article>

                                            <article id="post-1184" class="fusion-post-grid post-1184 post type-post status-publish format-standard has-post-thumbnail hentry category-bloqueos categoria-id-7">
                                                <div class="fusion-post-wrapper">
                                                    <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                                                        <ul class="slides">
                                                            <li>
                                                                <div class="fusion-image-wrapper" aria-haspopup="true">
                                                                    <a href="{{ route('paquete') }}">
                                                                        <img width="797" height="531" src="{{ url('images/cancun-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="fusion-post-content-wrapper">
                                                        <div class="fusion-post-content post-content">
                                                            <h2 class="blog-shortcode-post-title entry-title"><a href="{{ route('paquete') }}">Cancun</a></h2>
                                                            <div class="extracto-carousel"></div>
                                                            <div class="dias-noches-carrusel"></div>
                                                            <div class="precio-usd">$</div>
                                                            <div class="div-leermas-2">
                                                                <a href="{{ route('paquete') }}" class="link-leermas">Leer más</a>
                                                            </div>
                                                            <a class="fusion-modal-text-link boton-3" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar_1184" href="#">Cotizar</a>

                                                            <div class="fusion-modal modal fade modal-2 Form_Cotizar_1184 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-2" aria-hidden="true">
                                                                <style type="text/css">
                                                                    .modal-2 .modal-header, .modal-2 .modal-footer{border-color:#ebebeb;}
                                                                </style>
                <div class="modal-dialog modal-lg">
                    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="modal-heading-2" data-dismiss="modal" aria-hidden="true">Cotizar</h3>
                        </div>
                        <div class="modal-body">
                            <h3>Cancun</h3>
                            <h4 class="sub_titulo_post_form"></h4>
                            <div role="form" class="wpcf7" id="wpcf7-f162-p1184-o2" lang="es-ES" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="/discover/peru/vacaciones/familiar/#wpcf7-f162-p1184-o2" method="post" class="wpcf7-form" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="162" />
                                        <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                        <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p1184-o2" />
                                        <input type="hidden" name="_wpcf7_container_post" value="1184" />
                                    </div>

                                    <p>
                                        <label> 
                                            Nombre (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Nombre">
                                                <input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                                            </span> 
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Tu correo electrónico (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Email">
                                                <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Asunto<br />
                                            <span class="wpcf7-form-control-wrap Asunto"><input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" /></span> 
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Mensaje<br />
                                            <span class="wpcf7-form-control-wrap Mensaje">
                                                <textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <span class="wpcf7-form-control-wrap Producto">
                                            <input type="hidden" name="Producto" value="Cancun" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                        </span>
                                        <span class="wpcf7-form-control-wrap URL"><input type="hidden" name="URL" value="http://www.proyectoswebtilia.info/discover/peru/vacaciones/familiar/" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" /></span>
                                    </p>

                                    <p style="text-align:right">
                                        <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                                    </p>

                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fusion-clearfix"></div>
                                                </div>
                                            </article>

                                            <article id="post-1095" class="fusion-post-grid post-1095 post type-post status-publish format-standard has-post-thumbnail hentry category-tematica-mundo category-tematica-1-tematica-mundo categoria-id-12 categoria-id-48">
                                                <div class="fusion-post-wrapper">
                                                    <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                                                        <ul class="slides">
                                                            <li>
                                                                <div class="fusion-image-wrapper" aria-haspopup="true">
                                                                    <a href="{{ route('paquete') }}">
                                                                        <img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="fusion-post-content-wrapper">
                                                        <div class="fusion-post-content post-content">
                                                            <h2 class="blog-shortcode-post-title entry-title">
                                                                <a href="{{ route('paquete') }}">Súper Clásico FC Barcelona vs Real Madrid Fiestas Patrias</a>
                                                            </h2>
                                                            <div class="extracto-carousel"> 
                                                                Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te.
                                                            </div>
                                                            <div class="dias-noches-carrusel">5D 4N</div>
                                                            <div class="precio-usd">$5844</div>
                                                            <div class="div-leermas-2">
                                                                <a href="{{ route('paquete') }}" class="link-leermas">Leer más</a>
                                                            </div>
                                                            <a class="fusion-modal-text-link boton-3" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar_1095" href="#">Cotizar</a>

                                                            <div class="fusion-modal modal fade modal-3 Form_Cotizar_1095 max-width-500" tabindex="-1" role="dialog"
                                                            aria-labelledby="modal-heading-3" aria-hidden="true">
                <style type="text/css">
                    .modal-3 .modal-header, .modal-3 .modal-footer{border-color:#ebebeb;}
                </style>
                <div class="modal-dialog modal-lg">
                    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="modal-heading-3" data-dismiss="modal" aria-hidden="true">Cotizar</h3>
                        </div>
                        <div class="modal-body">
                            <h3>Súper Clásico FC Barcelona vs Real Madrid Fiestas Patrias</h3>
                            <h4 class="sub_titulo_post_form"></h4>
                            <div role="form" class="wpcf7" id="wpcf7-f162-p1095-o3" lang="es-ES" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="/discover/peru/vacaciones/familiar/#wpcf7-f162-p1095-o3" method="post" class="wpcf7-form" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="162" />
                                        <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                        <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p1095-o3" />
                                        <input type="hidden" name="_wpcf7_container_post" value="1095" />
                                    </div>

                                    <p>
                                        <label>
                                            Nombre (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Nombre">
                                                <input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                                            </span> 
                                        </label>
                                    </p>

                                    <p>
                                        <label> 
                                            Tu correo electrónico (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Email">
                                                <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Asunto<br />
                                            <span class="wpcf7-form-control-wrap Asunto">
                                                <input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Mensaje<br />
                                            <span class="wpcf7-form-control-wrap Mensaje">
                                                <textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <span class="wpcf7-form-control-wrap Producto">
                                            <input type="hidden" name="Producto" value="Súper Clásico FC Barcelona vs Real Madrid Fiestas Patrias" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                        </span>

                                        <span class="wpcf7-form-control-wrap URL">
                                            <input type="hidden" name="URL" value="#" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                        </span>
                                    </p>

                                    <p style="text-align:right">
                                        <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                                    </p>

                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fusion-clearfix"></div>
                                                </div>
                                            </article>

                                            <article id="post-556" class="fusion-post-grid post-556 post type-post status-publish format-standard has-post-thumbnail hentry category-tematicas-peru categoria-id-9">
                                                <div class="fusion-post-wrapper">
                                                    <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                                                        <ul class="slides">
                                                            <li>
                                                                <div class="fusion-image-wrapper" aria-haspopup="true">
                                                                    <a href="{{ route('paquete') }}">
                                                                        <img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="fusion-post-content-wrapper">
                                                        <div class="fusion-post-content post-content">
                                                            <h2 class="blog-shortcode-post-title entry-title">
                                                                <a href="{{ route('paquete') }}">Eliminatorias Mundial Rusia 2018 Ecuador vs Perú Partido</a>
                                                            </h2>
                                                            <div class="extracto-carousel">
                                                                Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te.
                                                            </div>
                                                            <div class="dias-noches-carrusel">4D 3N</div>
                                                            <div class="precio-usd">$300</div>
                                                            <div class="div-leermas-2">
                                                                <a href="{{ route('paquete') }}" class="link-leermas">Leer más</a>
                                                            </div>

                                                            <a class="fusion-modal-text-link boton-3"
                                                            data-toggle="modal" data-target=".fusion-modal.Form_Cotizar_556" href="#">Cotizar</a>

                                                            <div class="fusion-modal modal fade modal-4 Form_Cotizar_556 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-4"
                                                            aria-hidden="true">
                                                                <style type="text/css">
                                                                    .modal-4 .modal-header, .modal-4 .modal-footer{border-color:#ebebeb;}
                                                                </style>

                                                                <div class="modal-dialog modal-lg">
                <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                    <div class="modal-header"><button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="modal-heading-4" data-dismiss="modal" aria-hidden="true">Cotizar</h3>
                    </div>
                    <div class="modal-body">
                        <h3>Eliminatorias Mundial Rusia 2018 Ecuador vs Perú Partido</h3>
                        <h4 class="sub_titulo_post_form"></h4>
                        <div role="form" class="wpcf7" id="wpcf7-f162-p556-o4" lang="es-ES" dir="ltr">
                            <div class="screen-reader-response"></div>
                            <form action="/discover/peru/vacaciones/familiar/#wpcf7-f162-p556-o4" method="post" class="wpcf7-form" novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="162" />
                                    <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                    <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p556-o4" />
                                    <input type="hidden" name="_wpcf7_container_post" value="556" />
                                </div>

                                <p>
                                    <label>
                                        Nombre (requerido)<br />
                                        <span class="wpcf7-form-control-wrap Nombre">
                                            <input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                                        </span>
                                    </label>
                                </p>

                                <p>
                                    <label>
                                        Tu correo electrónico (requerido)<br />
                                        <span class="wpcf7-form-control-wrap Email">
                                            <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                                        </span>
                                    </label>
                                </p>

                                <p>
                                    <label>
                                        Asunto<br />
                                        <span class="wpcf7-form-control-wrap Asunto">
                                            <input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
                                        </span> 
                                    </label>
                                </p>

                                <p>
                                    <label>
                                        Mensaje<br />
                                        <span class="wpcf7-form-control-wrap Mensaje">
                                            <textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                                        </span>
                                    </label>
                                </p>

                                <p>
                                    <span class="wpcf7-form-control-wrap Producto">
                                        <input type="hidden" name="Producto" value="Eliminatorias Mundial Rusia 2018 Ecuador vs Perú Partido" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                    </span>

                                    <span class="wpcf7-form-control-wrap URL"><input type="hidden" name="URL" value="#" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden"
                                    aria-invalid="false" /></span>
                                </p>

                                <p style="text-align:right">
                                    <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                                </p>

                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                            </form>
                        </div>
                    </div>
                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="fusion-clearfix"></div>
                                                </div>
                                            </article>

                                            <article id="post-513" class="fusion-post-grid post-513 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
                                                <div class="fusion-post-wrapper">
                                                    <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                                                        <ul class="slides">
                                                            <li>
                                                                <div class="fusion-image-wrapper" aria-haspopup="true">
                                                                    <a href="#">
                                                                        <img width="275" height="420" src="{{ url('images/cancun-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="fusion-post-content-wrapper">
                                                        <div class="fusion-post-content post-content">
                                                            <h2 class="blog-shortcode-post-title entry-title">
                                                                <a href="{{ route('paquete') }}">CANCÚN / RIVIERA MAYA</a>
                                                            </h2>
                                                            <div class="extracto-carousel"></div>
                                                            <div class="dias-noches-carrusel">6D 5N</div>
                                                            <div class="precio-usd">$999</div>
                                                            <div class="div-leermas-2">
                                                                <a href="{{ route('paquete') }}" class="link-leermas">Leer más</a>
                                                            </div>
                                                            <a class="fusion-modal-text-link boton-3" data-toggle="modal"
                                                                data-target=".fusion-modal.Form_Cotizar_513" href="#">Cotizar</a>
                                                            <div class="fusion-modal modal fade modal-5 Form_Cotizar_513 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-5" aria-hidden="true">
                    <style type="text/css">
                        .modal-5 .modal-header, .modal-5 .modal-footer{border-color:#ebebeb;}
                    </style>
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="modal-heading-5" data-dismiss="modal" aria-hidden="true">Cotizar</h3>
                            </div>
                            <div class="modal-body">
                                <h3>CANCÚN / RIVIERA MAYA</h3>
                                <h4 class="sub_titulo_post_form"></h4>
                                <div role="form" class="wpcf7" id="wpcf7-f162-p513-o5" lang="es-ES" dir="ltr">
                                    <div class="screen-reader-response"></div>
                                    <form action="/discover/peru/vacaciones/familiar/#wpcf7-f162-p513-o5" method="post" class="wpcf7-form" novalidate="novalidate">
                                        <div style="display: none;">
                                            <input type="hidden" name="_wpcf7" value="162" />
                                            <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                            <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p513-o5" />
                                            <input type="hidden" name="_wpcf7_container_post" value="513" />
                                        </div>
                                        <p>
                                            <label>
                                                Nombre (requerido)<br />
                                                <span class="wpcf7-form-control-wrap Nombre">
                                                    <input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                                                </span>
                                            </label>
                                        </p>

                                        <p>
                                            <label>
                                                Tu correo electrónico (requerido)<br />
                                                <span class="wpcf7-form-control-wrap Email">
                                                    <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                                                </span> 
                                            </label>
                                        </p>

                                        <p>
                                            <label>
                                                Asunto<br />
                                                <span class="wpcf7-form-control-wrap Asunto">
                                                    <input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
                                                </span>
                                            </label>
                                        </p>

                                        <p>
                                            <label> 
                                                Mensaje<br />
                                                <span class="wpcf7-form-control-wrap Mensaje">
                                                    <textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                                                </span>
                                            </label>
                                        </p>

                                        <p>
                                            <span class="wpcf7-form-control-wrap Producto">
                                                <input type="hidden" name="Producto" value="CANCÚN / RIVIERA MAYA" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                            </span>
                                            <span class="wpcf7-form-control-wrap URL">
                                                <input type="hidden" name="URL" value="" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                            </span>
                                        </p>

                                        <p style="text-align:right">
                                            <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                                        </p>

                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="fusion-clearfix"></div>
                                                </div>
                                            </article>

                                            <article id="post-511" class="fusion-post-grid post-511 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
                                                <div class="fusion-post-wrapper">
                                                    <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                                                        <ul class="slides">
                                                            <li>
                                                                <div class="fusion-image-wrapper" aria-haspopup="true">
                                                                    <a href="">
                                                                        <img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="fusion-post-content-wrapper">
                                                        <div class="fusion-post-content post-content">
                                                            <h2 class="blog-shortcode-post-title entry-title">
                                                                <a href="{{ route('paquete') }}">¡Últimos Espacios!!! Iguazú  Fiestas Patrias</a>
                                                            </h2>
                                                            <div class="extracto-carousel"></div>
                                                            <div class="dias-noches-carrusel">6D 5N</div>
                                                            <div class="precio-usd">$835</div>
                                                            <div class="div-leermas-2">
                                                                <a href="{{ route('paquete') }}" class="link-leermas">Leer más</a>
                                                            </div>
                                                            <a class="fusion-modal-text-link boton-3" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar_511" href="#">Cotizar</a>

                                                            <div class="fusion-modal modal fade modal-6 Form_Cotizar_511 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-6" aria-hidden="true">
                <style type="text/css">
                    .modal-6 .modal-header, .modal-6 .modal-footer{border-color:#ebebeb;}
                </style>
                <div class="modal-dialog modal-lg">
                    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="modal-heading-6" data-dismiss="modal" aria-hidden="true">Cotizar</h3>
                        </div>

                        <div class="modal-body">
                            <h3>¡Últimos Espacios!!! Iguazú Fiestas Patrias</h3>
                            <h4 class="sub_titulo_post_form"></h4>
                            <div role="form" class="wpcf7" id="wpcf7-f162-p511-o6" lang="es-ES" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="/discover/peru/vacaciones/familiar/#wpcf7-f162-p511-o6" method="post" class="wpcf7-form" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="162" />
                                        <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                        <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p511-o6" />
                                        <input type="hidden" name="_wpcf7_container_post" value="511" />
                                    </div>

                                    <p>
                                        <label> 
                                            Nombre (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Nombre">
                                                <input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Tu correo electrónico (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Email">
                                                <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Asunto<br />
                                            <span class="wpcf7-form-control-wrap Asunto">
                                                <input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Mensaje<br />
                                            <span class="wpcf7-form-control-wrap Mensaje">
                                                <textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                                            </span> 
                                        </label>
                                    </p>

                                    <p>
                                        <span class="wpcf7-form-control-wrap Producto">
                                            <input type="hidden" name="Producto" value="¡Últimos Espacios!!! Iguazú  Fiestas Patrias" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                        </span>
                                        <span class="wpcf7-form-control-wrap URL">
                                            <input type="hidden" name="URL" value="h#" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                        </span>
                                    </p>

                                    <p style="text-align:right">
                                        <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                                    </p>

                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fusion-clearfix"></div>
                                                </div>
                                            </article>

                                            <article id="post-509" class="fusion-post-grid post-509 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
                                                <div class="fusion-post-wrapper">
                                                    <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                                                        <ul class="slides">
                                                            <li>
                                                                <div class="fusion-image-wrapper" aria-haspopup="true">
                                                                    <a href="{{ route('paquete') }}">
                                                                        <img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="fusion-post-content-wrapper">
                                                        <div class="fusion-post-content post-content">
                                                            <h2 class="blog-shortcode-post-title entry-title">
                                                                <a href="{{ route('paquete') }}">Los Mejores Conciertos en Las Vegas</a>
                                                            </h2>
                                                            <div class="extracto-carousel"></div>
                                                            <div class="dias-noches-carrusel">4D 3N</div>
                                                            <div class="precio-usd">$459</div>

                                                            <div class="div-leermas-2">
                                                                <a href="{{ route('paquete') }}" class="link-leermas">Leer más</a>
                                                            </div>
                                                            <a class="fusion-modal-text-link boton-3" data-toggle="modal"
                                                                data-target=".fusion-modal.Form_Cotizar_509" href="#">Cotizar</a>

                                                            <div class="fusion-modal modal fade modal-7 Form_Cotizar_509 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-7" aria-hidden="true">
                <style type="text/css">
                    .modal-7 .modal-header, .modal-7 .modal-footer{border-color:#ebebeb;}
                </style>
                <div class="modal-dialog modal-lg">
                    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                        <div class="modal-header"><button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="modal-heading-7" data-dismiss="modal" aria-hidden="true">Cotizar</h3>
                        </div>
                        <div class="modal-body">
                            <h3>Los Mejores Conciertos en Las Vegas</h3>
                            <h4 class="sub_titulo_post_form"></h4>
                            <div role="form" class="wpcf7" id="wpcf7-f162-p509-o7" lang="es-ES" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form action="/discover/peru/vacaciones/familiar/#wpcf7-f162-p509-o7" method="post" class="wpcf7-form" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="162" />
                                        <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                        <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p509-o7" />
                                        <input type="hidden" name="_wpcf7_container_post" value="509" />
                                    </div>
                                    
                                    <p>
                                        <label>
                                            Nombre (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Nombre">
                                                <input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Tu correo electrónico (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Email">
                                                <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                                            </span> 
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Asunto<br />
                                            <span class="wpcf7-form-control-wrap Asunto">
                                                <input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Mensaje<br />
                                            <span class="wpcf7-form-control-wrap Mensaje">
                                                <textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                                            </span>
                                        </label>
                                    </p>

                                    <p>
                                        <span class="wpcf7-form-control-wrap Producto">
                                            <input type="hidden" name="Producto" value="Los Mejores Conciertos en Las Vegas" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                        </span>

                                        <span class="wpcf7-form-control-wrap URL">
                                            <input type="hidden" name="URL" value="" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                        </span>
                                    </p>

                                    <p style="text-align:right">
                                        <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                                    </p>

                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fusion-clearfix"></div>
                                                </div>
                                            </article>

                                            <article id="post-508" class="fusion-post-grid post-508 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
                                                <div class="fusion-post-wrapper">
                                                    <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                                                        <ul class="slides">
                                                            <li>
                                                                <div class="fusion-image-wrapper" aria-haspopup="true">
                                                                    <a href="#">
                                                                        <img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    
                                                    <div class="fusion-post-content-wrapper">

                                                        <div class="fusion-post-content post-content">
                                                            <h2 class="blog-shortcode-post-title entry-title">
                                                                <a href="{{ route('paquete') }}">Colombia es Realismo Mágico</a>
                                                            </h2>

                                                            <div class="extracto-carousel"></div>
                                                            <div class="dias-noches-carrusel">4D 3N</div>
                                                            <div class="precio-usd">$445</div>

                                                            <div class="div-leermas-2">
                                                                <a href="{{ route('paquete') }}" class="link-leermas">Leer más</a>
                                                            </div>

                                                            <a class="fusion-modal-text-link boton-3" data-toggle="modal"
                                                                data-target=".fusion-modal.Form_Cotizar_508" href="#">Cotizar</a>
                                                            
                                                            <div class="fusion-modal modal fade modal-8 Form_Cotizar_508 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-8" aria-hidden="true">
                <style type="text/css">
                    .modal-8 .modal-header, .modal-8 .modal-footer{border-color:#ebebeb;}
                </style>
                <div class="modal-dialog modal-lg">
                    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="modal-heading-8" data-dismiss="modal" aria-hidden="true">Cotizar</h3>
                        </div>
                        <div class="modal-body">
                            <h3>Colombia es Realismo Mágico</h3>
                            <h4 class="sub_titulo_post_form"></h4>
                            <div role="form" class="wpcf7" id="wpcf7-f162-p508-o8" lang="es-ES" dir="ltr">
                                <div class="screen-reader-response"></div>
                                
                                <form action="/discover/peru/vacaciones/familiar/#wpcf7-f162-p508-o8" method="post" class="wpcf7-form" novalidate="novalidate">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="162" />
                                        <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                        <input type="hidden" name="_wpcf7_locale" value="es_ES" />
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p508-o8" />
                                        <input type="hidden" name="_wpcf7_container_post" value="508" />
                                    </div>

                                    <p>
                                        <label>
                                            Nombre (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Nombre">
                                                <input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                                            </span> 
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Tu correo electrónico (requerido)<br />
                                            <span class="wpcf7-form-control-wrap Email">
                                                <input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                                            </span> 
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Asunto<br />
                                            <span class="wpcf7-form-control-wrap Asunto">
                                                <input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
                                            </span> 
                                        </label>
                                    </p>

                                    <p>
                                        <label>
                                            Mensaje<br />
                                            <span class="wpcf7-form-control-wrap Mensaje">
                                                <textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                                            </span> 
                                        </label>
                                    </p>

                                    <p>
                                        <span class="wpcf7-form-control-wrap Producto">
                                            <input type="hidden" name="Producto" value="Colombia es Realismo Mágico" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                        </span>

                                        <span class="wpcf7-form-control-wrap URL">
                                            <input type="hidden" name="URL" value="" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
                                        </span>
                                    </p>

                                    <p style="text-align:right">
                                        <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                                    </p>

                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="fusion-clearfix"></div>
                                                </div>
                                            </article>
                                            <div class="fusion-clearfix"></div>
                                        </div>
                                    
                                        <div class='pagination infinite-scroll clearfix'>
                                            <span class="current">1</span>
                                            <a href="#" class="inactive">2</a>
                                            <a href="#" class="inactive">3</a>

                                            <a class="pagination-next" href="#">
                                                <span class="page-text">Next</span>
                                                <span class="page-next"></span>
                                            </a>

                                        </div>
                                        <div class="fusion-infinite-scroll-trigger"></div>
                                        <div class="fusion-load-more-button fusion-blog-button fusion-clearfix">Leer más</div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- fusion-row -->
</div>
<!-- #main -->



@stop 

@section('style')
<style type="text/css">
    .recentcomments a {
        display: inline !important;
        padding: 0 !important;
        margin: 0 !important;
    }
</style>
@stop

@section('script')
    <script type="text/javascript">
        function revslider_showDoubleJqueryError(sliderID) {
            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
                jQuery(sliderID).show().html(errorMessage);
        }
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpcf7 = {"apiSettings":{"root":"http:\/\/www.proyectoswebtilia.info\/discover\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}}};
        /* ]]> */
    </script>
    

    <script type='text/javascript'>
        /* <![CDATA[ */
        var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"admin-ajax.php","loadingTrans":"Cargando...","is_rtl":""};
        /* ]]> */
    </script>
@stop
