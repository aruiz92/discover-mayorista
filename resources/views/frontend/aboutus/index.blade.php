@extends('frontend.layout.layout-internal')

@section('title', 'Nosotros')
@section('description', 'Una ruta. Varios destinos. Disfruta de tus vacaciones, en los mejores lugares.')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-1184.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/hm_custom_js-custom.css')}}" />
<link rel="stylesheet" href="{{ asset('v2/css/app.css')}}" />
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
@stop

@section('custom-script')
<script src="{{ url('js/nosotros.custom.js')}}"></script>
@stop

@section('style')

@stop

@section('body-style')
ds-nosotros page-template page-template-100-width page-template-100-width-php page page-id-294 top-parent-294 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop


@section('content')

@include('frontend.partials.header')

<div id="sliders-container"></div>
<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
	<div class="fusion-row" style="max-width:100%;">
		<div id="content" class="full-width">
			<div id="post-294" class="post-294 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">Nosotros</span>
				<span class="vcard rich-snippet-hidden">
					<span class="fn">
						<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
					</span>
				</span>
				<span class="updated rich-snippet-hidden">2017-06-01T00:10:09+00:00</span>

				<div class="post-content">
					<div id="Div-Quienes-Somos" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-image: url("{{ url('images/Banner-Nosotros-1.jpg') }}");background-position: left center;background-repeat: no-repeat;padding-right:0px;padding-left:0px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="nosotros-text fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="padding: 100px 0px 100px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-size-two title-h2 color-celeste" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-left">Quiénes Somos</h2>
									</div>
                  {!! $aboutus->quienes_somos !!}
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-widget-area fusion-widget-area-1 fusion-content-widget-area">
										<style type="text/css" scoped="scoped">
											.fusion-widget-area-1 .widget h4 {color:#333333;}.fusion-widget-area-1 .widget .heading h4 {color:#333333;}
										</style>
										<div id="bcn_widget-2" class="widget widget_breadcrumb_navxt">
											<div class="breadcrumbs" vocab="https://schema.org/" typeof="BreadcrumbList">
												<!-- Breadcrumb NavXT 6.0.4 -->
												<span property="itemListElement" typeof="ListItem">
													<a property="item" typeof="WebPage" title="Ir a Discover." href="{{ route('home') }}" class="home">
														<span property="name">Inicio</span>
													</a>
													<meta property="position" content="1">
												</span>

												<span property="itemListElement" typeof="ListItem">
													<span property="name">Nosotros</span>
													<meta property="position" content="2">
												</span>
											</div>
										</div>
										<div class="fusion-additional-widget-content"></div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<div id="Div-Mision-Vision" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-tabs fusion-tabs-1 clean tabs-nosotros horizontal-tabs">
										<style type="text/css">
											#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#ffffff;border-top-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;text-align: center;}
										</style>
										<div class="nav">
											<ul class="nav-tabs nav-justified">
												<li class="active">
													<a class="tab-link" data-toggle="tab" id="fusion-tab-misión" href="#tab-736c9d49ae2e4af2df4">
														<h4 class="fusion-tab-heading">Misión</h4>
													</a>
												</li>
												<li>
													<a class="tab-link" data-toggle="tab" id="fusion-tab-visión" href="#tab-4ef6e70c8b9347d9158">
														<h4 class="fusion-tab-heading">Visión</h4>
													</a>
												</li>
											</ul>
										</div>
										<div class="tab-content">
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li class="active">
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-misión" href="#tab-736c9d49ae2e4af2df4">
															<h4 class="fusion-tab-heading">Misión</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade in active" id="tab-736c9d49ae2e4af2df4">
												{!! $aboutus->mision !!}
											</div>
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li>
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-visión" href="#tab-4ef6e70c8b9347d9158">
															<h4 class="fusion-tab-heading">Visión</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade" id="tab-4ef6e70c8b9347d9158">
												{!! $aboutus->vision !!}
											</div>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>


					<div id="Div-Ventajas" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-image: url("{{ url('images/Banner-Nosotros-2.jpg') }}");background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="padding: 70px 70px 50px 70px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-two title-h2 color-celeste" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-center">VALORES CORPORATIVOS</h2>
									</div>
									{!! $aboutus->ventajas !!}
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<div id="Div-Staff" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="padding: 70px 0px 30px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-two title-h2 color-celeste" style="margin-top:0px;margin-bottom:0px;">
										<h2 class="title-heading-center">STAFF DISCOVER</h2>
									</div>
									<p style="text-align: center;">Somos un grupo de profesionales del turismo con destacadas labores en el rubro de viajes.</p>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
              <style>
								.employees-content {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-orient: horizontal;
                  -webkit-box-direction: normal;
                      -ms-flex-direction: row;
                          flex-direction: row;
                  -ms-flex-wrap: nowrap;
                      flex-wrap: nowrap;
                  -webkit-box-pack: center;
                      -ms-flex-pack: center;
                          justify-content: center;

                  margin-bottom: 7em;
                }

                .employees {
                  width: 100%;
                  color: #ffffff;
                  font-size: 18px;

									display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-orient: horizontal;
                  -webkit-box-direction: normal;
                      -ms-flex-direction: row;
                          flex-direction: row;
                  -ms-flex-wrap: wrap;
                      flex-wrap: wrap;
                }

                .employee {
                  cursor: pointer;
                  min-height: 106px;

									width: 20%;
                }

                .employee-header-contenedor:hover .employee-hover {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
									background-color: rgba(0, 0, 0, 0.6);
									opacity: 1;
                }

                .employee-header-contenedor:hover .employee-info {
                  display: none;
                }

                .employee-header {
                  padding: 20px;
                }

								.employee-header-contenedor {
									position: relative;
								}

								.employee-header-contenedor .employee-info {
									position: absolute;
									width: 100%;
									height: 100%;
									z-index: 1;

									background-color: rgba(0, 0, 0, 0.4);

									display: -webkit-box;

									display: -ms-flexbox;

									display: flex;
									-webkit-box-orient: vertical;
									-webkit-box-direction: normal;
									    -ms-flex-direction: column;
									        flex-direction: column;
									-webkit-box-pack: center;
									    -ms-flex-pack: center;
									        justify-content: center;
									-webkit-box-align: center;
									    -ms-flex-align: center;
									        align-items: center;

									-webkit-transition: .5s ease;

									-o-transition: .5s ease;

									transition: .5s ease;
								}

								.employee-header-contenedor .employee-hover {
									position: absolute;
									width: 100%;
									height: 100%;
									z-index: 1;

									background-color: #000000;
									opacity: 0;

									display: none;
									-webkit-box-orient: vertical;
									-webkit-box-direction: normal;
									    -ms-flex-direction: column;
									        flex-direction: column;
									-webkit-box-pack: center;
									    -ms-flex-pack: center;
									        justify-content: center;
									-webkit-box-align: center;
									    -ms-flex-align: center;
									        align-items: center;

									-webkit-transition: .5s ease;

									-o-transition: .5s ease;

									transition: .5s ease;
                }

                .employee-header-contenedor .employee-hover p {
                  margin-bottom: 0;
									font-size: 12px !important;
                }

                .employee-header-contenedor .employee-info h2 {
                  font-family: 'Roboto', sans-serif !important;
                  font-size: .8em !important;
                  font-weight: bold !important;
                  color: #ffffff;
                  margin-bottom: 0;
                }

                .employee-header-contenedor .employee-info small {
                  font-size: .8em;
                  font-family: 'Roboto', sans-serif !important;
									text-align: center;
                }
              </style>
              <div class="employees-content">
                <div class="employees">
                  @foreach($employees as $employee)
	                  <div class="employee">
	                    <div class="employee-header">
												<div class="employee-header-contenedor">
													<div class="employee-info">
														<h2>{{ $employee->nombres.' '.$employee->apellidos }}</h2>
														@if($employee->cargo !== NULL)
															<small>{{ $employee->cargo }}</small>
														@endif
													</div>
													<div class="employee-hover">
														@if($employee->correo !== NULL)
															<p>{{ $employee->correo }}</p>
														@endif

														@if($employee->telefono !== NULL)
															<p>Teléfono: {{ $employee->telefono }}</p>
														@endif

														@if($employee->skype !== NULL)
															<p>{{ $employee->skype }}</p>
														@endif
													</div>
													<img src="{{ asset('files/employees/'.$employee->foto) }}" alt="{{ $employee->nombres.' '.$employee->apellidos }}">
												</div>
	                    </div>
	                  </div>
                  @endforeach
                </div>
              </div>
						</div>
					</div>

					<div id="Div-Hoteles-Proveedores" class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth"  style='background-color: #01bfcb;background-position: center center;background-repeat: no-repeat;padding-top:70px;padding-right:30px;padding-bottom:50px;padding-left:30px;margin-bottom: 5px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-tabs fusion-tabs-2 clean tabs-nosotros color-blanco horizontal-tabs">
										<style type="text/css">
											#wrapper .fusion-tabs.fusion-tabs-2.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-2 .nav-tabs li a{background-color:#ebeaea;}.fusion-tabs.fusion-tabs-2 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-2 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-2 .nav-tabs li.active a:focus{background-color:#ffffff;}.fusion-tabs.fusion-tabs-2 .nav-tabs li a:hover{background-color:#ffffff;border-top-color:#ffffff;}.fusion-tabs.fusion-tabs-2 .tab-pane{background-color:#ffffff;}.fusion-tabs.fusion-tabs-2 .nav,.fusion-tabs.fusion-tabs-2 .nav-tabs,.fusion-tabs.fusion-tabs-2 .tab-content .tab-pane{border-color:#ebeaea;}
										</style>

										<div class="nav">
											<ul class="nav-tabs nav-justified">
												<li class="active">
													<a class="tab-link" data-toggle="tab" id="fusion-tab-hoteles" href="#tab-4cec0c117c6c6dbbb54">
														<h4 class="fusion-tab-heading">SOCIOS COMERCIALES</h4>
													</a>
												</li>
												<!-- <li>
													<a class="tab-link" data-toggle="tab" id="fusion-tab-proveedores" href="#tab-2e9274aaf1f934bdbdd">
														<h4 class="fusion-tab-heading">Proveedores</h4>
													</a>
												</li> -->
											</ul>
										</div>
										<div class="tab-content">
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li class="active">
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-hoteles" href="#tab-4cec0c117c6c6dbbb54">
															<h4 class="fusion-tab-heading">SOCIOS COMERCIALES</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade in active" id="tab-4cec0c117c6c6dbbb54">
												<div class="socios">
													@foreach($partners as $partner)
														<div class="socios-item">
															<img src="{{ url('files/partners/'.$partner->imagen) }}" alt="{{ $partner->nombre }}">
														</div>
													@endforeach
												</div>
											</div>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<div id="Rev-Regulaciones" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-image: url("{{ url('images/Banner-Nosotros-3.jpg') }}");background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="padding: 30px 0px 20px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-two title-h2 color-blanco" style="margin-top:0px;margin-bottom:15px;">
										<h2 class="title-heading-center">Regulaciones</h2>
									</div>
									<p style="text-align: center;">Parte de nuestra política de transparencia, ponemos a disposición nuestros términos y condiciones.</p>
									<div class="fusion-button-wrapper fusion-aligncenter">
										<style type="text/css" scoped="scoped">
											.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{width:auto;}
										</style>
										<a class="fusion-button button-flat fusion-button-round button-large button-default button-1 boton-4" target="_self" title="Leer documento" href="{{ url('files/condiciones-generales.docx') }}">
											<span class="fusion-button-text">Leer documento</span>
										</a>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

@stop

@section('script')
<!-- <script src="{{ url('js/anijs-min.js') }}" charset="utf-8"></script> -->
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script type="text/javascript">
jQuery('.socios').slick({
	infinite: true,
	speed: 300,
	slidesToShow: 5,
	slidesToScroll: 1,
	responsive: [
	  {
	    breakpoint: 1200,
	    settings: {
	      slidesToShow: 4
	    }
	  },
	  {
	    breakpoint: 992,
	    settings: {
	      slidesToShow: 3
	    }
	  },
	  {
	    breakpoint: 768,
	    settings: {
	      slidesToShow: 2
	    }
	  },
		{
	    breakpoint: 576,
	    settings: {
	      slidesToShow: 1
	    }
	  }
	]
});
</script>
@stop
