<header class="fusion-header-wrapper">
  <div class="fusion-header-v2 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo-1 fusion-mobile-logo-1 fusion-mobile-menu-design-modern ">
    <div class="fusion-secondary-header">
      <div class="fusion-row">
        <div class="fusion-alignright">
          <nav class="fusion-secondary-menu" role="navigation" aria-label="Secondary Menu">
            <ul role="menubar" id="menu-menu-top" class="menu">
              @foreach($destinations as $key => $destination)
              <?php
                $arrDestination = array(
                  array('id' => '136'),
                  array('id' => '135')
                );
              ?>
              <li role="menuitem" id="menu-item-{{ $arrDestination[$key]['id'] }}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{{ $arrDestination[$key]['id'] }}">
                <a href="{{ route('destination', ['id' => $destination->id, 'name' => str_slug($destination->nombre)]) }}" class="fusion-flex-link">
                  <span class="fusion-megamenu-icon">
                    <i class="fa glyphicon fa-map-marker"></i>
                  </span>
                  <span class="menu-text">{{ $destination->nombre }}</span>
                </a>
              </li>
              @endforeach

              <li role="menuitem" id="menu-item-369" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-369">
                <a href="{{ url('registro-suscriptores') }}">
                  <span class="menu-text">Suscríbete</span>
                </a>
              </li>
              <li role="menuitem" id="menu-item-717" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-717">
                @if(!Auth::check())
                  <a href="{{ url('ingreso-agencias') }}">
                    <span class="menu-text">Ingreso Agencias</span>
                  </a>
                @else
                  <a href="{{ url('agency/'.Auth::user()->agency->usuario) }}">
                    <span class="menu-text">{{ Auth::user()->agency->nombre_comercial }}</span>
                  </a>
                @endif
              </li>
              @if(Auth::check())
              <li role="menuitem" id="menu-item-717" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-717">
                  <a href="{{ url('logout') }}">
                    <span class="menu-text">Salir</span>
                  </a>
              </li>
              @endif
              <li role="menuitem" id="menu-item-28" class="link-red-social menu-item menu-item-type-custom menu-item-object-custom menu-item-28"
              data-classes="link-red-social">
                <a title="Facebook" href="{{ $company->facebook }}" class="fusion-icon-only-link fusion-flex-link">
                  <span class="fusion-megamenu-icon">
                    <i class="fa glyphicon fa-facebook"></i>
                  </span>
                  <span class="menu-text">Facebook</span>
                </a>
              </li>
              <li role="menuitem" id="menu-item-29" class="link-red-social menu-item menu-item-type-custom menu-item-object-custom menu-item-29" data-classes="link-red-social">
                <a title="Youtube" href="{{ $company->youtube }}" class="fusion-icon-only-link fusion-flex-link">
                  <span class="fusion-megamenu-icon">
                    <i class="fa glyphicon fa-youtube"></i>
                  </span>
                  <span class="menu-text">Youtube</span>
                </a>
              </li>
              <li role="menuitem" id="menu-item-30" class="link-red-social menu-item menu-item-type-custom menu-item-object-custom menu-item-30" data-classes="link-red-social">
                <a title="Instagram" href="{{ $company->instagram }}" class="fusion-icon-only-link fusion-flex-link">
                  <span class="fusion-megamenu-icon">
                    <i class="fa glyphicon fa-instagram"></i>
                  </span>
                  <span class="menu-text">Instagram</span>
                </a>
              </li>
            </ul>
          </nav>
          <div class="fusion-mobile-nav-holder"></div>
        </div>
      </div>
    </div>

    <div class="fusion-header-sticky-height"></div>
    <style>

    </style>
    <div class="fusion-header">
      <div class="fusion-row">
        <div class="fusion-logo" data-margin-top="0px" data-margin-bottom="10px" data-margin-left="0px" data-margin-right="0px">
          <a class="fusion-logo-link" href="{{ url('/')}}" style="float: left;">
            <img class='logo-custom-ja' src='{{ asset('images/Logo-DM.png')}}' />
            <img src="{{ asset('images/Logo-Interno.png')}}" width="273" height="73" alt="Discover Logo" class="fusion-logo-1x fusion-standard-logo" />
            <img src="{{ asset('images/Logo-Interno.png')}}" width="273" height="73" alt="Discover Retina Logo" class="fusion-standard-logo fusion-logo-2x" />
            <!-- mobile logo -->
            <img src="{{ asset('images/Logo-Interno.png')}}" width="273" height="73" alt="Discover Mobile Logo" class="fusion-logo-1x fusion-mobile-logo-1x" />
            <img src="{{ asset('images/Logo-Interno.png')}}" width="273" height="73" alt="Discover Mobile Retina Logo" class="fusion-logo-2x fusion-mobile-logo-2x" />
            <!-- sticky header logo -->
            <img src="{{ asset('images/Logo-DM.png')}}" width="257" height="71" alt="Discover Sticky Logo" class="fusion-logo-1x fusion-sticky-logo-1x" />
          </a>
          @if(Auth::check())
              <img src="{{ asset('files/logos/'.Auth::user()->agency->logo)}}" style="height: 71px;margin-left: 20px;"/>
          @endif
        </div>
        <nav class="fusion-main-menu" aria-label="Main Menu">
          <ul role="menubar" id="menu-menu-principal-home" class="fusion-menu">
            <li role="menuitem" id="menu-item-297" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-297">
              <a href="{{ url('nosotros') }}">
                <span class="menu-text">Nosotros</span>
              </a>
            </li>
            <li role="menuitem" id="menu-item-239" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-239">
              <a href="{{ url('bloqueos') }}">
                <span class="menu-text">Bloqueos</span>
              </a>
            </li>
            <li role="menuitem" id="menu-item-300" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-300">
              <a href="{{ url('promociones') }}">
                <span class="menu-text">Promociones</span>
              </a>
            </li>
            <li role="menuitem" id="menu-item-329" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-329">
              <a href="{{ url('blog') }}">
                <span class="menu-text">Blog</span>
              </a>
            </li>
            <li role="menuitem" id="menu-item-332" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-332">
              <a href="{{ url('contacto') }}">
                <span class="menu-text">Contacto</span>
              </a>
            </li>
          </ul>
        </nav>

        <div class="fusion-mobile-menu-icons">
          <a href="#" class="fusion-icon fusion-icon-bars" aria-label="Toggle mobile menu"></a>
        </div>

        <nav class="fusion-mobile-nav-holder"></nav>
      </div>
    </div>
  </div>
  <div class="fusion-clearfix"></div>
</header>
