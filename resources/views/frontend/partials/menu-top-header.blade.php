<nav class="fusion-secondary-menu" role="navigation" aria-label="Secondary Menu">
  <ul role="menubar" id="menu-menu-top" class="menu">
    @foreach($destinations as $rowDestination)
    <?php
      $classMenuArray = array(
          'activo' => array(
              'id' => 'menu-item-136',
              'class' => 'menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-36 current_page_item menu-item-136'
          ),
          'inactivo' => array(
              'id' => 'menu-item-135',
              'class' => 'menu-item menu-item-type-post_type menu-item-object-page menu-item-135'
          )
      );

      $classMenu = $destination->id === $rowDestination->id ? $classMenuArray['activo']:$classMenuArray['inactivo'];
    ?>
    <li role="menuitem"  id="{{ $classMenu['id'] }}"  class="{{ $classMenu['class'] }}"  >
      <a href="{{ route('destination', ['id' => $rowDestination->id, 'name' => str_slug($rowDestination->nombre)]) }}" class="fusion-flex-link">
        <span class="fusion-megamenu-icon">
          <i class="fa glyphicon fa-map-marker"></i>
        </span>
        <span class="menu-text">{{ $rowDestination->nombre }}</span>
      </a>
    </li>
    @endforeach

    <li role="menuitem"  id="menu-item-369"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-369"  >
      <a  href="{{ url('registro-suscriptores') }}">
        <span class="menu-text">Suscribe</span>
      </a>
    </li>
    <li role="menuitem"  id="menu-item-717"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-717"  >
      @if(!Auth::check())
        <a href="{{ url('ingreso-agencias') }}">
          <span class="menu-text">Ingreso Agencias</span>
        </a>
      @else
        <a href="{{ url('agency') }}">
          <span class="menu-text">{{ Auth::user()->agency->nombre_comercial }}</span>
        </a>
      @endif
    </li>
    @if(Auth::check())
    <li role="menuitem" id="menu-item-717" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-717">
        <a href="{{ url('logout') }}">
          <span class="menu-text">Salir</span>
        </a>
    </li>
    @endif
    <li role="menuitem"  id="menu-item-28"  class="link-red-social menu-item menu-item-type-custom menu-item-object-custom menu-item-28"  data-classes="link-red-social" >
      <a  title="Facebook" href="#" class="fusion-icon-only-link fusion-flex-link">
        <span class="fusion-megamenu-icon">
          <i class="fa glyphicon fa-facebook"></i>
        </span>
        <span class="menu-text">Facebook</span>
      </a>
    </li>
    <li role="menuitem"  id="menu-item-29"  class="link-red-social menu-item menu-item-type-custom menu-item-object-custom menu-item-29"  data-classes="link-red-social" >
      <a  title="Youtube" href="#" class="fusion-icon-only-link fusion-flex-link">
        <span class="fusion-megamenu-icon">
          <i class="fa glyphicon fa-youtube"></i>
        </span>
        <span class="menu-text">Youtube</span>
      </a>
    </li>
    <li role="menuitem"  id="menu-item-30"  class="link-red-social menu-item menu-item-type-custom menu-item-object-custom menu-item-30"  data-classes="link-red-social" >
      <a  title="Instagram" href="#" class="fusion-icon-only-link fusion-flex-link">
        <span class="fusion-megamenu-icon">
          <i class="fa glyphicon fa-instagram"></i>
        </span>
        <span class="menu-text">Instagram</span>
      </a>
    </li>
  </ul>
</nav>
