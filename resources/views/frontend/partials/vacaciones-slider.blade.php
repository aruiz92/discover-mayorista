
<div class="ds-sliders">
	@foreach($sliders as $slider)
		<div class="ds-slider-item subcategorias" style="background-image: url({{ url('files/package_type_banners/'.$slider->imagen) }});">
			<div class="ds-slider-item--detalle">
				<div class="ds-slider-item--titulo">{{ $slider->titulo }}</div>
				<div class="ds-slider-item--parrafo">{{ $slider->subtitulo }}</div>
			</div>
		</div>
	@endforeach
</div>

<script>
jQuery(document).ready(function () {
	jQuery('.ds-sliders').slick({
		infinite: true,
		dots: false,
		arrows: true,
		autoplay: true,
		fade: true,
		autoplaySpeed: 9000,
		slidesToShow: 1,
		slidesToScroll: 1
	});
});
</script>