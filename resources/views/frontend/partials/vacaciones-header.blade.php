<header class="fusion-header-wrapper">
    <div class="fusion-header-v2 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo-1 fusion-mobile-logo-1 fusion-mobile-menu-design-modern ">
        <div class="fusion-secondary-header">
            <div class="fusion-row">
                <div class="fusion-alignright">
                    @include('frontend.partials.menu-top-header', ['destination' => $destination, 'destinations' => $destinations])
                    <div class="fusion-mobile-nav-holder"></div>
                </div>
            </div>
        </div>
        <div class="fusion-header-sticky-height"></div>
        <div class="fusion-header">
            <div class="fusion-row">
                <div class="fusion-logo" data-margin-top="0px" data-margin-bottom="10px" data-margin-left="0px" data-margin-right="0px">
                    <a class="fusion-logo-link" href="{{ route('home') }}">
                        <img class='logo-custom-ja' src='' />
                        @if(Auth::check())
                            <img class='logo-custom-ja' src="{{ asset('files/logos/'.Auth::user()->agency->logo)}}" style="height: 71px;margin-left: 20px;"/>
                        @endif
                        <img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Logo" class="fusion-logo-1x fusion-standard-logo" />
                        <img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Retina Logo" class="fusion-standard-logo fusion-logo-2x" />
                        <!-- mobile logo -->
                        <img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Mobile Logo" class="fusion-logo-1x fusion-mobile-logo-1x" />
                        <img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Mobile Retina Logo" class="fusion-logo-2x fusion-mobile-logo-2x" />
                        <!-- sticky header logo -->
                        <img src="{{ url('images/Logo-DM.png') }}" width="257" height="71" alt="Discover Sticky Logo" class="fusion-logo-1x fusion-sticky-logo-1x" />
                        <img src="{{ url('images/Logo-DM.png') }}" width="257" height="71" alt="Discover Sticky Logo Retina" class="fusion-logo-2x fusion-sticky-logo-2x" />
                    </a>
                </div>
                <nav class="fusion-main-menu" aria-label="Main Menu">
                    <ul role="menubar" id="menu-menu-peru" class="fusion-menu">
                        <li role="menuitem" id="menu-item-91" class="menu-item menu-item-type-post_type menu-item-object-page current-page-ancestor menu-item-91">
                          <a href="{{ route('destination', ['id' => $destination->id, 'name' => str_slug($destination->nombre)]) }}">
                            <span class="menu-text">{{ $destination->nombre }}</span>
                          </a>
                        </li>
                        @foreach($package_types as $package_type)
                        <li role="menuitem" id="menu-item-604" class="menu-item menu-item-type-post_type menu-item-object-page current-page-ancestor menu-item-604">
                          <a href="{{ route('packageType', ['idD' => $destination->id, 'nameD' => str_slug($destination->nombre), 'idT' => $package_type->id, 'nameT' => str_slug($package_type->nombre)]) }}">
                            <span class="menu-text">{{ $package_type->nombre }}</span>
                          </a>
                        </li>
                        @endforeach
                    </ul>
                </nav>
                <div class="fusion-mobile-menu-icons">
                    <a href="#" class="fusion-icon fusion-icon-bars" aria-label="Toggle mobile menu"></a>
                </div>
                <nav class="fusion-mobile-nav-holder"></nav>
            </div>
        </div>
    </div>
    <div class="fusion-clearfix"></div>
</header>
