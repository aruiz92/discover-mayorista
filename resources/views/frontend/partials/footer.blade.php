<div class="fusion-footer">

  <footer role="contentinfo" class="fusion-footer-widget-area fusion-widget-area">
    <div class="fusion-row">
      <div class="fusion-columns fusion-columns-3 fusion-widget-area">

        <div class="fusion-column col-lg-4 col-md-4 col-sm-4">
          <div id="text-2" class="fusion-footer-widget-column widget widget_text">
            <div class="textwidget">
              <p><img src="{{ url('images/logo-footer.png') }}" /></p>
              <!-- <p>{{ $company->direccion_1 }}<br />{{ $company->direccion_2 }}</p> -->
              <p><a href="tel:{{ $company->telefono_fijo }}" class="discover-telefono"><i class="fa fa-phone" aria-hidden="true"></i> {{ $company->telefono_fijo }}</a></p>
              <p><a href="mailto:{{ $company->correo_informacion }}"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{ $company->correo_informacion }}</a></p>
              <div class="redes-footer">
                <ul>
                  <li><a href="{{ $company->facebook }}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="{{ $company->youtube }}"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                  <li><a href="{{ $company->instagram }}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              </div>
            </div>
            <div style="clear:both;"></div>
          </div>
        </div>
        <div class="fusion-column col-lg-4 col-md-4 col-sm-4">
          <div id="nav_menu-2" class="fusion-footer-widget-column widget widget_nav_menu">
            <h4 class="widget-title">Descubre</h4>
            <div class="menu-descubre-footer-container">
              <ul id="menu-descubre-footer" class="menu">
                @foreach($destinations as $destination)
                <li id="menu-item-70" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-70">
                  <a href="{{ route('destination', ['id' => $destination->id, 'name' => str_slug($destination->nombre)]) }}">{{ $destination->nombre }}</a>
                </li>
                @endforeach
                <li id="menu-item-565" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-565"><a href="{{ url('promociones') }}">Promociones</a></li>
                <li id="menu-item-566" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-566"><a href="{{ url('bloqueos') }}">Bloqueos</a></li>
                <li id="menu-item-567" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-567"><a href="{{ url('nosotros') }}">Nosotros</a></li>
                <li id="menu-item-818" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-818"><a href="{{ url('agencias') }}">Agencias</a></li>
              </ul>
            </div>
            <div style="clear:both;"></div>
          </div>
        </div>
        <div class="fusion-column fusion-column-last col-lg-4 col-md-4 col-sm-4">
          <div id="sticky-posts-2" class="fusion-footer-widget-column widget widget_ultimate_posts">
            <h4 class="widget-title">Blog</h4>

            <div class="upw-posts hfeed">




              <article class="post-199 post type-post status-publish format-standard has-post-thumbnail hentry category-blog categoria-id-5">

                <header>

                  <div class="entry-image">
                    <a href="{{ url('blog/?p='.$articulo->ID) }}" rel="bookmark">
                      <img width="150" height="150" src="{{ $articulo->imagen }}" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" srcset=""
                      sizes="(max-width: 150px) 100vw, 150px" /> </a>
                  </div>

                  <h4 class="entry-title">
                <a href="{{ url('blog/?p='.$articulo->ID) }}" rel="bookmark">{{ $articulo->post_title }}</a>
              </h4>


                  <div class="entry-meta">
                    <?php setlocale(LC_ALL, 'es_ES.UTF-8'); ?>
                    <?php setlocale(LC_ALL, 'spanish'); ?>
                    <time class="published" datetime="2017-05-18T19:04:09+00:00">{{ strftime('%d %B, %Y', strtotime($articulo->post_date)) }}</time>





                  </div>


                </header>

                <div class="entry-summary">
                  <p>{{ $articulo->post_excerpt }}</p>
                </div>

                <footer>

                </footer>

              </article>

            </div>

            <div style="clear:both;"></div>
          </div>
          <div id="text-3" class="fusion-footer-widget-column widget widget_text">
            <div class="textwidget"><span style="display: block;margin-bottom: 5px;float: none;">
              <b>Suscribete</b> a nuestras promociones y noticias.</span>
              <div class="widget_wysija_cont shortcode_wysija">
                <div id="msg-form-wysija-shortcode5a89886375735-1" class="wysija-msg ajax"></div>
                <form id="form-wysija-shortcode5a89886375735-1" method="post" action="{{ url('api/newsletters') }}" class="widget_wysija shortcode_wysija frm-newletter">
                  <p class="wysija-paragraph">


                    <input type="email" name="correo" class="wysija-input validate[required,custom[email]]" title="Ingresa tu e-mail" placeholder="Ingresa tu e-mail" required/>



                    <!-- <span class="abs-req">
        <input type="email" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" required/>
    </span> -->

                  </p>

                  <input id="btnNewletter" class="wysija-submit wysija-submit-field" type="submit" value="Enviar" />

                  <!-- <input type="hidden" name="form_id" value="1" />
                  <input type="hidden" name="action" value="save" />
                  <input type="hidden" name="controller" value="subscribers" />
                  <input type="hidden" value="1" name="wysija-page" />


                  <input type="hidden" name="wysija[user_list][list_ids]" value="1" /> -->

                </form>
              </div>
            </div>
            <div style="clear:both;"></div>
          </div>
        </div>

        <div class="fusion-clearfix"></div>
      </div>
      <!-- fusion-columns -->
    </div>
    <!-- fusion-row -->
  </footer>
  <!-- fusion-footer-widget-area -->

</div>
<!-- fusion-footer -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
jQuery.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
  }
});

jQuery('.frm-newletter').submit(function( event ) {
  var action = jQuery(this).attr('action');
  var method = jQuery(this).attr('method');
  var data = jQuery(this).serialize();

  jQuery('#btnNewletter').prop('disabled', true);
  jQuery('#btnNewletter').val('Enviando...');

  jQuery.ajax({
    method: method,
    url: action,
    data: data,
    success: function (response) {
      removeClassError();
      clearForm();
      jQuery('#btnNewletter').prop('disabled', false);
      jQuery('#btnNewletter').val('Enviar');
      swal('Excelente!', 'Pronto recibirás noticias nuestras.', 'success');
    }
  })
  .fail(function(error) {
    var status = error.status;
    var data = error.responseJSON;

    jQuery('#btnNewletter').prop('disabled', false);
    jQuery('#btnNewletter').val('Enviar');

    if (status === 422) {
      removeClassError();
      jQuery.each(data, function(key, value) {
        var input = jQuery('.frm-newletter *[name="' + key + '"]').addClass('has-error');
      });
    }
  });

  event.preventDefault();
});

function clearForm() {
  document.getElementsByClassName('frm-newletter')[0].reset();
}

function removeClassError () {
  jQuery('.frm-newletter input, .frm-newletter select').removeClass('has-error');
}
</script>
