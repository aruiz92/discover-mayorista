@if ($paginator->lastPage() > 1)
<div class='pagination clearfix'>
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
      @if($paginator->currentPage() == $i)
        <span class="current">{{ $i }}</span>
      @else
        <a href="{{ $paginator->url($i) }}" class="inactive">{{ $i }}</a>
      @endif
    @endfor

    @if($paginator->currentPage() == $paginator->lastPage())
      <div class="pagination-next">
        <span class="page-text">Next</span>
        <span class="page-next"></span>
      </div>
    @else
      <a class="pagination-next" href="{{ $paginator->url($paginator->currentPage()+1) }}">
        <span class="page-text">Next</span>
        <span class="page-next"></span>
      </a>
    @endif
<div>
@endif
