<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ url('js/vendor/jquery.min.js')}}"></script>
<script src="{{ url('js/jquery-migrate.min.js')}}"></script>
<script src="{{ url('js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ url('js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{ url('js/custom-mundo.js')}}"></script>
<script src="{{ url('js/enscroll-0.6.2.min.js')}}"></script>
<script src="{{ url('js/vendor/jquery.bxslider.min.js')}}"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!--[if IE 9]>
<script type='text/javascript' src='{{ url('fusion-ie9.js') }}'></script>
<![endif]-->

<script src="{{ url('js/comment-reply.min.js')}}"></script>
<script src="{{ url('js/fusion-scripts.js')}}"></script>
<script src="{{ url('js/wp-embed.min.js')}}"></script>
<script src="{{ url('js/jquery.language.validationEngine-es.js')}}"></script>
@if(false)<script src="{{ url('js/front-subscribers.js')}}"></script>@endif
<script src="{{ url('js/jquery.validationEngine.js')}}"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-591cb76c2043d7da"></script>
<script src="{{ url('js/contact-script.js')}}"></script>
<script src="{{ url('js/slick.min.js') }}" charset="utf-8"></script>
