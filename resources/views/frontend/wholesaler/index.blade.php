@extends('frontend.layout.layout-internal') @section('title', 'Mayorista') @section('description', 'Discover')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-global.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/mayorista.custom.css')}}" />
@stop

@section('custom-script')
<script type='text/javascript' src="{{ url('js/contact-script.js') }}"></script>
@stop

@section('style')

@stop

@section('body-style')
archive author author-admin-discover author-1 categoria-id-20 categoria-id-13 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text
@stop


@section('content')

	@include('frontend.partials.header')

<div id="sliders-container"></div>
<div id="main" role="main" class="clearfix " style="">
	<div class="fusion-row" style="">
		<div id="content" class="full-width" style="width: 100%;">
			<div class="fusion-author">
				<div class="fusion-author-avatar">
					<img alt='' src='http://2.gravatar.com/avatar/8269ebafcf69718b1b3d480804d9f78e?s=82&#038;d=mm&#038;r=g' class='avatar avatar-82 photo' height='82' width='82' />
				</div>
				<div class="fusion-author-info">
					<h3 class="fusion-author-title vcard">
			About

						<span class="fn">Discover Mayorista</span>
					</h3>
		This author has not yet filled in any details.

					<br />So far Discover Mayorista has created 19 blog entries.

				</div>
				<div style="clear:both;"></div>
				<div class="fusion-author-social clearfix">
					<div class="fusion-author-tagline"></div>
				</div>
			</div>
			<div id="posts-container" class="fusion-blog-archive fusion-blog-layout-grid-wrapper fusion-clearfix">
				<div class="fusion-blog-layout-grid fusion-blog-layout-grid-3 isotope fusion-blog-pagination " data-pages="2">
					<article id="post-1186" class="fusion-post-grid  post fusion-clearfix post-1186 type-post status-publish format-standard has-post-thumbnail hentry category-familiar-vacaciones-mundo category-vacaciones-mundo categoria-id-20 categoria-id-13">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="797" height="531" src="{{ url('images/Lima-Tradicional-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">Euroferta I</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2018-01-08T12:45:17+00:00		</span>
										<span>enero 8, 2018</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>    días   noches  Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
					<article id="post-1184" class="fusion-post-grid  post fusion-clearfix post-1184 type-post status-publish format-standard has-post-thumbnail hentry category-bloqueos categoria-id-7">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="797" height="531" src="{{ url('images/cancun-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">Cancun</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2018-01-08T12:34:12+00:00		</span>
										<span>enero 8, 2018</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>       Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.  Solicitar</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
					<article id="post-1095" class="fusion-post-grid  post fusion-clearfix post-1095 type-post status-publish format-standard has-post-thumbnail hentry category-tematica-mundo category-tematica-1-tematica-mundo categoria-id-12 categoria-id-48">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="797" height="531" src="{{ url('images/Lima-Tradicional-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">Súper Clásico FC Barcelona vs Real Madrid Fiestas Patrias</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2017-12-28T18:58:13+00:00		</span>
										<span>julio 19, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
					<article id="post-556" class="fusion-post-grid  post fusion-clearfix post-556 type-post status-publish format-standard has-post-thumbnail hentry category-tematicas-peru categoria-id-9">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="797" height="531" src="{{ url('images/Lima-Tradicional-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">Eliminatorias Mundial Rusia 2018 Ecuador vs Perú Partido</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2017-12-29T08:31:38+00:00		</span>
										<span>mayo 26, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
					<article id="post-513" class="fusion-post-grid  post fusion-clearfix post-513 type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="275" height="420" src="{{ url('images/paquetes/cancun-collage-2.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">CANCÚN / RIVIERA MAYA</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2018-01-28T21:49:07+00:00		</span>
										<span>mayo 25, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>    días   noches  Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
					<article id="post-511" class="fusion-post-grid  post fusion-clearfix post-511 type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="797" height="531" src="{{ url('images/Lima-Tradicional-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">¡Últimos Espacios!!! Iguazú  Fiestas Patrias</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2017-12-29T14:19:49+00:00		</span>
										<span>mayo 25, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>    días   noches  Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
					<article id="post-509" class="fusion-post-grid  post fusion-clearfix post-509 type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="797" height="531" src="{{ url('images/Lima-Tradicional-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">Los Mejores Conciertos en Las Vegas</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2017-12-29T15:24:39+00:00		</span>
										<span>mayo 25, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>    días   noches  Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
					<article id="post-508" class="fusion-post-grid  post fusion-clearfix post-508 type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="797" height="531" src="{{ url('images/Lima-Tradicional-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">Colombia es Realismo Mágico</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2017-12-29T15:50:33+00:00		</span>
										<span>mayo 25, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>    días   noches  Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
					<article id="post-506" class="fusion-post-grid  post fusion-clearfix post-506 type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="797" height="531" src="{{ url('images/Lima-Tradicional-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">Caribbean Pride Chic Punta Cana</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2018-01-08T11:34:18+00:00		</span>
										<span>mayo 25, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>    días   noches  Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
					<article id="post-490" class="fusion-post-grid  post fusion-clearfix post-490 type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
						<div class="fusion-post-wrapper">
							<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
								<ul class="slides">
									<li>
										<div class="fusion-image-wrapper" aria-haspopup="true">
											<a href="{{ url('paquete') }}">
												<img width="797" height="531" src="{{ url('images/Lima-Tradicional-400x266.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(min-width: 784px) 407px, (min-width: 712px) 610px, (min-width: 640px) 712px, 100vw" />
											</a>
										</div>
									</li>
								</ul>
							</div>
							<div class="fusion-post-content-wrapper">
								<div class="fusion-post-content post-content">
									<h2 class="entry-title fusion-post-title">
										<a href="{{ url('paquete') }}">Vacaciones de Lujo por Fiestas Patrias  The Grand At Moon Palace</a>
									</h2>
									<p class="fusion-single-line-meta">
										<span class="vcard rich-snippet-hidden">
											<span class="fn">
												<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
											</span>
										</span>
										<span class="updated rich-snippet-hidden">
			2018-01-08T11:49:53+00:00		</span>
										<span>mayo 25, 2017</span>
										<span class="fusion-inline-sep">|</span>
									</p>
									<div class="fusion-content-sep"></div>
									<div class="fusion-post-content-container">
										<p>    días   noches  Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>
									</div>
								</div>
								<div class="fusion-meta-info">
									<div class="fusion-alignleft">
										<a href="{{ url('paquete') }}" class="fusion-read-more">
											Read More										</a>
									</div>
									<div class="fusion-alignright"></div>
								</div>
							</div>
						</div>
					</article>
				</div>
				<div class='pagination clearfix'>
					<span class="current">1</span>
					<a href="{{ url('mayorista') }}page/2/" class="inactive">2</a>
					<a class="pagination-next" href="{{ url('mayorista') }}page/2/">
						<span class="page-text">Next</span>
						<span class="page-next"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

@stop

@section('script')
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
@stop
