@extends('frontend.layout.layout-internal') @section('title', 'Registro de suscriptores') @section('description', 'Discover')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-365.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/registro-suscriptores.custom.css')}}" />
@stop

@section('style')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<link rel="stylesheet" href="{{ url('css/bootstrap-select.css') }}"/>
@stop

@section('custom-script')
<script src="{{ url('js/bloqueo.custom.js')}}"></script>
@stop

@section('body-style')
page-template page-template-100-width page-template-100-width-php page page-id-365 top-parent-365 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop

@section('content')

@include('frontend/partials/header')

<div id="sliders-container"></div>

<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
    <div class="fusion-page-title-row">
        <div class="fusion-page-title-wrapper">
            <div class="fusion-page-title-secondary">
                <div class="fusion-breadcrumbs">
                    <span><a href="{{ route('home') }}"><span>Home</span></a></span>
                    <span class="fusion-breadcrumb-sep">/</span>
                    <span class="breadcrumb-leaf">Registro de Suscriptores</span>
                </div>
            </div>
            <div class="fusion-page-title-captions">
                <h1 class="entry-title">Registro de Suscriptores</h1>
            </div>
        </div>
    </div>
</div>

<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
    <div class="fusion-row" style="max-width:100%;">
        <div id="content" class="full-width">
            <div id="post-365" class="post-365 page type-page status-publish hentry">
                <span class="entry-title rich-snippet-hidden">
                    Registro de Suscriptores
                </span>
                <span class="vcard rich-snippet-hidden">
                    <span class="fn">
                        <a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
                    </span>
                </span>

                <span class="updated rich-snippet-hidden">2017-05-21T20:37:25+00:00        </span>
                <div class="post-content">
                    <div id="Suscribe_Form" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style="background-color: rgba(255,255,255,0);background-image: url('{{ url('images/fondo_registro_turista.jpg') }}');background-position: center bottom;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;">
                    	<div class="fusion-builder-row fusion-row ">

	                    	<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-first 1_5"  style='margin-top:0px;margin-bottom:0px;width:16.8%; margin-right: 4%;'>
		                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
		                            <div class="fusion-clearfix"></div>
		                        </div>
	                    	</div>

		                    <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth 3_5"  style='margin-top:0px;margin-bottom:0px;width:58.4%; margin-right: 4%;'>
		                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
		                            <h3 style="text-align: center;">¡Hola!</h3>
		                            <p style="text-align: center;">Sabemos que te gusta viajar, por eso te ofrecemos promociones especiales.<br />Llena tus datos a continuación y disfruta de grandes sorpresas</p>
		                            <div role="form" class="wpcf7" id="wpcf7-f364-p365-o1" lang="es-ES" dir="ltr">
		                                <div class="screen-reader-response"></div>
		                                <form id="frmSuscriptores" action="/discover/registro-de-suscriptores/#wpcf7-f364-p365-o1" method="post" class="wpcf7-form" novalidate="novalidate">
		                                    <div style="display: none;">
		                                        <input type="hidden" name="_wpcf7" value="364" />
		                                        <input type="hidden" name="_wpcf7_version" value="4.9.2" />
		                                        <input type="hidden" name="_wpcf7_locale" value="es_ES" />
		                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f364-p365-o1" />
		                                        <input type="hidden" name="_wpcf7_container_post" value="365" />
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Nombres:</div>
		                                        <div class="col-sm-8">
		                                            <span class="wpcf7-form-control-wrap Nombres"><input type="text" name="Nombres" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span>
		                                        </div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Apellidos:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Apellidos"><input type="text" name="Apellidos" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Fecha de nacimiento:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Fecha_Nacimiento"><input type="text" name="Fecha_Nacimiento" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Teléfono:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Telefono"><input type="text" name="Telefono" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Dirección:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Direccion"><input type="text" name="Direccion" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Correo:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Email"><input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4">Destino de Viaje Preferencial:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Destinos"><select name="Destinos[]" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required selectpicker show-menu-arrow form-control" id="maxOption2m" aria-required="true" aria-invalid="false" multiple="multiple"><option value="Destino1">Destino1</option><option value="Destino2">Destino2</option><option value="Destino3">Destino3</option><option value="Destino4">Destino4</option></select></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div class="fila-cf7">
		                                        <div class="col-sm-4 fila-newsletter-subs">¿Te gustaría recibir información de nuestros paquetes y promociones?:</div>
		                                        <div class="col-sm-8"><span class="wpcf7-form-control-wrap Recibir_Newsletter"><span class="wpcf7-form-control wpcf7-radio"><span class="wpcf7-list-item first"><label><input type="radio" name="Recibir_Newsletter" value="Si" checked="checked" /><span class="wpcf7-list-item-label">Si</span></label></span><span class="wpcf7-list-item last"><label><input type="radio" name="Recibir_Newsletter" value="No" /><span class="wpcf7-list-item-label">No</span></label></span></span></span></div>
		                                        <div class="fusion-sep-clear"></div>
		                                    </div>
		                                    <div style="display:none"><span class="wpcf7-form-control-wrap mailpoetsignup-966"><input type="checkbox" name="mailpoetsignup-966" value="mailpoet_list_1" class="wpcf7-form-control wpcf7-mailpoetsignup wpcf7-validates-as-required" aria-required="true" checked="checked" />&nbsp;<label for="mailpoetsignup-966">¿Te gustaría recibir información de nuestros paquetes y promociones?</label></span>&nbsp;</div>
		                                    <p style="text-align:center"><input type="submit" value="Suscribirse" class="wpcf7-form-control wpcf7-submit" /></p>
		                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
		                                </form>
		                            </div>
		                            <div class="fusion-clearfix"></div>
		                        </div>
		                    </div>
		                    <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-last 1_5"  style='margin-top:0px;margin-bottom:0px;width:16.8%'>
		                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
		                            <div class="fusion-clearfix"></div>
		                        </div>
		                    </div>
                		</div>
            		</div>
        		</div>
    		</div>
		</div>
	</div>  <!-- fusion-row -->
</div>  <!-- #main -->

@stop

@section('script')
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
@stop
