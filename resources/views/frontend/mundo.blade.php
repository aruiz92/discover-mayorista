@extends('frontend.layout.layout-mundo') @section('title', 'Mundo') @section('description', 'Discover')


@section('content')

<header class="fusion-header-wrapper">
	<div class="fusion-header-v2 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo-1 fusion-mobile-logo-1 fusion-mobile-menu-design-modern ">
		<div class="fusion-secondary-header">
			<div class="fusion-row">
				<div class="fusion-alignright">
					<nav class="fusion-secondary-menu" role="navigation" aria-label="Secondary Menu">
						<ul role="menubar" id="menu-menu-top" class="menu">
							<li role="menuitem"  id="menu-item-136"  class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-36 current_page_item menu-item-136"  >
								<a  href="#" class="fusion-flex-link">
									<span class="fusion-megamenu-icon">
										<i class="fa glyphicon fa-map-marker"></i>
									</span>
									<span class="menu-text">Mundo</span>
								</a>
							</li>
							<li role="menuitem"  id="menu-item-135"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-135"  >
								<a  href="" class="fusion-flex-link">
									<span class="fusion-megamenu-icon">
										<i class="fa glyphicon fa-map-marker"></i>
									</span>
									<span class="menu-text">Perú</span>
								</a>
							</li>
							<li role="menuitem"  id="menu-item-369"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-369"  >
								<a  href="{{ route('suscriptores') }}">
									<span class="menu-text">Suscribe</span>
								</a>
							</li>
							<li role="menuitem"  id="menu-item-717"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-717"  >
								<a  href="{{ route('ingreso-agentes') }}">
									<span class="menu-text">Ingreso Agentes</span>
								</a>
							</li>
							<li role="menuitem"  id="menu-item-28"  class="link-red-social menu-item menu-item-type-custom menu-item-object-custom menu-item-28"  data-classes="link-red-social" >
								<a  title="Facebook" href="#" class="fusion-icon-only-link fusion-flex-link">
									<span class="fusion-megamenu-icon">
										<i class="fa glyphicon fa-facebook"></i>
									</span>
									<span class="menu-text">Facebook</span>
								</a>
							</li>
							<li role="menuitem"  id="menu-item-29"  class="link-red-social menu-item menu-item-type-custom menu-item-object-custom menu-item-29"  data-classes="link-red-social" >
								<a  title="Youtube" href="#" class="fusion-icon-only-link fusion-flex-link">
									<span class="fusion-megamenu-icon">
										<i class="fa glyphicon fa-youtube"></i>
									</span>
									<span class="menu-text">Youtube</span>
								</a>
							</li>
							<li role="menuitem"  id="menu-item-30"  class="link-red-social menu-item menu-item-type-custom menu-item-object-custom menu-item-30"  data-classes="link-red-social" >
								<a  title="Instagram" href="#" class="fusion-icon-only-link fusion-flex-link">
									<span class="fusion-megamenu-icon">
										<i class="fa glyphicon fa-instagram"></i>
									</span>
									<span class="menu-text">Instagram</span>
								</a>
							</li>
						</ul>
					</nav>
					<div class="fusion-mobile-nav-holder"></div>
				</div>
			</div>
		</div>
		<div class="fusion-header-sticky-height"></div>
		<div class="fusion-header">
		<div class="fusion-row">
		<div class="fusion-logo" data-margin-top="0px" data-margin-bottom="10px" data-margin-left="0px" data-margin-right="0px">
			<a class="fusion-logo-link" href="{{ route('home') }}">
				<img class='logo-custom-ja' src='' />
				<img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Logo" class="fusion-logo-1x fusion-standard-logo" />
				<img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Retina Logo" class="fusion-standard-logo fusion-logo-2x" />
				<!-- mobile logo -->
				<img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Mobile Logo" class="fusion-logo-1x fusion-mobile-logo-1x" />
				<img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Mobile Retina Logo" class="fusion-logo-2x fusion-mobile-logo-2x" />
				<!-- sticky header logo -->
				<img src="{{ url('images/Logo-DM.png') }}" width="257" height="71" alt="Discover Sticky Logo" class="fusion-logo-1x fusion-sticky-logo-1x" />
				<img src="{{ url('images/Logo-DM.png') }}" width="257" height="71" alt="Discover Sticky Logo Retina" class="fusion-logo-2x fusion-sticky-logo-2x" />
			</a>
		</div>
		<nav class="fusion-main-menu" aria-label="Main Menu">
			<ul role="menubar" id="menu-menu-mundo" class="fusion-menu">
				<li role="menuitem"  id="menu-item-1048"  class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-36 current_page_item menu-item-1048"  >
					<a  href="#">
						<span class="menu-text">Mundo</span>
					</a>
				</li>
				<li role="menuitem"  id="menu-item-1091"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1091"  >
					<a  href="{{ route('mundo-vacaciones') }}">
						<span class="menu-text">Vacaciones</span>
					</a>
				</li>
				<li role="menuitem"  id="menu-item-1089"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1089"  >
					<a  href="{{ route('mundo-vacaciones') }}">
						<span class="menu-text">Temáticas</span>
					</a>
				</li>
				<li role="menuitem"  id="menu-item-1088"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1088"  >
					<a  href="#">
						<span class="menu-text">Grupales</span>
					</a>
				</li>
				<li role="menuitem"  id="menu-item-1087"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1087"  >
					<a  href="#">
						<span class="menu-text">Premium</span>
					</a>
				</li>
			</ul>
		</nav>
		<div class="fusion-mobile-menu-icons">
			<a href="#" class="fusion-icon fusion-icon-bars" aria-label="Toggle mobile menu"></a>
		</div>
		<nav class="fusion-mobile-nav-holder"></nav>
		</div>
		</div>
	</div>
	<div class="fusion-clearfix"></div>
</header>

<div id="sliders-container">

	<div id="rev_slider_6_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

		<!-- START REVOLUTION SLIDER 5.4.6.6 fullwidth mode -->
		<div id="rev_slider_6_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.6">
			<ul>
				<!-- SLIDE  -->
				<li data-index="rs-29" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{ url('images/banner-peru-01-100x50.jpg') }}"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Cuzco" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
					<!-- MAIN IMAGE -->
					<img src="{{ url('images/banner-peru-01.jpg') }}"  alt="" title="Banner-Peru-01"  width="1700" height="858" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>

					<!-- LAYERS -->
					<!-- LAYER NR. 1 -->
					<div
						class="tp-caption NotGeneric-Title   tp-resizeme" 	id="slide-29-layer-1" 	data-x="center" data-hoffset="-538" data-y="center" data-voffset="-90" 	data-width="['auto','auto','auto','auto']" 	data-height="['auto','auto','auto','auto']" data-type="text" 	data-responsive_offset="on" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeInOut"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[10,10,10,10]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[10,10,10,10]"
						data-paddingleft="[0,0,0,0]"
						style="z-index: 5; white-space: nowrap; font-size: 65px; letter-spacing: px;font-family:AndrogyneR;">
						Cuzco
					</div>

					<!-- LAYER NR. 2 -->
					<p
						class="tp-caption NotGeneric-SubTitle   tp-resizeme"
						id="slide-29-layer-4"
						data-x="center" data-hoffset="-300"
						data-y="center" data-voffset="-22"
						data-width="['639','auto','auto','auto']"
						data-height="['auto','auto','auto','auto']"
						data-type="text"
						data-responsive_offset="on"

						data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeInOut"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"

						style="z-index: 6; min-width: 639px; max-width: 639px; white-space: normal; font-size: 22px; font-weight: 400; letter-spacing: 1px;font-family:'Roboto', sans-serif;;">
						Lorem ipsum dolor sit amet donec consectetur diam ac mi cursus.
					</p>

					<!-- LAYER NR. 3 -->
					<a
						class="tp-caption rev-btn "
						href="#" target="_self"			 id="slide-29-layer-6"
						data-x="183"
						data-y="456"
						data-width="['auto']"
						data-height="['auto']"

						data-type="button"
						data-actions=''
						data-responsive_offset="on"
						data-responsive="off"
						data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]'
						data-textAlign="['inherit','inherit','inherit','inherit']"
						data-paddingtop="[15,15,15,15]"
						data-paddingright="[35,35,35,35]"
						data-paddingbottom="[15,15,15,15]"
						data-paddingleft="[35,35,35,35]"

						style="z-index: 7; white-space: nowrap; font-size: 14px; line-height: 16px; font-weight: 500; color: rgba(255,255,255,1); letter-spacing: px;font-family:'Roboto', sans-serif;;background-color:rgb(6,177,246);border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">
						Consulta nuestras tarifas
					</a>

					<!-- LAYER NR. 4 -->
					<div
						class="tp-caption   tp-resizeme"
						id="slide-29-layer-8"
						data-x="107"
						data-y="273"
						data-width="['none','none','none','none']"
						data-height="['none','none','none','none']"

						data-type="image"
						data-responsive_offset="on"

						data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
						data-textAlign="['inherit','inherit','inherit','inherit']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"

						style="z-index: 8;">
						<img src="{{ url('images/icon.png') }}" alt="" data-ww="80px" data-hh="136px" width="80" height="136" data-no-retina>
					</div>
				</li>
				<!-- SLIDE  -->


				<li data-index="rs-32" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{ url('images/banner-peru-01.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Cuzco" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
					<!-- MAIN IMAGE -->
					<img src="{{ url('images/banner-peru-01.jpg') }}"  alt="" title="Banner-Peru-01"  width="1700" height="858" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>

					<!-- LAYERS -->
					<!-- LAYER NR. 5 -->
					<div class="tp-caption NotGeneric-Title   tp-resizeme"
						id="slide-32-layer-1"
						data-x="center" data-hoffset="-538"
						data-y="center" data-voffset="-90"
						data-width="['auto','auto','auto','auto']"
						data-height="['auto','auto','auto','auto']"

						data-type="text"
						data-responsive_offset="on"

						data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeInOut"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[10,10,10,10]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[10,10,10,10]"
						data-paddingleft="[0,0,0,0]"

						style="z-index: 5; white-space: nowrap; font-size: 65px; letter-spacing: px;font-family:AndrogyneR;">
						Cuzco
					</div>

					<!-- LAYER NR. 6 -->
					<p class="tp-caption NotGeneric-SubTitle   tp-resizeme"
						id="slide-32-layer-4"
						data-x="center" data-hoffset="-300"
						data-y="center" data-voffset="-22"
						data-width="['639','auto','auto','auto']"
						data-height="['auto','auto','auto','auto']"

						data-type="text"
						data-responsive_offset="on"

						data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeInOut"}]'
						data-textAlign="['left','left','left','left']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"

						style="z-index: 6; min-width: 639px; max-width: 639px; white-space: normal; font-size: 22px; font-weight: 400; letter-spacing: 1px;font-family:'Roboto', sans-serif;;">
						Lorem ipsum dolor sit amet donec consectetur diam ac mi cursus.
					</p>

					<!-- LAYER NR. 7 -->
					<a class="tp-caption rev-btn "
						href="#" target="_self"			 id="slide-32-layer-6"
						data-x="183"
						data-y="456"
						data-width="['auto']"
						data-height="['auto']"

						data-type="button"
						data-actions=''
						data-responsive_offset="on"
						data-responsive="off"
						data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]'
						data-textAlign="['inherit','inherit','inherit','inherit']"
						data-paddingtop="[15,15,15,15]"
						data-paddingright="[35,35,35,35]"
						data-paddingbottom="[15,15,15,15]"
						data-paddingleft="[35,35,35,35]"

						style="z-index: 7; white-space: nowrap; font-size: 14px; line-height: 16px; font-weight: 500; color: rgba(255,255,255,1); letter-spacing: px;font-family:'Roboto', sans-serif;;background-color:rgb(6,177,246);border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">
						Consulta nuestras tarifas
					</a>

					<!-- LAYER NR. 8 -->
					<div class="tp-caption   tp-resizeme"
						id="slide-32-layer-8"
						data-x="107"
						data-y="273"
						data-width="['none','none','none','none']"
						data-height="['none','none','none','none']"

						data-type="image"
						data-responsive_offset="on"

						data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
						data-textAlign="['inherit','inherit','inherit','inherit']"
						data-paddingtop="[0,0,0,0]"
						data-paddingright="[0,0,0,0]"
						data-paddingbottom="[0,0,0,0]"
						data-paddingleft="[0,0,0,0]"

						style="z-index: 8;">
						<img src="{{ url('images/icon-peru.png') }}" alt="" data-ww="80px" data-hh="136px" width="80" height="136" data-no-retina>
					</div>
				</li>
			</ul>

			<script>
				var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
				var htmlDivCss="";
				if(htmlDiv) {
					htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
				}else{
					var htmlDiv = document.createElement("div");
					htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
					document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
				}
			</script>

			<div class="tp-bannertimer" style="height: 7px; background: rgba(255,255,255,0.25);"></div>
		</div>

		<script>
			var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
			var htmlDivCss=".tp-caption.NotGeneric-Title,.NotGeneric-Title{color:rgba(255,255,255,1.00);font-size:70px;line-height:70px;font-weight:800;font-style:normal;font-family:Raleway;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;1:o;3:j;4:e;5:c;6:t;7:;8:O;9:b;10:j;11:e;12:c;13:t;14:];text-shadow:1px 1px 2px #333}.tp-caption.NotGeneric-SubTitle,.NotGeneric-SubTitle{color:rgba(255,255,255,1.00);font-size:13px;line-height:20px;font-weight:500;font-style:normal;font-family:Raleway;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;letter-spacing:4px}";
			if(htmlDiv) {
				htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
			}else{
				var htmlDiv = document.createElement("div");
				htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
				document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
			}
		</script>

		<script type="text/javascript">
			setREVStartSize({c: '#rev_slider_6_1', gridwidth: [1600], gridheight: [858], sliderLayout: 'fullwidth'});
			var revapi6, tpj;
			document.addEventListener("DOMContentLoaded", function() {
				tpj = jQuery;
				if(tpj("#rev_slider_6_1").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_6_1");
				}else{
					revapi6 = tpj("#rev_slider_6_1").show().revolution({
						sliderType:"standard",
						jsFileLocation:"{{ url('js/revslider/') }}/",
						sliderLayout:"fullwidth",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							keyboardNavigation:"off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation:"off",
							mouseScrollReverse:"default",
							onHoverStop:"off",
							touch:{
								touchenabled:"on",
								touchOnDesktop:"off",
								swipe_threshold: 75,
								swipe_min_touches: 1,
								swipe_direction: "horizontal",
								drag_block_vertical: false
							},
							arrows: {
								style:"hermes",
								enable:true,
								hide_onmobile:false,
								hide_onleave:true,
								hide_delay:200,
								hide_delay_mobile:1200,
								tmp:'<div class="tp-arr-allwrapper"><div class="tp-arr-imgholder"></div><div class="tp-arr-titleholder">Mi Título</div></div>',
								left: {
									h_align:"left",
									v_align:"center",
									h_offset:30,
									v_offset:0
								},
								right: {
									h_align:"right",
									v_align:"center",
									h_offset:30,
									v_offset:0
								}
							}
						},
						viewPort: {
							enable:true,
							outof:"pause",
							visible_area:"80%",
							presize:false
						},
						visibilityLevels:[1240,1024,778,480],
						gridwidth:1600,
						gridheight:858,
						lazyType:"none",
						parallax: {
							type:"mouse",
							origo:"slidercenter",
							speed:2000,
							speedbg:0,
							speedls:0,
							levels:[2,3,4,5,6,7,12,16,10,50,47,48,49,50,51,55],
						},
						shadow:0,
						spinner:"off",
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,
						shuffle:"off",
						autoHeight:"off",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}

			});	/*ready*/
		</script>

		<script>
			var htmlDivCss = unescape(".hermes.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%28255%2C%20193%2C%202%2C%201%29%3B%0A%09width%3A30px%3B%0A%09height%3A110px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%7D%0A%0A.hermes.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A15px%3B%0A%09color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%20110px%3B%0A%09text-align%3A%20center%3B%0A%20%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%0A%20%20%20%20transition%3Aall%200.3s%3B%0A%20%20%20%20-webkit-transition%3Aall%200.3s%3B%0A%7D%0A.hermes.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce824%22%3B%0A%7D%0A.hermes.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce825%22%3B%0A%7D%0A.hermes.tparrows.tp-leftarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A.hermes.tparrows.tp-rightarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A%0A.hermes%20.tp-arr-allwrapper%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%09width%3A180px%3B%0A%20%20%20%20height%3A140px%3B%0A%20%20%20%20top%3A0px%3B%0A%20%20%20%20left%3A0px%3B%0A%20%20%20%20visibility%3Ahidden%3B%0A%20%20%20%20%20%20-webkit-transition%3A%20-webkit-transform%200.3s%200.3s%3B%0A%20%20transition%3A%20transform%200.3s%200.3s%3B%0A%20%20-webkit-perspective%3A%201000px%3B%0A%20%20perspective%3A%201000px%3B%0A%20%20%20%20%7D%0A.hermes.tp-rightarrow%20.tp-arr-allwrapper%20%7B%0A%20%20%20right%3A0px%3Bleft%3Aauto%3B%0A%20%20%20%20%20%20%7D%0A.hermes.tparrows%3Ahover%20.tp-arr-allwrapper%20%7B%0A%20%20%20visibility%3Avisible%3B%0A%20%20%20%20%20%20%20%20%20%20%7D%0A.hermes%20.tp-arr-imgholder%20%7B%0A%20%20width%3A180px%3Bposition%3Aabsolute%3B%0A%20%20left%3A0px%3Btop%3A0px%3Bheight%3A110px%3B%0A%20%20transform%3Atranslatex%28-180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-180px%29%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20transition-delay%3A0.3s%3B%0A%7D%0A.hermes.tp-rightarrow%20.tp-arr-imgholder%7B%0A%20%20%20%20transform%3Atranslatex%28180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28180px%29%3B%0A%20%20%20%20%20%20%7D%0A%20%20%0A.hermes.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%20%20%20%20%20%20%20%20%20%20%20%20%0A%7D%0A.hermes%20.tp-arr-titleholder%20%7B%0A%20%20top%3A110px%3B%0A%20%20width%3A180px%3B%0A%20%20text-align%3Aleft%3B%20%0A%20%20display%3Ablock%3B%0A%20%20padding%3A0px%2010px%3B%0A%20%20line-height%3A30px%3B%20background%3A%23000%3B%0A%20%20background%3Argba%28255%2C%20193%2C%202%2C%200.75%29%3B%0A%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20font-weight%3A600%3B%20position%3Aabsolute%3B%0A%20%20font-size%3A12px%3B%0A%20%20white-space%3Anowrap%3B%0A%20%20letter-spacing%3A1px%3B%0A%20%20-webkit-transition%3A%20all%200.3s%3B%0A%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transform%3A%20rotatex%28-90deg%29%3B%0A%20%20transform%3A%20rotatex%28-90deg%29%3B%0A%20%20-webkit-transform-origin%3A%2050%25%200%3B%0A%20%20transform-origin%3A%2050%25%200%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%0A%7D%0A.hermes.tparrows%3Ahover%20.tp-arr-titleholder%20%7B%0A%20%20%20%20-webkit-transition-delay%3A%200.6s%3B%0A%20%20transition-delay%3A%200.6s%3B%0A%20%20-webkit-transform%3A%20rotatex%280deg%29%3B%0A%20%20transform%3A%20rotatex%280deg%29%3B%0A%7D%0A%0A");

			var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
			if(htmlDiv) {
			htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
			}
			else{
			var htmlDiv = document.createElement('div');
			htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
			document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
			}

		</script>
	</div>
	<!-- END REVOLUTION SLIDER -->

</div>

<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
	<div class="fusion-row" style="max-width:100%;">
		<div id="content" class="full-width">
			<div id="post-36" class="post-36 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">
Mundo		</span>
			<span class="vcard rich-snippet-hidden">
				<span class="fn">
					<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
				</span>
			</span>
			<span class="updated rich-snippet-hidden">
				2017-07-19T11:13:56+00:00
			</span>
				<div class="post-content">
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode hundred-percent-fullwidth fusion-equal-height-columns"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;margin-bottom: 40px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-spacing-no bloque-left font-script 1_2"  style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0 ) * 0.5 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="padding: 25px 40px 30px 40px;background-image: url('{{ url('images/2-banner-peru-r1-c1.png') }}');background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="{{ url('images/2-banner-peru-r1-c1.png') }}">
									<div class="fusion-title title fusion-sep-none fusion-title-size-two title-h2-alt" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-left">¡Sé parte de nuestros agentes asociados!</h2>
									</div>
									<p>Llena nuestro formulario y únete a la familia DISCOVER MAYORISTA</p>
									<div class="fusion-button-wrapper">
										<style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{background: #01a13d;}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #01a13d;}.fusion-button.button-1{width:auto;}</style>
										<a class="fusion-button button-flat fusion-button-square button-large button-custom button-1" target="_self" title="Registrate" href="{{ route('registro-agentes') }}">
											<span class="fusion-button-text">Registrate</span>
										</a>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-spacing-no div-right font-roboto text-blanco 1_2"  style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0 ) * 0.5 ) );'>
								<div class="fusion-column-wrapper" style="padding: 25px 40px 30px 40px;background-image: url('{{ url('images/2-banner-peru-r1-c2.png') }}');background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="{{ url('2-banner-peru-r1-c2.png') }}">
									<div class="fusion-title title fusion-sep-none fusion-title-size-two" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-left">Prepara tus maletas y viaja a Europa
											<br />
											<strong>2017 &#8211; 2018</strong>
										</h2>
									</div>
									<p>
										<sup>Desde</sup>
										<span class="usd-precio">$3,270</span> ó
										<span class="soles-precio">11,118</span>
									</p>
									<div class="fusion-button-wrapper">
										<style type="text/css" scoped="scoped">.fusion-button.button-2 .fusion-button-text, .fusion-button.button-2 i {color:#ffffff;}.fusion-button.button-2 {border-width:0px;border-color:#ffffff;}.fusion-button.button-2 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-2:hover .fusion-button-text, .fusion-button.button-2:hover i,.fusion-button.button-2:focus .fusion-button-text, .fusion-button.button-2:focus i,.fusion-button.button-2:active .fusion-button-text, .fusion-button.button-2:active{color:#ffffff;}.fusion-button.button-2:hover, .fusion-button.button-2:focus, .fusion-button.button-2:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-2{width:auto;}</style>
										<a class="fusion-button button-flat fusion-button-round button-large button-default button-2 boton-4" target="_self" title="Cotiza tu viaje" href="#">
											<span class="fusion-button-text">Cotiza tu viaje</span>
										</a>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-spacing-no 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-spacing-no titulo-estilo-2 font-script 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="imageframe-align-center">
										<span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none">
											<img src="{{ url('images/flecha-d-direccion.png') }}" width="49" height="37" alt="" title="flecha-d-direccion" class="img-responsive wp-image-536"/>
										</span>
									</div>
									<div class="fusion-title title fusion-title-center fusion-title-size-two" style="margin-top:15px;margin-bottom:31px;">
										<div class="title-sep-container title-sep-container-left">
											<div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
										</div>
										<h2 class="title-heading-center">Vacaciones</h2>
										<div class="title-sep-container title-sep-container-right">
											<div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
										</div>
									</div>
									<div class="fusion-button-wrapper fusion-aligncenter">
										<style type="text/css" scoped="scoped">.fusion-button.button-3 .fusion-button-text, .fusion-button.button-3 i {color:#ffffff;}.fusion-button.button-3 {border-width:0px;border-color:#ffffff;}.fusion-button.button-3 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-3:hover .fusion-button-text, .fusion-button.button-3:hover i,.fusion-button.button-3:focus .fusion-button-text, .fusion-button.button-3:focus i,.fusion-button.button-3:active .fusion-button-text, .fusion-button.button-3:active{color:#ffffff;}.fusion-button.button-3:hover, .fusion-button.button-3:focus, .fusion-button.button-3:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-3{width:auto;}</style>
										<a class="fusion-button button-flat fusion-button-round button-large button-default button-3 boton-5" target="_self" title="Ver mas paquetes" href="{{ route('mundo-vacaciones') }}">
											<span class="fusion-button-text">Ver mas paquetes</span>
										</a>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last fusion-spacing-no 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div id="Vacaciones_Div" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-large fusion-blog-no paquetes-carrusel">
										<div class="fusion-posts-container fusion-posts-container-no" data-pages="1">
											<article id="post-513" class="fusion-post-large post-513 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('paquete') }}">
																	<img width="275" height="420" src="{{ url('images/paquetes/cancun-collage-2.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>
												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('paquete') }}">CANCÚN / RIVIERA MAYA</a>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
											</article>
											<article id="post-511" class="fusion-post-large post-511 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('paquete') }}">
																	<img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>
												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('paquete') }}">¡Últimos Espacios!!! Iguazú  Fiestas Patrias</a>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
											</article>
											<article id="post-509" class="fusion-post-large post-509 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('paquete') }}">
																	<img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>
												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('paquete') }}">Los Mejores Conciertos en Las Vegas</a>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
											</article>
											<article id="post-508" class="fusion-post-large post-508 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('paquete') }}">
																	<img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>
												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('paquete') }}">Colombia es Realismo Mágico</a>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
											</article>
											<article id="post-506" class="fusion-post-large post-506 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('paquete') }}">
																	<img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>
												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('paquete') }}">Caribbean Pride Chic Punta Cana</a>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
											</article>
											<article id="post-490" class="fusion-post-large post-490 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('paquete') }}">
																	<img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>
												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('paquete') }}">Vacaciones de Lujo por Fiestas Patrias  The Grand At Moon Palace</a>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
											</article>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-spacing-no 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-spacing-no titulo-estilo-2 font-script 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="imageframe-align-center">
										<span class="fusion-imageframe imageframe-none imageframe-8 hover-type-none">
											<img src="{{ url('images/flecha-d-direccion.png') }}" width="49" height="37" alt="" title="flecha-d-direccion" class="img-responsive wp-image-536"/>
										</span>
									</div>
									<div class="fusion-title title fusion-title-center fusion-title-size-two" style="margin-top:15px;margin-bottom:31px;">
										<div class="title-sep-container title-sep-container-left">
											<div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
										</div>
										<h2 class="title-heading-center">Viajes Temáticos</h2>
										<div class="title-sep-container title-sep-container-right">
											<div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
										</div>
									</div>
									<div class="fusion-button-wrapper fusion-aligncenter">
										<style type="text/css" scoped="scoped">.fusion-button.button-10 .fusion-button-text, .fusion-button.button-10 i {color:#ffffff;}.fusion-button.button-10 {border-width:0px;border-color:#ffffff;}.fusion-button.button-10 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-10:hover .fusion-button-text, .fusion-button.button-10:hover i,.fusion-button.button-10:focus .fusion-button-text, .fusion-button.button-10:focus i,.fusion-button.button-10:active .fusion-button-text, .fusion-button.button-10:active{color:#ffffff;}.fusion-button.button-10:hover, .fusion-button.button-10:focus, .fusion-button.button-10:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-10:hover .fusion-button-icon-divider, .fusion-button.button-10:hover .fusion-button-icon-divider, .fusion-button.button-10:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-10{width:auto;}</style>
										<a class="fusion-button button-flat fusion-button-round button-large button-default button-10 boton-5" target="_self" title="Ver mas paquetes" href="#">
											<span class="fusion-button-text">Ver mas paquetes</span>
										</a>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last fusion-spacing-no 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div id="Vacaciones_Div" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-blog-shortcode fusion-blog-shortcode-2 fusion-blog-archive fusion-blog-layout-large fusion-blog-no paquetes-carrusel dias-precio-remove">
										<div class="fusion-posts-container fusion-posts-container-no" data-pages="1">
											<article id="post-556" class="fusion-post-large post-556 post type-post status-publish format-standard has-post-thumbnail hentry category-tematicas-peru categoria-id-9">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('paquete') }}">
																	<img width="797" height="531" src="{{ url('images/paquetes/lima-tradicional.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>
												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('paquete') }}">Eliminatorias Mundial Rusia 2018 Ecuador vs Perú Partido</a>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
											</article>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<script>

jQuery.noConflict();

jQuery( document ).ready(function( $ ) {


$('.paquetes-carrusel > .fusion-posts-container').bxSlider({
slideWidth: 275,
minSlides: 1,
maxSlides: 4,
moveSlides: 1,
slideMargin: 10,
responsive: true,
controls: true,
infiniteLoop: false,
useCSS: true,
nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
pager: false,
});


});

									</script>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

@stop


@section('style')


@stop


@section('script')

<script type="text/javascript">
	function revslider_showDoubleJqueryError(sliderID) {
		var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
		errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
		errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option: <strong> <b>Put JS Includes To Body</b> </strong> option to true.";
		errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it."; errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
		jQuery(sliderID).show().html(errorMessage);
	}
</script>

<script type='text/javascript' src="{{ url('js/mundo.contact.scripts.js') }}"></script>
<!--[if IE 9]>
<script type='text/javascript' src='{{ url('fusion-ie9.js') }}'></script>
<![endif]-->
<script type='text/javascript' src="{{ url('js/mundo.comment-reply.min.js') }}"></script>
<script type='text/javascript' src="{{ url('js/7fe5606794b62066a68cdda469435491.js') }}"></script>
<script type='text/javascript' src="{{ url('js/wp-embed.min.js') }}"></script>
<script type='text/javascript' src="{{ url('js/jquery.language.validationEngine-es.js') }}"></script>
<script type='text/javascript' src="{{ url('js/jquery.validationEngine.js') }}"></script>
<script type='text/javascript'>
/*
<![CDATA[ */
var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"admin-ajax.php","loadingTrans":"Cargando...","is_rtl":""};
/* ]]> */

</script>
<script type='text/javascript' src="{{ url('js/front-subscribers.js') }}"></script>

@stop
