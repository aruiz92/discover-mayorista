@extends('frontend.layout.layout-internal') @section('title', 'Bloqueo') @section('description', 'Discover') 

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-1184.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/bloqueo-custom.css')}}" />
@stop

@section('custom-script')
<script src="{{ url('js/bloqueo.custom.js')}}"></script>
@stop

@section('style')

@stop

@section('body-style')
post-template-default single single-post postid-1184 single-format-standard categoria-id-7 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop


@section('content')

@include('frontend.partials.header')
				
<div id="sliders-container"></div>
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
	<div class="fusion-page-title-row">
		<div class="fusion-page-title-wrapper">
			<div class="fusion-page-title-secondary">
				<div class="fusion-breadcrumbs">
					<span>
						<a href="{{ route('home') }}">
							<span>Home</span>
						</a>
					</span>
					<span class="fusion-breadcrumb-sep">/</span>
					<span>
						<a href="{{ route('bloqueos') }}">
							<span>Bloqueos</span>
						</a>
					</span>
					<span class="fusion-breadcrumb-sep">/</span>
					<span class="breadcrumb-leaf">Cancun</span>
				</div>
			</div>
			<div class="fusion-page-title-captions">
				<h1 class="entry-title">Bloqueos y Charters</h1>
			</div>
		</div>
	</div>
</div>

<div id="main" role="main" class="clearfix " style="">
	<div class="fusion-row" style="">
		<div id="content" style="width: 100%;">
			<article id="post-1184" class="post post-1184 type-post status-publish format-standard has-post-thumbnail hentry category-bloqueos categoria-id-7">
				<div class="fusion-flexslider flexslider fusion-flexslider-loading post-slideshow fusion-post-slideshow">
					<ul class="slides">
						<li>
							<a href="{{ url('images/cancun.jpg') }}" data-rel="iLightbox[gallery1184]" title="" data-title="Cancun" data-caption="" aria-label="Cancun">
								<span class="screen-reader-text">View Larger Image</span>
								<img width="797" height="531" src="{{ url('images/cancun.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
							</a>
						</li>
					</ul>
				</div>
				<h2 class="entry-title fusion-post-title">Cancun</h2>
				<div class="post-content">
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth fusion-column-first 3_5"  style='margin-top:0px;margin-bottom:20px;width:60%;width:calc(60% - ( ( 4% ) * 0.6 ) );margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none">
										<img src="{{ url('images/cancun.jpg') }}" width="797" height="531" alt="" title="Cancun" class="img-responsive wp-image-131" srcset="" sizes="(max-width: 800px) 100vw, 1300px" />
									</span>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_5  fusion-two-fifth fusion-column-last 2_5"  style='margin-top:0px;margin-bottom:20px;width:40%;width:calc(40% - ( ( 4% ) * 0.4 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<h3 class="titulo-paquete">Cancun</h3>
									<h4 id="Fecha-paq" class="fecha-inicio-fin">
										<strong>Enero</strong>
										<strong>Abril</strong>
									</h4>
									<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>
									<a href="#" target="_blank" class="boton-1">Descargar Información Detallada</a>
									<div class="fusion-sep-clear"></div>
									<div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:5px;margin-bottom:5px;width:100%;max-width:100%;"></div>
									<div class="fusion-button-wrapper">
										<style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{width:auto;}</style>
										<a class="fusion-button button-flat fusion-button-round button-large button-default button-1 boton-2" target="_self" title="Solicitar Cotización" href="#" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar">
											<span class="fusion-button-text">Solicitar Cotización</span>
										</a>
									</div>
									<div class="fusion-sep-clear"></div>
									<div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:15px;margin-bottom:15px;width:100%;max-width:100%;"></div>
									<!-- Go to www.addthis.com/dashboard to customize your tools -->
									<div class="addthis_inline_share_toolbox"></div>
									<div class="fusion-modal modal fade modal-1 Form_Cotizar max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true" id="form_cotizar_paq">
										<style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
										<div class="modal-dialog modal-lg">
											<div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
												<div class="modal-header">
													<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Solicita una cotización</h3>
												</div>
												<div class="modal-body">
													<p>Estas solicitando cotización de:</p>
													<h3>Cancun</h3>
													<h4>Enero &#8211; Abril</h4>
													<div role="form" class="wpcf7" id="wpcf7-f162-p1184-o1" lang="es-ES" dir="ltr">
														<div class="screen-reader-response"></div>
														<form action="/discover/bloqueos/cancun/#wpcf7-f162-p1184-o1" method="post" class="wpcf7-form" novalidate="novalidate">
															<div style="display: none;">
																<input type="hidden" name="_wpcf7" value="162" />
																<input type="hidden" name="_wpcf7_version" value="4.9.2" />
																<input type="hidden" name="_wpcf7_locale" value="es_ES" />
																<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p1184-o1" />
																<input type="hidden" name="_wpcf7_container_post" value="1184" />
															</div>
															<p>
																<label> Nombre (requerido)
																	<br />
																	<span class="wpcf7-form-control-wrap Nombre">
																		<input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
																	</span>
																</label>
															</p>
															<p>
																<label> Tu correo electrónico (requerido)
																	<br />
																	<span class="wpcf7-form-control-wrap Email">
																		<input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
																	</span>
																</label>
															</p>
															<p>
																<label> Asunto
																	<br />
																	<span class="wpcf7-form-control-wrap Asunto">
																		<input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
																	</span>
																</label>
															</p>
															<p>
																<label> Mensaje
																	<br />
																	<span class="wpcf7-form-control-wrap Mensaje">
																		<textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
																	</span>
																</label>
															</p>
															<p>
																<span class="wpcf7-form-control-wrap Producto">
																	<input type="hidden" name="Producto" value="Cancun" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
																</span>
																<span class="wpcf7-form-control-wrap URL">
																	<input type="hidden" name="URL" value="{{ route('bloqueo') }}" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
																</span>
															</p>
															<p style="text-align:right">
																<input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
															</p>
															<div class="wpcf7-response-output wpcf7-display-none"></div>
														</form>
													</div>
												</div>
												<div class="modal-footer">
													<a class="fusion-button button-default button-medium button default medium" data-dismiss="modal">Close</a>
												</div>
											</div>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:35px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-tabs fusion-tabs-1 classic tabs-post-interno horizontal-tabs">
										<style type="text/css">.fusion-tabs.fusion-tabs-1 .nav-tabs li a{border-top-color:#ebeaea;background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{border-right-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#ffffff;border-top-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}</style>
										<div class="nav">
											<ul class="nav-tabs nav-justified">
												<li class="active">
													<a class="tab-link" data-toggle="tab" id="fusion-tab-conboletosaéreos" href="#tab-6cd82385490d7c13b96">
														<h4 class="fusion-tab-heading">Con Boletos Aéreos</h4>
													</a>
												</li>
												<li>
													<a class="tab-link" data-toggle="tab" id="fusion-tab-adicionales" href="#tab-c2e942a762fb93b2abb">
														<h4 class="fusion-tab-heading">Adicionales</h4>
													</a>
												</li>
												<li>
													<a class="tab-link" data-toggle="tab" id="fusion-tab-itinerario" href="#tab-e756b53ec0266bf9cfc">
														<h4 class="fusion-tab-heading">Itinerario</h4>
													</a>
												</li>
												<li>
													<a class="tab-link" data-toggle="tab" id="fusion-tab-recomendaciones" href="#tab-f224573095c1db04326">
														<h4 class="fusion-tab-heading">Recomendaciones</h4>
													</a>
												</li>
												<li>
													<a class="tab-link" data-toggle="tab" id="fusion-tab-informacióndepagos" href="#tab-de66421ec96e1a5f172">
														<h4 class="fusion-tab-heading">Información de pagos</h4>
													</a>
												</li>
											</ul>
										</div>
										<div class="tab-content">
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li class="active">
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-conboletosaéreos" href="#tab-6cd82385490d7c13b96">
															<h4 class="fusion-tab-heading">Con Boletos Aéreos</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade in active" id="tab-6cd82385490d7c13b96">
												<p>
													<strong>Incluye:</strong>
												</p>
												<ul>
													<li>Veniam melius nusquam mei ad.</li>
													<li>Mea ea decore impedit voluptaria.</li>
													<li>Mei eu dolorum explicari. Ridens utroque instructior eu eum.</li>
													<li>Qui viderer feugait id. Nam no ignota abhorreant.</li>
													<li>Hinc brute eu sea, atqui iracundia sadipscing ei cum.</li>
													<li>Assum elitr dolorem cu sea, vis id esse putant praesent.</li>
												</ul>
											</div>
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li>
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-adicionales" href="#tab-c2e942a762fb93b2abb">
															<h4 class="fusion-tab-heading">Adicionales</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade" id="tab-c2e942a762fb93b2abb">
												<p>Adicionales</p>
											</div>
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li>
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-itinerario" href="#tab-e756b53ec0266bf9cfc">
															<h4 class="fusion-tab-heading">Itinerario</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade" id="tab-e756b53ec0266bf9cfc">
												<p>Itinerario</p>
											</div>
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li>
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-recomendaciones" href="#tab-f224573095c1db04326">
															<h4 class="fusion-tab-heading">Recomendaciones</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade" id="tab-f224573095c1db04326">
												<p>Recomendaciones</p>
											</div>
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li>
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-informacióndepagos" href="#tab-de66421ec96e1a5f172">
															<h4 class="fusion-tab-heading">Información de pagos</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade" id="tab-de66421ec96e1a5f172">
												<p>Información de pagos</p>
											</div>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="fusion-meta-info">
					<div class="fusion-meta-info-wrapper">
						<span class="vcard rich-snippet-hidden">
							<span class="fn">
								<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
							</span>
						</span>
						<span class="updated rich-snippet-hidden">
2018-01-08T12:34:12+00:00		</span>
						<span>enero 8, 2018</span>
						<span class="fusion-inline-sep">|</span>
					</div>
				</div>
				<div class="related-posts single-related-posts">
					<h3 class="title-heading-left title-post-relacionados">
Otros Bloqueos Vigentes			</h3>
					<div class="fusion-carousel fusion-carousel-title-below-image" data-imagesize="fixed" data-metacontent="yes" data-autoplay="no" data-touchscroll="no" data-columns="3" data-itemmargin="44px" data-itemwidth="180" data-touchscroll="yes" data-scrollitems="1">
						<div class="fusion-carousel-positioner">
							<div class="max-width-1030">
								<ul class="fusion-carousel-holder">
									<li class="fusion-carousel-item">
										<div class="fusion-carousel-item-wrapper">
											<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
												<a href="{{ route('bloqueo') }}">
													<img src="{{ url('images/cancun-500x383.jpg') }}" srcset="" width="500" height="383" alt="Boston" />
												</a>
											</div>
											<div class="contenedor-info-related-post">
												<h4 class="fusion-carousel-title">
													<a href="{{ route('bloqueo') }}"_self>Boston</a>
												</h4>
												<div class="fusion-carousel-meta info-adicional">
													<span class="fusion-date" style="display:none">mayo 19, 2017</span>
													<span class="fusion-inline-sep" style="display:none">|</span>
													<div class="info-adicional-content">                                                USA                                                										</div>
													<a class="leer-mas-related-post" href="{{ route('bloqueo') }}"_self>Leer más</a>
												</div>
												<!-- fusion-carousel-meta -->
											</div>
										</div>
										<!-- fusion-carousel-item-wrapper -->
									</li>
									<li class="fusion-carousel-item">
										<div class="fusion-carousel-item-wrapper">
											<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
												<a href="#">
													<img src="{{ url('images/cancun-500x383.jpg') }}" srcset="" width="500" height="383" alt="Amsterdan" />
												</a>
											</div>
											<div class="contenedor-info-related-post">
												<h4 class="fusion-carousel-title">
													<a href="{{ route('bloqueo') }}"_self>Amsterdan</a>
												</h4>
												<div class="fusion-carousel-meta info-adicional">
													<span class="fusion-date" style="display:none">mayo 19, 2017</span>
													<span class="fusion-inline-sep" style="display:none">|</span>
													<div class="info-adicional-content">                                                Holanda                                                										</div>
													<a class="leer-mas-related-post" href="{{ route('bloqueo') }}"_self>Leer más</a>
												</div>
												<!-- fusion-carousel-meta -->
											</div>
										</div>
										<!-- fusion-carousel-item-wrapper -->
									</li>
									<li class="fusion-carousel-item">
										<div class="fusion-carousel-item-wrapper">
											<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
												<a href="{{ route('bloqueo') }}">
													<img src="{{ url('images/cancun-500x383.jpg') }}" srcset="" width="500" height="383" alt="Miami" />
												</a>
											</div>
											<div class="contenedor-info-related-post">
												<h4 class="fusion-carousel-title">
													<a href="{{ route('bloqueo') }}"_self>Miami</a>
												</h4>
												<div class="fusion-carousel-meta info-adicional">
													<span class="fusion-date" style="display:none">mayo 19, 2017</span>
													<span class="fusion-inline-sep" style="display:none">|</span>
													<div class="info-adicional-content">                                                Brasil                                                										</div>
													<a class="leer-mas-related-post" href="{{ route('bloqueo') }}"_self>Leer más</a>
												</div>
												<!-- fusion-carousel-meta -->
											</div>
										</div>
										<!-- fusion-carousel-item-wrapper -->
									</li>
									<li class="fusion-carousel-item">
										<div class="fusion-carousel-item-wrapper">
											<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
												<a href="{{ route('bloqueo') }}">
													<img src="{{ url('images/cancun-500x383.jpg') }}" srcset="" width="500" height="383" alt="Sao Paulo" />
												</a>
											</div>
											<div class="contenedor-info-related-post">
												<h4 class="fusion-carousel-title">
													<a href="#"_self>Sao Paulo</a>
												</h4>
												<div class="fusion-carousel-meta info-adicional">
													<span class="fusion-date" style="display:none">mayo 18, 2017</span>
													<span class="fusion-inline-sep" style="display:none">|</span>
													<div class="info-adicional-content">                                                Brasil                                                										</div>
													<a class="leer-mas-related-post" href="#"_self>Leer más</a>
												</div>
												<!-- fusion-carousel-meta -->
											</div>
										</div>
										<!-- fusion-carousel-item-wrapper -->
									</li>
								</ul>
								<!-- fusion-carousel-holder -->
							</div>
							<div class="fusion-carousel-nav">
								<span class="fusion-nav-prev"></span>
								<span class="fusion-nav-next"></span>
							</div>
						</div>
						<!-- fusion-carousel-positioner -->
					</div>
					<!-- fusion-carousel -->
				</div>
				<!-- related-posts -->
			</article>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

@stop

@section('script')

<script type='text/javascript' src="{{ url('js/contact-script.js') }}"></script>
<!--[if IE 9]>
	<script type='text/javascript' src='{{ url('js/fusion-ie9.js') }}'></script>
<![endif]-->
<script type='text/javascript' src="{{ url('js/comment-reply.min.js') }}"></script>
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
<script type='text/javascript' src="{{ url('js/wp-embed.min.js') }}"></script>
<script type='text/javascript' src="{{ url('js/jquery.language.validationEngine-es.js') }}"></script>
<script type='text/javascript' src="{{ url('js/jquery.validationEngine.js') }}"></script>
<script type='text/javascript'>
/* 
<![CDATA[ */
var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"admin-ajax.php","loadingTrans":"Cargando...","is_rtl":""};
/* ]]> */
</script>
<script type='text/javascript' src="{{ url('js/front-subscribers.js') }}"></script>

@stop

