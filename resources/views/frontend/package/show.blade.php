@extends('frontend.layout.layout-internal') @section('title', 'Paquete') @section('description', 'Discover')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-513.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/slick.css')}}" />
<link rel="stylesheet" href="{{ asset('css/slick-theme.css')}}" />
<link rel="stylesheet" href="{{ asset('css/paquete.custom.css')}}" />
<link rel="stylesheet" href="{{ asset('v2/css/app.css')}}" />
@stop

@section('custom-script')

@stop

@section('style')

@stop

@section('body-style')
post-template-default single single-post postid-513 single-format-standard categoria-id-8 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text
@stop


@section('content')

    @include('frontend.partials.header')

    <div id="sliders-container"></div>

    <div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
        <div class="fusion-page-title-row">
            <div class="fusion-page-title-wrapper">

                <div class="fusion-page-title-secondary">
                    <div class="fusion-breadcrumbs">
                      <span>
                        <a href="{{ route('home') }}">
                          <span>Home</span>
                        </a>
                      </span>
                      <span class="fusion-breadcrumb-sep">/</span>
                      <span>
                        <a href="{{ route('destination', ['id' => $package->d_id, 'name' => str_slug($package->d_nombre)]) }}">
                          <span>{{ $package->d_nombre }}</span>
                        </a>
                      </span>
                      <span class="fusion-breadcrumb-sep">/</span>
                      <span>
                        <a href="{{ route('packageSubType', ['idD' => $package->d_id, 'nameD' => str_slug($package->d_nombre), 'idT' => $package->pt_id, 'nameT' => str_slug($package->pt_nombre), 'idS' => $package->ps_id, 'nameS' => str_slug($package->ps_nombre)]) }}">
                          <span>{{ $package->ps_nombre }}</span>
                        </a>
                      </span>
                      <span class="fusion-breadcrumb-sep">/</span>
                      <span class="breadcrumb-leaf">{{ $package->nombre }}</span>
                    </div>
                </div>
                <div class="fusion-page-title-captions">
                    <h1 class="entry-title">{{ $package->ps_nombre }}</h1>
                </div>
            </div>
        </div>
    </div>

    <div id="main" role="main" class="clearfix " style="">
        <div class="fusion-row" style="">

            <div id="content" style="width: 100%;">

                <article id="post-513" class="post post-513 type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">

                    <div class="fusion-flexslider flexslider fusion-flexslider-loading post-slideshow fusion-post-slideshow">
                        <ul class="slides">
                            <li>
                                <a href="{{ url('images/paquetes/cancun-collage-2.jpg')}}" data-rel="iLightbox[gallery513]" title="" data-title="CancúnCollage2" data-caption="" aria-label="CancúnCollage2">
                                    <span class="screen-reader-text">View Larger Image</span>
                                    <img width="275" height="420" src="{{ url('images/paquetes/cancun-collage-2.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(max-width: 800px) 100vw, 1300px" />
                                </a>
                            </li>
                        </ul>
                    </div>

                    <h2 class="entry-title fusion-post-title">{{ $package->nombre }}</h2>

                    <div class="post-content">
                        <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
                            <div class="fusion-builder-row fusion-row ">
                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth fusion-column-first 3_5" style='margin-top:0px;margin-bottom:20px;width:60%;width:calc(60% - ( ( 4% ) * 0.6 ) );margin-right: 4%;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                        <span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none">
                                            <img src="{{ url('files/packages/secundario/'.$package->imagen_2) }}" width="800" height="1202" alt="" title="Mailing Bloqueos Fiestas Patrias Cun-Faranda_" class="img-responsive wp-image-1145" srcset="" sizes="(max-width: 800px) 100vw, 1300px" />
                                        </span>
                                        <div class="fusion-clearfix"></div>
                                    </div>
                                </div>

                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_2_5  fusion-two-fifth fusion-column-last contenido-vacaciones 2_5" style='margin-top:0px;margin-bottom:20px;width:40%;width:calc(40% - ( ( 4% ) * 0.4 ) );'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                        <h3 class="titulo-paquete">{{ $package->nombre }}</h3>
                                        <h4 id="Fecha-paq" class="fecha-inicio-fin">
                                            <strong>
                                              {{ $package->nro_dias }} <span class="span-dias"> días</span>
                                            </strong>
                                            <strong>
                                              {{ $package->nro_noches }} <span class="span-noches"> noches</span>
                                            </strong>
                                            @if(!empty($package->precio_ser) && !is_null($package->precio_ser) && $package->precio_ser !== '0.00')
                                              <div class="precio-paquetes">
                                                <p>Desde USD {{ $package->precio_ser }} solo servicios</p>
                                                <p>Con boleto aereo USD {{ $package->precio_bol }}</p>
                                              </div>
                                            @else
                                              <div class="precio-paquetes">
                                                <p>Desde ${{ $package->precio_min }}</p>
                                              </div>
                                            @endif
                                        </h4>

                                        @if(!empty($package->descripcion) || !is_null($package->descripcion))
                                          <div>
                                            {!! $package->descripcion !!}
                                          </div>
                                        @endif

                                        @if(Auth::check())
                                        <br>
                                          @foreach($package->files as $file)
                                            <a href="{{ url('files/packages/documents/'.$file->archivo) }}" target="_blank" class="boton-1" style="margin-bottom: 1rem;" download>{{ $file->nombre }}</a>
                                          @endforeach
                                        <br>
                                          <?php
                                            $flyers = explode(',', $package->flyers);
                                          ?>
                                          @if(!empty($package->flyers) || !is_null($package->flyers))
                                            @for($i = 0; $i < count($flyers); $i++)
                                              <a href="{{ url('files/flyers/'.$flyers[$i]) }}" target="_blank" class="boton-1" style="margin-bottom: 1rem;" download>Flyers</a>
                                            @endfor
                                          @endif
                                        @endif
                                            <div class="fusion-sep-clear"></div>
                                            <div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:5px;margin-bottom:5px;width:100%;max-width:100%;"></div>
                                            <div class="fusion-button-wrapper">
                                                <style type="text/css" scoped="scoped">
                                                    .fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{width:auto;}
                                                </style>
                                                <a class="fusion-button button-flat fusion-button-round button-large button-default button-1 boton-2" target="_self" title="Solicitar Cotización" href="#" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar"><span class="fusion-button-text">Solicitar Cotización</span></a>
                                            </div>
                                            <div class="fusion-sep-clear"></div>
                                            <div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:15px;margin-bottom:15px;width:100%;max-width:100%;"></div>

                                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                            <div class="addthis_inline_share_toolbox"></div>
                                            <div class="fusion-modal modal fade modal-1 Form_Cotizar max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true" id="form_cotizar_paq">
        <style type="text/css">
            .modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}
        </style>
        <div class="modal-dialog modal-lg">
            <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Solicita una cotización</h3>
                </div>
                <div class="modal-body">
                    <div class="frm-header">
                      <p>Estas solicitando cotización de:</p>
                      <h3>{{ $package->nombre }}</h3>
                      <!-- <h4> &#8211; </h4> -->
                    </div>
                    <div role="form" class="wpcf7" id="wpcf7-f162-p513-o1" lang="es-ES" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <div class="frm-response">
                          <h2 class="text-center"></h2>
                        </div>
                        <?php $action = Auth::check() ? url('api/quoteAuth') : url('api/quote'); ?>
                        <form id="frmQuote" action="{{ $action }}" method="post" class="wpcf7-form" novalidate="novalidate">
                            <div style="display: none;">
                              <input type="hidden" name="package_id" value="{{ $package->id }}" />
                            </div>
                            @if(!Auth::check())
                            <p>
                                <label> Nombre (requerido)<br />
                                    <span class="wpcf7-form-control-wrap Nombre">
                                        <input type="text" name="nombres" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                                    </span>
                                </label>
                            </p>
                            <p>
                                <label> Tu correo electrónico (requerido)<br />
                                <span class="wpcf7-form-control-wrap Email">
                                    <input type="email" name="correo" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span>
                                </label>
                            </p>
                            <p class="group-pais">
                              <label>País<br/>
                                  <span class="wpcf7-form-control-wrap">
                                    <select id="selectPais" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="pais">
                                      <option value="">Seleccionar</option>
                                      @foreach($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->nombre }}</option>
                                      @endforeach
                                    </select>
                                  </span>
                              </label>
                            </p>
                            <p class="group-provincia">
                              <label>Provincia<br/>
                                  <span class="wpcf7-form-control-wrap">
                                    <select id="selectProvincia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="provincia">
                                      <option value="">Seleccionar</option>
                                    </select>
                                  </span>
                              </label>
                            </p>
                            <p class="group-distrito">
                              <label>Distrito<br/>
                                  <span class="wpcf7-form-control-wrap">
                                    <select id="selectDistrito" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="distrito">
                                      <option value="">Seleccionar</option>
                                    </select>
                                    <div class="select-arrow" style="height: 27px; width: 27px; line-height: 27px;"></div>
                                  </span>
                              </label>
                            </p>
                            <p class="group-agencia">
                              <label>Agencia<br/>
                                  <span class="wpcf7-form-control-wrap">
                                    <select id="selectAgencia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="agencia">
                                      <option value="">Seleccionar</option>
                                    </select>
                                  </span>
                              </label>
                            </p>
                            @endif
                            <p>
                                <label> Mensaje<br />
                                    <span class="wpcf7-form-control-wrap Mensaje">
                                      <textarea name="mensaje" rows="2" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" style="height: 100px;"></textarea>
                                    </span>
                                </label>
                            </p>
                            <p style="text-align:right">
                                <input id="btnSubmit" type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
                            </p>
                            <div class="wpcf7-response-output wpcf7-display-none">
                              <ul></ul>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="fusion-button button-default button-medium button default medium" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:35px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:20px;'>
                                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                            <div class="fusion-tabs fusion-tabs-1 classic tabs-post-interno horizontal-tabs">
                                                <style type="text/css">
                                                    .fusion-tabs.fusion-tabs-1 .nav-tabs li a{border-top-color:#ebeaea;background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{border-right-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#ffffff;border-top-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}
                                                </style>
                                                <div class="nav">
                                                    <ul class="nav-tabs nav-justified">
                                                        @foreach($package->package_tabs as $key => $package_tab)
                                                            <li class="{{ ($key === 0) ? 'active':'' }}">
                                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-package-{{ $package_tab->id }}" href="#package-{{ $package_tab->id }}">
                                                                    <h4 class="fusion-tab-heading">{{ $package_tab->nombre }}</h4>
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div class="tab-content">
                                                    @foreach($package->package_tabs as $key => $package_tab)
                                                        <div class="nav fusion-mobile-tab-nav">
                                                            <ul class="nav-tabs nav-justified">
                                                                <li class="active">
                                                                    <a class="tab-link" data-toggle="tab" id="mobile-fusion-package-{{ $package_tab->id }}" href="#package-{{ $package_tab->id }}">
                                                                        <h4 class="fusion-tab-heading">{{ $package_tab->nombre }}</h4>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="tab-pane fade{{ $key === 0 ? ' in active':'' }}" id="package-{{ $package_tab->id }}">
                                                            {!! $package_tab->descripcion !!}
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="fusion-meta-info">
                            <div class="fusion-meta-info-wrapper">

                                <span class="vcard rich-snippet-hidden">
                                <span class="fn">
                                    <a href="{{ url('mayorista') }}" rel="author">Discover Mayorista</a></span>
                                </span>

                                <span class="updated rich-snippet-hidden">2018-01-28T21:49:07+00:00       </span>
                                <span>mayo 25, 2017</span><span class="fusion-inline-sep">|</span>
                            </div>
                        </div>




                        <div class="relacionado">
                            <div class="relacionado-head">
                                <h4>Otros Bloqueos Vigentes</h4>
                            </div>

                            <div class="relacionado-contenedor">
                                @foreach($bloqueos as $bloqueo)
                                    <div class="relacionado-item">
                                        <div class="relacionado-header">
                                            <a class="relacionado-titulo" href="{{ route('package', ['idPackage' => $bloqueo->id, 'urlPakage' => str_slug($bloqueo->nombre)]) }}">{{ $bloqueo->nombre }}</a>
                                            <div class="relacionado-dias">{{ $bloqueo->nro_dias }}D — {{ $bloqueo->nro_noches }}N</div>
                                            <div class="relacionado-precio">
                                                @if($bloqueo->precio_min==0.00 )
                                                    Consultar Precios

                                                @else
                                                        {{ $bloqueo->precio_min }}                                  
                                                @endif
                                            </div>
                                            <div class="relacionado-leer">
                                                <a href="{{ route('package', ['idPackage' => $bloqueo->id, 'urlPakage' => str_slug($bloqueo->nombre)]) }}">Leer más</a>
                                            </div>
                                        </div>
                                        <div class="relacionado-imagen">
                                            <img src="{{ url('files/packages/secundario/'.$bloqueo->imagen_2) }}" alt="{{ $bloqueo->nombre }}">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- related-posts -->
                    </article>
                </div>

            </div>
        <!-- fusion-row -->
        </div>
        <!-- #main -->
@stop

@section('script')
<script src="{{ url('js/slick.min.js') }}" charset="utf-8"></script>
<script type='text/javascript' src="{{ url('js/2b853892b45b6c7c62e273b1eba75fcb.js') }}"></script>
<script src="{{ url('v2/js/app.js') }}" charset="utf-8"></script>
<script type="text/javascript">
  var site_url = '{{ url('') }}/';
</script>
<?php if(Auth::check()): ?>
<script src="{{ url('js/quote-auth.js') }}" charset="utf-8"></script>
<?php else: ?>
<script src="{{ url('js/quote.js') }}" charset="utf-8"></script>
<?php endif; ?>
@stop
