@extends('frontend.layout.layout-mundo')

@section('title')
{{ $destination->nombre }}
@endsection

@section('description', 'Una ruta. Varios destinos. Disfruta de tus vacaciones, en los mejores lugares.')


@section('content')

<header class="fusion-header-wrapper">
	<div class="fusion-header-v2 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo-1 fusion-mobile-logo-1 fusion-mobile-menu-design-modern ">
		<div class="fusion-secondary-header">
			<div class="fusion-row">
				<div class="fusion-alignright">
					@include('frontend.partials.menu-top-header', ['destination' => $destination, 'destinations' => $destinations])
					<div class="fusion-mobile-nav-holder"></div>
				</div>
			</div>
		</div>
		<div class="fusion-header-sticky-height"></div>
		<div class="fusion-header">
		<div class="fusion-row">
		<div class="fusion-logo" data-margin-top="0px" data-margin-bottom="10px" data-margin-left="0px" data-margin-right="0px">
			<a class="fusion-logo-link" href="{{ route('home') }}">
				<img class='logo-custom-ja' src='' />
				<img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Logo" class="fusion-logo-1x fusion-standard-logo" />
				@if(Auth::check())
					<img class='logo-custom-ja' src="{{ asset('files/logos/'.Auth::user()->agency->logo)}}" style="height: 71px;margin-left: 20px;"/>
				@endif
				<img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Retina Logo" class="fusion-standard-logo fusion-logo-2x" />
				<!-- mobile logo -->
				<img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Mobile Logo" class="fusion-logo-1x fusion-mobile-logo-1x" />
				<img src="{{ url('images/Logo-Interno.png') }}" width="273" height="73" alt="Discover Mobile Retina Logo" class="fusion-logo-2x fusion-mobile-logo-2x" />
				<!-- sticky header logo -->
				<img src="{{ url('images/Logo-DM.png') }}" width="257" height="71" alt="Discover Sticky Logo" class="fusion-logo-1x fusion-sticky-logo-1x" />
				<img src="{{ url('images/Logo-DM.png') }}" width="257" height="71" alt="Discover Sticky Logo Retina" class="fusion-logo-2x fusion-sticky-logo-2x" />
			</a>
		</div>
		<nav class="fusion-main-menu" aria-label="Main Menu">
			<ul role="menubar" id="menu-menu-mundo" class="fusion-menu">
				<li role="menuitem"  id="menu-item-1048"  class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-36 current_page_item menu-item-1048"  >
					<a  href="{{ route('destination', ['id' => $destination->id, 'name' => str_slug($destination->nombre)]) }}">
						<span class="menu-text">{{ $destination->nombre }}</span>
					</a>
				</li>
				@foreach($package_types as $package_type)
					<li role="menuitem"  id="menu-item-{{ $package_type->id }}"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{{ $package_type->id }}"  >
						<a  href="{{ route('packageType', ['idD' => $destination->id, 'nameD' => str_slug($destination->nombre), 'idT' => $package_type->id, 'nameT' => str_slug($package_type->nombre)]) }}">
							<span class="menu-text">{{ $package_type->nombre }}</span>
						</a>
					</li>
				@endforeach
			</ul>
		</nav>
		<div class="fusion-mobile-menu-icons discover-header-interno">
			<a href="#" class="fusion-icon fusion-icon-bars" aria-label="Toggle mobile menu"></a>
		</div>
		<nav class="fusion-mobile-nav-holder"></nav>
		</div>
		</div>
	</div>
	<div class="fusion-clearfix"></div>
</header>

<div class="ds-sliders">
	@foreach($destination_banners as $destination_banner)
		<div class="ds-slider-item destinos" style="background-image: url({{ url('files/destination_banners/'.$destination_banner->imagen) }});">
			<div class="ds-slider-item--detalle">
				<div class="ds-slider-item--titulo">{{ $destination_banner->titulo }}</div>
				<div class="ds-slider-item--parrafo">{{ $destination_banner->subtitulo }}</div>
			</div>
		</div>
	@endforeach
</div>

<script>
jQuery(document).ready(function () {
	jQuery('.ds-sliders').slick({
		infinite: true,
		dots: false,
		arrows: true,
		autoplay: true,
		fade: true,
		autoplaySpeed: 9000,
		slidesToShow: 1,
		slidesToScroll: 1
	});
});
</script>

<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
	<div class="fusion-row" style="max-width:100%;">
		<div id="content" class="full-width">
			<div id="post-36" class="post-36 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">
Mundo		</span>
			<span class="vcard rich-snippet-hidden">
				<span class="fn">
					<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
				</span>
			</span>
			<span class="updated rich-snippet-hidden">
				2017-07-19T11:13:56+00:00
			</span>
				<div class="post-content">
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode hundred-percent-fullwidth fusion-equal-height-columns"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;margin-bottom: 40px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-spacing-no bloque-left font-script 1_2"  style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0 ) * 0.5 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="padding: 25px 40px 30px 40px;background-image: url('{{ url('images/2-banner-peru-r1-c1.png') }}');background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="{{ url('images/2-banner-peru-r1-c1.png') }}">
									<div class="fusion-title title fusion-sep-none fusion-title-size-two title-h2-alt" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-left">¡Sé parte de nuestras agencias asociadas!</h2>
									</div>
									<p>Llena nuestro formulario y únete a la familia DISCOVER MAYORISTA</p>
									<div class="fusion-button-wrapper">
										<style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{background: #01a13d;}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #01a13d;}.fusion-button.button-1{width:auto;}</style>
										<a class="fusion-button button-flat fusion-button-square button-large button-custom button-1" target="_self" title="Registrate" href="{{ url('registro-agencias') }}">
											<span class="fusion-button-text">Registrate</span>
										</a>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-spacing-no div-right font-roboto text-blanco 1_2"  style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0 ) * 0.5 ) );'>
								<div class="fusion-column-wrapper" style="padding: 25px 40px 30px 40px;background-image: url('{{ url('files/offers/'.$offer->imagen_bg) }}');background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="{{ url('2-banner-peru-r1-c2.png') }}">
									<div class="fusion-title title fusion-sep-none fusion-title-size-two" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-left">{{ $offer->titulo }}</h2>
									</div>
									<p>
										<sup>Desde</sup>
										<span class="usd-precio">${{ $offer->precio_min }}</span>
										 <!-- ó
										<span class="soles-precio">11,118</span> -->
									</p>
									<div class="fusion-button-wrapper">
										<style type="text/css" scoped="scoped">.fusion-button.button-2 .fusion-button-text, .fusion-button.button-2 i {color:#ffffff;}.fusion-button.button-2 {border-width:0px !important;border-color:#ffffff;}.fusion-button.button-2 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-2:hover .fusion-button-text, .fusion-button.button-2:hover i,.fusion-button.button-2:focus .fusion-button-text, .fusion-button.button-2:focus i,.fusion-button.button-2:active .fusion-button-text, .fusion-button.button-2:active{color:#ffffff;}.fusion-button.button-2:hover, .fusion-button.button-2:focus, .fusion-button.button-2:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-2{width:auto;}</style>
										<a class="fusion-button button-flat fusion-button-round button-large button-default button-2 boton-4" target="_self" title="Cotiza tu viaje" href="{{ $offer->url_accion }}">
											<span class="fusion-button-text">Cotiza tu viaje</span>
										</a>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>

          @foreach($package_types as $package_type)
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-spacing-no titulo-estilo-2 font-script 1_2 categoria-titulo"  style='margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="imageframe-align-center">
										<span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none">
											<img src="{{ url('images/flecha-d-direccion.png') }}" width="49" height="37" alt="" title="flecha-d-direccion" class="img-responsive wp-image-536"/>
										</span>
									</div>
									<div class="fusion-title title fusion-title-center fusion-title-size-two" style="margin-top:15px;margin-bottom:31px;">
										<div class="title-sep-container title-sep-container-left">
											<div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
										</div>
										<h2 class="title-heading-center">{{ $package_type->nombre }}</h2>
										<div class="title-sep-container title-sep-container-right">
											<div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
										</div>
									</div>
									<div class="fusion-button-wrapper fusion-aligncenter">
										<style type="text/css" scoped="scoped">.fusion-button.button-3 .fusion-button-text, .fusion-button.button-3 i {color:#ffffff;}.fusion-button.button-3 {border-width:0px;border-color:#ffffff;}.fusion-button.button-3 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-3:hover .fusion-button-text, .fusion-button.button-3:hover i,.fusion-button.button-3:focus .fusion-button-text, .fusion-button.button-3:focus i,.fusion-button.button-3:active .fusion-button-text, .fusion-button.button-3:active{color:#ffffff;}.fusion-button.button-3:hover, .fusion-button.button-3:focus, .fusion-button.button-3:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-3{width:auto;}</style>
										<a href="{{ route('packageType', ['idD' => $destination->id, 'nameD' => str_slug($destination->nombre), 'idT' => $package_type->id, 'nameT' => str_slug($package_type->nombre)]) }}" class="fusion-button button-flat fusion-button-round button-large button-default button-3 boton-5" target="_self" title="Ver mas paquetes">
											<span class="fusion-button-text">Ver mas paquetes</span>
										</a>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div id="Vacaciones_Div" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-large fusion-blog-no paquetes-carrusel">
										<div class="fusion-posts-container fusion-posts-container-no" data-pages="1">
                      @foreach($package_type->packages as $package)
											<article id="post-513" class="fusion-post-large post-513 post type-post status-publish format-standard has-post-thumbnail hentry category-vacaciones-peru categoria-id-8">
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">
																	<img width="275" height="420" src="{{ url('files/packages/'.$package->imagen_1) }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>
												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">{{ $package->nombre }}</a>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
											</article>
                      @endforeach
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
          @endforeach

					<script>
						jQuery.noConflict();
						jQuery( document ).ready(function( $ ) {
							$('.paquetes-carrusel > .fusion-posts-container').bxSlider({
								slideWidth: 275,
								minSlides: 1,
								maxSlides: 4,
								moveSlides: 1,
								slideMargin: 10,
								responsive: true,
								controls: true,
								infiniteLoop: false,
								useCSS: true,
								nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
								prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
								pager: false,
							});
						});
					</script>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

@stop


@section('style')
<link rel="stylesheet" href="{{ asset('css/slick.css')}}" />
<link rel="stylesheet" href="{{ asset('css/slick-theme.css')}}" />
@stop


@section('script')
<script src="{{ url('js/slick.min.js') }}" charset="utf-8"></script>
<script type="text/javascript">
	function revslider_showDoubleJqueryError(sliderID) {
		var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
		errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
		errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option: <strong> <b>Put JS Includes To Body</b> </strong> option to true.";
		errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it."; errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
		jQuery(sliderID).show().html(errorMessage);
	}
</script>

<script type='text/javascript' src="{{ url('js/mundo.contact.scripts.js') }}"></script>
<!--[if IE 9]>
<script type='text/javascript' src='{{ url('fusion-ie9.js') }}'></script>
<![endif]-->
<script type='text/javascript' src="{{ url('js/mundo.comment-reply.min.js') }}"></script>
<script type='text/javascript' src="{{ url('js/7fe5606794b62066a68cdda469435491.js') }}"></script>
<script type='text/javascript' src="{{ url('js/wp-embed.min.js') }}"></script>
<script type='text/javascript' src="{{ url('js/jquery.language.validationEngine-es.js') }}"></script>
<script type='text/javascript' src="{{ url('js/jquery.validationEngine.js') }}"></script>
<script type='text/javascript'>
/*
<![CDATA[ */
var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"admin-ajax.php","loadingTrans":"Cargando...","is_rtl":""};
/* ]]> */

</script>
<script type='text/javascript' src="{{ url('js/front-subscribers.js') }}"></script>

@stop
