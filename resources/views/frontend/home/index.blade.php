@extends('frontend.layout.layout_home')

@section('title', 'DISCOVER - Mayorista de Turismo en Perú')
@section('description', 'Una ruta. Varios destinos. Disfruta de tus vacaciones, en los mejores lugares.')
@section('content')

@section('style')
<link rel="stylesheet" href="{{ asset('v2/css/app.css')}}" />
@stop
<script src="{{url('/assets/js/moment.js')}}" type="text/javascript">
</script>
<script src="{{url('/assets/js/cargarlistcombo.js')}}" type="text/javascript">
</script>
@include('frontend.partials.header')
<!-- Latest compiled and minified CSS -->

<script type="text/javascript">
    $(document).ready(function(){

     fun_fecha_salidas_nrodias();
  });
</script>

<script type="text/javascript">
$(document).ready(function()
{
  $('#form_search').on('submit', function(e) {
    e.preventDefault();

    var packages = $('#listpackages').val();
    var mes      = $('#fecha_salida').val();
    var nro_dias = $('#cant_dias').val();
    const site_url = '{{ url('/') }}/';
    $.ajax({
        type: "GET",
        url: 'resultado-filtro',
        data: $(this).serialize(),
        success: function(msg) {
          if(msg == "NOHAY"){
            toastr["error"]("!!Lo Sentimos!!", "No se encontraron resultados")
          } else {

            toastr["success"]("!!Excelente!!", "Se encontraron resultados")
            window.location.href = site_url + 'resultado-filtro?listpackages='+packages+"&fecha_salida="+mes+"&cant_dias="+nro_dias;
          }

        }
    });

});

  if($('input[name=destinations]').is(':checked')) {
    var valor= $("input[name=destinations]:checked").prop("value");
    console.log(valor);
    if(valor){
      $('#listpackages').empty();
      $.ajax({
          async: false,
          type: 'GET',
          url: '{{route('allpackage')}}',
          data: {},
          dataType: 'json',
          success: function(data) {
              $('#listpackages').append('<option value="">::Destino::</option>');
              $.each(data, function(i, value) {
                  $('#listpackages').append('<option value="' + value.nombre + '">' + value.nombre + '</option>');
              });
          },
          error: function(jqXHR, status, err) {
              alert("Local error callback.");
          }
        });
      }else {
          $('#listpackages').empty();
          $('#listpackages').append('<option value="">::Fecha::</option>');
      }
   }

  $("input[name=destinations]").click(function () {
    var $destino =  $(this).val();
  //  console.log($destino);
   if ($destino == 1 || $destino == 2) {
     console.log("entro 1");
   $('#listpackages').empty();
   $.ajax({
       async: false,
       type: 'GET',
       url: 'packages/' + $destino,
       data: {},
       dataType: 'json',
       success: function(data) {
           $('#listpackages').append('<option value="">::Seleccionar Destino::</option>');
           $.each(data, function(i, value) {
               $('#listpackages').append('<option value="' + value.nombre + '">' + value.nombre + '</option>');
           });
       },
       error: function(jqXHR, status, err) {
           alert("Local error callback.");
       }
     });
   }else if($destino == "todos"){
     $('#listpackages').empty();
     $.ajax({
         async: false,
         type: 'GET',
         url: '{{route('allpackage')}}',
         data: {},
         dataType: 'json',
         success: function(data) {
             $('#listpackages').append('<option value="">::Seleccionar Destino::</option>');
             $.each(data, function(i, value) {
                 $('#listpackages').append('<option value="' + value.nombre + '">' + value.nombre + '</option>');
             });
         },
         error: function(jqXHR, status, err) {
             alert("Local error callback.");
         }
       });
     console.log("Entro todos");
   }
   else {
       $('#listpackages').empty();
       $('#listpackages').append('<option value="">::Seleccionar Destino::</option>');
   }

  });

});
</script>
<script type="text/javascript">
   $(document).ready(function() {
   $('#listpackages').select2();
  // $('.selectpicker').selectpicker();

});
</script>
<div id="sliders-container">
</div>

<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
  <div class="fusion-row" style="max-width:100%;">
    <div id="content" class="full-width">
      <div id="post-2" class="post-2 page type-page status-publish hentry">

        <span class="entry-title rich-snippet-hidden">Inicio</span>
        <span class="vcard rich-snippet-hidden">
          <span class="fn">
            <a href="{{ url ('home') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
          </span>
        </span>

        <span class="updated rich-snippet-hidden">2017-07-21T17:22:37+00:00</span>

        <div class="post-content">

          <div class="home-slider fusion-fullwidth">

            @foreach($sliders as $slider)
              <div class="fullwidth-box fusion-blend-mode fusion-parallax-none main-banner-home hundred-percent-fullwidth fusion-equal-height-columns" style="background-color: rgba(255,255,255,0);background-image: url('{{ url('files/sliders/'.$slider->imagen) }}'); background-position: center top;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;">
                <div class="fusion-builder-row fusion-row ">
                  <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:20px;'>
                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                      <script>
                        // <![CDATA[
                        jQuery(document).ready(function(cash) {
                          var alto_total = jQuery(window).height() - 100;

                          //var usado = jQuery("#footer").height() + jQuery(".header-wrapper").height() + 35;
                          // var nuevo_alto = alto_total - usado;

                          jQuery('.main-banner-home').css('min-height', alto_total);
                        });
                        // ]]&gt;
                      </script>

                      <div class="fusion-sep-clear"></div>
                      <div class="fusion-separator fusion-full-width-sep sep-none" style="margin-left: auto;margin-right: auto;margin-top:100px;margin-bottom:100px;width:100%;max-width:100%;"></div>
                      <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-spacing-no fusion-one-fifth fusion-spacing-no 1_5" style='margin-top: 0px;margin-bottom: 20px;width:100%;margin-right:0px;'>
                          <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                            <div class="imageframe-align-center">
                              <span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none">
                                <img src="{{ url('images/icono-mundo-home.png') }}" width="85" height="92" alt="" title="Mundo" class="img-responsive wp-image-42"/>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="text-banner-home">
                        @if($slider->url === '#')
                          <h3>{{ $slider->texto_1 }}</h3>
                          <h2>{{ $slider->texto_2 }}</h2>
                          <h4>{{ $slider->texto_3 }}</h4>

                        @else
                          <a href="{{ $slider->url }}">
                            <h3>{{ $slider->texto_1 }}</h3>
                            <h2>{{ $slider->texto_2 }}</h2>
                            <h4>{{ $slider->texto_3 }} <a href="{{$slider->url}}">(Leer mas)</a></h4>

                          </a>
                        @endif
                      </div>
                      <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-spacing-no fusion-one-third fusion-spacing-no 1_3" style='margin-top: 0px;margin-bottom: 20px;width:100%;margin-right:0px;'>
                          <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                            <ul class="fusion-checklist fusion-checklist-1 link-mp-home destination-home" style="font-size:30px;line-height:51px;">
                              @foreach($destinations as $destination)
                              <li class="fusion-li-item">
                                <span style="height:51px;width:51px;margin-right:21px;" class="icon-wrapper circle-no">
                                  <i class="fusion-li-icon fa fa-map-marker" style="color:#ffffff;"></i>
                                </span>

                                <div class="fusion-li-item-content" style="margin-left:72px;">
                                  <a href="{{ route('destination', ['id' => $destination->id, 'name' => str_slug($destination->nombre)]) }}">{{ $destination->nombre }}</a>
                                </div>
                              </li>
                              @endforeach
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="fusion-clearfix"></div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>

          @include('frontend.home.partials.buscador')

          <div class="fusion-fullwidth fullwidth-box fusion-blend-mode hundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;'>
            <div class="fusion-builder-row fusion-row ">
              <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:0px;'>
                <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                  <div class="fusion-tabs fusion-tabs-1 classic nav-not-justified forms-reservas-paquetes horizontal-tabs">
                    <style type="text/css">
                      .fusion-tabs.fusion-tabs-1 .nav-tabs li a {
                        border-top-color: #ebeaea;
                        background-color: #ebeaea;
                      }

                      .fusion-tabs.fusion-tabs-1 .nav-tabs {
                        background-color: #ffffff;
                      }

                      .fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,
                      .fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,
                      .fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus {
                        border-right-color: #ffffff;
                      }

                      .fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,
                      .fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,
                      .fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus {
                        background-color: #ffffff;
                      }

                      .fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover {
                        background-color: #ffffff;
                        border-top-color: #ffffff;
                      }

                      .fusion-tabs.fusion-tabs-1 .tab-pane {
                        background-color: #ffffff;
                      }

                      .fusion-tabs.fusion-tabs-1 .nav,
                      .fusion-tabs.fusion-tabs-1 .nav-tabs,
                      .fusion-tabs.fusion-tabs-1 .tab-content .tab-pane {
                        border-color: #ebeaea;
                      }
                    </style>
                    <div class="nav">
                      <ul class="nav-tabs">
                        <li class="active">
                          <a class="tab-link" data-toggle="tab" id="fusion-tab-reservas" href="#tab-7d995e545d2e06bbc5c">
                            <h4 class="fusion-tab-heading"><i class="fa fontawesome-icon fa-globe"></i>Bloqueo</h4>
                          </a>
                        </li>

                        <li>
                          <a class="tab-link" data-toggle="tab" id="fusion-tab-paquetes" href="#tab-7a1c2ce580833ebafb0">
                            <h4 class="fusion-tab-heading"><i class="fa fontawesome-icon fa-globe"></i>Vacaciones</h4>
                          </a>
                        </li>
                      </ul>
                    </div>

                    <div class="tab-content">
                      <div class="nav fusion-mobile-tab-nav">
                        <ul class="nav-tabs">
                          <li class="active">
                            <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-reservas" href="#tab-7d995e545d2e06bbc5c">
                              <h4 class="fusion-tab-heading"><i class="fa fontawesome-icon fa-globe"></i>Bloqueo</h4>
                            </a>
                          </li>
                        </ul>
                      </div>

                      <div class="tab-pane fade in active" id="tab-7d995e545d2e06bbc5c">
                          <div class="lista">
                            @if($locks)
                              @foreach($locks as $lock)
                                <div class="item">
                                  <div class="imagen">
                                    <a href="{{ route('package', ['idPackage' => $lock->id, 'urlPakage' => str_slug($lock->nombre)]) }}">
                                      <img src="{{ url('files/packages/'.$lock->imagen_1) }}" alt="">
                                    </a>
                                  </div>
                                  <div class="titulo">
                                    <h4>
                                      <a href="{{ route('package', ['idPackage' => $lock->id, 'urlPakage' => str_slug($lock->nombre)]) }}">
                                        {{ $lock->nombre }}
                                      </a>
                                    </h4>
                                  </div>
                                </div>
                              @endforeach
                            @endif
                          </div>
                      </div>

                      <div class="nav fusion-mobile-tab-nav">
                        <ul class="nav-tabs">
                          <li>
                            <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-paquetes" href="#tab-7a1c2ce580833ebafb0">
                              <h4 class="fusion-tab-heading"><i class="fa fontawesome-icon fa-globe"></i>Vacaciones</h4>
                            </a>
                          </li>
                        </ul>
                      </div>

                      <div class="tab-pane fade" id="tab-7a1c2ce580833ebafb0">
                        <div class="lista">
                          @if($holidays)
                            @foreach($holidays as $holiday)
                              <div class="item">
                                <!-- <pre>{{ dump($holiday) }}</pre> -->
                                <div class="imagen">
                                  <a href="{{ route('package', ['idPackage' => $holiday->id, 'urlPakage' => str_slug($holiday->nombre)]) }}">
                                    <img src="{{ url('files/packages/'.$holiday->imagen_1) }}" alt="">
                                  </a>
                                </div>
                                <div class="titulo">
                                  <h4>
                                    <a href="{{ route('package', ['idPackage' => $holiday->id, 'urlPakage' => str_slug($holiday->nombre)]) }}">
                                      {{ $holiday->nombre }}
                                    </a>
                                  </h4>
                                </div>
                              </div>
                            @endforeach
                          @endif
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="fusion-clearfix"></div>

                </div>
              </div>
            </div>
          </div>
          <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:35px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
            <div class="fusion-builder-row fusion-row ">
              <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );margin-right: 0px;'>
                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">

                  <div class="fusion-clearfix"></div>

                </div>
              </div>

              <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-spacing-no titulo-estilo-2 font-script 1_2" style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                  <div class="imageframe-align-center">
                    <span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none">
                      <img src="{{ url('images/flecha-d-direccion.png') }}" width="49" height="37" alt="" title="flecha-d-direccion" class="img-responsive wp-image-536"/>
                    </span>
                  </div>

                  <div
                  class="fusion-title title fusion-title-center fusion-title-size-two" style="margin-top:15px;margin-bottom:31px;">
                    <div class="title-sep-container title-sep-container-left">
                      <div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
                    </div>
                    <h2 class="title-heading-center">Paquetes Internacionales</h2>
                    <div class="title-sep-container title-sep-container-right">
                      <div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
                    </div>
                </div>
                <div class="fusion-button-wrapper fusion-aligncenter">
                  <style type="text/css" scoped="scoped">
                    .fusion-button.button-1 .fusion-button-text,
                    .fusion-button.button-1 i {
                      color: #ffffff;
                    }

                    .fusion-button.button-1 {
                      border-width: 0px;
                      border-color: #ffffff;
                    }

                    .fusion-button.button-1 .fusion-button-icon-divider {
                      border-color: #ffffff;
                    }

                    .fusion-button.button-1:hover .fusion-button-text,
                    .fusion-button.button-1:hover i,
                    .fusion-button.button-1:focus .fusion-button-text,
                    .fusion-button.button-1:focus i,
                    .fusion-button.button-1:active .fusion-button-text,
                    .fusion-button.button-1:active {
                      color: #ffffff;
                    }

                    .fusion-button.button-1:hover,
                    .fusion-button.button-1:focus,
                    .fusion-button.button-1:active {
                      border-width: 0px;
                      border-color: #ffffff;
                    }

                    .fusion-button.button-1:hover .fusion-button-icon-divider,
                    .fusion-button.button-1:hover .fusion-button-icon-divider,
                    .fusion-button.button-1:active .fusion-button-icon-divider {
                      border-color: #ffffff;
                    }

                    .fusion-button.button-1 {
                      width: auto;
                    }
                  </style>
                  <a href="{{ route('destination', ['id' => $destinations[0]->id, 'name' => str_slug($destinations[0]->nombre)]) }}" class="fusion-button button-flat fusion-button-round button-large button-default button-1 boton-5" target="_self" title="Ver mas paquetes">
                    <span class="fusion-button-text">Ver mas paquetes</span>
                  </a>
                </div>

                <div class="fusion-clearfix"></div>

            </div>
          </div>

          <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );'>
            <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">

              <div class="fusion-clearfix"></div>

            </div>
          </div>
        </div>
      </div>

      <div id="Vacaciones_Div" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
        <div class="fusion-builder-row fusion-row ">
          <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:20px;'>
            <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
              <div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-large fusion-blog-no paquetes-carrusel">
                <div class="fusion-posts-container fusion-posts-container-no" data-pages="1">
                  @if($international_packages)
                    @foreach($international_packages as $intPackage)
                      <article id="post-556" class="fusion-post-large post-556 post type-post status-publish format-standard has-post-thumbnail hentry category-tematicas-peru categoria-id-9">
                        <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                          <ul class="slides">
                            <li>
                              <div class="fusion-image-wrapper" aria-haspopup="true">
                                <a href="{{ route('package', ['idPackage' => $intPackage->id, 'urlPakage' => str_slug($intPackage->nombre)]) }}">
                                  <img width="797" height="531" src="{{ url('files/packages/'.$intPackage->imagen_1) }}" class="attachment-full size-full wp-post-image" alt="" srcset=""
                                  sizes="(max-width: 800px) 100vw, 1300px" />
                                </a>
                              </div>
                            </li>
                          </ul>
                        </div>
                        <div class="fusion-post-content post-content">
                          <h2 class="blog-shortcode-post-title entry-title">
                            <a href="{{ route('package', ['idPackage' => $intPackage->id, 'urlPakage' => str_slug($intPackage->nombre)]) }}">{{ $intPackage->nombre }}</a>
                          </h2>
                        </div>
                        <div class="fusion-clearfix"></div>
                      </article>
                    @endforeach
                  @endif
                </div>
              </div>

              <div class="fusion-clearfix"></div>
            </div>
          </div>
        </div>
      </div>

      @if(count($national_packages) > 0)
      <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
        <div class="fusion-builder-row fusion-row ">
          <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );margin-right: 0px;'>
            <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
              <div class="fusion-clearfix"></div>
            </div>
          </div>

          <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-spacing-no titulo-estilo-2 font-script 1_2" style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
            <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
              <div class="imageframe-align-center">
                <span class="fusion-imageframe imageframe-none imageframe-10 hover-type-none">
                  <img src="{{ url('images/flecha-d-direccion.png') }}" width="49" height="37" alt="" title="flecha-d-direccion" class="img-responsive wp-image-536"/>
                </span>
              </div>

              <div
              class="fusion-title title fusion-title-center fusion-title-size-two" style="margin-top:15px;margin-bottom:31px;">
                <div class="title-sep-container title-sep-container-left">
                  <div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
                </div>
                <h2 class="title-heading-center">Paquetes Nacionales</h2>
                <div class="title-sep-container title-sep-container-right">
                  <div class="title-sep sep-single sep-solid" style="border-color:#059ddc;"></div>
                </div>
              </div>
            <div class="fusion-button-wrapper fusion-aligncenter">
              <style type="text/css" scoped="scoped">
                .fusion-button.button-9 .fusion-button-text,
                .fusion-button.button-9 i {
                  color: #ffffff;
                }

                .fusion-button.button-9 {
                  border-width: 0px;
                  border-color: #ffffff;
                }

                .fusion-button.button-9 .fusion-button-icon-divider {
                  border-color: #ffffff;
                }

                .fusion-button.button-9:hover .fusion-button-text,
                .fusion-button.button-9:hover i,
                .fusion-button.button-9:focus .fusion-button-text,
                .fusion-button.button-9:focus i,
                .fusion-button.button-9:active .fusion-button-text,
                .fusion-button.button-9:active {
                  color: #ffffff;
                }

                .fusion-button.button-9:hover,
                .fusion-button.button-9:focus,
                .fusion-button.button-9:active {
                  border-width: 0px;
                  border-color: #ffffff;
                }

                .fusion-button.button-9:hover .fusion-button-icon-divider,
                .fusion-button.button-9:hover .fusion-button-icon-divider,
                .fusion-button.button-9:active .fusion-button-icon-divider {
                  border-color: #ffffff;
                }

                .fusion-button.button-9 {
                  width: auto;
                }
              </style>
              <a href="{{ route('destination', ['id' => $destinations[1]->id, 'name' => str_slug($destinations[1]->nombre)]) }}" class="fusion-button button-flat fusion-button-round button-large button-default button-9 boton-5" target="_self" title="Ver mas paquetes" >
                <span class="fusion-button-text">Ver mas paquetes</span>
              </a>
            </div>
            <div
            class="fusion-clearfix"></div>
          </div>
        </div>

        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );'>
          <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
            <div class="fusion-clearfix"></div>
          </div>
        </div>
      </div>
    </div>

    <div id="Vacaciones_Div" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
      <div class="fusion-builder-row fusion-row ">
        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:20px;'>
          <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
            <div class="fusion-blog-shortcode fusion-blog-shortcode-2 fusion-blog-archive fusion-blog-layout-large fusion-blog-no paquetes-carrusel">
              <div class="fusion-posts-container fusion-posts-container-no" data-pages="1">
                  @foreach($national_packages as $natPackage)
                    <article id="post-{{ $natPackage->id }}" class="fusion-post-large post-{{ $natPackage->id }} post type-post status-publish format-standard has-post-thumbnail hentry category-familiar-vacaciones-mundo category-vacaciones-mundo categoria-id-20 categoria-id-13">
                      <style type="text/css"></style>
                      <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                        <ul class="slides">
                          <li>
                            <div class="fusion-image-wrapper" aria-haspopup="true">
                              <a href="{{ route('package', ['idPackage' => $natPackage->id, 'urlPakage' => str_slug($natPackage->nombre)]) }}">
                                <img width="797" height="531" src="{{ url('files/packages/'.$natPackage->imagen_1) }}" class="attachment-full size-full wp-post-image" alt="" srcset=""
                              sizes="(max-width: 800px) 100vw, 1300px" />
                              </a>
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div class="fusion-post-content post-content">
                        <h2 class="blog-shortcode-post-title entry-title">
                          <a href="{{ route('package', ['idPackage' => $natPackage->id, 'urlPakage' => str_slug($natPackage->nombre)]) }}">
                            {{ $natPackage->nombre }}
                          </a>
                        </h2>
                      </div>
                      <div class="fusion-clearfix"></div>
                    </article>
                  @endforeach
              </div>
            </div>

            <div class="fusion-clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    @endif

    <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
    <div class="fusion-builder-row fusion-row ">
      <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:20px;'>
        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
          <script>
            jQuery.noConflict();

            jQuery(document).ready(function($) {


              $('.paquetes-carrusel > .fusion-posts-container').bxSlider({
                slideWidth: 275,
                minSlides: 1,
                maxSlides: 4,
                moveSlides: 1,
                slideMargin: 10,
                responsive: true,
                controls: true,
                infiniteLoop: false,
                useCSS: true,
                nextText: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                pager: false,
              });


            });
          </script>
          <div class="fusion-clearfix"></div>

        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('script')
<script src="{{ url('js/home.js')}}"></script>
@stop
