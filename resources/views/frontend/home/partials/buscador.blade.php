<style>

.forms-reservas-paquetes {
    margin-top: -157px;
    position: relative;
    z-index: 99;
    margin-bottom: 0;
}

.radioo{
  color:#fff;
  margin-bottom: -1px;
  font-size:20px;
}
.styleicon{
      background-color: #ffffff;
}
.select2-container--default .select2-selection--single{
  height: 38px;
  padding: 5px;
  font: 100% Arial, Helvetica, sans-serif;
  width: 100%;
}
.search{
  width: auto;
  position: relative;
  bottom: 260px;
  padding: 15px;
}
.select2-container {
  box-sizing: border-box;
  display: inline-block;
  margin: 0px;
  position: relative;
  vertical-align: middle;
  width: 65px;
  flex: 1 1 auto;
}

@media (max-width: 576px) {
  .imageframe-align-center {
    text-align: center;
    position: relative;
    bottom: 58px;
    width: 10%;
    opacity: 0;
}
.btn-primary {
  width: 100%;
}
.btn_destino{
  width: 100%;
}
.text-banner-home{
  position: relative;
    bottom: 120px;
}
.destination-home{
    opacity: 0;
}

  .search{
    background-color: rgba(0, 120, 184, .85);
    padding: 10px;
    opacity: 1;
    position: relative;
    bottom: 245px;
  }
  .forms-reservas-paquetes{
    margin-top: -242px;
  }
}



  @media (min-width: 576px) {
    .search{
        background-color: rgba(0, 120, 184, .85);
    }
    .select2-container {
      box-sizing: border-box;
      display: inline-block;
      margin: 0px;
      position: relative;
      vertical-align: middle;
      width: 65px;
      flex: 1 1 auto;
    }
    .select2-container .select2-selection--single .select2-selection__rendered {
      padding-right: 0;
    }
  }

 /* Medium devices (tablets, 768px and up) */
@media (min-width: 768px) {
  .select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0px;
    position: relative;
    vertical-align: middle;
    width: 65px;
    flex: 1 1 auto;
  }
}

 /* Large devices (desktops, 992px and up) */
@media (min-width: 992px) {
 .search{
    width: auto;
    position: relative;
    bottom: 260px;
  }

}

 /* Extra large devices (large desktops, 1200px and up) */
@media (min-width: 1200px) {
  .search{
    width: auto;
    position: relative;
    bottom: 260px;
  }
  .select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0px;
    position: relative;
    vertical-align: middle;
    width: 65px;
    flex: 1 1 auto;
  }
  @media (max-height:820px){
    .search{
      background-color: rgba(0, 120, 184, .85);
      padding: 15px;
      opacity: 1;
      position: relative;
      bottom: 210px;
    }
    .destination-home{
      margin-top: 135px !important;
    }
  }
  @media (max-height:768px){
    .search{
      background-color: rgba(0, 120, 184, .85);
      padding: 15px;
      opacity: 1;
      position: relative;
      bottom: 210px;
    }
    .destination-home{
      margin-top: 135px !important;
    }
  }
}

</style>
<div class="container search">
<form  id="form_search" action="{{route('filtrospackage')}}" method="GET">
   <div class="radioo">
      <div class="form-check form-check-inline">
      <input class="form-check-input" type="radio" name="destinations" id="inlineRadio1" value="todos" checked>
      <label class="form-check-label" for="inlineRadio1">Todos</label>
      </div>
       @foreach($destinations as $destination)
      <div class="form-check form-check-inline">
      <input class="form-check-input" type="radio" name="destinations" id="inlineRadio2" value="{{$destination->id}}">
      <label class="form-check-label" for="inlineRadio2">{{ $destination->nombre }}</label>
      </div>
       @endforeach
  </div>
  <div class="form-row align-items-center">
    <div class="col-sm-3 my-1 styledestino">
      <label class="sr-only" for="inlineFormInputGroupUsername">DESTINO</label>
      <div class="persoseach">
      <div class="input-group stylebuscador">
        <div class="input-group-prepend">
          <div class="input-group-text styleicon">
              <img src="location.png"></img>
          </div>
        </div>
        <select class="form-control" id="listpackages" name="listpackages"  onchange="fun_fecha_salidas_nrodias()">
        </select>
        </div>
      </div>
    </div>
    <div class="col-sm-3 my-1">
      <label class="sr-only" for="inlineFormInputGroupUsername">FECHA</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text styleicon">
            <img src="date.png"></img>
          </div>
        </div>
        <select class="form-control" id="fecha_salida" name="fecha_salida">
       </select>
      </div>
    </div>
    <div class="col-sm-3 my-1">
      <label class="sr-only" for="inlineFormInputGroupUsername">FECHA</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text styleicon">
            <img src="date.png"></img>
          </div>
        </div>
        <select class="form-control" id="cant_dias" name="cant_dias">
       </select>
      </div>
    </div>

    <div class="col-auto my-1 btn_destino">
      <button type="submit" class="btn btn-primary">BUSCAR DESTINO</button>
    </div>

  </div>
</form>
</div>
