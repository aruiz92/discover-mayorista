<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <meta name="description" content="@yield('description')" />
        <link rel="icon" href="{{ url('images/Favicon.png') }}" sizes="32x32" />
      	<link rel="icon" href="{{ url('images/Favicon.png') }}" sizes="192x192" />
      	<link rel="apple-touch-icon-precomposed" href="{{ url('images/Favicon.png') }}" />
      	<meta name="msapplication-TileImage" content="{{ url('images/Favicon.png') }}" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oleo+Script:400,700" rel="stylesheet">

        {{-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> --}}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <link rel="stylesheet" href="{{ asset('css/vendor/validationEngine.jquery.css')}}" />
        <link rel="stylesheet" href="{{ asset('css/styles.css')}}" />
        <link rel="stylesheet" href="{{ asset('css/avada.css')}}" />
        <link rel="stylesheet" href="{{ asset('css/fusion.css')}}" />
        <link rel="stylesheet" href="{{ asset('css/vendor/jquery.bxslider.min.css')}}" />
        <link rel="stylesheet" href="{{ asset('css/custom.css')}}" />
        <link rel="stylesheet" href="{{ asset('css/slick.css')}}" />
        <link rel="stylesheet" href="{{ asset('css/slick-theme.css')}}" />
        <link rel='stylesheet' id='general-css'  href="{{ url('css/general.css') }}" type='text/css' media='all' />
        <link rel="stylesheet" href="{{ asset('css/discover.css') }}">
        @include('frontend.partials.scripts')

        @yield('style')
        <script type="text/javascript">
            var URL_SITE='{{ url('')}}';
        </script>
      </head>
      <body class="home page-template page-template-100-width page-template-100-width-php page page-id-2 top-parent-2 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text">
  				<div id="wrapper" class="">

            @yield('content')
            @include('frontend.partials.footer')

         </div>


        @yield('script')
        @yield('css')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119548675-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-119548675-1');
    </script>
    </body>
</html>
