<!DOCTYPE html>
<html class="" lang="es-ES" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <meta name='robots' content='noindex,follow' />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <meta property="og:image" content="{{ asset('images/Logo-Interno.png') }}" />

    <link rel="stylesheet" href="{{ asset('css/vendor/validationEngine.jquery.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/contact-form-styles.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/page-list.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/settings.css')}}" />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {}
    </style>
    <link rel="stylesheet" href="{{ asset('css/avada.css')}}" />

    <!--[if lte IE 9]>
    <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='{{ asset('css/font-awesome.css')}}' type='text/css' media='all' />
    <![endif]-->
        <!--[if IE]>
    <link rel='stylesheet' id='avada-IE-css'  href='{{ asset('css/ie.css') }}' type='text/css' media='all' />
    <![endif]-->

    <link rel="stylesheet" href="{{ asset('css/upw-theme-standard.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/fusion-563.css')}}" />
    <link rel='stylesheet' id='avada_google_fonts-css' href='https://fonts.googleapis.com/css?family=PT+Sans%3A400%7CAntic+Slab%3A400&#038;subset=latin' type='text/css' media='all' />
    <link rel="stylesheet" href="{{ asset('css/hm_custom_js-custom.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/wp-members.css')}}" />
    <link rel='stylesheet' id='general-css'  href="{{ url('css/general.css') }}" type='text/css' media='all' />
    <link rel="stylesheet" href="{{ asset('css/discover.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/css/app.css') }}">
    @yield('style')

    <script src="{{ url('js/vendor/jquery.min.js')}}"></script>
    <script src="{{ url('js/jquery-migrate.min.js')}}"></script>
    <script src="{{ url('js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{ url('js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{ url('js/custom-mundo.js')}}"></script>

    <style type="text/css">
        .recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}
    </style>

    <script type="text/javascript">
        function setREVStartSize(e){
            document.addEventListener("DOMContentLoaded", function() {
                try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                    if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
                }catch(d){console.log("Failure at Presize of Slider:"+d)}
            });
        };
    </script>

    <script type="text/javascript">
        var doc = document.documentElement;
                doc.setAttribute('data-useragent', navigator.userAgent);
    </script>

    <script src="{{ url('js/enscroll-0.6.2.min.js')}}"></script>

</head>

<body class="ds-nosotros page-template page-template-100-width page-template-100-width-php page page-id-563 page-child parent-pageid-601 top-parent-38 fusion-image-hovers seccion-peru pag-vacaciones fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text discover-header-interno">
    <div id="wrapper" class="">
        <div id="home" style="position:relative;top:1px;"></div>

            @yield('content')

            @include('frontend.partials.footer')

        </div>
    </body>
    <a class="fusion-one-page-text-link fusion-page-load-link"></a>

    @yield('script')
    @yield('css')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119548675-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119548675-1');
</script>
</body>
</html>
