<!DOCTYPE html>
<html class="" lang="es-ES" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title')</title>
	<meta name='robots' content='noindex,follow' />
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	<meta property="og:title" content="Cancun"/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="{{ url('bloqueo') }}"/>
	<meta property="og:site_name" content="Discover"/>
	<meta property="og:description" content=""/>
	<meta property="og:image" content="{{ url('images/cancun.jpg') }}"/>

	<link rel="stylesheet" href="{{ asset('css/vendor/validationEngine.jquery.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/contact-form-styles.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/page-list.css')}}" />
    <link rel="stylesheet" href="{{ asset('css/settings.css')}}" />
	<link rel="stylesheet" href="{{ asset('css/style.min.css')}}" />

	<!--[if lte IE 9]>
    <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='{{ asset('css/font-awesome.css')}}' type='text/css' media='all' />
    <![endif]-->
        <!--[if IE]>z
    <link rel='stylesheet' id='avada-IE-css'  href='{{ asset('css/ie.css') }}' type='text/css' media='all' />
    <![endif]-->

	<link rel="stylesheet" href="{{ asset('css/upw-theme-standard.min.css')}}" />
	@yield('fusion-style')
	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Oleo+Script:400,700" rel="stylesheet">
	@yield('custom-style')

    <link rel="stylesheet" href="{{ asset('css/wp-members.css')}}" />
		<link rel='stylesheet' id='general-css'  href="{{ url('css/general.css') }}" type='text/css' media='all' />
		<link rel="stylesheet" href="{{ asset('css/discover.css') }}">

	<script src="{{ url('js/jquery.1.12.4.js')}}"></script>
    <script src="{{ url('js/jquery-migrate.min.js')}}"></script>
    <script src="{{ url('js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{ url('js/jquery.themepunch.revolution.min.js')}}"></script>
    @yield('custom-script')

	<style type="text/css">
		.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}
	</style>

	<link rel="icon" href="{{ url('images/Favicon.png') }}" sizes="32x32" />
	<link rel="icon" href="{{ url('images/Favicon.png') }}" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="{{ url('images/Favicon.png') }}" />
	<meta name="msapplication-TileImage" content="{{ url('images/Favicon.png') }}" />

	@yield('style')

	<script type="text/javascript">function setREVStartSize(e){
		document.addEventListener("DOMContentLoaded", function() {
			try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
				if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
			}catch(d){console.log("Failure at Presize of Slider:"+d)}
			});
		};
	</script>
	<script type="text/javascript">
		var doc = document.documentElement;
		doc.setAttribute('data-useragent', navigator.userAgent);
	</script>
	<script src="{{ url('js/enscroll-0.6.2.min.js') }}"></script>
</head>

<body class="@yield('body-style')">
	<div id="wrapper" class="">
		<div id="home" style="position:relative;top:1px;"></div>


		@yield('content')


		@include('frontend.partials.footer')

	</div>
	<!-- wrapper -->
	<a class="fusion-one-page-text-link fusion-page-load-link"></a>



	<!-- scripts -->
	<script type="text/javascript">
		function revslider_showDoubleJqueryError(sliderID) {
			var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
			errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
			errorMessage += "<br><br> To fix it you can: <br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option: <strong><b>Put JS Includes To Body</b></strong> option to true.";
			errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
			jQuery(sliderID).show().html(errorMessage);
		}

	</script>
	<script type='text/javascript' src="{{ url('js/contact-script.js') }}"></script>
	<!--[if IE 9]>
		<script type='text/javascript' src='{{ url('js/fusion-ie9.js') }}'></script>
	<![endif]-->
	<script type='text/javascript' src="{{ url('js/comment-reply.min.js') }}"></script>
	<script type='text/javascript' src="{{ url('js/wp-embed.min.js') }}"></script>
	<script type='text/javascript' src="{{ url('js/jquery.language.validationEngine-es.js') }}"></script>
	<script type='text/javascript' src="{{ url('js/jquery.validationEngine.js') }}"></script>
	<script type='text/javascript'>
	/*
	<![CDATA[ */
	var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"admin-ajax.php","loadingTrans":"Cargando...","is_rtl":""};
	/* ]]> */
	</script>
	<script type='text/javascript' src="{{ url('js/front-subscribers.js') }}"></script>

	@yield('script')
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119548675-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119548675-1');
	</script>
</body>

</html>
