@extends('frontend.layout.layout-internal') @section('title', 'Bloqueos y Charters') @section('description', 'Discover') 

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-237.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/bloqueos.custom.css')}}" />
@stop

@section('custom-script')
<script type='text/javascript' src="{{ url('js/bloqueos.custom.js') }}"></script>
@stop

@section('style')

@stop

@section('body-style')
page-template-default page page-id-237 top-parent-237 fusion-image-hovers seccion-peru fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text
@stop

@section('content')

@include('frontend.partials.header')



<div id="sliders-container">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400%2C500" rel="stylesheet" property="stylesheet" type="text/css" media="all">
	<div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
		<!-- START REVOLUTION SLIDER 5.4.6.6 fullwidth mode -->
		<div id="rev_slider_3_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.6">
			<ul>
				<!-- SLIDE  -->
				<li data-index="rs-13" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="{{ url('images/cancun-100x50.jpg') }}"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Cuzco" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
					<!-- MAIN IMAGE -->
					<img src="{{ url('images/cancun.jpg') }}"  alt="" title="Cancun"  width="797" height="531" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
					
					<!-- LAYERS -->
					<!-- LAYER NR. 1 -->
					<div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-13-layer-1" data-x="center" data-hoffset="-521" data-y="center" data-voffset="-91" data-width="['auto','auto','auto','auto']" data-height="['auto','auto','auto','auto']" 			data-type="text" data-responsive_offset="on" data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]" data-paddingright="[0,0,0,0]"			data-paddingbottom="[10,10,10,10]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 65px; letter-spacing: px;font-family:AndrogyneR;">Cancun </div>
					<!-- LAYER NR. 2 -->
					<p class="tp-caption NotGeneric-SubTitle   tp-resizeme" id="slide-13-layer-4" data-x="center" data-hoffset="-300" data-y="center" data-voffset="-22" data-width="['639','auto','auto','auto']" data-height="['auto','auto','auto','auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1500,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; min-width: 639px; max-width: 639px; white-space: normal; font-size: 22px; font-weight: 400; letter-spacing: normalpx;font-family:Roboto;">
						Lorem ipsum dolor sit amet donec consectetur diam ac mi cursus.
					</p>
				</li>
			</ul>
			
			<script>
				var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); 
				var htmlDivCss="";
				if(htmlDiv) {
					htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
				}else{
					var htmlDiv = document.createElement("div");
					htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
					document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
				}
				
			</script>
			<div class="tp-bannertimer" style="height: 7px; background: rgba(255,255,255,0.25);"></div>
		</div>
							
		<script>
			var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); 
			var htmlDivCss=".tp-caption.NotGeneric-Title,.NotGeneric-Title{color:rgba(255,255,255,1.00);font-size:70px;line-height:70px;font-weight:800;font-style:normal;font-family:Raleway;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;1:o;3:j;4:e;5:c;6:t;7:;8:O;9:b;10:j;11:e;12:c;13:t;14:];text-shadow:1px 1px 2px #333}.tp-caption.NotGeneric-SubTitle,.NotGeneric-SubTitle{color:rgba(255,255,255,1.00);font-size:13px;line-height:20px;font-weight:500;font-style:normal;font-family:Raleway;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;letter-spacing:4px}";
			if(htmlDiv) {
				htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
			}else{
				var htmlDiv = document.createElement("div");
				htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
				document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
			}
		</script>
		
		<script type="text/javascript">
			setREVStartSize({c: '#rev_slider_3_1', gridwidth: [1600], gridheight: [500], sliderLayout: 'fullwidth'});
			var revapi3,tpj;
			document.addEventListener("DOMContentLoaded", function() {
				tpj = jQuery;	
				if(tpj("#rev_slider_3_1").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_3_1");
				}else{
					revapi3 = tpj("#rev_slider_3_1").show().revolution({
						sliderType:"standard",
						jsFileLocation:"{{ url('js/revslider/') }}/",
						sliderLayout:"fullwidth",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							keyboardNavigation:"off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation:"off",
			 							mouseScrollReverse:"default",
							onHoverStop:"off",
							touch:{
								touchenabled:"on",
								touchOnDesktop:"off",
								swipe_threshold: 75,
								swipe_min_touches: 1,
								swipe_direction: "horizontal",
								drag_block_vertical: false
							}
							,
							arrows: {
								style:"gyges",
								enable:true,
								hide_onmobile:false,
								hide_onleave:true,
								hide_delay:200,
								hide_delay_mobile:1200,
								tmp:'',
								left: {
									h_align:"left",
									v_align:"center",
									h_offset:30,
									v_offset:0
								},
								right: {
									h_align:"right",
									v_align:"center",
									h_offset:30,
									v_offset:0
								}
							}
						},
						viewPort: {
							enable:true,
							outof:"pause",
							visible_area:"80%",
							presize:false
						},
						visibilityLevels:[1240,1024,778,480],
						gridwidth:1600,
						gridheight:500,
						lazyType:"none",
						parallax: {
							type:"mouse",
							origo:"slidercenter",
							speed:2000,
							speedbg:0,
							speedls:0,
							levels:[2,3,4,5,6,7,12,16,10,50,47,48,49,50,51,55],
						},
						shadow:0,
						spinner:"off",
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,
						shuffle:"off",
						autoHeight:"off",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}
				
			});	/*ready*/
		</script>
	</div>
	<!-- END REVOLUTION SLIDER -->
</div>

<div id="main" role="main" class="clearfix " style="">
	<div class="fusion-row" style="">
		<div id="content" style="width: 100%;">
			<div id="post-237" class="post-237 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">Bloqueos</span>
				<span class="vcard rich-snippet-hidden">
					<span class="fn">
						<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
					</span>
				</span>
				<span class="updated rich-snippet-hidden">2017-05-20T12:10:18+00:00</span>
				
				<div class="post-content">
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-widget-area fusion-widget-area-1 fusion-content-widget-area">
										<style type="text/css" scoped="scoped">
											.fusion-widget-area-1 {padding:0px 0px 0px 0px;}.fusion-widget-area-1 .widget h4 {color:#333333;}.fusion-widget-area-1 .widget .heading h4 {color:#333333;}
										</style>
										<div id="bcn_widget-2" class="widget widget_breadcrumb_navxt">
											<div class="breadcrumbs" vocab="https://schema.org/" typeof="BreadcrumbList">
												<!-- Breadcrumb NavXT 6.0.4 -->
												<span property="itemListElement" typeof="ListItem">
													<a property="item" typeof="WebPage" title="Ir a Discover." href="{{ route('home') }}" class="home">
														<span property="name">Inicio</span>
													</a>
													<meta property="position" content="1">
												</span>
												>
												<span property="itemListElement" typeof="ListItem">
													<span property="name">Bloqueos</span>
													<meta property="position" content="2">
												</span>
											</div>
										</div>
										<div class="fusion-additional-widget-content"></div>
									</div>

									<div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-large fusion-blog-pagination blog-tipo1">
										<div class="fusion-posts-container fusion-posts-container-pagination" data-pages="3">
											<article id="post-1184" class="fusion-post-large post-1184 post type-post status-publish format-standard has-post-thumbnail hentry category-bloqueos categoria-id-7">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('bloqueo') }}">
																	<img width="797" height="531" src="{{ url('images/cancun.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>

												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('bloqueo') }}">Cancun</a>
													</h2>
													<div class="subtitulo-post">USA</div>
													<div class="fusion-post-content-container">
														<p>       Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et.</p>
													</div>
													<div class="descargar_pdf">
														<a class="boton-1" href="#" target="_blank">Descargar Información Detallada</a>
													</div>
													
													<a class="fusion-modal-text-link boton-2" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar_1184" href="#">Solicitar Cotización</a>

													<div class="fusion-modal modal fade modal-1 Form_Cotizar_1184 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
														<style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
														<div class="modal-dialog modal-lg">
															<div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
																<div class="modal-header">
																	<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
																	<h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Solicita una cotización</h3>
																</div>

																<div class="modal-body">
																	<h3>Cancun</h3>
																	<h4 class="sub_titulo_post_form"></h4>
																	<div role="form" class="wpcf7" id="wpcf7-f162-p1184-o1" lang="es-ES" dir="ltr">
																		<div class="screen-reader-response"></div>
																		
																		<form action="/discover/bloqueos/#wpcf7-f162-p1184-o1" method="post" class="wpcf7-form" novalidate="novalidate">
																			<div style="display: none;">
																				<input type="hidden" name="_wpcf7" value="162" />
																				<input type="hidden" name="_wpcf7_version" value="4.9.2" />
																				<input type="hidden" name="_wpcf7_locale" value="es_ES" />
																				<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p1184-o1" />
																				<input type="hidden" name="_wpcf7_container_post" value="1184" />
																			</div>

																			<p>
																				<label> Nombre (requerido)
																					<br />
																					<span class="wpcf7-form-control-wrap Nombre">
																						<input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
																					</span>
																				</label>
																			</p>

																			<p>
																				<label> Tu correo electrónico (requerido)
																					<br />
																					<span class="wpcf7-form-control-wrap Email">
																						<input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
																					</span>
																				</label>
																			</p>

																			<p>
																				<label> Asunto
																					<br />
																					<span class="wpcf7-form-control-wrap Asunto">
																						<input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
																					</span>
																				</label>
																			</p>

																			<p>
																				<label> Mensaje
																					<br />
																					<span class="wpcf7-form-control-wrap Mensaje">
																						<textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
																					</span>
																				</label>
																			</p>

																			<p>
																				<span class="wpcf7-form-control-wrap Producto">
																					<input type="hidden" name="Producto" value="Cancun" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
																				</span>
																				<span class="wpcf7-form-control-wrap URL">
																					<input type="hidden" name="URL" value="{{ route('bloqueos') }}" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
																				</span>
																			</p>

																			<p style="text-align:right">
																				<input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
																			</p>

																			<div class="wpcf7-response-output wpcf7-display-none"></div>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<div class="div-leermas">
														<a href="{{ route('bloqueo') }}" class="link-leermas">Leer más</a>
													</div>
												</div>
												<div class="fusion-clearfix"></div>
											</article>


											<article id="post-266" class="fusion-post-large post-266 post type-post status-publish format-standard has-post-thumbnail hentry category-bloqueos categoria-id-7">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('bloqueo') }}">
																	<img width="797" height="531" src="{{ url('images/cancun.jpg') }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>

												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('bloqueo') }}">Boston</a>
													</h2>

													<div class="subtitulo-post">USA</div>

													<div class="fusion-post-content-container">
														<p>       Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et.</p>
													</div>

													<div class="descargar_pdf">
														<a class="boton-1" href="#" target="_blank">Descargar Información Detallada</a>
													</div>

													<a class="fusion-modal-text-link boton-2" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar_266" href="#">Solicitar Cotización</a>

													<div class="fusion-modal modal fade modal-2 Form_Cotizar_266 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-2" aria-hidden="true">
														<style type="text/css">.modal-2 .modal-header, .modal-2 .modal-footer{border-color:#ebebeb;}</style>
														<div class="modal-dialog modal-lg">
															<div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
																<div class="modal-header">
																	<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
																	<h3 class="modal-title" id="modal-heading-2" data-dismiss="modal" aria-hidden="true">Solicita una cotización</h3>
																</div>
																<div class="modal-body">
																	<h3>Boston</h3>
																	<h4 class="sub_titulo_post_form"></h4>
																	<div role="form" class="wpcf7" id="wpcf7-f162-p266-o2" lang="es-ES" dir="ltr">
																		<div class="screen-reader-response"></div>
																		<form action="/discover/bloqueos/#wpcf7-f162-p266-o2" method="post" class="wpcf7-form" novalidate="novalidate">
																			<div style="display: none;">
																				<input type="hidden" name="_wpcf7" value="162" />
																				<input type="hidden" name="_wpcf7_version" value="4.9.2" />
																				<input type="hidden" name="_wpcf7_locale" value="es_ES" />
																				<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f162-p266-o2" />
																				<input type="hidden" name="_wpcf7_container_post" value="266" />
																			</div>
																			<p>
																				<label> Nombre (requerido)
																					<br />
																					<span class="wpcf7-form-control-wrap Nombre">
																						<input type="text" name="Nombre" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
																					</span>
																				</label>
																			</p>
																			<p>
																				<label> Tu correo electrónico (requerido)
																					<br />
																					<span class="wpcf7-form-control-wrap Email">
																						<input type="email" name="Email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
																					</span>
																				</label>
																			</p>
																			<p>
																				<label> Asunto
																					<br />
																					<span class="wpcf7-form-control-wrap Asunto">
																						<input type="text" name="Asunto" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
																					</span>
																				</label>
																			</p>
																			<p>
																				<label> Mensaje
																					<br />
																					<span class="wpcf7-form-control-wrap Mensaje">
																						<textarea name="Mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
																					</span>
																				</label>
																			</p>
																			<p>
																				<span class="wpcf7-form-control-wrap Producto">
																					<input type="hidden" name="Producto" value="Boston" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
																				</span>
																				<span class="wpcf7-form-control-wrap URL">
																					<input type="hidden" name="URL" value="{{ route('bloqueos') }}" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
																				</span>
																			</p>
																			<p style="text-align:right">
																				<input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
																			</p>
																			<div class="wpcf7-response-output wpcf7-display-none"></div>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="div-leermas">
														<a href="{{ route('bloqueo') }}" class="link-leermas">Leer más</a>
													</div>
												</div>
												<div class="fusion-clearfix"></div>
											</article>

										</div>

										<div class='pagination clearfix'>
											<span class="current">1</span>
											<a href="#" class="inactive">2</a>
											<a href="#" class="inactive">3</a>
											<a class="pagination-next" href="#">
												<span class="page-text">Next</span>
												<span class="page-next"></span>
											</a>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

@stop


@section('style')
    <link rel="stylesheet" href="{{ asset('css/fusion-237.css')}}" />
@stop


@section('script')

<script type='text/javascript' src="{{ url('js/e08e754308f5132d5d069f552db3ad80.js') }}"></script>

@stop