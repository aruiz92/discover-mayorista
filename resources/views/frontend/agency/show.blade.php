@extends('frontend.layout.layout-internal') @section('title', 'Agentes') @section('description', 'Discover')


@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-1184.css')}}" />
@stop


@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/hm_custom_js-custom.css')}}" />
@stop


@section('custom-script')
<script src="{{ url('js/bloqueo.custom.js')}}"></script>
@stop


@section('style')
<link rel="stylesheet" href="{{ asset('css/agency.css') }}">
@stop

@section('body-style')
page-template page-template-100-width page-template-100-width-php page page-id-738 page-parent top-parent-738 fusion-image-hovers page-agentes fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop

@section('content')

@include('frontend.partials.header')

<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
	<div class="fusion-row" style="max-width:100%;">
		<div id="content" class="full-width">
      <div class="agency">
        <div class="agency-cover">
          <div class="agency-card">
            <div class="agency-card-header">
              <div class="agency-card-logo">
                <img src="{{ asset('files/logos/'.$agency->logo) }}" alt="">
              </div>
              <div class="agency-card-info">
                <span class="agency-card-title">{{ $agency->nombre_comercial }}</span>
                <span class="agency-card-user">
                  <a href="{{ url('agency/'.$agency->usuario) }}">{{ '@'.$agency->usuario }}</a>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="agency-menu">
          <ul>
            <li data-tab="agencia" class="active">
              <i class="fa fa-plane" aria-hidden="true"></i> Agencia
            </li>
						@if(Auth::user()->role->id == 1)
	            <li data-tab="agentes">
	              <i class="fa fa-user" aria-hidden="true"></i> Agentes
	            </li>
						@endif


	            <li data-tab="profile">
	              <i class="fa fa-user" aria-hidden="true"></i> Perfil
	            </li>

          </ul>
        </div>
        <div class="agency-contenedor">
          <div class="agency-tabs">
            <div id="tab-agencia" class="agency-tab active">
              <form id="frmAgency" class="frm-agency" action="{{ url('api/agency/'.$agency->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <h2>Información</h2>
                <p>Esta Información es muy importante. Si desea realizar algun cambio, por favor, rellenar todos los campos con datos relevantes. Gracias.</p>
                <ul class="agency-tab-info">
                  <li>
                    <span>Usuario *</span>
                    <input type="text" name="usuario" value="{{ $agency->usuario }}">
                  </li>
                  <li>
                    <span>Correo *</span>
                    <input type="text" name="correo" value="{{ $agency->correo }}">
                  </li>
                  <li>
                    <span>RUC *</span>
                    <input type="text" name="ruc" value="{{ $agency->ruc }}">
                  </li>
                  <li>
                    <span>Razón Social *</span>
                    <input type="text" name="razon_social" value="{{ $agency->razon_social }}">
                  </li>
                  <li>
                    <span>Nombre Comercial *</span>
                    <input type="text" name="nombre_comercial" value="{{ $agency->nombre_comercial }}">
                  </li>
                  <li>
                    <span>Responsable *</span>
                    <input type="text" name="nombres" value="{{ $agency->nombres }}">
                  </li>
                  <li>
                    <span>Teléfono Fijo *</span>
                    <input type="text" name="telefono" value="{{ $agency->telefono }}">
                  </li>
                  <li>
                    <span>Teléfono Movíl (Opcional)</span>
                    <input type="text" name="celular" value="{{ $agency->celular }}">
                  </li>
                  <li>
                    <span>País *</span>
                    <select id="selectPais" name="pais">
                      <option value="{{ $country->id }}">{{ $country->nombre }}</option>
                    </select>
                  </li>
                  <li>
                    <span>Provincia *</span>
                    <select id="selectProvincia" name="provincia">
                      <option value="">Seleccionar</option>
                      @foreach($provinces as $province)
                        <option value="{{ $province->id }}"{{ ($province->id === $agency->p_id) ? '':'' }}>{{ $province->nombre }}</option>
                      @endforeach
                    </select>
                  </li>
                  <li>
                    <span>Distrito *</span>
                    <select id="selectDistrito" name="distrito">
                      <option value="">Seleccionar</option>
                      @foreach($districts as $district)
                        <option value="{{ $district->id }}"{{ ($district->id == $agency->district_id) ? 'selected':'' }}>{{ $district->nombre }}</option>
                      @endforeach
                    </select>
                  </li>
                  <li>
                    <span>Dirección *</span>
                    <input type="text" name="direccion_1" value="{{ $agency->direccion_1 }}">
                  </li>
                  <li class="w-100">
                    <span>Dirección (Opcional)</span>
                    <input type="text" name="direccion_2" value="{{ $agency->direccion_2 }}">
                  </li>
                  <li class="w-100">
                    <span>Logo *</span>
                    <div class="agency-frm-logo">
                      <img src="{{ asset('files/logos/'.$agency->logo) }}" alt="{{ $agency->nombre_comercial }}">
											@if(Auth::user()->role->id == 1)
												<label class="custom-file-upload">
													<input type="file" name="logo" data-action="{{ url('api/updatelogo') }}" data-method="POST"/>
													<i class="fa fa-cloud-upload"></i> Actualizar foto
												</label>
											@endif
                    </div>
                    <small>Tamaño: 100x100 pixeles.</small><br>
                    <small>Tipo: PNG con transparencia.</small>
                  </li>
                </ul>
								@if(Auth::user()->role->id == 1)
                	<button id="btnSubmit" type="submit" name="button" class="btn-agency">Editar</button>
								@endif
              </form>
            </div>
            <div id="tab-agentes" class="agency-tab">
							<div class="contenedor-agentes">
								<h2>Información</h2>
								<p>Esta Información es muy importante. Si desea realizar algun cambio, por favor, rellenar todos los campos con datos relevantes. Gracias.</p>
								<div class="contenedor-actions">
									<a href="#" class="btn-editar btn-azul btn-add" data-toggle="modal" data-target=".fusion-modal.Form_Agente">Agregar</a>
								</div>
							</div>
							<ul class="lista-agentes">
								@foreach($agencyUsers as $key => $agencyUser)
								<li>
									<div>
										<strong>Nombres:</strong> {{ $agencyUser->nombre }}
									</div>
									<div>
										<strong>Correo:</strong> {{  $agencyUser->correo }}
									</div>
									<div>
										<strong>Estado:</strong> {{ $agencyUser->estado }}
									</div>
									<div class="lista-agentes-footer">
										<a class="btn-editar" href="#" data-index="{{ $key }}" data-toggle="modal" data-target=".fusion-modal.Form_Agente">Editar</a>
										<a class="btn-editar btn-blanco btn-reset" href="#" data-index="{{ $key }}" data-id="{{ $agencyUser->id }}">Restablecer</a>
										<a class="btn-editar btn-blanco btn-eliminar" href="#" data-index="{{ $key }}" data-id="{{ $agencyUser->id }}">Borrar</a>
									</div>
								</li>
								@endforeach
							</ul>
            </div>
						<div id="tab-profile" class="agency-tab">

							<!-- <form id="frmProfile" class="frm-agency" action="{{ url('api/agency/'.$agency->id) }}" method="post">
								<input type="hidden" name="_method" value="PUT">

								<h2 class="profile__title">Mi Perfil</h2>
								<ul class="agency-tab-info">
									<li>
										<span>Nombre *</span>
										<input type="text" name="usuario" value="{{ Auth::user()->nombre }}">
									</li>
									<li>
										<span>Correo *</span>
										<input type="text" name="correo" value="{{ Auth::user()->correo }}">
									</li>
								</ul>

								<button id="btnProfile" type="submit" name="button" class="btn-agency">Editar</button>
							</form> -->

							<form id="frmPassword" class="frm-agency mt-5" action="{{ url('api/password/'.$agency->id) }}" method="post">
								<input type="hidden" name="_method" value="PUT">

								<h2 class="profile__title">Cambiar contraseña</h2>
								<ul class="agency-tab-info">
									<li style="width: 100%;">
										<span>Contraseña actual *</span>
										<input type="password" name="passwordOld">
									</li>
								</ul>

								<ul class="agency-tab-info">
									<li>
										<span>Nueva contraseña *</span>
										<input type="password" name="password">
									</li>
									<li>
										<span>Repetir contraseña *</span>
										<input type="password" name="password_confirmation">
									</li>
								</ul>

								<button id="btnPassword" type="submit" name="button" class="btn-agency">Cambiar</button>
							</form>
						</div>
          </div>
        </div>
      </div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

<!-- Modal -->
<div class="fusion-modal modal fade modal-1 Form_Agente max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
	<div class="modal-dialog modal-lg">
			<div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
					<div class="modal-header">
							<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Editar Agente</h3>
					</div>
					<div class="modal-body">
							<div role="form" class="wpcf7">
									<form id="frmAgente" action="{{ url('dashboard/agency_users') }}" method="post">
										{{ csrf_field() }}
										<input type="hidden" name="agency_id" value="{{ $agency->id }}">
					          <input type="hidden" name="agency_user_id" value="">

										<ul class="form-control-agente">
											<li>
												<label>Nombre</label>
												<input type="text" name="nombre" value="">
												<div class="invalid-feedback"></div>
											</li>
											<li>
												<label>Correo</label>
												<input type="text" name="correo" value="">
												<div class="invalid-feedback"></div>
											</li>
											<li>
												<label>Rol</label>
												<select name="role">
													<option value="">Seleccionar</option>
													@foreach($roles as $role)
														<option value="{{ $role->id }}">{{ $role->nombre }}</option>
													@endforeach
												</select>
												<div class="invalid-feedback"></div>
											</li>
											<li class="select-estado">
												<label>Estado</label>
												<select name="estado">
													<option value="">Seleccionar</option>
													<option value="1">Activo</option>
													<option value="0">Inactivo</option>
												</select>
												<div class="invalid-feedback"></div>
											</li>
										</ul>
									</form>
							</div>
					</div>
					<div class="modal-footer">
							<a class="fusion-button button-default button-medium button medium btn-editar-add" style="background-color:#039ecc;">Editar</a>
							<a class="fusion-button button-default button-medium button default medium" data-dismiss="modal">Cerrar</a>
					</div>
			</div>
	</div>
</div>

<script type="text/template" id="component-li">
	<li>
		<div>
			<strong>Nombres:</strong> <%= data.nombre %>
		</div>
		<div>
			<strong>Correo:</strong> <%= data.correo %>
		</div>
		<div>
			<strong>Estado:</strong> <%= data.estado %>
		</div>
		<div class="lista-agentes-footer">
			<a class="btn-editar" href="#" data-index="<%= data.index %>" data-toggle="modal" data-target=".fusion-modal.Form_Agente">Editar</a>
			<a class="btn-editar btn-blanco btn-reset" href="#" data-index="<%= data.index %>" data-id="<%= data.id %>">Restablecer</a>
		</div>
	</li>
</script>
@stop

@section('script')
<script type='text/javascript' src="{{ url('js/2b853892b45b6c7c62e273b1eba75fcb.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('admin/vendors/jquery.component/lodash.min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/underscore-min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/jquery.component.min.js') }}"></script>

<script type="text/javascript">
const site_url = '{{ url('/') }}/';
let jsonAgencyUsers = {!! $agencyUsers !!};
let isEdit = false;
let indexEdit = null;

let coLi = jQuery.component({
  template: jQuery('#component-li').html()
});

jQuery.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
  }
});

jQuery('#frmAgency input[name="logo"]').change(function() {
  readURL(this);
});

jQuery('.lista-agentes .btn-editar').click(function () {
	isEdit = true;
	indexEdit = jQuery(this).attr('data-index');

	const agente = jsonAgencyUsers[indexEdit];

	jQuery('.Form_Agente .modal-title').text('Editar Agente');
	jQuery('.Form_Agente .btn-editar-add').text('Editar');

	jQuery('#frmAgente input[name="agency_user_id"]').val(agente.id);
	jQuery('#frmAgente input[name="nombre"]').val(agente.nombre);
	jQuery('#frmAgente input[name="correo"]').val(agente.correo);
	jQuery('#frmAgente select[name="role"]').val(agente.role_id);
	jQuery('#frmAgente select[name="estado"]').val(agente.estado);

	jQuery('#frmAgente .select-estado').show();
});

jQuery('.contenedor-agentes .btn-add').click(function () {
	isEdit = false;

	jQuery('.Form_Agente .modal-title').text('Agregar Agente');
	jQuery('.Form_Agente .btn-editar-add').text('Agregar');

	jQuery('#frmAgente input[name="agency_user_id"]').val('');
	jQuery('#frmAgente input[name="nombre"]').val('');
	jQuery('#frmAgente input[name="correo"]').val('');
	jQuery('#frmAgente select[name="role"]').val('');
	jQuery('#frmAgente select[name="estado"]').val('');

	jQuery('#frmAgente .select-estado').hide();
});

jQuery('.lista-agentes .btn-eliminar').click(function () {
	var index = jQuery(this).attr('data-index');

	const agente = jsonAgencyUsers[index];

	if (agente.role_id === 1) {
	  swal("Upsss!", "No puedes eliminar a un administrador!", "info");
		return false;
	}

	swal({
      title: 'Estas seguro de eliminar al agente?',
      text: 'Si procede, no podrá recuperar la información y actividad del agente.',
      icon: 'info',
			buttons: {
				cancel: {
			    text: 'No',
			    className: 'btn btn-secondary',
			  },
			  confirm: {
			    text: 'Si, eliminar!',
			    className: 'btn btn-success',
					closeModal: false
			  }
			}
  })
	.then(willDelete => {
    if (willDelete === true) {
			jQuery.ajax({
				method: 'POST',
				url: site_url + 'dashboard/ajaxDeleteAgencyUser',
				data: { id: agente.id },
				success: function (response) {
					jQuery('.lista-agentes li:nth-child(' + (parseInt(index) + 1) + ')').remove();

					swal("Excelente!", "Hemos eliminado al agente!", "success");
				}
			})
			.fail(function(error) {
				swal("Upsss!", "Hubo un error al eliminar al agente!", "warning");
			});
    }
  });
});

jQuery('.agency-menu ul li').click(function () {
  var tab = jQuery(this).attr('data-tab');

  jQuery('.agency-menu ul li').removeClass('active');
  jQuery(this).addClass('active');

  if (tab === 'agencia') {
    jQuery('#tab-agentes').removeClass('active');
		jQuery('#tab-profile').removeClass('active');
    jQuery('#tab-agencia').addClass('active');
	}else if (tab === 'profile') {
    jQuery('#tab-agentes').removeClass('active');
		jQuery('#tab-agencia').removeClass('active');
    jQuery('#tab-profile').addClass('active');
  } else {
    jQuery('#tab-agencia').removeClass('active');
		jQuery('#tab-profile').removeClass('active');
    jQuery('#tab-agentes').addClass('active');
  }
});

jQuery('#selectPais').change(function () {
  var value = jQuery(this).val();

  var form = {
    method: 'GET',
    action: '{{ url("api/province") }}/' + value,
    data: {}
  }

  ajaxLocation(form, 'selectProvincia');
});

jQuery('#selectProvincia').change(function () {
  var value = jQuery(this).val();

  var form = {
    method: 'GET',
    action: '{{ url("api/district") }}/' + value,
    data: {}
  }

  ajaxLocation(form, 'selectDistrito');
});

jQuery('#frmAgency').submit(function( event ) {
  var action = jQuery(this).attr('action');
  var method = jQuery(this).attr('method');
  var data = jQuery(this).serialize();

  jQuery('#btnSubmit').prop('disabled', true);
  jQuery('#btnSubmit').val('Editando...');

  jQuery.ajax({
    method: method,
    url: action,
    data: data,
    success: function (response) {
      jQuery('#btnSubmit').prop('disabled', false);
      removeClassError();
      swal("Excelente!", "Hemos actualizado la información!", "success");
    }
  })
  .fail(function(error) {
    var status = error.status;
    var data = error.responseJSON;

    jQuery('#btnSubmit').prop('disabled', false);
    jQuery('#btnSubmit').val('Editar');

    if (status === 422) {
      removeClassError();
      jQuery.each(data, function(key, value) {
        var input = jQuery('#frmAgency *[name="' + key + '"]').addClass('has-error');
      });
    }
  });

  event.preventDefault();
});

jQuery('#frmPassword').submit(function( event ) {
  var action = jQuery(this).attr('action');
  var method = jQuery(this).attr('method');
  var data = jQuery(this).serialize();

  jQuery('#btnPassword').prop('disabled', true);
  jQuery('#btnPassword').val('Editando...');

  jQuery.ajax({
    method: method,
    url: action,
    data: data,
    success: function (response) {
      jQuery('#btnPassword').prop('disabled', false);
      removeClassError();
      swal("Excelente!", "Hemos actualizado su contraseña!", "success");
			jQuery('#frmPassword').trigger('reset');
    }
  })
  .fail(function(error) {
    var status = error.status;
    var data = error.responseJSON;

    jQuery('#btnPassword').prop('disabled', false);
    jQuery('#btnPassword').val('Editar');

    if (status === 422) {
      removeClassError();
      jQuery.each(data, function(key, value) {
				if (key === 'currentPassword') {
						swal("Upsss!", value, "warning");
				} else {
						var input = jQuery('#frmPassword *[name="' + key + '"]').addClass('has-error');
				}
      });
    }
  });

  event.preventDefault();
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      jQuery('.agency-frm-logo img').attr('src', e.target.result);
      ajaxUpdateLogo();
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function ajaxUpdateLogo() {
  var input = jQuery('#frmAgency input[type=file]');

  var action = input.attr('data-action');
  var method = input.attr('data-method');
  var data = new FormData();
	// Attach file
  data.append('logo', jQuery('input[type=file]')[0].files[0]);

  jQuery.ajax({
    method: method,
    url: action,
    data: data,
    processData: false,
  	contentType: false,
    success: function (response) {
      swal("Excelente!", "Hemos actualizado el logo!", "success");
    }
  })
  .fail(function(error) {
    var status = error.status;
    var data = error.responseJSON;

    if (status === 422) {
      swal("Upss!", data.logo[0], "info");
    }
  });
}


const frmAgente = '#frmAgente';

jQuery('.Form_Agente .btn-editar-add').click(function (event) {
  const method = jQuery(frmAgente).attr('method');
  let url = jQuery(frmAgente).attr('action');
  let data = jQuery(frmAgente).serialize();

  const agency_user_id = jQuery(frmAgente + ' input[name="agency_user_id"]').val();

  if (isEdit) {
    data = data + '&_method=PUT';
    url = url + '/' + agency_user_id;
  }

  jQuery.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmAgente);

    jQuery('.Form_Agente').modal('hide');

    let swalText = 'Hemos actualizado la información correctamente';

    if (!isEdit) {
      swalText = 'Se ha enviado un correo al agente con sus datos de acceso';
    }

    setTimeout(() => {
      swal({
        title: 'Excelente!',
        text: swalText,
        type: 'success',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary'
      });
    }, 250);

    if (isEdit) {
      jsonAgencyUsers[indexEdit].nombre = success.nombre;
      jsonAgencyUsers[indexEdit].correo = success.correo;
			jsonAgencyUsers[indexEdit].estado = success.estado;
      jsonAgencyUsers[indexEdit].role_id = success.role_id;
    } else {
      jsonAgencyUsers.push(success);
    }

    jQuery('.lista-agentes').html('');

    jQuery.each(jsonAgencyUsers, function (index, item) {
      item.index = index;
      jQuery('.lista-agentes').append(coLi.render(item));
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmAgente);

    if (status === 422) {
      errorAjax(frmAgente, errors);
    }
  });

  event.preventDefault();
});

jQuery('.lista-agentes').on('click', '.btn-reset', function () {
  const id = jQuery(this).attr('data-id');
  indexEdit = jQuery(this).attr('data-index');

  swal({
      title: 'Estas seguro de restablecer contraseña?',
      text: 'Si procede, se cambiará la contraseña del agente',
      icon: 'info',
			buttons: {
				cancel: {
			    text: 'No',
			    className: 'btn btn-secondary',
			  },
			  confirm: {
			    text: 'Si, restablecer!',
			    className: 'btn btn-success',
					closeModal: false
			  }
			}
  })
	.then(willDelete => {
    if (willDelete === true) {
      jQuery.ajax({
        method: 'get',
        url: site_url + 'dashboard/ajaxResetContrasena/' + id,
      })
      .done(function (success) {
        swal({
          title: 'Excelente!',
          text: 'Hemos enviado la información al agente',
          icon: 'success',
					buttons: {
					  confirm: {
					    className: 'btn btn-primary',
					  }
					}
        });
      })
      .fail(function (error) {
        swal({
          title: 'Ups!',
          text: 'Hubo un error al restablecer el agente',
          type: 'info',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      });
    }
  });
});

function removeClassError () {
  jQuery('#frmAgency input, #frmAgency select').removeClass('has-error');
	jQuery('#frmPassword input, #frmPassword select').removeClass('has-error');
	jQuery('#frmProfile input, #frmProfile select').removeClass('has-error');
	jQuery('#frmAgente input, #frmAgente select').removeClass('has-error');
}

function removeOption (select) {
  jQuery('#' + select).empty();
  jQuery('#' + select).append(jQuery('<option>', {
    value: "",
    text : 'Seleccionar'
  }));
}

function ajaxLocation (form, select) {
  removeOption(select);

  jQuery.ajax({
    method: form.method,
    url: form.action,
    data: form.data,
    success: function (response) {
      jQuery.each(response, function (key, item) {
        jQuery('#' + select).append(jQuery('<option>', {
          value: item.id,
          text : item.nombre
        }));
      });
    }
  });
}

function errorAjax(frm, errors) {
  jQuery.each(errors, function (item, value) {
    let input, msg, parent;

    input = jQuery(frm + ' *[name="' + item + '"]');
    parent = jQuery(input).parent()[0];
    msg = jQuery(parent).children()[2];

    jQuery(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  jQuery(frm + ' .invalid-feedback').text('');
}
</script>

@stop
