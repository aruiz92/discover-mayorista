@extends('frontend.layout.layout-internal') @section('title', 'Agentes') @section('description', 'Discover')


@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-1184.css')}}" />
@stop


@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/hm_custom_js-custom.css')}}" />
@stop


@section('custom-script')
<script src="{{ url('js/bloqueo.custom.js')}}"></script>
@stop


@section('style')

@stop

@section('body-style')
page-template page-template-100-width page-template-100-width-php page page-id-738 page-parent top-parent-738 fusion-image-hovers page-agentes fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop

@section('content')

@include('frontend.partials.header')

<div id="sliders-container"></div>
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
	<div class="fusion-page-title-row">
		<div class="fusion-page-title-wrapper">
			<div class="fusion-page-title-secondary">
				<div class="fusion-breadcrumbs">
					<span>
						<a href="{{ route('home') }}">
							<span>Home</span>
						</a>
					</span>
					<span class="fusion-breadcrumb-sep">/</span>
					<span class="breadcrumb-leaf">Agencias</span>
				</div>
			</div>
			<div class="fusion-page-title-captions">
				<h1 class="entry-title">Agencias Asociados</h1>
			</div>
		</div>
	</div>
</div>
<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
	<div class="fusion-row" style="max-width:100%;">
		<div id="content" class="full-width">
			<div id="post-738" class="post-738 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">Agentes</span>
				<span class="vcard rich-snippet-hidden">
					<span class="fn">
						<a href="{{ url('mayorista') }}" title="Entradas de Discover" rel="author">
							Discover Mayorista
						</a>
					</span>
				</span>
				<span class="updated rich-snippet-hidden">2017-06-07T01:49:30+00:00</span>
				<div class="post-content">
					<div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6  fusion-one-sixth fusion-column-first 1_6"  style='margin-top:0px;margin-bottom:0px;width:16.66%;width:calc(16.66% - ( ( 4% ) * 0.1666 ) );margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<p>Filtro de búsqueda &gt;</p>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_5_6  fusion-five-sixth fusion-column-last 5_6"  style='margin-top:0px;margin-bottom:0px;width:83.33%;width:calc(83.33% - ( ( 4% ) * 0.8333 ) );'>
								<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6  fusion-one-sixth fusion-column-first 1_6"  style='margin-top:0px;margin-bottom:20px;width:16.66%;width:calc(16.66% - ( ( 4% ) * 0.1666 ) );margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<!-- Page-list plugin v.5.1 wordpress.org/plugins/page-list/ -->
									<ul class="page-list ">
										<li class="page_item page-item-836 page_item_has_children">
											<a href="{{ url('agencias') }}">Lima</a>
											<ul class='children'>
												@foreach($distritos as $distrito)
												<li class="page_item page-item-844">
													<a href="{{ route('district', ['pro_id' => '1', 'pro_url' => 'lima', 'dis_id' => $distrito->id, 'dis_url' => str_slug($distrito->nombre)]) }}">
														{{ $distrito->nombre }}
													</a>
												</li>
												@endforeach
											</ul>
										</li>
										<li class="page_item page-item-842 page_item_has_children">
											<a href="{{ url('agencias') }}">Provincias</a>
											<ul class='children'>
												@foreach($provincias as $provincia)
												<li class="page_item page-item-860">
													<a href="{{ route('province', ['pro_id' => $provincia->id, 'pro_url' => str_slug($provincia->nombre)]) }}">
														{{ $provincia->nombre }}
													</a>
												</li>
												@endforeach
											</ul>
										</li>
									</ul>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_5_6  fusion-five-sixth fusion-column-last 5_6"  style='margin-top:0px;margin-bottom:20px;width:83.33%;width:calc(83.33% - ( ( 4% ) * 0.8333 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<style type="text/css">.fusion-portfolio-wrapper#fusion-portfolio-1 .fusion-portfolio-content{  text-align: left; }</style>
									<div class="fusion-recent-works fusion-portfolio fusion-portfolio-1 fusion-portfolio-grid-with-text fusion-portfolio-paging-load-more-button fusion-portfolio fusion-portfolio-four fusion-portfolio-unboxed fusion-portfolio-text" data-id="-rw-1" data-columns="four">
										<style type="text/css">.fusion-portfolio-1 .fusion-portfolio-wrapper .fusion-col-spacing{padding:10px;}</style>
										<div id="agencies" class="fusion-portfolio-wrapper" id="fusion-portfolio-1" data-picturesize="fixed" data-pages="2" style="margin:-10px;">
											@foreach($agencies as $agency)
												<article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
													<div class="fusion-portfolio-content-wrapper">
														<span class="vcard rich-snippet-hidden">
															<span class="fn">
																<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
															</span>
														</span>
														<span class="updated rich-snippet-hidden">2017-06-05T00:57:56+00:00</span>
														<div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
															<a href="#">
																<img width="240" height="160" src="{{ url('files/logos/'.$agency->logo) }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
															</a>
														</div>
														<div class="fusion-portfolio-content">
															<h2 class="entry-title fusion-post-title">
																<a href="#">{{ $agency->nombre_comercial }}</a>
															</h2>
															<div class="fusion-post-content">
																<p>
																	teléf. {{ $agency->telefono }}<br />{{ $agency->correo }}
																</p>
															</div>
														</div>
													</div>
												</article>
											@endforeach
										</div>
										<div class="fusion-load-more-button fusion-portfolio-button fusion-clearfix"
										data-action="{{ url('api/agencies') }}"
										data-method="POST"
										data-pro-id="{{ $province }}"
										data-dis-id="{{ $district }}"
										data-limit="{{ $limit }}">Ver más</div>
										<div class="fusion-infinite-scroll-trigger"></div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div id="registrate-calltoaction" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-image: url("{{ url('images/Banner-registo-agentes.png') }}");background-position: left center;background-repeat: no-repeat;padding-top:155px;padding-bottom:10px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-spacing-no 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-size-two font-script title-h2 color-verde" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-left">¡Sé parte de nuestras agencias asociadas!</h2>
									</div>
									<p>
										<strong>Llena nuestro formulario y únete a la familia DISCOVER MAYORISTA</strong>
									</p>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6  fusion-one-sixth fusion-spacing-no 1_6"  style='margin-top:0px;margin-bottom:20px;width:16.66%;width:calc(16.66% - ( ( 0 + 0 ) * 0.1666 ) );margin-right: 0px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-column-content-centered">
										<div class="fusion-column-content">
											<div class="fusion-button-wrapper fusion-aligncenter">
												<style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{background: #01a13d;}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #0260a0;}.fusion-button.button-1{width:auto;}</style>
												<a class="fusion-button button-flat fusion-button-square button-large button-custom button-1" target="_self" title="Registrate" href="{{ url('registro-agencias') }}">
													<span class="fusion-button-text">Registrate</span>
												</a>
											</div>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last fusion-spacing-no 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 0 + 0 ) * 0.3333 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

@stop

@section('script')
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
<script type="text/javascript">
jQuery.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
	}
});

var countLoad = 1;
var btnLoad = jQuery('.fusion-load-more-button');
var containerPackages = jQuery('#agencies');

btnLoad.click(function (event) {

	var action = jQuery(this).attr('data-action');
	var method = jQuery(this).attr('data-method');
	var data = {
		province: jQuery(this).attr('data-pro-id'),
		district: jQuery(this).attr('data-dis-id'),
		limit: jQuery(this).attr('data-limit'),
		page: countLoad
	};

	jQuery(this).html('Cargando...');

	jQuery.ajax({
		method: method,
		url: action,
		data: data,
		success: function (response) {
			if (response.load === true) {
				btnLoad.html('Ver más');
				countLoad++;
				containerPackages.append(response.view);
			} else {
				btnLoad.hide();
			}
		}
	})
	.fail(function(error) {
		var status = error.status;
		var data = error.responseJSON;

		btnLoad.html('Ver más');
	});

	event.preventDefault();
});
</script>
@stop
