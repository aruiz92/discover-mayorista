@foreach($agencies as $agency)
  <article class="fusion-portfolio-post arequipa ate barranco brena cercado comas cuzco huancayo miraflores san-borja trujillo fusion-col-spacing">
    <div class="fusion-portfolio-content-wrapper">
      <span class="vcard rich-snippet-hidden">
        <span class="fn">
          <a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
        </span>
      </span>
      <span class="updated rich-snippet-hidden">2017-06-05T00:57:56+00:00</span>
      <div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
        <a href="#">
          <img width="240" height="160" src="{{ url('images/240x160.jpg') }}" class="attachment-portfolio-three size-portfolio-three wp-post-image" alt="" srcset="" sizes="" />
        </a>
      </div>
      <div class="fusion-portfolio-content">
        <h2 class="entry-title fusion-post-title">
          <a href="#">{{ $agency->nombre_comercial }}</a>
        </h2>
        <div class="fusion-post-content">
          <p>
            teléf. {{ $agency->telefono }}<br />{{ $agency->correo }}
          </p>
        </div>
      </div>
    </div>
  </article>
@endforeach
