@extends('frontend.layout.layout-internal') @section('title', 'Nosotros') @section('description', 'Discover') 

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-1184.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/hm_custom_js-custom.css')}}" />
@stop

@section('custom-script')
<script src="{{ url('js/nosotros.custom.js')}}"></script>
@stop

@section('style')

@stop

@section('body-style')
page-template page-template-100-width page-template-100-width-php page page-id-294 top-parent-294 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop


@section('content')

@include('frontend.partials.header')

<div id="sliders-container"></div>
<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
	<div class="fusion-row" style="max-width:100%;">
		<div id="content" class="full-width">
			<div id="post-294" class="post-294 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">Nosotros</span>
				<span class="vcard rich-snippet-hidden">
					<span class="fn">
						<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
					</span>
				</span>
				<span class="updated rich-snippet-hidden">2017-06-01T00:10:09+00:00</span>

				<div class="post-content">
					<div id="Div-Quienes-Somos" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-image: url("{{ url('images/Banner-Nosotros-1.jpg') }}");background-position: left center;background-repeat: no-repeat;padding-right:0px;padding-left:0px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="padding: 100px 0px 100px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-size-two title-h2 color-celeste" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-left">Quienes Somos</h2>
									</div>

									<p>Dedicados a la creación de paquetes de vacaciones, representaciones de operadores y productos exclusivos, innovadores y diferentes, somos una empresa joven, dinámica y agresiva en el actuar. Conformada por un grupo de profesionales del turismo de destacadas labores en el medio. Enfocados en un proyecto basado en altos valores humanos y éticos comerciales, servicios personalizados más propuestas diferentes e innovadoras en nuestras redes de agencias de viajes.</p>

									<div class="fusion-clearfix"></div>
								</div>
							</div>
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-widget-area fusion-widget-area-1 fusion-content-widget-area">
										<style type="text/css" scoped="scoped">
											.fusion-widget-area-1 .widget h4 {color:#333333;}.fusion-widget-area-1 .widget .heading h4 {color:#333333;}
										</style>
										<div id="bcn_widget-2" class="widget widget_breadcrumb_navxt">
											<div class="breadcrumbs" vocab="https://schema.org/" typeof="BreadcrumbList">
												<!-- Breadcrumb NavXT 6.0.4 -->
												<span property="itemListElement" typeof="ListItem">
													<a property="item" typeof="WebPage" title="Ir a Discover." href="{{ route('home') }}" class="home">
														<span property="name">Inicio</span>
													</a>
													<meta property="position" content="1">
												</span>
																
												<span property="itemListElement" typeof="ListItem">
													<span property="name">Nosotros</span>
													<meta property="position" content="2">
												</span>
											</div>
										</div>
										<div class="fusion-additional-widget-content"></div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<div id="Div-Mision-Vision" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-tabs fusion-tabs-1 clean tabs-nosotros horizontal-tabs">
										<style type="text/css">
											#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#ffffff;border-top-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}
										</style>
										<div class="nav">
											<ul class="nav-tabs nav-justified">
												<li class="active">
													<a class="tab-link" data-toggle="tab" id="fusion-tab-misión" href="#tab-736c9d49ae2e4af2df4">
														<h4 class="fusion-tab-heading">Misión</h4>
													</a>
												</li>
												<li>
													<a class="tab-link" data-toggle="tab" id="fusion-tab-visión" href="#tab-4ef6e70c8b9347d9158">
														<h4 class="fusion-tab-heading">Visión</h4>
													</a>
												</li>
											</ul>
										</div>
										<div class="tab-content">
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li class="active">
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-misión" href="#tab-736c9d49ae2e4af2df4">
															<h4 class="fusion-tab-heading">Misión</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade in active" id="tab-736c9d49ae2e4af2df4">
												<p>Está determinada en la imagen de nuestro logo y es la de unir puntos a través de una línea infinita, dando comienzo a los viajes de los pasajeros confiados por nuestras agencias de viajes aliadas y socias, convirtiendo la unión de los puntos en un sin cesar de viajes de placer y vacaciones, llenos de satisfacción, aventura, lujo y disfrute a cualquier parte del mundo. La versatilidad de nuestros programas nos hace innovadores y buscamos estar siempre diferenciados del común de la oferta.</p>
											</div>
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li>
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-visión" href="#tab-4ef6e70c8b9347d9158">
															<h4 class="fusion-tab-heading">Visión</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade" id="tab-4ef6e70c8b9347d9158">
												<p>Está determinada en la imagen de nuestro logo y es la de unir puntos a través de una línea infinita, dando comienzo a los viajes de los pasajeros confiados por nuestras agencias de viajes aliadas y socias, convirtiendo la unión de los puntos en un sin cesar de viajes de placer y vacaciones, llenos de satisfacción, aventura, lujo y disfrute a cualquier parte del mundo. La versatilidad de nuestros programas nos hace innovadores y buscamos estar siempre diferenciados del común de la oferta.</p>
											</div>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>


					<div id="Div-Ventajas" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-image: url("{{ url('images/Banner-Nosotros-2.jpg') }}");background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="padding: 70px 70px 50px 70px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-two title-h2 color-celeste" style="margin-top:0px;margin-bottom:31px;">
										<h2 class="title-heading-center">VENTAJAS DE COMPRAR EN DISCOVER</h2>
									</div>
									<ul>
										<li>Somos una empresa enfocada a las necesidades de los agentes de viajes. Como tal y bajo esa premisa partimos del respeto al canal de ventas.</li>
										<li>El pasajero de nuestras agencias partners, debe sentir que lo ofrecido a un precio determinado tiene más que ese valor.</li>
										<li>Nuestra negociación está enfocada en favor de nuestros pasajeros.</li>
										<li>La innovación y búsqueda de destinos o productos nuevos u olvidados.</li>
										<li>La experiencia y el enfoque de los productos a desarrollar esta plenamente pensado en el beneficio que tendrá nuestro cliente. Queremos que siempre viaje con Discover.</li>
									</ul>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<div id="Div-Staff" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="padding: 70px 0px 30px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-two title-h2 color-celeste" style="margin-top:0px;margin-bottom:0px;">
										<h2 class="title-heading-center">STAFF DISCOVER</h2>
									</div>
									<p style="text-align: center;">Somos un grupo de profesionales del turismo con destacadas labores en el rubro de viajes.</p>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-content-boxes content-boxes columns row fusion-columns-1 fusion-columns-total-3 fusion-content-boxes-1 content-boxes-icon-with-title content-left content-staff" data-animationOffset="100%" style="margin-top:0px;margin-bottom:60px;">
										<style type="text/css" scoped="scoped">
											.fusion-content-boxes-1 .heading h2{color:#333333;}
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading h2,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
											.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:after,
											.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:before,
											.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
											.fusion-content-boxes-1 .heading .heading-link:hover .content-box-heading {
												color: #a0ce4e;
											}
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
												color: #a0ce4e !important;
											}.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #0260a0;color: #ffffff;}.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: #ffffff;}
											.fusion-content-boxes-1 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
												background-color: #a0ce4e !important;
												border-color: #a0ce4e !important;
											}
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
												background-color: #a0ce4e !important;
											}
											.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
												border-color: #a0ce4e !important;
											}
										</style>
										<div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com
														<br />
														Skype: nachogarcia67
													</p>
												</div>
											</div>
										</div>

										<div class="fusion-clearfix"></div>
										<div class="fusion-column content-box-column content-box-column content-box-column-2 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com<br />
														Skype: nachogarcia67				
													</p>
												</div>
											</div>
										</div>
										<div class="fusion-clearfix"></div>
										<div class="fusion-column content-box-column content-box-column content-box-column-3 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com<br />
														Skype: nachogarcia67
													</p>
												</div>
											</div>
										</div>
										<div class="fusion-clearfix"></div>
										<div class="fusion-clearfix"></div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
							
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-content-boxes content-boxes columns row fusion-columns-1 fusion-columns-total-4 fusion-content-boxes-2 content-boxes-icon-with-title content-left content-staff" data-animationOffset="100%" style="margin-top:0px;margin-bottom:60px;">
										<style type="text/css" scoped="scoped">
											.fusion-content-boxes-2 .heading h2{color:#333333;}
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading h2,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
											.fusion-content-boxes-2 .fusion-content-box-hover .fusion-read-more:hover:after,
											.fusion-content-boxes-2 .fusion-content-box-hover .fusion-read-more:hover:before,
											.fusion-content-boxes-2 .fusion-content-box-hover .fusion-read-more:hover,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
											.fusion-content-boxes-2 .heading .heading-link:hover .content-box-heading {
												color: #a0ce4e;
											}
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
												color: #a0ce4e !important;
											}.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #0260a0;color: #ffffff;}.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: #ffffff;}
											.fusion-content-boxes-2 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
												background-color: #a0ce4e !important;
												border-color: #a0ce4e !important;
											}
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
												background-color: #a0ce4e !important;
											}
											.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
												border-color: #a0ce4e !important;
											}
										</style>
										<div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com<br />
														Skype: nachogarcia67
													</p>
												</div>
											</div>
										</div>
										<div class="fusion-clearfix"></div>
										<div class="fusion-column content-box-column content-box-column content-box-column-2 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com<br />
														Skype: nachogarcia67
													</p>
												</div>
											</div>
										</div>
										<div class="fusion-clearfix"></div>
										<div class="fusion-column content-box-column content-box-column content-box-column-3 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com<br />
														Skype: nachogarcia67
													</p>
												</div>
											</div>
										</div>
										<div class="fusion-clearfix"></div>
										<div class="fusion-column content-box-column content-box-column content-box-column-4 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com<br />
														Skype: nachogarcia67
													</p>
												</div>
											</div>
										</div>
										<div class="fusion-clearfix"></div>
										<div class="fusion-clearfix"></div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>

							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-content-boxes content-boxes columns row fusion-columns-1 fusion-columns-total-3 fusion-content-boxes-3 content-boxes-icon-with-title content-left content-staff" data-animationOffset="100%" style="margin-top:0px;margin-bottom:60px;">
										<style type="text/css" scoped="scoped">
											.fusion-content-boxes-3 .heading h2{color:#333333;}
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover .heading h2,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
											.fusion-content-boxes-3 .fusion-content-box-hover .fusion-read-more:hover:after,
											.fusion-content-boxes-3 .fusion-content-box-hover .fusion-read-more:hover:before,
											.fusion-content-boxes-3 .fusion-content-box-hover .fusion-read-more:hover,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
											.fusion-content-boxes-3 .heading .heading-link:hover .content-box-heading {
												color: #a0ce4e;
											}
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
												color: #a0ce4e !important;
											}.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #0260a0;color: #ffffff;}.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: #ffffff;}
											.fusion-content-boxes-3 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
												background-color: #a0ce4e !important;
												border-color: #a0ce4e !important;
											}
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
												background-color: #a0ce4e !important;
											}
											.fusion-content-boxes-3 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
												border-color: #a0ce4e !important;
											}
										</style>
										<div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com<br />
														Skype: nachogarcia67
													</p>
												</div>
											</div>
										</div>
										<div class="fusion-clearfix"></div>
										<div class="fusion-column content-box-column content-box-column content-box-column-2 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com<br />
														Skype: nachogarcia67
													</p>
												</div>
											</div>
										</div>
										<div class="fusion-clearfix"></div>
										<div class="fusion-column content-box-column content-box-column content-box-column-3 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last content-box-column-last-in-row">
											<div class="col content-wrapper link-area-link-icon icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
												<div class="heading icon-left">
													<h2 class="content-box-heading" style="font-size:18px;line-height:23px;">
														<b>Raymi Garcia Garcia-Bedoya</b>
														<small>Administracion &amp; RR.HH</small>
													</h2>
												</div>
												<div class="fusion-clearfix"></div>
												<div class="content-container" style="color:#747474;">
													<p>
														Teléfono: 952-902-418<br />
														Correo: ignacio@discovermayorista.com<br />
														Skype: nachogarcia67
													</p>
												</div>
											</div>
										</div>
										<div class="fusion-clearfix"></div>
										<div class="fusion-clearfix"></div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="Div-Hoteles-Proveedores" class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth"  style='background-color: #01bfcb;background-position: center center;background-repeat: no-repeat;padding-top:70px;padding-right:30px;padding-bottom:50px;padding-left:30px;margin-bottom: 5px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-tabs fusion-tabs-2 clean tabs-nosotros color-blanco horizontal-tabs">
										<style type="text/css">
											#wrapper .fusion-tabs.fusion-tabs-2.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-2 .nav-tabs li a{background-color:#ebeaea;}.fusion-tabs.fusion-tabs-2 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-2 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-2 .nav-tabs li.active a:focus{background-color:#ffffff;}.fusion-tabs.fusion-tabs-2 .nav-tabs li a:hover{background-color:#ffffff;border-top-color:#ffffff;}.fusion-tabs.fusion-tabs-2 .tab-pane{background-color:#ffffff;}.fusion-tabs.fusion-tabs-2 .nav,.fusion-tabs.fusion-tabs-2 .nav-tabs,.fusion-tabs.fusion-tabs-2 .tab-content .tab-pane{border-color:#ebeaea;}
										</style>

										<div class="nav">
											<ul class="nav-tabs nav-justified">
												<li class="active">
													<a class="tab-link" data-toggle="tab" id="fusion-tab-hoteles" href="#tab-4cec0c117c6c6dbbb54">
														<h4 class="fusion-tab-heading">Hoteles</h4>
													</a>
												</li>
												<li>
													<a class="tab-link" data-toggle="tab" id="fusion-tab-proveedores" href="#tab-2e9274aaf1f934bdbdd">
														<h4 class="fusion-tab-heading">Proveedores</h4>
													</a>
												</li>
											</ul>
										</div>
										<div class="tab-content">
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li class="active">
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-hoteles" href="#tab-4cec0c117c6c6dbbb54">
															<h4 class="fusion-tab-heading">Hoteles</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade in active" id="tab-4cec0c117c6c6dbbb54">
												<ul>
													<li>360 Experience</li>
													<li>Abreu</li>
													<li>Aquarel</li>
													<li>CL Mundo</li>
													<li>Collibri</li>
													<li>Convencional</li>
													<li>Destinos Costa Rica</li>
													<li>DMC</li>
													<li>Global Assist</li>
													<li>Grayline</li>
													<li>Hover Tours</li>
													<li>Infinitas</li>
													<li>Life Destionation</li>
													<li>MMC</li>
													<li>Nexus</li>
													<li>Otium</li>
													<li>Panamericana de viajes</li>
													<li>Rge Style</li>
													<li>Seven Tours</li>
													<li>Sistema Moderno</li>
													<li>Surland</li>
													<li>Trapsatur</li>
													<li>Travelplan</li>
													<li>Turar</li>
													<li>Welcomebeds</li>
												</ul>
											</div>
											<div class="nav fusion-mobile-tab-nav">
												<ul class="nav-tabs nav-justified">
													<li>
														<a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-proveedores" href="#tab-2e9274aaf1f934bdbdd">
															<h4 class="fusion-tab-heading">Proveedores</h4>
														</a>
													</li>
												</ul>
											</div>
											<div class="tab-pane fade" id="tab-2e9274aaf1f934bdbdd">
												<ul>
													<li>360 Experience</li>
													<li>Abreu</li>
													<li>Aquarel</li>
													<li>CL Mundo</li>
													<li>Collibri</li>
													<li>Convencional</li>
													<li>Destinos Costa Rica</li>
													<li>DMC</li>
													<li>Global Assist</li>
													<li>Grayline</li>
													<li>Hover Tours</li>
													<li>Infinitas</li>
													<li>Life Destionation</li>
													<li>MMC</li>
													<li>Nexus</li>
													<li>Otium</li>
													<li>Panamericana de viajes</li>
													<li>Rge Style</li>
													<li>Seven Tours</li>
													<li>Sistema Moderno</li>
													<li>Surland</li>
													<li>Trapsatur</li>
													<li>Travelplan</li>
													<li>Turar</li>
													<li>Welcomebeds</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<div id="Rev-Regulaciones" class="fusion-fullwidth fullwidth-box fusion-blend-mode fusion-parallax-none nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-image: url("{{ url('images/Banner-Nosotros-3.jpg') }}");background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-attachment:none;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="padding: 30px 0px 20px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-two title-h2 color-blanco" style="margin-top:0px;margin-bottom:15px;">
										<h2 class="title-heading-center">Regulaciones</h2>
									</div>
									<p style="text-align: center;">Parte de nuestra política de transparencia, ponemos a disposición nuestros términos y condiciones.</p>
									<div class="fusion-button-wrapper fusion-aligncenter">
										<style type="text/css" scoped="scoped">
											.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:#ffffff;}.fusion-button.button-1 {border-width:0px;border-color:#ffffff;}.fusion-button.button-1 .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#ffffff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#ffffff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#ffffff;}.fusion-button.button-1{width:auto;}
										</style>
										<a class="fusion-button button-flat fusion-button-round button-large button-default button-1 boton-4" target="_self" title="Leer documento" href="#">
											<span class="fusion-button-text">Leer documento</span>
										</a>
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

@stop

@section('script')
<script type='text/javascript' src="{{ url('js/5e65d4fdd65e17b76eb4aeb0f45769cb.js') }}"></script>
@stop
