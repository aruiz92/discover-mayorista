@foreach($packages as $key => $package)
<li class="paquete-item">
    <div class="paquete-main">
        <div class="paquete-header">
            <a class="paquete-titulo" href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">{{ $package->nombre }}</a>
            <div class="paquete-dias">{{ $package->nro_dias }}D — {{ $package->nro_noches }}N</div>
            <div class="paquete-precio">
                @if(!empty($package->precio_ser) && !is_null($package->precio_ser) && $package->precio_ser !== '0.00')
                    $ {{ $package->precio_ser }}
                @else
                    $ {{ $package->precio_min }}
                @endif
            </div>
            <div class="paquete-footer">
                <div class="paquete-leer">
                    <a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">Leer más</a>
                </div>
                <div class="paquete-cotizar">
                    <a class="btnSolicitar" data-toggle="modal" data-id="{{ $package->id }}" data-target=".fusion-modal.Form_Cotizar_1184" href="javascript:;">Cotizar</a>
                </div>
            </div>
        </div>
    </div>
    <div class="paquete-imagen">
        <img src="{{ url('files/packages/'.$package->imagen_1) }}" alt="{{ $package->nombre }}">
    </div>
</li>
@endforeach
