@extends('frontend.layout.layout-mundo-vacaciones')

@section('title', 'Mundo - Vacaciones')
@section('description', 'Discover')


@section('content')

@include('frontend.partials.vacaciones-header', ['destination' => $destination, 'destinations' => $destinations, 'package_types' => $packageTypes])
@include('frontend.partials.vacaciones-slider', ['sliders' => $package_type_banners])


<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
    <div class="fusion-row" style="max-width:100%;">
        <div id="content" class="full-width">
            <div id="post-563" class="post-563 page type-page status-publish hentry">
                <span class="entry-title rich-snippet-hidden">Familiar</span>
                <span class="vcard rich-snippet-hidden">
                    <span class="fn">
                        <a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
                    </span>
                </span>
                <span class="updated rich-snippet-hidden">2017-05-28T02:04:42+00:00 </span>

                <div class="post-content">
                    <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth subcategoria-maleta" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first fusion-spacing-no 1_3" style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 0 + 0 ) * 0.3333 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-spacing-no icon-sobre-slider 1_3" style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 0 + 0 ) * 0.3333 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="background-image: url('{{ url('images/fondo-titulo.png') }}');background-position:center top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                    data-bg-url="{{ url('images/fondo-titulo.png') }}">
                                    <div class="imageframe-align-center">
                                        <span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none">
                                            <img src="{{ url('images/icon-title-vacaciones.png') }}" width="68" height="56" alt="Paquetes Vacacionales" title="Paquetes Vacacionales" class="img-responsive wp-image-589"/>
                                        </span>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last fusion-spacing-no 1_3" style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 0 + 0 ) * 0.3333 ) );'>
                                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>

                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-spacing-no titulo-estilo-2 font-script 1_2" style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-two line-height-normal" style="margin-top:15px;margin-bottom:31px;">
                                        <h2 class="title-heading-center">Paquetes<br /> {{ $package_type->nombre }}</h2>
                                    </div>

                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );'>
                                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth tabs-paquetes" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:30px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:0px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-spacing-no 1_2" style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0 + 0 ) * 0.5 ) );margin-right: 0px;'>
                                <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">

                                    <!-- Page-list plugin v.5.1 wordpress.org/plugins/page-list/ -->
                                    @if($subTypes)
                                      <ul class="page-list menu-estilo-1" data-package-type="{{ $package_type_child->id }}">
                                          @foreach($subTypes as $key => $subType)
                                          <li class="page_item page-item-563{{ $subType->id == $package_type_child->id ? ' current_page_item':'' }}">
                                            <a href="{{ route('packageSubType', ['idD' => $destination->id, 'nameD' => str_slug($destination->nombre), 'idT' => $package_type->id, 'nameT' => str_slug($package_type->nombre), 'idS' => $subType->id, 'nameS' => str_slug($subType->nombre)]) }}">{{ $subType->nombre }}</a>
                                          </li>
                                          @endforeach
                                      </ul>
                                    @endif
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>

                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last fusion-spacing-no 1_4" style='margin-top:0px;margin-bottom:0px;width:25%;width:calc(25% - ( ( 0 + 0 ) * 0.25 ) );'>
                                <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-clearfix"></div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="Vacaciones_Div" class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1" style='margin-top:0px;margin-bottom:0px;'>
                                <div class="fusion-column-wrapper" style="background-color:#ffffff;padding: 20px 0px 10px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                    <div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-grid-wrapper fusion-blog-infinite paquetes-carrusel sin-carrusel">
                                        <style type="text/css">
                                            .fusion-blog-shortcode-1 .fusion-blog-layout-grid .fusion-post-grid{padding:7.5px;}.fusion-blog-shortcode-1 .fusion-posts-container{margin-left: -7.5px !important; margin-right:-7.5px !important;}
                                        </style>
                                        <div id="packagesubtypes" class="fusion-posts-container fusion-posts-container-infinite fusion-posts-container-load-more fusion-blog-layout-grid fusion-blog-layout-grid-4 isotope">
                                            <div class="paquete">
                                                <ul class="paquete-contenedor">
                                                    @foreach($packages as $package)
                                                        <li class="paquete-item">
                                                            <div class="paquete-main">
                                                                <div class="paquete-header">
                                                                    <a class="paquete-titulo" href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">{{ $package->nombre }}</a>
                                                                    <div class="paquete-dias">{{ $package->nro_dias }}D — {{ $package->nro_noches }}N</div>
                                                                    <div class="paquete-precio">
                                                                        @if(!empty($package->precio_ser) && !is_null($package->precio_ser) && $package->precio_ser !== '0.00')
                                                                            $ {{ $package->precio_ser }}
                                                                        @else
                                                                            $ {{ $package->precio_min }}
                                                                        @endif
                                                                    </div>
                                                                    <div class="paquete-footer">
                                                                        <div class="paquete-leer">
                                                                            <a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">Leer más</a>
                                                                        </div>
                                                                        <div class="paquete-cotizar">
                                                                            <a class="btnSolicitar" data-toggle="modal" data-id="{{ $package->id }}" data-target=".fusion-modal.Form_Cotizar_1184" href="javascript:;">Cotizar</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="paquete-imagen">
                                                                <img src="{{ url('files/packages/'.$package->imagen_1) }}" alt="{{ $package->nombre }}">
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="fusion-clearfix"></div>
                                        </div>
                                        <div class="fusion-infinite-scroll-trigger">

                                        </div>
                                        <div class="fusion-load-more-button fusion-blog-button fusion-clearfix"
                                             data-action="{{ url('api/packagesubtypes') }}"
                                             data-method="POST"
                                             data-des-id="{{ $destination->id }}"
                                             data-typ-id="{{ $package_type->id }}"
                                             data-sub-id="{{ $package_type_child->id }}"
                                             data-limit="{{ $limit }}">Ver más</div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- fusion-row -->
</div>
<!-- #main -->


<!-- Modal cotización -->
<div id="modalQuote" class="fusion-modal modal fade modal-1 Form_Cotizar_1184 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
	<style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
	<div class="modal-dialog modal-lg">
		<div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Solicita una cotización</h3>
			</div>
			<div class="modal-body">
				<h3 class="modal-title-quote frm-header">Cancun</h3>
        <div class="frm-response">
          <h2 class="text-center"></h2>
        </div>
				<h4 class="sub_titulo_post_form"></h4>
				<div role="form" class="wpcf7" id="wpcf7-f162-p1184-o1" lang="es-ES" dir="ltr">
					<div class="screen-reader-response"></div>
          <?php $action = Auth::check() ? url('api/quoteAuth') : url('api/quote'); ?>
					<form id="frmQuote" action="{{ $action }}" method="POST" class="wpcf7-form" novalidate="novalidate">
						<div style="display: none;">
							<input type="hidden" name="package_id" value="" />
						</div>
            @if(!Auth::check())
						<p>
							<label> Nombre (requerido)
								<br />
								<span class="wpcf7-form-control-wrap Nombre">
									<input type="text" name="nombres" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
								</span>
							</label>
						</p>
						<p>
							<label> Tu correo electrónico (requerido)
								<br />
								<span class="wpcf7-form-control-wrap Email">
									<input type="email" name="correo" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
								</span>
							</label>
						</p>
						<p class="group-pais">
							<label>País<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectPais" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="pais">
											<option value="">Seleccionar</option>
											@foreach($countries as $country)
												<option value="{{ $country->id }}">{{ $country->nombre }}</option>
											@endforeach
										</select>
									</span>
							</label>
						</p>
						<p class="group-provincia">
							<label>Provincia<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectProvincia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="provincia">
											<option value="">Seleccionar</option>
										</select>
									</span>
							</label>
						</p>
						<p class="group-distrito">
							<label>Distrito<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectDistrito" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="distrito">
											<option value="">Seleccionar</option>
										</select>
										<div class="select-arrow" style="height: 27px; width: 27px; line-height: 27px;"></div>
									</span>
							</label>
						</p>
						<p class="group-agencia">
							<label>Agencia<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectAgencia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="agencia">
											<option value="">Seleccionar</option>
										</select>
									</span>
							</label>
						</p>
            @endif
						<p>
							<label> Mensaje
								<br />
								<span class="wpcf7-form-control-wrap Mensaje">
									<textarea name="mensaje" rows="2" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" style="height: 100px;"></textarea>
								</span>
							</label>
						</p>
						<p>
							<span class="wpcf7-form-control-wrap Producto">
								<input type="hidden" name="Producto" value="Cancun" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
							</span>
							<span class="wpcf7-form-control-wrap URL">
								<input type="hidden" name="URL" value="" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
							</span>
						</p>
						<p style="text-align:right">
							<input id="btnSubmit" type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
						</p>
						<div class="wpcf7-response-output wpcf7-display-none"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


@stop

@section('style')
<link rel="stylesheet" href="{{ asset('css/slick.css')}}" />
<link rel="stylesheet" href="{{ asset('css/slick-theme.css')}}" />
<style type="text/css">
    .recentcomments a {
        display: inline !important;
        padding: 0 !important;
        margin: 0 !important;
    }
</style>
@stop

@section('script')
<script src="{{ url('js/slick.min.js') }}" charset="utf-8"></script>
<script type='text/javascript' src="{{ url('js/e08e754308f5132d5d069f552db3ad80.js') }}"></script>

<script type="text/javascript">
	var packages = JSON.parse('{!! json_encode($packages) !!}');
	var site_url = '{{ url('') }}/';
</script>

<?php if(Auth::check()): ?>
<script src="{{ url('js/quote-auth.js') }}" charset="utf-8"></script>
<?php else: ?>
<script src="{{ url('js/quote.js') }}" charset="utf-8"></script>
<?php endif; ?>

<script type="text/javascript">
jQuery.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
	}
});

var countLoad = 1;
var btnLoad = jQuery('.fusion-load-more-button');
var containerPackages = jQuery('.paquete .paquete-contenedor');

btnLoad.click(function (event) {

	var action = jQuery(this).attr('data-action');
	var method = jQuery(this).attr('data-method');
	var data = {
        destination: jQuery(this).attr('data-des-id'),
        type: jQuery(this).attr('data-typ-id'),
        sub: jQuery(this).attr('data-sub-id'),
        limit: jQuery(this).attr('data-limit'),
        page: countLoad
    };

	jQuery(this).html('Cargando...');
    jQuery('.fusion-loading-container').hide();

	jQuery.ajax({
		method: method,
		url: action,
		data: data,
		success: function (response) {
            if (response.load === true) {
                btnLoad.html('Ver más');
                countLoad++;
                containerPackages.append(response.view);
            } else {
                btnLoad.hide();
            }
		}
	})
	.fail(function(error) {
		var status = error.status;
		var data = error.responseJSON;

		btnLoad.html('Ver más');
	});

	event.preventDefault();
});
</script>
@stop
