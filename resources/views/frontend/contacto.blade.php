@extends('frontend.layout.layout-internal') @section('title', 'Contacto') @section('description', 'Discover') 

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-330.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/contacto.custom.css')}}" />
@stop

@section('custom-script')
<script type='text/javascript' src="{{ url('js/contact-script.js') }}"></script>
@stop

@section('style')

@stop

@section('body-style')
page-template page-template-100-width page-template-100-width-php page page-id-330 top-parent-330 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text do-animate
@stop


@section('content')

	@include('frontend.partials.header')
					
	<div id="sliders-container"></div>
						
	<div id="main" role="main" class="clearfix width-100" style="padding-left:20px;padding-right:20px">
		<div class="fusion-row" style="max-width:100%;">
			<div id="content" class="full-width">
				<div id="post-330" class="post-330 page type-page status-publish hentry">
					<span class="entry-title rich-snippet-hidden">Contacto</span>
					<span class="vcard rich-snippet-hidden">
						<span class="fn">
							<a href="{{ route('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
						</span>
					</span>
	
					<span class="updated rich-snippet-hidden">
						2017-05-21T11:31:04+00:00
					</span>

					<div class="post-content">
						<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode hundred-percent-fullwidth fusion-equal-height-columns"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;'>
							<div class="fusion-builder-row fusion-row ">
								<div id="Contacto-Form" class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
									<div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-image: url('{{ url('images/Fondo-Contacto.jpg') }}');background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="{{ url('images/Fondo-Contacto.jpg') }}">
										<div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
											<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-one-half fusion-column-first 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right:4%;'>
												<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
			<div class="imageframe-align-center">
				<span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none">
					<img src="{{ url('images/Icon-Form.png') }}" width="53" height="69" alt="" title="Icon-Form" class="img-responsive wp-image-344"/>
				</span>
			</div>
			<div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-two" style="margin-top:0px;margin-bottom:31px;">
				<h2 class="title-heading-center">¿Alguna consulta sobre los paquetes?</h2>
			</div>
			<p style="text-align: center;">
				Déjenos un mensaje y nuestras <strong>agencias partners</strong> se contactarán contigo
			</p>
				<div role="form" class="wpcf7" id="wpcf7-f5-p330-o1" lang="es-ES" dir="ltr">
					<div class="screen-reader-response"></div>
					<form action="/discover/contacto/#wpcf7-f5-p330-o1" method="post" class="wpcf7-form" novalidate="novalidate">
						<div style="display: none;">
							<input type="hidden" name="_wpcf7" value="5" />
							<input type="hidden" name="_wpcf7_version" value="4.9.2" />
							<input type="hidden" name="_wpcf7_locale" value="es_ES" />
							<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f5-p330-o1" />
							<input type="hidden" name="_wpcf7_container_post" value="330" />
						</div>
						<div class="col-sm-6">
							<span class="wpcf7-form-control-wrap Nombres">
								<input type="text" name="Nombres" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nombres" />
							</span>
						</div>
						<div class="col-sm-6">
							<span class="wpcf7-form-control-wrap Apellidos">
								<input type="text" name="Apellidos" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Apellidos" />
							</span>
						</div>
						<div class="fusion-sep-clear"></div>
							<div class="col-sm-6">
								<span class="wpcf7-form-control-wrap Telefono">
									<input type="text" name="Telefono" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Teléfono" />
								</span>
							</div>
							<div class="col-sm-6">
								<span class="wpcf7-form-control-wrap Distrito">
									<input type="text" name="Distrito" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Distrito" />
								</span>
							</div>
						<div class="fusion-sep-clear"></div>
						<div class="col-sm-6">
							<span class="wpcf7-form-control-wrap E-mail">
								<input type="email" name="E-mail" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email" />
							</span>
						</div>
						<div class="col-sm-6">
							<span class="wpcf7-form-control-wrap Agente">
								<select name="Agente" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="">---</option><option value="Agente1">Agente1</option><option value="Agente2">Agente2</option><option value="Agente3">Agente3</option></select>
							</span>
						</div>
						<div class="fusion-sep-clear"></div>
						<div class="col-sm-12">
							<span class="wpcf7-form-control-wrap Consulta">
								<textarea name="Consulta" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Deja tu consulta"></textarea>
							</span>
						</div>
						<div class="fusion-sep-clear"></div>
						<div class="col-sm-12" style="text-align:center">
							<input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
						</div>
						<div class="fusion-sep-clear"></div>
						<div class="wpcf7-response-output wpcf7-display-none"></div>
					</form>
				</div>
				<div class="fusion-sep-clear"></div>
				<div class="fusion-separator sep-single sep-solid" style="border-color:#000000;border-top-width:1px;margin-left: auto;margin-right: auto;margin-top:10px;margin-bottom:10px;width:100%;max-width:65%;"></div>
				<div class="fusion-content-boxes content-boxes columns row fusion-columns-2 fusion-columns-total-2 fusion-content-boxes-1 content-boxes-icon-with-title content-left" data-animationOffset="100%" style="margin-top:0px;margin-bottom:0px;">
					<style type="text/css" scoped="scoped">
						.fusion-content-boxes-1 .heading h2{color:#333333;}
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading h2,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
						.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:after,
						.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:before,
						.fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
						.fusion-content-boxes-1 .heading .heading-link:hover .content-box-heading {
							color: #a0ce4e;
						}
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
							color: #a0ce4e !important;
						}.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #0260a0;color: #ffffff;}.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: #ffffff;}
						.fusion-content-boxes-1 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
							background-color: #a0ce4e !important;
							border-color: #a0ce4e !important;
						}
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
							background-color: #a0ce4e !important;
						}
						.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
							border-color: #a0ce4e !important;
						}
					</style>
					<div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-6 col-md-6 col-sm-6 fusion-content-box-hover  content-box-column-first-in-row">
						<div class="col content-wrapper link-area-link-icon link-type-button icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
							<div class="heading icon-left">
								<a class="heading-link" class="fusion-read-more-button fusion-content-box-button fusion-button button-default button-large button-round button-flat" style="float:left;" href="#" target="_self"></a>
							</div>
							<div class="fusion-clearfix"></div>
							<div class="content-container" style="color:#747474;">
								<p>¿Eres uno de nuestros agentes asociados?</p>
							</div>
							<div class="fusion-clearfix"></div>
							<a class="fusion-read-more-button fusion-content-box-button fusion-button button-default button-large button-round button-flat" style="float:left;" href="#" target="_self">
								<span class="fusion-button-text">Registrate aquí</span>
							</a>
							<div class="fusion-clearfix"></div>
						</div>
					</div>
					<div class="fusion-column content-box-column content-box-column content-box-column-2 col-lg-6 col-md-6 col-sm-6 fusion-content-box-hover  content-box-column-last content-box-column-last-in-row">
						<div class="col content-wrapper link-area-link-icon link-type-button icon-hover-animation-fade" style="background-color:rgba(255,255,255,0);" data-animationOffset="100%">
							<div class="heading icon-left">
								<a class="heading-link" class="fusion-read-more-button fusion-content-box-button fusion-button button-default button-large button-round button-flat" style="float:left;" href="#" target="_self"></a>
							</div>
							<div class="fusion-clearfix"></div>
							<div class="content-container" style="color:#747474;">
								<p>¿Te gusta viajar y deseas acceder a promociones?</p>
							</div>
							<div class="fusion-clearfix"></div>
							<a class="fusion-read-more-button fusion-content-box-button fusion-button button-default button-large button-round button-flat" style="float:left;" href="#" target="_self"><span class="fusion-button-text">Suscríbete</span></a>
							<div class="fusion-clearfix"></div>
						</div>
					</div>
					<div class="fusion-clearfix"></div>
					<div class="fusion-clearfix"></div>
				</div>
			</div>
												</div>
												<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-one-half fusion-column-last 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
													<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
													</div>
												</div>
											</div>
											<div class="fusion-clearfix"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>  <!-- fusion-row -->
		</div>  <!-- #main -->
			

@stop

@section('script')

<script type='text/javascript' src="{{ url('js/7fe5606794b62066a68cdda469435491.js') }}"></script>

@stop
