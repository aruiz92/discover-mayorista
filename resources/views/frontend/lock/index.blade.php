@extends('frontend.layout.layout-internal')

@section('title')
{{ ($modo === 'bloqueos') ? 'Bloqueos':'Promociones' }}
@endsection

@section('description', 'Una ruta. Varios destinos. Disfruta de tus vacaciones, en los mejores lugares.')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-237.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/bloqueos.custom.css')}}" />
<link rel="stylesheet" href="{{ asset('v2/css/app.css')}}" />
@stop

@section('custom-script')
<script src="{{ url('js/slick.min.js') }}" charset="utf-8"></script>
<script type='text/javascript' src="{{ url('js/bloqueos.custom.js') }}"></script>
@stop

@section('style')
<link rel="stylesheet" href="{{ asset('css/slick.css')}}" />
<link rel="stylesheet" href="{{ asset('css/slick-theme.css')}}" />
@stop

@section('body-style')
ds-nosotros page-template-default page page-id-237 top-parent-237 fusion-image-hovers seccion-peru fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text
@stop

@section('content')

@include('frontend.partials.header')

<div class="ds-sliders">
	@foreach($sliders as $slider)
		<div class="ds-slider-item" style="background-image: url({{ url('files/sliders/'.$slider->imagen) }});">
			<div class="ds-slider-item--detalle">
				<div class="ds-slider-item--titulo">{{ $slider->texto_2 }}</div>
				<div class="ds-slider-item--parrafo">{{ $slider->texto_3 }}</div>
			</div>
		</div>
	@endforeach
</div>

<script>
jQuery(document).ready(function () {
	jQuery('.ds-sliders').slick({
		infinite: true,
		dots: false,
		arrows: true,
		autoplay: true,
		fade: true,
		autoplaySpeed: 9000,
		slidesToShow: 1,
		slidesToScroll: 1
	});
});
</script>

<div id="main" role="main" class="clearfix " style="">
	<div class="fusion-row" style="">
		<div id="content" style="width: 100%;">
			<div id="post-237" class="post-237 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">Bloqueos</span>
				<span class="vcard rich-snippet-hidden">
					<span class="fn">
						<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
					</span>
				</span>
				<span class="updated rich-snippet-hidden">2017-05-20T12:10:18+00:00</span>

				<div class="post-content">
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-widget-area fusion-widget-area-1 fusion-content-widget-area">
										<style type="text/css" scoped="scoped">
											.fusion-widget-area-1 {padding:0px 0px 0px 0px;}.fusion-widget-area-1 .widget h4 {color:#333333;}.fusion-widget-area-1 .widget .heading h4 {color:#333333;}
										</style>
										<div id="bcn_widget-2" class="widget widget_breadcrumb_navxt">
											<div class="breadcrumbs" vocab="https://schema.org/" typeof="BreadcrumbList">
												<!-- Breadcrumb NavXT 6.0.4 -->
												<span property="itemListElement" typeof="ListItem">
													<a property="item" typeof="WebPage" title="Ir a Discover." href="{{ route('home') }}" class="home">
														<span property="name">Inicio</span>
													</a>
													<meta property="position" content="1">
												</span>
												>
												<span property="itemListElement" typeof="ListItem">
													<span property="name">{{ ($modo === 'bloqueos') ? 'Bloqueos':'Promociones' }}</span>
													<meta property="position" content="2">
												</span>
											</div>
										</div>
										<div class="fusion-additional-widget-content"></div>
									</div>

									<div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-large fusion-blog-pagination blog-tipo1">
										<div class="fusion-posts-container fusion-posts-container-pagination" data-pages="3">
											@foreach($packages as $package)
											<article id="post-{{ $package->id }}" class="fusion-post-large post-{{ $package->id }} post type-post status-publish format-standard has-post-thumbnail hentry category-bloqueos categoria-id-7">
												<style type="text/css"></style>
												<div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
													<ul class="slides">
														<li>
															<div class="fusion-image-wrapper" aria-haspopup="true">
																<a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">
																	<img width="797" height="531" src="{{ url('files/packages/secundario/'.$package->imagen_2) }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
																</a>
															</div>
														</li>
													</ul>
												</div>

												<div class="fusion-post-content post-content">
													<h2 class="blog-shortcode-post-title entry-title">
														<a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">{{ $package->nombre }}</a>
													</h2>
													<h4 id="Fecha-paq" class="fecha-inicio-fin">
														<strong>
														{{ $package->nro_dias }} <span class="span-dias"> días</span>
														</strong>
														<strong>
														{{ $package->nro_noches }} <span class="span-noches"> noches</span>
														</strong>
														@if(!empty($package->precio_ser) && !is_null($package->precio_ser) && $package->precio_ser !== '0.00')
														<div class="precio-paquetes">
															<p>Desde USD {{ $package->precio_ser }} solo servicios</p>
															<p>Con boleto aereo USD {{ $package->precio_bol }}</p>
														</div>
														@else
														<div class="precio-paquetes">
															<p>Desde ${{ $package->precio_min }}</p>
														</div>
														@endif
													</h4>
													<!-- <div class="subtitulo-post">USA</div> -->
													<div class="fusion-post-content-container">
														<p>{!! $package->descripcion !!}</p>
													</div>
													@if(Auth::check())
														<div class="descargar_pdf">
																@foreach($package->files as $file)
																	<a href="{{ url('files/packages/documents/'.$file->archivo) }}" target="_blank" class="boton-1" download>{{ $file->nombre }}</a>
																	<br><br>
																@endforeach
														</div>
													@endif
													<a class="fusion-modal-text-link boton-2 btnSolicitar" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar_1184" data-id="{{ $package->id }}" href="#">Solicitar Cotización</a>
													<div class="div-leermas">
														<a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}" class="link-leermas">Leer más</a>
													</div>
												</div>
												<div class="fusion-clearfix"></div>
											</article>
											@endforeach
										</div>

										@include('frontend.partials.pagination', ['paginator' => $packages])
									</div>
									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->

<!-- Modal cotización -->
<div id="modalQuote" class="fusion-modal modal fade modal-1 Form_Cotizar_1184 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
	<style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
	<div class="modal-dialog modal-lg">
		<div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Solicita una cotización</h3>
			</div>
			<div class="modal-body">
				<h3 class="modal-title-quote frm-header">Cancun</h3>
				<div class="frm-response">
          <h2 class="text-center"></h2>
        </div>
				<h4 class="sub_titulo_post_form"></h4>
				<div role="form" class="wpcf7" id="wpcf7-f162-p1184-o1" lang="es-ES" dir="ltr">
					<div class="screen-reader-response"></div>
					<?php $action = Auth::check() ? url('api/quoteAuth') : url('api/quote'); ?>
					<form id="frmQuote" action="{{ $action }}" method="POST" class="wpcf7-form" novalidate="novalidate">
						<div style="display: none;">
							<input type="hidden" name="package_id" value="" />
						</div>
						@if(!Auth::check())
						<p>
							<label> Nombre (requerido)
								<br />
								<span class="wpcf7-form-control-wrap Nombre">
									<input type="text" name="nombres" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
								</span>
							</label>
						</p>
						<p>
							<label> Tu correo electrónico (requerido)
								<br />
								<span class="wpcf7-form-control-wrap Email">
									<input type="email" name="correo" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
								</span>
							</label>
						</p>
						<p class="group-pais">
							<label>País<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectPais" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="pais">
											<option value="">Seleccionar</option>
											@foreach($countries as $country)
												<option value="{{ $country->id }}">{{ $country->nombre }}</option>
											@endforeach
										</select>
									</span>
							</label>
						</p>
						<p class="group-provincia">
							<label>Provincia<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectProvincia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="provincia">
											<option value="">Seleccionar</option>
										</select>
									</span>
							</label>
						</p>
						<p class="group-distrito">
							<label>Distrito<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectDistrito" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="distrito">
											<option value="">Seleccionar</option>
										</select>
										<div class="select-arrow" style="height: 27px; width: 27px; line-height: 27px;"></div>
									</span>
							</label>
						</p>
						<p class="group-agencia">
							<label>Agencia<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectAgencia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="agencia">
											<option value="">Seleccionar</option>
										</select>
									</span>
							</label>
						</p>
						@endif
						<p>
							<label> Mensaje
								<br />
								<span class="wpcf7-form-control-wrap Mensaje">
									<textarea name="mensaje" rows="2" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" style="height: 100px;"></textarea>
								</span>
							</label>
						</p>
						<p>
							<span class="wpcf7-form-control-wrap Producto">
								<input type="hidden" name="Producto" value="Cancun" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
							</span>
							<span class="wpcf7-form-control-wrap URL">
								<input type="hidden" name="URL" value="" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
							</span>
						</p>
						<p style="text-align:right">
							<input id="btnSubmit" type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
						</p>
						<div class="wpcf7-response-output wpcf7-display-none"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@stop


@section('style')
<link rel="stylesheet" href="{{ asset('css/fusion-237.css')}}" />
@stop


@section('script')

<script type='text/javascript' src="{{ url('js/e08e754308f5132d5d069f552db3ad80.js') }}"></script>
<script type="text/javascript">
	var packages = JSON.parse('{!! $packages->toJson() !!}');
	packages = packages.data;
	var site_url = '{{ url('') }}/';
</script>

<?php if(Auth::check()): ?>
<script src="{{ url('js/quote-auth.js') }}" charset="utf-8"></script>
<?php else: ?>
<script src="{{ url('js/quote.js') }}" charset="utf-8"></script>
<?php endif; ?>
@stop
