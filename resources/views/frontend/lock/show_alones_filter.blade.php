@extends('frontend.layout.layout-internal')

@section('title')
{{ ($modo === 'bloqueos') ? 'Bloqueos':'Promociones' }}
@endsection

@section('description', 'Una ruta. Varios destinos. Disfruta de tus vacaciones, en los mejores lugares.')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-237.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/bloqueos.custom.css')}}" />
<link rel="stylesheet" href="{{ asset('v2/css/app.css')}}" />
@stop

@section('custom-script')
<script src="{{ url('js/slick.min.js') }}" charset="utf-8"></script>
<script type='text/javascript' src="{{ url('js/bloqueos.custom.js') }}"></script>
@stop

@section('style')
<link rel="stylesheet" href="{{ asset('css/slick.css')}}" />
<link rel="stylesheet" href="{{ asset('css/slick-theme.css')}}" />
@stop

@section('body-style')
ds-nosotros page-template-default page page-id-237 top-parent-237 fusion-image-hovers seccion-peru fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text
@stop

@section('content')

@include('frontend.partials.header')

<div class="ds-sliders">
	@foreach($sliders as $slider)
		<div class="ds-slider-item" style="background-image: url({{ url('files/sliders/'.$slider->imagen) }});">
			<div class="ds-slider-item--detalle">
				<div class="ds-slider-item--titulo">{{ $slider->texto_2 }}</div>
				<div class="ds-slider-item--parrafo">{{ $slider->texto_3 }}</div>
			</div>
		</div>
	@endforeach
</div>

<script>
jQuery(document).ready(function () {
	jQuery('.ds-sliders').slick({
		infinite: true,
		dots: false,
		arrows: true,
		autoplay: true,
		fade: true,
		autoplaySpeed: 9000,
		slidesToShow: 1,
		slidesToScroll: 1
	});
});
</script>

<!--contenido general del buscado -->
	@include('frontend.lock.partials.content_search')
<!-- #main -->
<!--termina el contendo del buscado-->

<!-- Modal cotización -->
  @include('frontend.lock.partials.modal_cotizacion')
<!-- Termino Modal cotización -->

@stop

@section('style')
<link rel="stylesheet" href="{{ asset('css/fusion-237.css')}}" />
@stop


@section('script')

<script type='text/javascript' src="{{ url('js/e08e754308f5132d5d069f552db3ad80.js') }}"></script>
<script type="text/javascript">
	var packages = JSON.parse('{!! $packages->toJson() !!}');
	packages = packages.data;
	var site_url = '{{ url('') }}/';
</script>

<?php if(Auth::check()): ?>
<script src="{{ url('js/quote-auth.js') }}" charset="utf-8"></script>
<?php else: ?>
<script src="{{ url('js/quote.js') }}" charset="utf-8"></script>
<?php endif; ?>
@stop
