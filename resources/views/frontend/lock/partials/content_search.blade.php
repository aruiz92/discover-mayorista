<div id="main" role="main" class="clearfix " style="">
	<div class="fusion-row" style="">
		<div id="content" style="width: 100%;">
			<div id="post-237" class="post-237 page type-page status-publish hentry">
				<span class="entry-title rich-snippet-hidden">Bloqueos</span>
				<span class="vcard rich-snippet-hidden">
					<span class="fn">
						<a href="{{ url('mayorista') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
					</span>
				</span>
				<span class="updated rich-snippet-hidden">2017-05-20T12:10:18+00:00</span>

				<div class="post-content">
					<div  class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth"  style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;'>
						<div class="fusion-builder-row fusion-row ">
							<div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
								<div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
									<div class="fusion-widget-area fusion-widget-area-1 fusion-content-widget-area">
										<style type="text/css" scoped="scoped">
											.fusion-widget-area-1 {padding:0px 0px 0px 0px;}.fusion-widget-area-1 .widget h4 {color:#333333;}.fusion-widget-area-1 .widget .heading h4 {color:#333333;}
										</style>
										<div id="bcn_widget-2" class="widget widget_breadcrumb_navxt">
											<div class="breadcrumbs" vocab="https://schema.org/" typeof="BreadcrumbList">
												<!-- Breadcrumb NavXT 6.0.4 -->
												<span property="itemListElement" typeof="ListItem">
													<a property="item" typeof="WebPage" title="Ir a Discover." href="{{ route('home') }}" class="home">
														<span property="name">Inicio</span>
													</a>
													<meta property="position" content="1">
												</span>
												>
												<span property="itemListElement" typeof="ListItem">
													<span property="name">{{ $modo }}</span>
													<meta property="position" content="2">
												</span>
											</div>
										</div>
										<div class="fusion-additional-widget-content"></div>
									</div>

                  <!-- RESULTADO DEL BUSCADOR-->
                 @include('frontend.lock.partials.resul_search')
                 <!-- FIN RESULTADO DEL BUSCADOR-->

									<div class="fusion-clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- fusion-row -->
</div>
<!-- #main -->
