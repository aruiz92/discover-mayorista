<div id="modalQuote" class="fusion-modal modal fade modal-1 Form_Cotizar_1184 max-width-500" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
	<style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
	<div class="modal-dialog modal-lg">
		<div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Solicita una cotización</h3>
			</div>
			<div class="modal-body">
				<h3 class="modal-title-quote frm-header">Cancun</h3>
				<div class="frm-response">
          <h2 class="text-center"></h2>
        </div>
				<h4 class="sub_titulo_post_form"></h4>
				<div role="form" class="wpcf7" id="wpcf7-f162-p1184-o1" lang="es-ES" dir="ltr">
					<div class="screen-reader-response"></div>
					<?php $action = Auth::check() ? url('api/quoteAuth') : url('api/quote'); ?>
					<form id="frmQuote" action="{{ $action }}" method="POST" class="wpcf7-form" novalidate="novalidate">
						<div style="display: none;">
							<input type="hidden" name="package_id" value="" />
						</div>
						@if(!Auth::check())
						<p>
							<label> Nombre (requerido)
								<br />
								<span class="wpcf7-form-control-wrap Nombre">
									<input type="text" name="nombres" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
								</span>
							</label>
						</p>
						<p>
							<label> Tu correo electrónico (requerido)
								<br />
								<span class="wpcf7-form-control-wrap Email">
									<input type="email" name="correo" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
								</span>
							</label>
						</p>
						<p class="group-pais">
							<label>País<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectPais" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="pais">
											<option value="">Seleccionar</option>
											@foreach($countries as $country)
												<option value="{{ $country->id }}">{{ $country->nombre }}</option>
											@endforeach
										</select>
									</span>
							</label>
						</p>
						<p class="group-provincia">
							<label>Provincia<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectProvincia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="provincia">
											<option value="">Seleccionar</option>
										</select>
									</span>
							</label>
						</p>
						<p class="group-distrito">
							<label>Distrito<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectDistrito" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="distrito">
											<option value="">Seleccionar</option>
										</select>
										<div class="select-arrow" style="height: 27px; width: 27px; line-height: 27px;"></div>
									</span>
							</label>
						</p>
						<p class="group-agencia">
							<label>Agencia<br/>
									<span class="wpcf7-form-control-wrap">
										<select id="selectAgencia" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" name="agencia">
											<option value="">Seleccionar</option>
										</select>
									</span>
							</label>
						</p>
						@endif
						<p>
							<label> Mensaje
								<br />
								<span class="wpcf7-form-control-wrap Mensaje">
									<textarea name="mensaje" rows="2" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" style="height: 100px;"></textarea>
								</span>
							</label>
						</p>
						<p>
							<span class="wpcf7-form-control-wrap Producto">
								<input type="hidden" name="Producto" value="Cancun" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
							</span>
							<span class="wpcf7-form-control-wrap URL">
								<input type="hidden" name="URL" value="" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamichidden" aria-invalid="false" />
							</span>
						</p>
						<p style="text-align:right">
							<input id="btnSubmit" type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit" />
						</p>
						<div class="wpcf7-response-output wpcf7-display-none"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
