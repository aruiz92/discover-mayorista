<div class="fusion-blog-shortcode fusion-blog-shortcode-1 fusion-blog-archive fusion-blog-layout-large fusion-blog-pagination blog-tipo1">
  <div class="fusion-posts-container fusion-posts-container-pagination" data-pages="3">
    @foreach($packages as $package)
    <article id="post-{{ $package->id }}" class="fusion-post-large post-{{ $package->id }} post type-post status-publish format-standard has-post-thumbnail hentry category-bloqueos categoria-id-7">
      <style type="text/css"></style>
      <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
        <ul class="slides">
          <li>
            <div class="fusion-image-wrapper" aria-haspopup="true">
              <a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">
                <img width="797" height="531" src="{{ url('files/packages/secundario/'.$package->imagen_2) }}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="" />
              </a>
            </div>
          </li>
        </ul>
      </div>

      <div class="fusion-post-content post-content">
        <h2 class="blog-shortcode-post-title entry-title">
          <a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}">{{ $package->nombre }}</a>
        </h2>
        <h4 id="Fecha-paq" class="fecha-inicio-fin">
          <strong>
          {{ $package->nro_dias }} <span class="span-dias"> días</span>
          </strong>
          <strong>
          {{ $package->nro_noches }} <span class="span-noches"> noches</span>
          </strong>
          @if(!empty($package->precio_ser) && !is_null($package->precio_ser) && $package->precio_ser !== '0.00')
          <div class="precio-paquetes">
            <p>Desde USD {{ $package->precio_ser }} solo servicios</p>
            <p>Con boleto aereo USD {{ $package->precio_bol }}</p>
          </div>
          @else
          <div class="precio-paquetes">
            <p>Desde ${{ $package->precio_min }}</p>
          </div>
          @endif
        </h4>
        <!-- <div class="subtitulo-post">USA</div> -->
        <div class="fusion-post-content-container">
          <p>{!! $package->descripcion !!}</p>
        </div>
        {{-- @if(Auth::check())
          <div class="descargar_pdf">
              @foreach($package->files as $file)
                <a href="{{ url('files/packages/documents/'.$file->archivo) }}" target="_blank" class="boton-1" download>{{ $file->nombre }}</a>
                <br><br>
              @endforeach
          </div>
        @endif --}}
        @if(Auth::check())
        <br>
          @foreach($package->files as $file)
            <a href="{{ url('files/packages/documents/'.$file->archivo) }}" target="_blank" class="boton-1" style="margin-bottom: 1rem;" download>{{ $file->nombre }}</a>
          @endforeach
        <br>
          <?php
            $flyers = explode(',', $package->flyers);
          ?>
          @if(!empty($package->flyers) && !is_null($package->flyers))
            @for($i = 0; $i < count($flyers); $i++)
            <a href="{{ url('files/flyers/'.$flyers[$i]) }}" target="_blank" class="boton-1" style="margin-bottom: 1rem;" download>Flyers</a>
            @endfor
          @endif
        @endif




        <a class="fusion-modal-text-link boton-2 btnSolicitar" data-toggle="modal" data-target=".fusion-modal.Form_Cotizar_1184" data-id="{{ $package->id }}" href="#">Solicitar Cotización</a>
        <div class="div-leermas">
          <a href="{{ route('package', ['idPackage' => $package->id, 'urlPakage' => str_slug($package->nombre)]) }}" class="link-leermas">Leer más</a>
        </div>
      </div>
      <div class="fusion-clearfix"></div>
    </article>
    @endforeach
  </div>

  @include('frontend.partials.pagination', ['paginator' => $packages->appends(['listpackages' => $listpackages,'fecha_salida' => $mes,'cant_dias' => $dias])])

</div>
