@extends('admin.layouts.app')

@section('title')
  Suscriptor #{{ $subscriber->id }}
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Suscriptor #{{ $subscriber->id }}</h1>

    <div class="actions">
      <a href="{{ url('dashboard/subscribers') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Información</h4>
      <ul>
        <li><strong>Nombres:</strong> {{ $subscriber->nombres }}</li>
        <li><strong>Apellidos:</strong> {{ $subscriber->apellidos }}</li>
        <li><strong>Correo:</strong> {{ $subscriber->correo }}</li>
        <li><strong>Fecha de nacimiento:</strong> {{ $subscriber->fecnac }}</li>
        <li><strong>Teléfono:</strong> {{ $subscriber->telefono }}</li>
        <li><strong>Dirección:</strong> {{ $subscriber->direccion }}</li>
        <li><strong>Distrito:</strong> {{ $subscriber->district->nombre }}</li>
        <li><strong>Provincia:</strong> {{ $subscriber->district->province->nombre }}</li>
        <li><strong>País:</strong> {{ $subscriber->district->province->country->nombre }}</li>
      </ul>
    </div>
  </div>
</div>
@endsection

@section('scripts')
@endsection
