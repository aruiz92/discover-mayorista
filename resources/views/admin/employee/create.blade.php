@extends('admin.layouts.app')

@section('title', 'Crear Colaborador')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/dropzone/dist/dropzone.css') }}">
<style media="screen">
.fotografia {
  margin-bottom: 40px;
}
.dropzone .dz-preview .dz-image img {
  width: 120px;
}
</style>
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Crear Colaborador</h1>

    <div class="actions">
      <a href="{{ url('dashboard/employees') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <form id="frmEmployee" action="{{ url('dashboard/employees') }}" method="post">
        {{ csrf_field() }}

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombres">
              <label>Nombres</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="apellidos">
              <label>Apellidos</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="correo">
              <label>Correo</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="telefono">
              <label>Teléfono</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-4">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="skype">
              <label>Skype</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="cargo">
              <label>Cargo</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="orden">
              <label>Orden</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>


        <div class="form-group">
          <div id="fotografia" class="dropzone"></div>
          <input type="hidden" class="form-control" name="foto" value="">
          <small id="emailHelp" class="form-text text-muted">Recomendaciones: 500x500 JPG, PNG</small>
          <div class="invalid-feedback"></div>
        </div>

        <button type="submit" class="btn btn-primary">Crear</button>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
Dropzone.autoDiscover = false;

const site_url = '{{ url('/') }}/';
const _token = '{{ csrf_token() }}';
const frmEmployee = '#frmEmployee';
let fotoDropzone = [];

$(document).ready(function () {
  fotoDropzone = new Dropzone('#fotografia', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilesPackage',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: null,
    dictDefaultMessage: 'Selecciona o arrastra la fotografía',
    acceptedFiles: 'image/*',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.on('removedfile', function (file) {
        if (fotoDropzone.files.length === 0) {
          $('input[name="foto"]').val('');
        }
      });

      dropZone.on('addedfile', function(file) {
        if (dropZone.files.length > 1) {
          swal({
            title: 'Estas seguro de cambiar?',
            text: 'Si procede, se cambiará la fotografía actual',
            type: 'info',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, continuar!',
            cancelButtonClass: 'btn btn-secondary'
          }).then(function (response) {
            if (response.value === true) {
              dropZone.removeAllFiles();
              dropZone.addFile(file);
            } else {
              dropZone.removeFile(file);
            }
          });
        }
      });
    },
    success: function (file, response) {
      $('input[name="foto"]').val(response.fileName);
    }
  });
});

$(frmEmployee).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmEmployee);

    swal({
      title: 'Excelente!',
      text: 'Hemos recibido la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    })
    .then(function (response) {
      window.location.href = site_url + 'dashboard/employees';
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmEmployee);

    if (status === 422) {
      errorAjax(frmEmployee, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    let input, msg, parent;

    input = $(frm + ' *[name="' + item + '"]');
    parent = $(input).parent()[0];

    if (item === 'foto') {
      msg = $(parent).children()[3];
    } else {
      msg = $(parent).children()[3];
    }

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
