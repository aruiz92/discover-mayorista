@extends('admin.layouts.app')

@section('title', 'Crear País')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Crear País</h1>

    <div class="actions">
      <a href="{{ url('dashboard/countries') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <form id="frmCountry" action="{{ url('dashboard/countries') }}" method="post">
        {{ csrf_field() }}

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombre">
              <label>Nombre</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <button type="submit" class="btn btn-primary">Crear</button>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>

<script type="text/javascript">

const site_url = '{{ url('/') }}/';
const frmCountry = '#frmCountry';

$(frmCountry).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmCountry);

    swal({
      title: 'Excelente!',
      text: 'Hemos recibido la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    })
    .then(function (response) {
      window.location.href = site_url + 'dashboard/countries';
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmCountry);

    if (status === 422) {
      errorAjax(frmCountry, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    const input = $(frm + ' *[name="' + item + '"]');
    const parent = $(input).parent()[0];
    const msg = $(parent).children()[3];

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
