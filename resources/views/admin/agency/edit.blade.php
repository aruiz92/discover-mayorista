@extends('admin.layouts.app')

@section('title', 'Editar Agencia')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/dropzone/dist/dropzone.css') }}">
<style media="screen">
.dropzone .dz-preview .dz-image img {
  width: 120px;
}
</style>
@endsection

@section('content')
<header class="content__title">
  <h1>Editar Agencia</h1>

  <div class="actions">
    <a href="{{ url('dashboard/agencies') }}" class="actions__item zmdi zmdi-arrow-left"></a>
  </div>
</header>


<form id="frmAgency" class="d-flex flex-wrap" action="{{ url('dashboard/agencies/'.$agency->id) }}" method="post">
  {{ csrf_field() }}
  <input type="hidden" name="_method" value="PUT">
  <input type="hidden" name="id" value="{{ $agency->id }}">

  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Agencia</h4>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="usuario" value="{{ $agency->usuario }}">
              <label>Usuario</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="correo" value="{{ $agency->correo }}">
              <label>Correo</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="ruc" value="{{ $agency->ruc }}">
              <label>RUC</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="razon_social" value="{{ $agency->razon_social }}">
              <label>Razón Social</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombre_comercial" value="{{ $agency->nombre_comercial }}">
              <label>Nombre Comercial</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombres" value="{{ $agency->nombres }}">
              <label>Nombres del Responsable</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="telefono" value="{{ $agency->telefono }}">
              <label>Teléfono</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="celular" value="{{ $agency->celular }}">
              <label>Celular</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="direccion_1" value="{{ $agency->direccion_1 }}">
              <label>Dirección (Linéa 1)</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="direccion_2" value="{{ $agency->direccion_2 }}">
              <label>Dirección (Linéa 2)</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row selectLocalizacion">
          <div class="col-sm-4">
            <div class="form-group">
              <label>País</label>
              <select id="selectPais" class="form-control" name="pais">
                <option value="">Seleccionar</option>
                @foreach($countries as $country)
                  <option value="{{ $country->id }}"{{ $country->id == $agency->c_id ? ' selected':'' }}>{{ $country->nombre }}</option>
                @endforeach
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              <label>Provincia</label>
              <select id="selectProvincia" data-action="ajaxProvinces" class="form-control" name="provincia">
                <option value="">Seleccionar</option>
                @foreach($provinces as $province)
                  <option value="{{ $province->id }}"{{ $province->id == $agency->p_id ? ' selected':'' }}>{{ $province->nombre }}</option>
                @endforeach
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              <label>Distrito</label>
              <select id="selectDistrito" data-action="ajaxDistricts" class="form-control" name="distrito">
                <option value="">Seleccionar</option>
                @foreach($districts as $district)
                  <option value="{{ $district->id }}"{{ $district->id == $agency->district_id ? ' selected':'' }}>{{ $district->nombre }}</option>
                @endforeach
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div id="logo" class="dropzone"></div>
          <input type="hidden" class="form-control" name="logo" value="{{ $agency->logo }}">
          <small id="emailHelp" class="form-text text-muted">Recomendaciones: 250x250 PNG (Con transparencia)</small>
          <div class="invalid-feedback"></div>
        </div>

        <div class="form-group">
          <label>Activo?</label>
          <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn<?= ($agency->estado == 3) ? ' focus active':'' ?>">
              <input type="radio" name="estado" autocomplete="off" value="3"{{ ($agency->estado == 3) ? ' checked':'' }}> No
            </label>
            <label class="btn<?= ($agency->estado == 1) ? ' focus active':'' ?>">
              <input type="radio" name="estado" autocomplete="off" value="1"{{ ($agency->estado == 1) ? ' checked':'' }}> Si
            </label>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Agentes</h4>
        <div class="actions">
            <a href="javascript:void(0);" id="btnAddAgente" class="actions__item zmdi zmdi-plus"></a>
        </div>

        <table id="data-table" class="table table-bordered table-agency-users">
          <thead class="thead-default">
            <tr>
              <th>#</th>
              <th>Nombres</th>
              <th>Correo</th>
              <th></th>
            </tr>
          </thead>
          <tfoot>
            <tr>
            <th>#</th>
            <th>Nombres</th>
            <th>Correo</th>
            <th></th>
            </tr>
          </tfoot>
          <tbody>
            @foreach($agencyUsers as $key => $agencyUser)
            <tr id="agencyUser-{{ $agencyUser->id }}">
              <td>{{ $agencyUser->id }}</td>
              <td>{{ $agencyUser->nombre }}</td>
              <td>{{ $agencyUser->correo }}</td>
              <td>
                <a class="btn btn-dark btn-sm btn-editar" data-index="{{ $key }}" data-id="{{ $agencyUser->id }}" href="javascript:;" role="button" title="Editar">
                  <i class="zmdi zmdi-edit"></i>
                </a>
                <a class="btn btn-danger btn-sm btn-eliminar" data-index="{{ $key }}" data-id="{{ $agencyUser->id }}" href="javascript:;" role="button" title="Eliminar">
                  <i class="zmdi zmdi-delete"></i>
                </a>
                <a class="btn btn-info btn-sm btn-reset" data-index="{{ $key }}" data-id="{{ $agencyUser->id }}" href="javascript:;" role="button" title="Restablecer Contraseña">
                  <i class="zmdi zmdi-mail-send"></i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>

  <div class="col-sm-12">
    <button type="submit" class="btn btn-primary">Actualizar</button>
  </div>
</form>

<!-- Modals -->
<div class="modal fade" id="modalAgente" tabindex="-1" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Agregar Agente</h5>
      </div>
      <form id="frmAgencyUser" action="{{ url('dashboard/agency_users') }}" method="post">
        <div class="modal-body">
          {{ csrf_field() }}
          <input type="hidden" name="agency_id" value="{{ $agency->id }}">
          <input type="hidden" name="agency_user_id" value="">

          <div class="form-group form-group--float">
            <input type="text" class="form-control" name="nombre">
            <label>Nombres</label>
            <i class="form-group__bar"></i>
            <div class="invalid-feedback"></div>
          </div>

          <div class="form-group form-group--float">
            <input type="text" class="form-control" name="correo">
            <label>Correo</label>
            <i class="form-group__bar"></i>
            <div class="invalid-feedback"></div>
          </div>

          <div class="form-group">
            <label>Rol</label>
            <select id="selectRole" class="form-control" name="role">
              <option value="">Seleccionar</option>
              @foreach($roles as $role)
                <option value="{{ $role->id }}">{{ $role->nombre }}</option>
              @endforeach
            </select>
            <i class="form-group__bar"></i>
            <div class="invalid-feedback"></div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-link btn-editar">Agregar</button>
          <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Componentes -->
<script type="text/template" id="component-select">
  <option value="<%= data.id %>"><%= data.nombre %></option>
</script>

<script type="text/template" id="component-tr">
  <tr id="agencyUser-<%= data.id %>">
    <td><%= data.id %></td>
    <td><%= data.nombre %></td>
    <td><%= data.correo %></td>
    <td>
      <a class="btn btn-dark btn-sm btn-editar" data-index="<%= data.index %>" data-id="<%= data.id %>" href="javascript:;" role="button" title="Editar">
        <i class="zmdi zmdi-edit"></i>
      </a>
      <a class="btn btn-danger btn-sm btn-eliminar" data-index="<%= data.index %>" data-id="<%= data.id %>" href="javascript:;" role="button" title="Eliminar">
        <i class="zmdi zmdi-delete"></i>
      </a>
      <a class="btn btn-info btn-sm btn-reset" data-index="<%= data.index %>" data-id="<%= data.id %>" href="javascript:;" role="button" title="Restablecer Contraseña">
        <i class="zmdi zmdi-mail-send"></i>
      </a>
    </td>
  </tr>
</script>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/lodash.min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/underscore-min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/jquery.component.min.js') }}"></script>

<script type="text/javascript">
Dropzone.autoDiscover = false;

let jsonAgencyUsers = {!! $agencyUsers !!};

const site_url = '{{ url('/') }}/';
const _token = '{{ csrf_token() }}';

const frmAgency = '#frmAgency';
const frmAgencyUser = '#frmAgencyUser';

let isEdit = false;
let indexEdit = null;

const mockFile = {
   name: '{{ $agency->logo }}',
   size: {{ $agency->size }},
   type: '{{ $agency->type }}',
   accepted: true,
   dataURL: '{{ url('files/logos/'.$agency->logo) }}'
};

let logoDropzone = [];

let coSelect = $.component({
  template: $('#component-select').html()
});

let coTr = $.component({
  template: $('#component-tr').html()
});

$('#btnAddAgente').click(function () {
  isEdit = false;

  clearErrorAjax(frmAgencyUser);

  $('#modalAgente .modal-title').text('Agregar Agente');
  $('#modalAgente .btn-editar').text('Agregar');

  $(frmAgencyUser + ' input[name="nombre"]').val('');
  $(frmAgencyUser + ' input[name="correo"]').val('');
  $(frmAgencyUser + ' input[name="agency_user_id"]').val('');
  $(frmAgencyUser + ' select[name="role"]').val('');

  $(frmAgencyUser + ' .form-group--float .form-control').removeClass('form-control--active');

  $('#modalAgente').modal('show');
});

$('#data-table').on('click', '.btn-editar', function () {
  isEdit = true;

  clearErrorAjax(frmAgencyUser);

  $('#modalAgente .modal-title').text('Editar Agente');
  $('#modalAgente .btn-editar').text('Editar');

  indexEdit = $(this).attr('data-index');
  const user = jsonAgencyUsers[indexEdit];

  $(frmAgencyUser + ' input[name="nombre"]').val(user.nombre);
  $(frmAgencyUser + ' input[name="correo"]').val(user.correo);
  $(frmAgencyUser + ' input[name="agency_user_id"]').val(user.id);
  $(frmAgencyUser + ' select[name="role"]').val(user.role_id);

  $(frmAgencyUser + ' .form-group--float .form-control').removeClass('form-control--active');
  $(frmAgencyUser + ' .form-group--float .form-control').addClass('form-control--active');

  $('#modalAgente').modal('show');
});

// Select Country
$('#selectPais').change(function () {
  const value = $(this).val();

  if (value === '') {
    return false;
  }

  $.ajax({
    method: 'get',
    url: site_url + 'dashboard/ajaxProvinces/' + value,
  })
  .done(function (success) {
    // Vaciando componente
    $('#selectProvincia').html('');
    $('#selectDistrito').html('');
    // Agregar component tr
    $('#selectProvincia').append(coSelect.render({ id: '', nombre:'Seleccionar' }));
    $('#selectDistrito').append(coSelect.render({ id: '', nombre:'Seleccionar' }));
    $.each(success, function (index, value) {
      $('#selectProvincia').append(coSelect.render(value));
    });
  })
  .fail(function (error) {
    console.log(success);
  });
});

// Select Country
$('#selectProvincia').change(function () {
  const value = $(this).val();

  if (value === '') {
    return false;
  }

  $.ajax({
    method: 'get',
    url: site_url + 'dashboard/ajaxDistricts/' + value,
  })
  .done(function (success) {
    // Vaciando componente
    $('#selectDistrito').html('');
    // Agregar component tr
    $('#selectDistrito').append(coSelect.render({ id: '', nombre:'Seleccionar' }));
    $.each(success, function (index, value) {
      $('#selectDistrito').append(coSelect.render(value));
    });
  })
  .fail(function (error) {
    console.log(success);
  });
});

$(document).ready(function () {
  logoDropzone = new Dropzone('#logo', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilesPackage',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: null,
    dictDefaultMessage: 'Selecciona o arrastra el logo',
    acceptedFiles: 'image/*',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.files.push(mockFile);
      dropZone.emit('addedfile', mockFile);
      dropZone.createThumbnailFromUrl(
      	mockFile,
      	dropZone.options.thumbnailWidth,
      	dropZone.options.thumbnailHeight,
      	dropZone.options.thumbnailMethod,
      	true,
      	function(thumbnail) {
      		dropZone.emit('thumbnail', mockFile, thumbnail);
      		dropZone.emit('complete', mockFile);
      	}
      );

      dropZone.on('removedfile', function (file) {
        if (logoDropzone.files.length === 0) {
          $('input[name="logo"]').val('');
        }
      });

      dropZone.on('addedfile', function(file) {
        if (dropZone.files.length > 1) {
          swal({
            title: 'Estas seguro de cambiar?',
            text: 'Si procede, se cambiará el logo actual',
            type: 'info',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, continuar!',
            cancelButtonClass: 'btn btn-secondary'
          }).then(function (response) {
            if (response.value === true) {
              dropZone.removeAllFiles();
              dropZone.addFile(file);
            } else {
              dropZone.removeFile(file);
            }
          });
        }
      });
    },
    success: function (file, response) {
      $('input[name="logo"]').val(response.fileName);
    }
  });
});

$(frmAgency).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmAgency);

    swal({
      title: 'Excelente!',
      text: 'Hemos actualizado la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmAgency);

    if (status === 422) {
      errorAjax(frmAgency, errors);
    }
  });

  event.preventDefault();
});

$(frmAgencyUser).submit(function (event) {
  const method = $(this).attr('method');
  let url = $(this).attr('action');
  let data = $(this).serialize();

  const agency_user_id = $(frmAgencyUser + ' input[name="agency_user_id"]').val();

  if (isEdit) {
    data = data + '&_method=PUT';
    url = url + '/' + agency_user_id;
  }

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmAgencyUser);

    $('#modalAgente').modal('hide');

    let swalText = 'Hemos actualizado la información correctamente';

    if (!isEdit) {
      swalText = 'Se ha enviado un correo al agente con sus datos de acceso.';
    }

    setTimeout(() => {
      swal({
        title: 'Excelente!',
        text: swalText,
        type: 'success',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary'
      });
    }, 250);

    if (isEdit) {
      jsonAgencyUsers[indexEdit].nombre = success.nombre;
      jsonAgencyUsers[indexEdit].correo = success.correo;
      jsonAgencyUsers[indexEdit].role_id = success.role_id;
    } else {
      jsonAgencyUsers.push(success);
    }

    $('.table-agency-users tbody').html('');

    $.each(jsonAgencyUsers, function (index, item) {
      item.index = index;
      $('.table-agency-users tbody').append(coTr.render(item));
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmAgencyUser);

    if (status === 422) {
      errorAjax(frmAgencyUser, errors);
    }
  });

  event.preventDefault();
});

$('#data-table').on('click', '.btn-eliminar', function () {

  const id = $(this).attr('data-id');
  indexEdit = $(this).attr('data-index');

  swal({
      title: 'Estas seguro de eliminar?',
      text: 'Si procede, no podrá recuperar el registro',
      type: 'warning',
      showCancelButton: true,
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonClass: 'btn btn-secondary'
  }).then(function (willDelete) {
    if (willDelete.value === true) {
      $.ajax({
        method: 'post',
        url: site_url + 'dashboard/agency_users/' + id,
        data: { id: id, _method: 'DELETE', _token: _token }
      })
      .done(function (success) {
        $('#agencyUser-' + id).remove();

        delete jsonAgencyUsers[indexEdit];
        console.log(indexEdit);
        swal({
          title: 'Excelente!',
          text: 'Hemos eliminado el registro',
          type: 'success',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      })
      .fail(function (error) {
        swal({
          title: 'Ups!',
          text: 'Hubo un error al eliminar el registro',
          type: 'info',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      });
    }
  });
});


$('#data-table').on('click', '.btn-reset', function () {
  const id = $(this).attr('data-id');
  indexEdit = $(this).attr('data-index');

  swal({
      title: 'Estas seguro de restablecer contraseña?',
      text: 'Si procede, se cambiará la contraseña del agente',
      type: 'info',
      showCancelButton: true,
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-success',
      confirmButtonText: 'Si, restablecer!',
      cancelButtonClass: 'btn btn-secondary'
  }).then(function (willDelete) {
    if (willDelete.value === true) {
      $.ajax({
        method: 'get',
        url: site_url + 'dashboard/ajaxResetContrasena/' + id,
      })
      .done(function (success) {
        swal({
          title: 'Excelente!',
          text: 'Hemos enviado la información al agente',
          type: 'success',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      })
      .fail(function (error) {
        swal({
          title: 'Ups!',
          text: 'Hubo un error al restablecer el agente',
          type: 'info',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      });
    }
  });
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    let input, msg, parent;

    input = $(frm + ' *[name="' + item + '"]');
    parent = $(input).parent()[0];

    if (item === 'logo') {
      msg = $(parent).children()[3];
    } else {
      msg = $(parent).children()[3];
    }

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
