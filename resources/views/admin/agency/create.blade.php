@extends('admin.layouts.app')

@section('title', 'Editar Agencia')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/dropzone/dist/dropzone.css') }}">
<style media="screen">
.dropzone .dz-preview .dz-image img {
  width: 120px;
}
</style>
@endsection

@section('content')
<header class="content__title">
  <h1>Crear Agencia</h1>

  <div class="actions">
    <a href="{{ url('dashboard/agencies') }}" class="actions__item zmdi zmdi-arrow-left"></a>
  </div>
</header>


<form id="frmAgency" class="d-flex flex-wrap" action="{{ url('dashboard/agencies') }}" method="post">
  {{ csrf_field() }}

  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Agencia</h4>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="usuario">
              <label>Usuario</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="correo">
              <label>Correo</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="ruc">
              <label>RUC</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="razon_social">
              <label>Razón Social</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombre_comercial">
              <label>Nombre Comercial</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombres">
              <label>Nombres del Responsable</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="telefono">
              <label>Teléfono</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="celular">
              <label>Celular</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="direccion_1">
              <label>Dirección (Linéa 1)</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="direccion_2">
              <label>Dirección (Linéa 2)</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row selectLocalizacion">
          <div class="col-sm-4">
            <div class="form-group">
              <label>País</label>
              <select id="selectPais" class="form-control" name="pais">
                <option value="">Seleccionar</option>
                @foreach($countries as $country)
                  <option value="{{ $country->id }}">{{ $country->nombre }}</option>
                @endforeach
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              <label>Provincia</label>
              <select id="selectProvincia" data-action="ajaxProvinces" class="form-control" name="provincia">
                <option value="">Seleccionar</option>
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              <label>Distrito</label>
              <select id="selectDistrito" data-action="ajaxDistricts" class="form-control" name="distrito">
                <option value="">Seleccionar</option>
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div id="logo" class="dropzone"></div>
          <input type="hidden" class="form-control" name="logo" value="">
          <small id="emailHelp" class="form-text text-muted">Recomendaciones: 250x250 PNG (Con transparencia)</small>
          <div class="invalid-feedback"></div>
        </div>

      </div>
    </div>
  </div>

  <div class="col-sm-12">
    <button type="submit" class="btn btn-primary">Crear</button>
  </div>
</form>

<!-- Componentes -->
<script type="text/template" id="component-select">
  <option value="<%= data.id %>"><%= data.nombre %></option>
</script>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/lodash.min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/underscore-min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/jquery.component.min.js') }}"></script>

<script type="text/javascript">
Dropzone.autoDiscover = false;

const _token = '{{ csrf_token() }}';
const site_url = '{{ url('/') }}/';
const frmAgency = '#frmAgency';
let logoDropzone = [];

let coSelect = $.component({
  template: $('#component-select').html()
});

// Select Country
$('#selectPais').change(function () {
  const value = $(this).val();

  if (value === '') {
    return false;
  }

  $.ajax({
    method: 'get',
    url: site_url + 'dashboard/ajaxProvinces/' + value,
  })
  .done(function (success) {
    // Vaciando componente
    $('#selectProvincia').html('');
    $('#selectDistrito').html('');
    // Agregar component tr
    $('#selectProvincia').append(coSelect.render({ id: '', nombre:'Seleccionar' }));
    $('#selectDistrito').append(coSelect.render({ id: '', nombre:'Seleccionar' }));
    $.each(success, function (index, value) {
      $('#selectProvincia').append(coSelect.render(value));
    });
  })
  .fail(function (error) {
    console.log(success);
  });
});

// Select Country
$('#selectProvincia').change(function () {
  const value = $(this).val();

  if (value === '') {
    return false;
  }

  $.ajax({
    method: 'get',
    url: site_url + 'dashboard/ajaxDistricts/' + value,
  })
  .done(function (success) {
    // Vaciando componente
    $('#selectDistrito').html('');
    // Agregar component tr
    $('#selectDistrito').append(coSelect.render({ id: '', nombre:'Seleccionar' }));
    $.each(success, function (index, value) {
      $('#selectDistrito').append(coSelect.render(value));
    });
  })
  .fail(function (error) {
    console.log(success);
  });
});

$(document).ready(function () {
  logoDropzone = new Dropzone('#logo', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilesPackage',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: null,
    dictDefaultMessage: 'Selecciona o arrastra el logo',
    acceptedFiles: 'image/*',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.on('removedfile', function (file) {
        if (logoDropzone.files.length === 0) {
          $('input[name="logo"]').val('');
        }
      });

      dropZone.on('addedfile', function(file) {
        if (dropZone.files.length > 1) {
          swal({
            title: 'Estas seguro de cambiar?',
            text: 'Si procede, se cambiará el logo actual',
            type: 'info',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, continuar!',
            cancelButtonClass: 'btn btn-secondary'
          }).then(function (response) {
            if (response.value === true) {
              dropZone.removeAllFiles();
              dropZone.addFile(file);
            } else {
              dropZone.removeFile(file);
            }
          });
        }
      });
    },
    success: function (file, response) {
      $('input[name="logo"]').val(response.fileName);
    }
  });
});

$(frmAgency).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmAgency);

    swal({
      title: 'Excelente!',
      text: 'Hemos recibido la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    })
    .then(function (response) {
      window.location.href = site_url + 'dashboard/agencies';
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmAgency);

    if (status === 422) {
      errorAjax(frmAgency, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    let input, msg, parent;

    input = $(frm + ' *[name="' + item + '"]');
    parent = $(input).parent()[0];

    if (item === 'logo') {
      msg = $(parent).children()[3];
    } else {
      msg = $(parent).children()[3];
    }

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
