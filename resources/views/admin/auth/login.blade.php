@extends('admin.layouts.auth')

@section('title', 'Iniciar Sesión')

@section('content')
<div class="login">
    <!-- Login -->
    <div class="login__block active" id="l-login">
        <div class="login__block__header">
            <i class="zmdi zmdi-account-circle"></i>
            Bienvenido! Ingrese sus datos

            <div class="actions actions--inverse login__block__actions">
                <div class="dropdown">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                    <div class="dropdown-menu dropdown-menu-right">
                        <!-- <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-register" href="">Create an account</a> -->
                        <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-forget-password" href="">Olvidó su contraseña?</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="login__block__body">
          <form id="frmLogin" action="{{ url('dashboard/loginAjax') }}" method="post" novalidate>
            {{ csrf_field() }}
            <div class="form-group form-group--float form-group--centered">
              <input type="text" class="form-control" name="correo" required>
              <label>Correo</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>

            <div class="form-group form-group--float form-group--centered">
              <input type="password" class="form-control" name="contrasena" required>
              <label>Contraseña</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>

            <button type="submit" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-long-arrow-right"></i></button>
          </form>
        </div>
    </div>

    <!-- Register -->
    <div class="login__block" id="l-register">
        <div class="login__block__header palette-Blue bg">
            <i class="zmdi zmdi-account-circle"></i>
            Create an account

            <div class="actions actions--inverse login__block__actions">
                <div class="dropdown">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-login" href="">Already have an account?</a>
                        <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-forget-password" href="">Forgot password?</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="login__block__body">
            <div class="form-group form-group--float form-group--centered">
                <input type="text" class="form-control">
                <label>Name</label>
                <i class="form-group__bar"></i>
            </div>

            <div class="form-group form-group--float form-group--centered">
                <input type="text" class="form-control">
                <label>Email Address</label>
                <i class="form-group__bar"></i>
            </div>

            <div class="form-group form-group--float form-group--centered">
                <input type="password" class="form-control">
                <label>Password</label>
                <i class="form-group__bar"></i>
            </div>

            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Accept the license agreement</span>
                </label>
            </div>

            <button href="index.html" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-check"></i></button>
        </div>
    </div>

    <!-- Forgot Password -->
    <div class="login__block" id="l-forget-password">
        <div class="login__block__header palette-Purple bg">
            <i class="zmdi zmdi-account-circle"></i>
            Olvidó su contraseña?

            <div class="actions actions--inverse login__block__actions">
                <div class="dropdown">
                    <i data-toggle="dropdown" class="zmdi zmdi-more-vert actions__item"></i>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-login" href="">Ya tienes una cuenta?</a>
                        <!-- <a class="dropdown-item" data-ma-action="login-switch" data-ma-target="#l-register" href="">Create an account</a> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="login__block__body">
            <p class="mt-4">Ingrese su correo y le enviaremos un enlace para restabler su contraseña.</p>

            <div class="form-group form-group--float form-group--centered">
                <input type="text" class="form-control">
                <label>Correo</label>
                <i class="form-group__bar"></i>
            </div>

            <button href="index.html" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-check"></i></button>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

const site_url = '{{ url('/') }}/';
const frmLogin = '#frmLogin';

$(frmLogin).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmLogin);

    if (success.auth === true) {
      window.location.href = site_url + 'dashboard';
    }
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    if (status === 422) {
      errorAjax(frmLogin, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    const input = $(frm + ' *[name="' + item + '"]');
    const parent = $(input).parent()[0];
    const msg = $(parent).children()[3];

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
