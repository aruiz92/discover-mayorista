@extends('admin.layouts.app')

@section('title', 'Editar Socio Comercial')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/dropzone/dist/dropzone.css') }}">
<style media="screen">
.slider {
  margin-bottom: 40px;
}
/* .dropzone .dz-preview .dz-image img {
  width: 120px;
} */
</style>
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Editar Socio Comercial</h1>

    <div class="actions">
      <a href="{{ url('dashboard/partners') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <form id="frmPartner" action="{{ url('dashboard/partners/'.$partner->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombre" value="{{ $partner->nombre }}">
              <label>Nombre</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="orden" value="{{ $partner->orden }}">
              <label>Orden</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div id="slider" class="dropzone"></div>
          <input type="hidden" class="form-control" name="imagen" value="{{ $partner->imagen }}">
          <small id="emailHelp" class="form-text text-muted">Recomendaciones: 300x150 pixeles</small>
          <div class="invalid-feedback"></div>
        </div>

        <button type="submit" class="btn btn-primary">Actualizar</button>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js') }}"></script>

<script type="text/javascript">
Dropzone.autoDiscover = false;

const site_url = '{{ url('/') }}/';
const frmPartner = '#frmPartner';
const _token = '{{ csrf_token() }}';

var sliderDropzone = [];
const mockFile = {
   name: '{{ $partner->imagen }}',
   size: {{ Storage::disk('discover')->size('partners/'.$partner->imagen) }},
   type: '{{ Storage::disk('discover')->mimeType('partners/'.$partner->imagen) }}',
   accepted: true,
   dataURL: '{{ url('files/partners/'.$partner->imagen) }}'
};

$(document).ready(function () {
  sliderDropzone = new Dropzone('#slider', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilePartner',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: '',
    dictDefaultMessage: 'Selecciona o arrastra la imagen',
    acceptedFiles: 'image/*',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.files.push(mockFile);
      dropZone.emit('addedfile', mockFile);
      dropZone.createThumbnailFromUrl(
      	mockFile,
      	dropZone.options.thumbnailWidth,
      	dropZone.options.thumbnailHeight,
      	dropZone.options.thumbnailMethod,
      	true,
      	function(thumbnail) {
      		dropZone.emit('thumbnail', mockFile, thumbnail);
      		dropZone.emit("complete", mockFile);
      	}
      );

      mockFile.previewElement.classList.add('dz-success');
      mockFile.previewElement.classList.add('dz-complete');

      dropZone.on('removedfile', function (file) {
        if (dropZone.files.length === 0) {
          $('input[name="imagen"]').val('');
        }
      });

      dropZone.on('addedfile', function(file) {
        if (dropZone.files.length > 1) {
          swal({
            title: 'Estas seguro de cambiar?',
            text: 'Si procede, se cambiará la fotografía actual',
            type: 'info',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, continuar!',
            cancelButtonClass: 'btn btn-secondary'
          }).then(function (response) {
            if (response.value === true) {
              dropZone.removeAllFiles();
              dropZone.addFile(file);
            } else {
              dropZone.removeFile(file);
            }
          });
        }
      });
    },
    success: function (file, response) {
      $(frmPartner + ' input[name="imagen"]').val(response.fileName);
    }
  });
});

$(frmPartner).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmPartner);

    swal({
      title: 'Excelente!',
      text: 'Hemos actualizado la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmPartner);

    if (status === 422) {
      errorAjax(frmPartner, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    const input = $(frm + ' *[name="' + item + '"]');
    const parent = $(input).parent()[0];
    const msg = $(parent).children()[3];

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
