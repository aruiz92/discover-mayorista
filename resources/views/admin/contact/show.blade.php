@extends('admin.layouts.app')

@section('title')
  Consulta #{{ $contact->id }}
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Consulta #{{ $contact->id }}</h1>

    <div class="actions">
      <a href="{{ url('dashboard/contacts') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Consulta</h4>
      <ul>
        <li><strong>Nombres:</strong> {{ $contact->nombres }}</li>
        <li><strong>Apellidos:</strong> {{ $contact->apellidos }}</li>
        <li><strong>Correo:</strong> {{ $contact->correo }}</li>
        <li><strong>Teléfono:</strong> {{ $contact->telefono }}</li>
        <li><strong>Agencia:</strong> {{ $contact->agency->nombre_comercial }}</li>
        <li><strong>Consulta:</strong> {{ $contact->consulta }}</li>
      </ul>
    </div>
  </div>
</div>
@endsection

@section('scripts')
@endsection
