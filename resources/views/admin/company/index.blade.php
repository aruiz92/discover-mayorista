@extends('admin.layouts.app')

@section('title', 'Discover')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Discover</h1>

    <div class="actions">
      <a href="" class="actions__item zmdi zmdi-refresh-alt"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <form id="frmCompany" action="{{ url('dashboard/discover/'.$company->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group form-group--float">
          <input type="text" class="form-control" name="nombre" value="{{ $company->nombre }}">
          <label>Nombre</label>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="telefono_fijo" value="{{ $company->telefono_fijo }}">
              <label>Teléfono fijo*</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="telefono_movil" value="{{ $company->telefono_movil }}">
              <label>Teléfono celular</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>
        <div class="form-group form-group--float">
          <input type="text" class="form-control" name="correo_informacion" value="{{ $company->correo_informacion }}">
          <label>Correo general*</label>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="correo_visitante" value="{{ $company->correo_visitante }}">
              <label>Correo cotización visitantes*</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="correo_agente" value="{{ $company->correo_agente }}">
              <label>Correo cotización agentes*</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>
        <div class="form-group form-group--float">
          <input type="text" class="form-control" name="facebook" value="{{ $company->facebook }}">
          <label>Facebook URL*</label>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="instagram" value="{{ $company->instagram }}">
              <label>Instagram URL*</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="youtube" value="{{ $company->youtube }}">
              <label>YouTube URL*</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
const site_url = '{{ url('/') }}/';
const frmCompany = '#frmCompany';

$(frmCompany).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    swal({
        title: 'Excelente!',
        text: 'Hemos actualizado la información correctamente',
        type: 'success',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary'
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    if (status === 422) {
      errorAjax(frmCompany, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    const input = $(frm + ' *[name="' + item + '"]');
    const parent = $(input).parent()[0];
    const msg = $(parent).children()[3];

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}
</script>
@endsection
