@extends('admin.layouts.app')

@section('title', 'Editar Subcategoría')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/animate.css/animate.min.css') }}">
<style media="screen">
.form-group--float .form-control:focus~label.textarea {
    bottom: 3.4rem;
}
</style>
@endsection

@section('content')

<div class="content__inner content__inner--sm">

  <header class="content__title">
    <h1>Crear Subcategoría</h1>

    <div class="actions">
      <a href="{{ url('dashboard/package_subtypes') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <form id="frmPackageSubType" action="{{ url('dashboard/package_subtypes') }}" method="post">
    {{ csrf_field() }}

    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label>Categoría</label>
              <select name="categoria" class="form-control">
                @foreach($package_types as $package_type)
                  <option value="{{ $package_type->id }}">{{ $package_type->nombre }}</option>
                @endforeach
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombre">
              <label>Nombre</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="orden">
              <label>Orden</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <button type="submit" class="btn btn-primary">Crear</button>
  </form>

</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/autosize/dist/autosize.min.js') }}"></script>

<script type="text/javascript">

const site_url = '{{ url('/') }}/';
const frmPackageSubType = '#frmPackageSubType';

$(frmPackageSubType).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  let data = $(this).serializeArray();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmPackageSubType);

    swal({
      title: 'Excelente!',
      text: 'Hemos actualizado la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    })
    .then(function (response) {
      window.location.href = site_url + 'dashboard/package_subtypes';
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmPackageSubType);

    if (status === 422) {
      errorAjax(frmPackageSubType, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    let input, msg, parent;

    input = $(frm + ' *[name="' + item + '"]');
    parent = $(input).parent()[0];

    if (item === 'categoria') {
      msg = $(parent).children()[2];
    } else {
      msg = $(parent).children()[3];
    }

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
