<!-- Vendors -->
<script src="{{ asset('admin/vendors/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- App functions and actions -->
<script src="{{ asset('admin/js/app.min.js') }}"></script>
