<aside class="sidebar">
    <div class="scrollbar-inner">
        <div class="user">
            <div class="user__info" data-toggle="dropdown">
                <img class="user__img" src="{{ asset('files/logos/'.Auth::user()->agency->logo) }}" alt="">
                <div>
                    <div class="user__name">{{ Auth::user()->nombre }}</div>
                    <div class="user__email">{{ Auth::user()->correo }}</div>
                </div>
            </div>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ url('logout') }}">Salir</a>
            </div>
        </div>

        <ul class="navigation">
            <li class="{{ ($active === 'inicio') ? 'navigation__active':'' }}">
              <a href="{{ url('dashboard') }}">
                <i class="zmdi zmdi-home"></i> Inicio
              </a>
            </li>

            <li class="navigation__sub{{ ($active === 'discover' || $active === 'nosotros' || $active === 'colaboradores' || $active === 'socios' || $active === 'newsletters' || $active === 'sliders' || $active === 'offers' || $active === 'subscribers' || $active === 'contacts') ? ' navigation__sub--active navigation__sub--toggled':'' }}">
              <a href="">
                <i class="zmdi zmdi-email"></i> Discover
              </a>

              <ul>
                <li class="{{ ($active === 'discover') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/discover') }}">General</a>
                </li>
                <li class="{{ ($active === 'nosotros') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/aboutus') }}">Nosotros</a>
                </li>
                <li class="{{ ($active === 'colaboradores') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/employees') }}">Colaboradores</a>
                </li>
                <li class="{{ ($active === 'socios') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/partners') }}">Socios</a>
                </li>
                <li class="{{ ($active === 'sliders') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/sliders') }}">Sliders</a>
                </li>
                <li class="{{ ($active === 'offers') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/offers') }}">Oferta</a>
                </li>
                <li class="{{ ($active === 'newsletters') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/newsletters') }}">Newsletters</a>
                </li>
                <li class="{{ ($active === 'subscribers') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/subscribers') }}">Suscriptores</a>
                </li>
                <li class="{{ ($active === 'contacts') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/contacts') }}">Consultas</a>
                </li>
              </ul>
            </li>

            <li class="navigation__sub{{ ($active === 'packages' || $active === 'package_types' || $active === 'package_subtypes' || $active === 'destinations') ? ' navigation__sub--active navigation__sub--toggled':'' }}">
              <a href="">
                <i class="zmdi zmdi-card-travel"></i> Catalogo
              </a>

              <ul>
                <li class="{{ ($active === 'packages') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/packages') }}">Paquetes</a>
                </li>
                <li class="{{ ($active === 'destinations') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/destinations') }}">Destinos</a>
                </li>
                <li class="{{ ($active === 'package_types') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/package_types') }}">Categorías</a>
                </li>
                <li class="{{ ($active === 'package_subtypes') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/package_subtypes') }}">Subcategorías</a>
                </li>
              </ul>
            </li>

            <li class="{{ ($active === 'agencies') ? 'navigation__active':'' }}">
              <a href="{{ url('dashboard/agencies') }}">
                <i class="zmdi zmdi-pin"></i> Agencias
              </a>
            </li>

            <li class="navigation__sub{{ ($active === 'countries' || $active === 'provinces' || $active === 'districts') ? ' navigation__sub--active navigation__sub--toggled':'' }}">
              <a href="">
                <i class="zmdi zmdi-globe"></i> Localización
              </a>

              <ul>
                <li class="{{ ($active === 'countries') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/countries') }}">Paises</a>
                </li>
                <li class="{{ ($active === 'provinces') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/provinces') }}">Provincias</a>
                </li>
                <li class="{{ ($active === 'districts') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/districts') }}">Distritos</a>
                </li>
              </ul>
            </li>

            <li class="navigation__sub{{ ($active === 'quotations' || $active === 'quotation_users') ? ' navigation__sub--active navigation__sub--toggled':'' }}">
              <a href="">
                <i class="zmdi zmdi-email"></i> Cotizaciones
              </a>

              <ul>
                <li class="{{ ($active === 'quotations') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/quotations') }}">General</a>
                </li>
                <li class="{{ ($active === 'quotation_users') ? 'navigation__active':'' }}">
                  <a href="{{ url('dashboard/quotation_users') }}">Agentes</a>
                </li>
              </ul>
            </li>
        </ul>
    </div>
</aside>
