<div class="modal fade" id="modal-tab" tabindex="-1" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Nueva Pestaña</h5>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombre" value="null">
              <label>Nombre</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="number" min="1" class="form-control" name="orden" value="1">
              <label>Orden</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="frm-tab">
          <label>Detalle</label>
          <div class="wysiwyg-tab"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-link btn-add">Agregar</button>
        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-doc" tabindex="-1" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Nuevo Documento</h5>
      </div>
      <div class="modal-body">

        <div class="row">
          <input type="hidden" name="archivo" value="">

          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombre" value="null">
              <label>Nombre</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="number" min="1" class="form-control" name="orden" value="1">
              <label>Orden</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div id="documento" class="dropzone"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-link btn-add">Agregar</button>
        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
