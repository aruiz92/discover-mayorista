<script type="text/template" id="tr-tab">
  <tr id="tab-<%= data.id %>">
    <th scope="row"><%= data.id %></th>
    <td><%= data.nombre %></td>
    <td><%= data.orden %></td>
    <td>
      <a class="btn btn-dark btn-sm btn-editar" data-id="<%= data.id %>" href="javascript:;" role="button" title="Editar">
        <i class="zmdi zmdi-edit"></i>
      </a>
      <a class="btn btn-danger btn-sm btn-eliminar" data-id="<%= data.id %>" href="javascript:;" role="button" title="Eliminar">
        <i class="zmdi zmdi-delete"></i>
      </a>
    </td>
  </tr>
</script>

<script type="text/template" id="tr-doc">
  <tr id="doc-<%= data.id %>">
    <th scope="row"><%= data.id %></th>
    <td><%= data.nombre %></td>
    <td><%= data.orden %></td>
    <td>
      <a class="btn btn-dark btn-sm btn-editar" data-id="<%= data.id %>" href="javascript:;" role="button" title="Editar">
        <i class="zmdi zmdi-edit"></i>
      </a>
      <a class="btn btn-danger btn-sm btn-eliminar" data-id="<%= data.id %>" href="javascript:;" role="button" title="Eliminar">
        <i class="zmdi zmdi-delete"></i>
      </a>
    </td>
  </tr>
</script>

<script type="text/template" id="component-select">
  <option value="<%= data.id %>"><%= data.nombre %></option>
</script>
