<footer class="footer hidden-xs-down">
    <p>© Webtilia. Todos los derechos reservados.</p>

    <ul class="nav footer__nav">
        <a class="nav-link" href="{{ url('/') }}">Inicio</a>
        <a class="nav-link" href="{{ url('nosotros') }}">Nosotros</a>
        <a class="nav-link" href="{{ url('bloqueos') }}">Bloqueos</a>
        <a class="nav-link" href="{{ url('blog') }}">Blog</a>
        <a class="nav-link" href="{{ url('contacto') }}">Contacto</a>
    </ul>
</footer>
