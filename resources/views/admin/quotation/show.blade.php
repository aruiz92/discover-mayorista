@extends('admin.layouts.app')

@section('title')
  Cotización General #{{ $quotation->id }}
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Cotización #{{ $quotation->id }}</h1>

    <div class="actions">
      <a href="{{ url('dashboard/quotations') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Información</h4>
      <ul>
        <li><strong>Nombres:</strong> {{ $quotation->nombres }}</li>
        <li><strong>Correo:</strong> {{ $quotation->correo }}</li>
        <li><strong>Paquete:</strong> <a href="{{ url('dashboard/packages/'.$quotation->package->id.'/edit') }}">{{ $quotation->package->nombre }}</a></li>
        <li><strong>Agencia:</strong> <a href="{{ url('dashboard/agencies/'.$quotation->agency->id.'/edit') }}">{{ $quotation->agency->nombre_comercial }}</a></li>
        <li><strong>Mensaje:</strong> {{ $quotation->mensaje }}</li>
      </ul>
    </div>
  </div>
</div>
@endsection

@section('scripts')
@endsection
