<div class="modal fade" id="modal-slider" tabindex="-1" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Agregar Slider</h5>
      </div>
      <div class="modal-body">

        <div class="row">
          <input type="hidden" name="imagen" value="">

          <div class="col-sm-12">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="titulo" value="null">
              <label>Título</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="subtitulo" value="null">
              <label>Subtítulo</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group form-group--float">
              <textarea class="form-control" name="descripcion">null</textarea>
              <label class="textarea">Descripción</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="form-group form-group--float">
              <input type="number" min="1" class="form-control" name="orden" value="1">
              <label>Orden</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div id="fileSlider" class="dropzone"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-link btn-add">Agregar</button>
        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>