@extends('admin.layouts.app')

@section('title')
  Cotización Agente #{{ $quotation_user->id }}
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Cotización #{{ $quotation_user->id }}</h1>

    <div class="actions">
      <a href="{{ url('dashboard/quotation_users') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Información</h4>
      <ul>
        <li><strong>Nombres:</strong> {{ $quotation_user->agency_user->nombre }}</li>
        <li><strong>Correo:</strong> {{ $quotation_user->agency_user->correo }}</li>
        <li><strong>Paquete:</strong> <a href="{{ url('dashboard/packages/'.$quotation_user->package->id.'/edit') }}">{{ $quotation_user->package->nombre }}</a></li>
        <li><strong>Agencia:</strong> <a href="{{ url('dashboard/agencies/'.$quotation_user->agency_user->agency->id.'/edit') }}">{{ $quotation_user->agency_user->agency->nombre_comercial }}</a></li>
        <li><strong>Mensaje:</strong> {{ $quotation_user->mensaje }}</li>
      </ul>
    </div>
  </div>
</div>
@endsection

@section('scripts')
@endsection
