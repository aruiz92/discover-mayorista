@extends('admin.layouts.app')

@section('title', 'Crear Paquete')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/trumbowyg/dist/ui/trumbowyg.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/dropzone/dist/dropzone.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/animate.css/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/flatpickr/flatpickr.min.css') }}" />
<style media="screen">
.slider {
  margin-bottom: 40px;
}
.form-group--float .form-control:focus~label.textarea {
    bottom: 3.4rem;
}
.row-listview {
  margin-left: -30px;
  margin-right: -30px;
}
.trumbowyg-box, .trumbowyg-editor {
  min-height: 100px;
  margin-top: 0;
}
.trumbowyg-editor, .trumbowyg-textarea {
  min-height: 100px;
}
/* .dropzone .dz-preview .dz-image img {
  width: 120px;
} */
</style>
@endsection

@section('content')
<header class="content__title">
  <h1>Crear Paquete</h1>

  <div class="actions">
    <a href="{{ url('dashboard/packages') }}" class="actions__item zmdi zmdi-arrow-left"></a>
  </div>
</header>


<form id="frmPackage" class="d-flex flex-wrap" action="{{ url('dashboard/packages') }}" method="post">
  {{ csrf_field() }}
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Información General</h4>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="cod_discover">
              <label>Código Discover</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <div>
                <label>En promoción?</label>
              </div>
              <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn active">
                  <input type="radio" name="promocion" autocomplete="off" value="0" checked=""> No
                </label>
                <label class="btn">
                  <input type="radio" name="promocion" autocomplete="off" value="1"> Si
                </label>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group form-group--float">
          <input type="text" class="form-control" name="nombre">
          <label>Nombre</label>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nro_dias">
              <label>Nro. Días</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nro_noches">
              <label>Nro. Noches</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="form-group form-group--float">
          <textarea class="form-control textarea-autosize" name="descripcion" style="overflow: hidden; word-wrap: break-word; height: 47px;"></textarea>
          <label class="textarea">Descripción</label>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="precio_min">
              <label>Precio mínimo</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="precio_max">
              <label>Precio máximo</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="precio_ser">
              <label>Precio solo servicio</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="precio_bol">
              <label>Precio con boleto aéreo</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>Fecha salida</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                </div>
                <input type="text" name="salida" class="form-control date-picker">
                <div class="invalid-feedback"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label>Fecha retorno</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                </div>
                <input type="text" name="regreso" class="form-control date-picker">
                <div class="invalid-feedback"></div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label>Vigencia</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
            </div>
            <input type="text" name="vigencia" class="form-control date-picker">
            <div class="invalid-feedback"></div>
          </div>
        </div>

        <div class="form-group">
          <select class="form-control" name="destino">
            <option value="">Destino</option>
            @foreach($destinations as $destination)
              <option value="{{ $destination->id }}">{{ $destination->nombre }}</option>
            @endforeach
          </select>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <select id="selectPackageType" class="form-control" name="categoria">
                <option value="">Categoría</option>
                @foreach($types as $type)
                  <option value="{{ $type->id }}">{{ $type->nombre }}</option>
                @endforeach
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <select id="selectPackageSubType" class="form-control" name="subcategoria">
                <option value="">Subcategoría</option>
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Destacar</h4>

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <div>
                <label>Destacar?</label>
              </div>
              <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn active">
                  <input type="radio" name="destacado" autocomplete="off" checked value="0"> No
                </label>
                <label class="btn">
                  <input type="radio" name="destacado" autocomplete="off" value="1"> Si
                </label>
              </div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="orden_destacado">
              <label>Orden Destacado</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Pestañas</h4>
            <div class="actions">
              <a href="javascript:;" class="actions__item zmdi zmdi-plus btn-modal-tab"></a>
            </div>
            <div class="table-responsive">
              <table class="table table-tab mb-0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Orden</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Documentos</h4>
            <div class="actions">
              <a href="javascript:;" class="actions__item zmdi zmdi-plus btn-modal-doc"></a>
            </div>
            <div class="table-responsive">
              <table class="table table-doc mb-0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Orden</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Imagen (800x1202)</h4>

            <div class="form-group mb-0">
              <div id="imagen1Dropzone" class="dropzone"></div>
              <input type="hidden" class="form-control" name="imagen_1" value="">
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Imagen (797x531)</h4>

            <div class="form-group mb-0">
              <div id="imagen2Dropzone" class="dropzone"></div>
              <input type="hidden" class="form-control" name="imagen_2" value="">
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Flyers</h4>

            <div class="form-group mb-0">
              <div id="flyers" class="dropzone"></div>
              <input type="hidden" class="form-control" name="flyers" value="">
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-12">
    <button type="submit" class="btn btn-primary">Crear</button>
  </div>
</form>


<!-- Modals -->
@include('admin.partials.package.modals')
<!-- Componentes -->
@include('admin.partials.package.components')

@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/trumbowyg/dist/trumbowyg.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/autosize/dist/autosize.min.js') }}"></script>
<script src="{{ asset('admin/vendors/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('admin/vendors/flatpickr/es.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/lodash.min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/underscore-min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/jquery.component.min.js') }}"></script>

<script type="text/javascript">
Dropzone.autoDiscover = false;

let tabsEdit = false;
let tabsIdEdit = null;
let docEdit = false;
let docIdEdit = null;
let tabs = [];
let documentos = [];
let documentoDropzone = [];
let imagen1Dropzone = [];
let imagen2Dropzone = [];
let flyersDropzone = [];
const site_url = '{{ url('/') }}/';
const _token = '{{ csrf_token() }}';
const frmPackage = '#frmPackage';
const btns = [
  ['viewHTML'],
  ['undo', 'redo'],
  ['strong', 'em', 'del'],
  ['unorderedList', 'orderedList'],
  ['removeformat'],
  ['fullscreen']
];

let trTab = $.component({
  template: $('#tr-tab').html()
});

let trDoc = $.component({
  template: $('#tr-doc').html()
});

let coSelect = $.component({
  template: $('#component-select').html()
});

$('.wysiwyg-tab').trumbowyg({
  btns: btns,
  autogrow: true
});


$('.btn-modal-tab').click(function () {
  const orden = tabs.length + 1;
  // Limpiando elementos modal
  $('#modal-tab input[name="nombre"]').val('');
  $('#modal-tab input[name="orden"]').val('');
  $('#modal-tab .wysiwyg-tab').trumbowyg('html', '');
  // Asignando valor orden
  $('#modal-tab input[name="orden"]').val(orden);
  // Mostrando Modal
  $('#modal-tab').modal('show');
  // Estableciendo modo
  tabsEdit = false;
  // Editando modal
  $('#modal-tab .modal-title').text('Agregar Pestaña');
  $('#modal-tab .btn-add').text('Agregar');
});

$('#modal-tab .btn-add').click(function () {
  const nombre = $('#modal-tab input[name="nombre"]').val();
  const orden = $('#modal-tab input[name="orden"]').val();
  const descripcion = $('#modal-tab .wysiwyg-tab').trumbowyg('html');
  // Construyendo datos del tab
  const data = {
    id: tabs.length + 1,
    idd: null,
    modo: 'crear',
    nombre: nombre,
    descripcion: descripcion,
    orden: orden
  };

  if (tabsEdit) {
    // Editando tab
    data.idd = tabs[tabsIdEdit].idd;
    data.modo = tabs[tabsIdEdit].modo;
    tabs[tabsIdEdit] = data;
  } else {
    // Agregando nuevo tab
    tabs.push(data);
  }

  // Limpiando component tr}
  $('.table-tab tbody').html('');
  // Agregando tabs al component tr
  for (let index = 0; index < tabs.filter(function (item) {
    return item.modo !== 'eliminar'
  }).length; index++) {
    // Asignando nuevo id
    tabs[index].id = index + 1;
    // Agregar component tr
    $('.table-tab tbody').append(trTab.render(tabs[index]));
  }

  // Ocultar modal
  $('#modal-tab').modal('hide');
  // Limpiando elementos modal
  $('#modal-tab .wysiwyg-tab').trumbowyg('html', '');
  $('#modal-tab input[name="nombre"]').val('');
});

$('.table-tab').on('click', '.btn-eliminar', function () {
  const id = $(this).attr('data-id');
  const index = id - 1;

  tabs[index].modo = 'eliminar';

  // Filtrando modo tabs
  const filterTabs = tabs.filter(function (item) {
    return item.modo !== 'eliminar'
  });
  // Limpiando component tr
  $('.table-tab tbody').html('');
  // Agregando tabs al component tr
  for (let indexx = 0; indexx < filterTabs.length; indexx++) {
    // Asignando nuevo id
    filterTabs[indexx].id = indexx + 1;
    // Agregar component tr
    $('.table-tab tbody').append(trTab.render(filterTabs[indexx]));
  }
});

$('.table-tab').on('click', '.btn-editar', function () {
  const id = $(this).attr('data-id');
  const index = id - 1;

  // Reasignando valores
  $('#modal-tab input[name="nombre"]').val(tabs[index].nombre);
  $('#modal-tab input[name="orden"]').val(tabs[index].orden);
  $('#modal-tab .wysiwyg-tab').trumbowyg('html', tabs[index].descripcion);

  // Editando modal
  $('#modal-tab .modal-title').text('Editar Pestaña');
  $('#modal-tab .btn-add').text('Editar');

  // Mostrando modal
  $('#modal-tab').modal('show');

  // Estableciendo modo
  tabsEdit = true;
  tabsIdEdit = index;
});


$('.btn-modal-doc').click(function () {
  const orden = documentos.length + 1;
  // Limpiando Dropzone
  documentoDropzone.removeAllFiles();
  // Limpiando elementos modal
  $('#modal-doc input[name="nombre"]').val('');
  $('#modal-doc input[name="orden"]').val('');
  $('#modal-doc input[name="archivo"]').val('');
  // Asignando valor orden
  $('#modal-doc input[name="orden"]').val(orden);
  // Editando modal
  $('#modal-doc .modal-title').text('Nuevo Pestaña');
  $('#modal-doc .btn-add').text('Agregar');
  // Mostrando Modal
  $('#modal-doc').modal('show');
  // Editando modo
  docEdit = false;
  docIdEdit = null;
});

$('#modal-doc .btn-add').click(function () {
  const nombre = $('#modal-doc input[name="nombre"]').val();
  const orden = $('#modal-doc input[name="orden"]').val();
  const archivo = $('#modal-doc input[name="archivo"]').val();
  // Construyendo datos del tab
  const data = {
    id: documentos.length + 1,
    idd: null,
    modo: 'crear',
    nombre: nombre,
    archivo: archivo,
    orden: orden,
    file: documentoDropzone.files[0]
  };

  if (docEdit) {
    // Editando tab
    data.idd = documentos[docIdEdit].idd;
    data.modo = documentos[docIdEdit].modo;
    documentos[docIdEdit] = data;
  } else {
    // Agregando nuevo doc
    documentos.push(data);
  }

  // Limpiando component tr}
  $('.table-doc tbody').html('');
  // Agregando tabs al component tr
  for (let index = 0; index < documentos.filter(function (item) {
    return item.modo !== 'eliminar'
  }).length; index++) {
    // Asignando nuevo id
    documentos[index].id = index + 1;
    // Agregar component tr
    $('.table-doc tbody').append(trDoc.render(documentos[index]));
  }

  // Ocultar modal
  $('#modal-doc').modal('hide');
  // Limpiando elementos modal
  $('#modal-doc input[name="nombre"]').val('');
  $('#modal-doc input[name="orden"]').val('');
  $('#modal-doc input[name="archivo"]').val('');
});

$('.table-doc').on('click', '.btn-eliminar', function () {
  const id = $(this).attr('data-id');
  const index = id - 1;

  documentos[index].modo = 'eliminar';

  // Filtrando modo tabs
  const filterDocs = documentos.filter(function (item) {
    return item.modo !== 'eliminar'
  });
  // Limpiando component tr
  $('.table-doc tbody').html('');
  // Agregando tabs al component tr
  for (let indexx = 0; indexx < filterDocs.length; indexx++) {
    // Asignando nuevo id
    filterDocs[indexx].id = indexx + 1;
    // Agregar component tr
    $('.table-doc tbody').append(trDoc.render(filterDocs[indexx]));
  }
});

$('.table-doc').on('click', '.btn-editar', function () {
  const id = $(this).attr('data-id');
  const index = id - 1;

  // Reasignando valores
  $('#modal-doc input[name="nombre"]').val(documentos[index].nombre);
  $('#modal-doc input[name="orden"]').val(documentos[index].orden);
  $('#modal-doc input[name="archivo"]').val(documentos[index].archivo);

  // Limpiando Dropzone
  documentoDropzone.removeAllFiles();
  // Reasignando dropzone
  const editFile = documentos[index].file;

  documentoDropzone.files.push(editFile);
  documentoDropzone.emit('addedfile', editFile);
  documentoDropzone.emit('complete', editFile);

  // Editando modal
  $('#modal-doc .modal-title').text('Editar Pestaña');
  $('#modal-doc .btn-add').text('Editar');

  // Mostrando modal
  $('#modal-doc').modal('show');

  // Estableciendo modo
  docEdit = true;
  docIdEdit = index;
});

// Select PackageType
$('#selectPackageType').change(function () {
  const value = $(this).val();

  $.ajax({
    method: 'get',
    url: site_url + 'dashboard/ajaxSubTypes/' + value,
  })
  .done(function (success) {
    // Vaciando componente
    $('#selectPackageSubType').html('');
    // Agregar component tr
    $('#selectPackageSubType').append(coSelect.render({ id: '', nombre:'Subcategoría' }));
    $.each(success, function (index, value) {
      $('#selectPackageSubType').append(coSelect.render(value));
    });
  })
  .fail(function (error) {
    console.log(success);
  });
});

// Dropzone: Subir Documentos
$(document).ready(function () {
  // Dropzone
  documentoDropzone = new Dropzone('#documento', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilesPackage',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: '',
    dictCancelUpload: '',
    dictUploadCanceled: '',
    dictDefaultMessage: 'Selecciona o arrastra el documento',
    acceptedFiles: 'application/pdf,application/msword,text/csv,application/vnd.ms-excel',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.on('addedfile', function(file) {
        if (dropZone.files.length > 1) {
          swal({
            title: 'Estas seguro de cambiar?',
            text: 'Si procede, se cambiará la documentación actual',
            type: 'info',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, continuar!',
            cancelButtonClass: 'btn btn-secondary'
          }).then(function (response) {
            if (response.value === true) {
              dropZone.removeAllFiles();
              dropZone.addFile(file);
            } else {
              dropZone.removeFile(file);
            }
          });
        }
      });
    },
    success: function (file, response) {
      $('#modal-doc input[name="archivo"]').val(response.fileName);
    }
  });

  imagen1Dropzone = new Dropzone('#imagen1Dropzone', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilesPackage',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: '',
    dictCancelUpload: '',
    dictUploadCanceled: '',
    dictDefaultMessage: 'Selecciona o arrastra la imagen',
    acceptedFiles: 'image/*',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.on('removedfile', function (file) {
        $('input[name="imagen_1"]').val('');
      });

      dropZone.on('addedfile', function(file) {
        if (dropZone.files.length > 1) {
          swal({
            title: 'Estas seguro de cambiar?',
            text: 'Si procede, se cambiará la fotografía actual',
            type: 'info',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, continuar!',
            cancelButtonClass: 'btn btn-secondary'
          })
          .then(function (response) {
            if (response.value === true) {
              dropZone.removeAllFiles();
              dropZone.addFile(file);
            } else {
              dropZone.removeFile(file);
            }
          });
        }
      });
    },
    success: function (file, response) {
      $('input[name="imagen_1"]').val(response.fileName);
    }
  });

  imagen2Dropzone = new Dropzone('#imagen2Dropzone', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilesPackage',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: '',
    dictCancelUpload: '',
    dictUploadCanceled: '',
    dictDefaultMessage: 'Selecciona o arrastra la imagen',
    acceptedFiles: 'image/*',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.on('removedfile', function (file) {
        $('input[name="imagen_2"]').val('');
      });

      dropZone.on('addedfile', function(file) {
        if (dropZone.files.length > 1) {
          swal({
            title: 'Estas seguro de cambiar?',
            text: 'Si procede, se cambiará la fotografía actual',
            type: 'info',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, continuar!',
            cancelButtonClass: 'btn btn-secondary'
          }).then(function (response) {
            if (response.value === true) {
              dropZone.removeAllFiles();
              dropZone.addFile(file);
            } else {
              dropZone.removeFile(file);
            }
          });
        }
      });
    },
    success: function (file, response) {
      $('input[name="imagen_2"]').val(response.fileName);
    }
  });

  flyersDropzone = new Dropzone('#flyers', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilesPackage',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: '',
    dictCancelUpload: '',
    dictUploadCanceled: '',
    dictDefaultMessage: 'Selecciona o arrastra los flyers',
    acceptedFiles: 'image/*',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.on('removedfile', function (file) {
        var files = $(frmPackage + ' input[name="flyers"]');

        arrFiles = files.val().split(',');

        arrFiles.forEach(function(value) {
          if (arrFiles.length > 1) {
              if (value !== file.newName) {
                  files.val(value);
              }
          } else {
            files.val('');
          }
        });
      });
    },
    success: function (file, response) {
      var files = $(frmPackage + ' input[name="flyers"]');
      var separator = ',';

      if (files.val().length === 0) {
        separator = '';
      }

      files.val(files.val() + separator + response.fileName);
      file.newName = response.fileName;
    }
  });
});

$(frmPackage).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  let data = $(this).serializeArray();

  const dataTabs = [];
  $.each(tabs, function (index, item) {
    dataTabs.push({
      id: item.id,
      idd: item.idd,
      modo: item.modo,
      nombre: item.nombre,
      orden: item.orden,
      descripcion: item.descripcion,
    });
  });

  const dataDocs = [];
  $.each(documentos, function (index, item) {
    dataDocs.push({
      id: item.id,
      idd: item.idd,
      modo: item.modo,
      nombre: item.nombre,
      orden: item.orden,
      archivo: item.archivo,
    });
  });

  data.push({
    name: 'tabs',
    value: JSON.stringify(dataTabs)
  });

  data.push({
    name: 'docs',
    value: JSON.stringify(dataDocs)
  });

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    // console.log(success);
    clearErrorAjax(frmPackage);
    swal({
      title: 'Excelente!',
      text: 'Hemos recibido la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    })
    .then(function (response) {
      window.location.href = site_url + 'dashboard/packages';
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmPackage);

    if (status === 422) {
      errorAjax(frmPackage, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    let input, msg, parent;

    input = $(frm + ' *[name="' + item + '"]');
    parent = $(input).parent()[0];

    if (item === 'destino' || item === 'categoria' || item === 'subcategoria' || item === 'salida' || item === 'regreso' || item === 'vigencia' || item === 'imagen_1' || item === 'imagen_2') {
      msg = $(parent).children()[2];
    } else {
      msg = $(parent).children()[3];
    }

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
