@extends('admin.layouts.app')

@section('title', 'Discover')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner">
  <header class="content__title">
    <h1>Paquetes</h1>

    <div class="actions">
      <a href="{{ url('dashboard/packages/create') }}" class="actions__item zmdi zmdi-plus"></a>
      <a href="" class="actions__item zmdi zmdi-refresh-alt"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table id="data-table" class="table table-bordered">
          <thead class="thead-default">
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Destino</th>
              <th>Categoría</th>
              <th>Subcategoría</th>
              <th>Destacado</th>
              <th>Promoción</th>
              <th>Vigencia</th>
              <th>Público</th>
              <th></th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Destino</th>
              <th>Categoría</th>
              <th>Subcategoría</th>
              <th>Destacado</th>
              <th>Promoción</th>
              <th>Vigencia</th>
              <th>Público</th>
              <th></th>
            </tr>
          </tfoot>
          <tbody>
            @foreach($packages as $package)
            <tr id="package-{{ $package->id }}">
              <td>{{ $package->id }}</td>
              <td>{{ $package->nombre }}</td>
              <td>{{ $package->destination->nombre }}</td>
              <td>{{ $package->pt_nombre }}</td>
              <td>{{ $package->ps_nombre }}</td>
              <td>
                <span class="badge badge-pill{{ (boolean) $package->destacado ? ' badge-warning':' badge-primary' }}">
                  {{ (boolean) $package->destacado ?   'Si-'.$package->orden_destacado : 'No' }}
                </span>
              </td>
              <td>
                <span class="badge badge-pill{{ (boolean) $package->promocion ? ' badge-primary':' badge-warning' }}">
                  {{ (boolean) $package->promocion ? 'Si' : 'No' }}
                </span>
              </td>
              <td>
                <?php
                $estado = 'Inactivo';
                $isActive = TRUE;

                if (is_null($package->vigencia)) {
                  if ($package->vigencia === '0000-00-00') {
                    $package->vigencia = date('Y-m-d');
                  }

                  $package->vigencia = date('Y-m-d');
                }

                $vigencia = \DateTime::createFromFormat('Y-m-d', $package->vigencia);
                $now = new DateTime('now');

                if ((boolean) $package->estado === TRUE) {
                  if ($now > $vigencia) {
                    $estado = 'Vencido';
                    $isActive = FALSE;
                  } else {
                    $estado = 'Activo';
                  }
                } else {
                  $isActive = FALSE;
                }

                ?>
                <span class="badge badge-pill{{ $isActive ? ' badge-success':' badge-danger' }}">
                  {{ $estado }}
                </span>
              </td>
              <td>
                <span class="badge badge-pill{{ (boolean) $package->publico ? ' badge-primary':' badge-warning' }}">
                {{ (boolean) $package->publico ? 'Si' : 'No' }}
                <a class="btn btn-sm btn-publico" data-id="{{ $package->id }}" href="javascript:;" role="button" title="Cambiar estado visualizacion">
                  <i class="zmdi zmdi-edit"></i>
                </a>
                </span>
              </td>
              <td>
                <a class="btn btn-dark btn-sm" href="{{ url('dashboard/packages/'.$package->id.'/edit') }}" role="button" title="Editar">
                  <i class="zmdi zmdi-edit"></i>
                </a>
                <a class="btn btn-danger btn-sm btn-eliminar" data-id="{{ $package->id }}" href="javascript:;" role="button" title="Eliminar">
                  <i class="zmdi zmdi-delete"></i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>

<script type="text/javascript">

const _token = '{{ csrf_token() }}';
const site_url = '{{ url('/') }}/';

$('#data-table .btn-eliminar').click(function () {
  const id = $(this).attr('data-id');

  swal({
      title: 'Estas seguro de eliminar?',
      text: 'Si procede, no podrá recuperar el registro',
      type: 'warning',
      showCancelButton: true,
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonClass: 'btn btn-secondary'
  }).then(function (willDelete) {
    if (willDelete.value === true) {
      $.ajax({
        method: 'post',
        url: site_url + 'dashboard/packages/' + id,
        data: { id: id, _method: 'DELETE', _token: _token }
      })
      .done(function (success) {
        $('#package-' + id).remove();

        swal({
          title: 'Excelente!',
          text: 'Hemos eliminado el registro',
          type: 'success',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      })
      .fail(function (error) {
        swal({
          title: 'Ups!',
          text: 'Hubo un error al eliminar el registro',
          type: 'info',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      });
    }
  });
});
$('#data-table .btn-publico').click(function () {
  const id = $(this).attr('data-id');

  swal({
      title: 'Estas seguro cambiar estado público?',
      type: 'warning',
      showCancelButton: true,
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si, cambiar!',
      cancelButtonClass: 'btn btn-secondary'
  }).then(function (willDelete) {
    if (willDelete.value === true) {
      $.ajax({
        method: 'post',
        url: site_url + 'dashboard/cambiarestadopublico/' + id,
        data: { id: id, _token: _token }
      })
      .done(function (success) {

        swal({
          title: 'Excelente!',
          text: 'Hemos actualizado su registro',
          type: 'success',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
        location.reload();
      })
      .fail(function (error) {
        swal({
          title: 'Ups!',
          text: 'Hubo un error al eliminar el registro',
          type: 'info',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      });
    }
  });
});
</script>
@endsection
