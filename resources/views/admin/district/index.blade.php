@extends('admin.layouts.app')

@section('title', 'Distritos')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Distritos</h1>

    <div class="actions">
      <a href="{{ url('dashboard/districts/create') }}" class="actions__item zmdi zmdi-plus"></a>
      <a href="" class="actions__item zmdi zmdi-refresh-alt"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table id="data-table" class="table table-bordered">
          <thead class="thead-default">
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Provincia</th>
              <th>País</th>
              <th>Estado</th>
              <th></th>
            </tr>
          </thead>
          <tfoot>
            <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Provincia</th>
            <th>País</th>
            <th>Estado</th>
            <th></th>
            </tr>
          </tfoot>
          <tbody>
            @foreach($districts as $district)
            <tr id="district-{{ $district->id }}">
              <td>{{ $district->id }}</td>
              <td>{{ $district->nombre }}</td>
              <td>{{ $district->provincia }}</td>
              <td>{{ $district->pais }}</td>
              <td>
                <span class="badge badge-pill{{ (bool) $district->estado === TRUE ? ' badge-success':'badge-danger' }}">
                  {{ (bool) $district->estado === TRUE ? 'Activo':'Inactivo' }}
                </span>
              </td>
              <td>
                <a class="btn btn-dark btn-sm" href="{{ url('dashboard/districts/'.$district->id.'/edit') }}" role="button" title="Editar">
                  <i class="zmdi zmdi-edit"></i>
                </a>
                <a class="btn btn-danger btn-sm btn-eliminar" data-id="{{ $district->id }}" href="javascript:;" role="button" title="Eliminar">
                  <i class="zmdi zmdi-delete"></i>
                </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>

<script type="text/javascript">

const _token = '{{ csrf_token() }}';
const site_url = '{{ url('/') }}/';

$('#data-table .btn-eliminar').click(function () {
  const id = $(this).attr('data-id');

  swal({
      title: 'Estas seguro de eliminar?',
      text: 'Si procede, no podrá recuperar el registro',
      type: 'warning',
      showCancelButton: true,
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonClass: 'btn btn-secondary'
  }).then(function (willDelete) {
    if (willDelete.value === true) {
      $.ajax({
        method: 'post',
        url: site_url + 'dashboard/districts/' + id,
        data: { id: id, _method: 'DELETE', _token: _token }
      })
      .done(function (success) {
        $('#district-' + id).remove();

        swal({
          title: 'Excelente!',
          text: 'Hemos eliminado el registro',
          type: 'success',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      })
      .fail(function (error) {
        swal({
          title: 'Ups!',
          text: 'Hubo un error al eliminar el registro',
          type: 'info',
          buttonsStyling: false,
          confirmButtonClass: 'btn btn-primary'
        });
      });
    }
  });
});
</script>
@endsection
