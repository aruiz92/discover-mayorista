@extends('admin.layouts.app')

@section('title', 'Crear Distrito')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Crear Distrito</h1>

    <div class="actions">
      <a href="{{ url('dashboard/districts') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <form id="frmProvince" action="{{ url('dashboard/districts') }}" method="post">
        {{ csrf_field() }}

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>País</label>
              <select id="selectCountry" class="form-control" name="pais">
                <option value="">Seleccionar</option>
                @foreach($countries as $country)
                  <option value="{{ $country->id }}">{{ $country->nombre }}</option>
                @endforeach
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group">
              <label>Provincia</label>
              <select id="selectProvince" class="form-control" name="provincia">
                <option value="">Seleccionar</option>
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <div class="form-group form-group--float">
          <input type="text" class="form-control" name="nombre">
          <label>Nombre</label>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>

        <button type="submit" class="btn btn-primary">Crear</button>
      </form>
    </div>
  </div>
</div>

<!-- Componentes -->
<script type="text/template" id="component-select">
  <option value="<%= data.id %>"><%= data.nombre %></option>
</script>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/lodash.min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/underscore-min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/jquery.component.min.js') }}"></script>

<script type="text/javascript">

const site_url = '{{ url('/') }}/';
const frmProvince = '#frmProvince';

let coSelect = $.component({
  template: $('#component-select').html()
});

// Select PackageType
$('#selectCountry').change(function () {
  const value = $(this).val();

  $.ajax({
    method: 'get',
    url: site_url + 'dashboard/ajaxProvinces/' + value,
  })
  .done(function (success) {
    // Vaciando componente
    $('#selectProvince').html('');
    // Agregar component tr
    $('#selectProvince').append(coSelect.render({ id: '', nombre:'Seleccionar' }));
    $.each(success, function (index, value) {
      $('#selectProvince').append(coSelect.render(value));
    });
  })
  .fail(function (error) {
    console.log(success);
  });
});

$(frmProvince).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmProvince);

    swal({
      title: 'Excelente!',
      text: 'Hemos recibido la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    })
    .then(function (response) {
      window.location.href = site_url + 'dashboard/districts';
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmProvince);

    if (status === 422) {
      errorAjax(frmProvince, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    const input = $(frm + ' *[name="' + item + '"]');
    const parent = $(input).parent()[0];
    const msg = $(parent).children()[3];

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
