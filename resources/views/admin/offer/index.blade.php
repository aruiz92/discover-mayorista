@extends('admin.layouts.app')

@section('title', 'Oferta')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/dropzone/dist/dropzone.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Oferta</h1>

    <div class="actions">
      <a href="" class="actions__item zmdi zmdi-refresh-alt"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <form id="frmOffer" action="{{ url('dashboard/offers/'.$offer->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">

        <div class="form-group form-group--float">
          <input type="text" class="form-control" name="titulo" value="{{ $offer->titulo }}">
          <label>Título</label>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>

        <div class="form-group form-group--float">
          <input type="text" class="form-control" name="precio_min" value="{{ $offer->precio_min }}">
          <label>Precio mínimo</label>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>

        <div class="form-group form-group--float">
          <input type="text" class="form-control" name="url_accion" value="{{ $offer->url_accion }}">
          <label>URL Acción</label>
          <i class="form-group__bar"></i>
          <div class="invalid-feedback"></div>
        </div>

        <div class="form-group">
          <div id="file" class="dropzone"></div>
          <input type="hidden" class="form-control" name="imagen_bg" value="{{ $offer->imagen_bg }}">
          <small id="emailHelp" class="form-text text-muted">Recomendaciones: 952x186 — JPG, PNG</small>
          <div class="invalid-feedback"></div>
        </div>

        <button type="submit" class="btn btn-primary">Actualizar</button>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
Dropzone.autoDiscover = false;

const site_url = '{{ url('/') }}/';
const frmOffer = '#frmOffer';
const _token = '{{ csrf_token() }}';

var fileDropzone = [];
const mockFile = {
   name: '{{ $offer->imagen_bg }}',
   size: {{ Storage::disk('discover')->size('offers/'.$offer->imagen_bg) }},
   type: '{{ Storage::disk('discover')->mimeType('offers/'.$offer->imagen_bg) }}',
   accepted: true,
   dataURL: '{{ url('files/offers/'.$offer->imagen_bg) }}'
};

$(document).ready(function () {
  fileDropzone = new Dropzone('#file', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilesPackage',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: '',
    dictDefaultMessage: 'Selecciona o arrastra la imagen',
    acceptedFiles: 'image/*',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.files.push(mockFile);
      dropZone.emit('addedfile', mockFile);
      dropZone.createThumbnailFromUrl(
      	mockFile,
      	dropZone.options.thumbnailWidth,
      	dropZone.options.thumbnailHeight,
      	dropZone.options.thumbnailMethod,
      	true,
      	function(thumbnail) {
      		dropZone.emit('thumbnail', mockFile, thumbnail);
      		dropZone.emit("complete", mockFile);
      	}
      );

      mockFile.previewElement.classList.add('dz-success');
      mockFile.previewElement.classList.add('dz-complete');

      dropZone.on('removedfile', function (file) {
        if (dropZone.files.length === 0) {
          $('input[name="imagen_bg"]').val("");
        }
      });

      dropZone.on('addedfile', function(file) {
        if (dropZone.files.length > 1) {
          swal({
            title: 'Estas seguro de cambiar?',
            text: 'Si procede, se cambiará la imagen actual',
            type: 'info',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, continuar!',
            cancelButtonClass: 'btn btn-secondary'
          }).then(function (response) {
            if (response.value === true) {
              dropZone.removeAllFiles();
              dropZone.addFile(file);
            } else {
              dropZone.removeFile(file);
            }
          });
        }
      });
    },
    success: function (file, response) {
      $(frmOffer + ' input[name="imagen_bg"]').val(response.fileName);
    }
  });
});

$(frmOffer).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    swal({
        title: 'Excelente!',
        text: 'Hemos actualizado la información correctamente',
        type: 'success',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary'
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    if (status === 422) {
      errorAjax(frmOffer, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    const input = $(frm + ' *[name="' + item + '"]');
    const parent = $(input).parent()[0];
    const msg = $(parent).children()[3];

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}
</script>
@endsection
