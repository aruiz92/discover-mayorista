@extends('admin.layouts.app')

@section('title', 'Editar Categoría')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/trumbowyg/dist/ui/trumbowyg.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/dropzone/dist/dropzone.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/animate.css/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/flatpickr/flatpickr.min.css') }}" />
<style media="screen">
.slider {
  margin-bottom: 40px;
}
.form-group--float .form-control:focus~label.textarea {
    bottom: 3.4rem;
}
.row-listview {
  margin-left: -30px;
  margin-right: -30px;
}
.trumbowyg-box, .trumbowyg-editor {
  min-height: 100px;
  margin-top: 0;
}
.trumbowyg-editor, .trumbowyg-textarea {
  min-height: 100px;
}
</style>
@endsection

@section('content')

<div class="content__inner content__inner--sm">

  <header class="content__title">
    <h1>Editar Categoría</h1>

    <div class="actions">
      <a href="{{ url('dashboard/package_types') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <form id="frmPackageType" action="{{ url('dashboard/package_types/'.$package_type->id) }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombre" value="{{ $package_type->nombre }}">
              <label>Nombre</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="orden" value="{{ $package_type->orden }}">
              <label>Orden</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Sliders</h4>
        <div class="actions">
          <a href="javascript:;" class="actions__item zmdi zmdi-plus btn-modal-slider"></a>
        </div>
        <div class="table-responsive">
          <table class="table table-slider mb-0">
            <thead>
              <tr>
                <th>#</th>
                <th>Destino</th>
                <th>Orden</th>
                <th></th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>

    <button type="submit" class="btn btn-primary">Actualizar</button>
  </form>

</div>

<!-- Modals -->
@include('admin.package_type.includes.modals')
<!-- Componentes -->
@include('admin.package_type.includes.components')

@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/trumbowyg/dist/trumbowyg.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/autosize/dist/autosize.min.js') }}"></script>
<script src="{{ asset('admin/vendors/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('admin/vendors/flatpickr/es.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/lodash.min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/underscore-min.js') }}"></script>
<script src="{{ asset('admin/vendors/jquery.component/jquery.component.min.js') }}"></script>

<script type="text/javascript">
Dropzone.autoDiscover = false;

const jsonSliders = {!! $package_type_banners !!};

let sliderEdit = false;
let sliderIdEdit = null;

let sliders = [];
let fileDropzone = [];

const site_url = '{{ url('/') }}/';
const _token = '{{ csrf_token() }}';
const frmPackageType = '#frmPackageType';

let trSlider = $.component({
  template: $('#tr-slider').html()
});

$('.btn-modal-slider').click(function () {
  const orden = sliders.length + 1;
  // Limpiando elementos modal
  $('#modal-slider input[name="imagen"]').val('');
  $('#modal-slider input[name="titulo"]').val('');
  $('#modal-slider input[name="subtitulo"]').val('');
  $('#modal-slider textarea[name="descripcion"]').val('');
  $('#modal-slider input[name="orden"]').val('');
  // Asignando valor orden
  $('#modal-slider input[name="orden"]').val(orden);
  // Mostrando Modal
  $('#modal-slider').modal('show');
  // Clean Dropzone
  fileDropzone.removeAllFiles();
  // Estableciendo modo
  sliderEdit = false;
  // Editando modal
  $('#modal-slider .modal-title').text('Agregar Slider');
  $('#modal-slider .btn-add').text('Agregar');
});

$('#modal-slider .btn-add').click(function () {
  const destino_id = $('#modal-slider select[name="destino"]').val();
  const destino_nombre = $('#modal-slider select[name="destino"] option[value="' + destino_id + '"]').text();
  const imagen = $('#modal-slider input[name="imagen"]').val();
  const titulo = $('#modal-slider input[name="titulo"]').val();
  const subtitulo = $('#modal-slider input[name="subtitulo"]').val();
  const descripcion = $('#modal-slider textarea[name="descripcion"]').val();
  const orden = $('#modal-slider input[name="orden"]').val();

  // Construyendo datos del tab
  const data = {
    id: sliders.length + 1,
    idd: null,
    modo: 'crear',
    d_id: destino_id,
    d_nombre: destino_nombre,
    titulo: titulo,
    subtitulo: subtitulo,
    descripcion: descripcion,
    imagen: imagen,
    orden: orden,
    file: fileDropzone.files[0]
  };

  if (sliderEdit) {
    // Editar
    data.idd = sliders[sliderIdEdit].idd;
    data.modo = sliders[sliderIdEdit].modo;
    sliders[sliderIdEdit] = data;
  } else {
    // Agregar
    sliders.push(data);
  }

  // Limpiando component tr}
  $('.table-slider tbody').html('');
  // Agregando slider al component tr
  for (let index = 0; index < sliders.filter(function (item) {
    return item.modo !== 'eliminar'
  }).length; index++) {
    // Asignando nuevo id
    sliders[index].id = index + 1;
    // Agregar component tr
    $('.table-slider tbody').append(trSlider.render(sliders[index]));
  }

  // Ocultar modal
  $('#modal-slider').modal('hide');
});

$('.table-slider').on('click', '.btn-eliminar', function () {
  const id = $(this).attr('data-id');
  const index = id - 1;

  sliders[index].modo = 'eliminar';

  // Filtrando modo
  const filterSliders = sliders.filter(function (item) {
    return item.modo !== 'eliminar'
  });
  // Limpiando component tr
  $('.table-slider tbody').html('');
  // Agregando al component tr
  for (let indexx = 0; indexx < filterSliders.length; indexx++) {
    // Asignando nuevo id
    filterSliders[indexx].id = indexx + 1;
    // Agregar component tr
    $('.table-slider tbody').append(trSlider.render(filterSliders[indexx]));
  }
});

$('.table-slider').on('click', '.btn-editar', function () {
  const id = $(this).attr('data-id');
  const index = id - 1;

  // Reasignando valores
  $('#modal-slider select[name="destino"] option[value="' + sliders[index].d_id + '"]').prop('selected', true);
  $('#modal-slider input[name="titulo"]').val(sliders[index].titulo);
  $('#modal-slider input[name="subtitulo"]').val(sliders[index].subtitulo);
  $('#modal-slider textarea[name="descripcion"]').val(sliders[index].descripcion);
  $('#modal-slider input[name="orden"]').val(sliders[index].orden);

  // Limpiando Dropzone
  fileDropzone.removeAllFiles();
  // Reasignando dropzone
  const editFile = sliders[index].file;

  fileDropzone.files.push(editFile);
  fileDropzone.emit('addedfile', editFile);
  fileDropzone.createThumbnailFromUrl(
    editFile,
    fileDropzone.options.thumbnailWidth,
    fileDropzone.options.thumbnailHeight,
    fileDropzone.options.thumbnailMethod,
    true,
    function(thumbnail) {
      fileDropzone.emit('thumbnail', editFile, thumbnail);
      fileDropzone.emit('complete', editFile);
    }
  );

  // Asignando valor
  $('#modal-slider input[name="imagen"]').val(sliders[index].imagen);

  // Editando modal
  $('#modal-slider .modal-title').text('Editar Slider');
  $('#modal-slider .btn-add').text('Editar');

  // Mostrando modal
  $('#modal-slider').modal('show');

  // Estableciendo modo
  sliderEdit = true;
  sliderIdEdit = index;
});


// Dropzone: Subir files
$(document).ready(function () {
  // Dropzone
  fileDropzone = new Dropzone('#fileSlider', {
    headers: {
      'X-CSRF-TOKEN': _token
    },
    url: site_url + 'dashboard/ajaxFilesPackage',
    method: 'post',
    addRemoveLinks: true,
    parallelUploads: true,
    uploadMultiple: false,
    autoProcessQueue: true,
    resizeMethod: 'crop',
    maxFiles: 1,
    dictRemoveFile: '',
    dictCancelUpload: '',
    dictUploadCanceled: '',
    dictDefaultMessage: 'Selecciona o arrastra la imagen',
    acceptedFiles: 'image/*',
    error: function (file, errorMessage) {
      $(file.previewElement).find('.dz-error-message span').text('No puedes subir archivos de este tipo.');
      $(file.previewElement).addClass('dz-error');
    },
    init: function() {
      let dropZone = this;

      dropZone.on('removedfile', function (file) {
        if (dropZone.files.length === 0) {
          $('#modal-slider input[name="imagen"]').val('');
        }
      });

      dropZone.on('addedfile', function(file) {
        if (dropZone.files.length > 1) {
          swal({
            title: 'Estas seguro de cambiar?',
            text: 'Si procede, se cambiará la documentación actual',
            type: 'info',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Si, continuar!',
            cancelButtonClass: 'btn btn-secondary'
          }).then(function (response) {
            if (response.value === true) {
              dropZone.removeAllFiles();
              dropZone.addFile(file);
            } else {
              dropZone.removeFile(file);
            }
          });
        }
      });
    },
    success: function (file, response) {
      $('#modal-slider input[name="imagen"]').val(response.fileName);
    }
  });

  // Inicializando
  $.each(jsonSliders, function (index, item) {
    const mockFile = {
       name: item.imagen,
       size: item.size,
       type: item.type,
       accepted: true,
       dataURL: site_url + 'files/package_type_banners/' + item.imagen
    };

    fileDropzone.files.push(mockFile);
    fileDropzone.emit('addedfile', mockFile);
    fileDropzone.createThumbnailFromUrl(
      mockFile,
      fileDropzone.options.thumbnailWidth,
      fileDropzone.options.thumbnailHeight,
      fileDropzone.options.thumbnailMethod,
      true,
      function(thumbnail) {
        fileDropzone.emit('thumbnail', mockFile, thumbnail);
        fileDropzone.emit('complete', mockFile);
      }
    );

    const itemSlider = {
      id: index + 1,
      idd: item.id,
      modo: 'editar',
      d_id: item.d_id,
      d_nombre: item.d_nombre,
      titulo: item.titulo,
      subtitulo: item.subtitulo,
      descripcion: item.descripcion,
      imagen: item.imagen,
      orden: item.orden,
      file: fileDropzone.files[index]
    };

    sliders.push(itemSlider);

    $('.table-slider tbody').append(trSlider.render(itemSlider));
  });
});

$(frmPackageType).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  let data = $(this).serializeArray();

  const dataSliders = [];
  $.each(sliders, function (index, item) {
    dataSliders.push({
      id: item.id,
      idd: item.idd,
      modo: item.modo,
      d_id: item.d_id,
      d_nombre: item.d_nombre,
      titulo: item.titulo,
      subtitulo: item.subtitulo,
      descripcion: item.descripcion,
      imagen: item.imagen,
      orden: item.orden
    });
  });

  data.push({
    name: 'sliders',
    value: JSON.stringify(dataSliders)
  });

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmPackageType);

    swal({
      title: 'Excelente!',
      text: 'Hemos actualizado la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmPackageType);

    if (status === 422) {
      errorAjax(frmPackageType, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    let input, msg, parent;

    input = $(frm + ' *[name="' + item + '"]');
    parent = $(input).parent()[0];

    if (item === 'imagen') {
      msg = $(parent).children()[2];
    } else {
      msg = $(parent).children()[3];
    }

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
