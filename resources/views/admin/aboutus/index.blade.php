@extends('admin.layouts.app')

@section('title', 'Nosotros')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/trumbowyg/dist/ui/trumbowyg.min.css') }}">
<style media="screen">
.trumbowyg-box, .trumbowyg-editor {
  min-height: 100px;
  margin-top: 0;
}
.trumbowyg-editor, .trumbowyg-textarea {
  min-height: 100px;
}
</style>
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Nosotros</h1>

    <div class="actions">
      <a href="" class="actions__item zmdi zmdi-refresh-alt"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <form id="frmAboutus" action="{{ url('dashboard/aboutus/'.$aboutus->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
          <label>Quienes Somos</label>
          <div class="wysiwyg-quienes-somos">{!! $aboutus->quienes_somos !!}</div>
          <div class="invalid-feedback quienes_somos"></div>
        </div>

        <div class="form-group">
          <label>Misión</label>
          <div class="wysiwyg-mision">{!! $aboutus->mision !!}</div>
          <div class="invalid-feedback mision"></div>
        </div>

        <div class="form-group">
          <label>Visión</label>
          <div class="wysiwyg-vision">{!! $aboutus->vision !!}</div>
          <div class="invalid-feedback vision"></div>
        </div>

        <div class="form-group">
          <label>Ventajas Corporativas</label>
          <div class="wysiwyg-ventajas">{!! $aboutus->ventajas !!}</div>
          <div class="invalid-feedback ventajas"></div>
        </div>

        <button type="submit" class="btn btn-primary">Actualizar</button>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('admin/vendors/bower_components/trumbowyg/dist/trumbowyg.min.js') }}"></script>

<script type="text/javascript">
const site_url = '{{ url('/') }}/';
const frmAboutus = '#frmAboutus';
const btns = [
  ['viewHTML'],
  ['undo', 'redo'],
  ['strong', 'em', 'del'],
  ['unorderedList', 'orderedList'],
  ['removeformat'],
  ['fullscreen']
];

$('.wysiwyg-quienes-somos').trumbowyg({
  btns: btns,
  autogrow: true
});

$('.wysiwyg-mision').trumbowyg({
  btns: btns,
  autogrow: true
});

$('.wysiwyg-vision').trumbowyg({
  btns: btns,
  autogrow: true
});

$('.wysiwyg-ventajas').trumbowyg({
  btns: btns,
  autogrow: true
});

$(frmAboutus).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();
  const editores = '&quienes_somos=' + $('.wysiwyg-quienes-somos').trumbowyg('html')
                 + '&mision=' + $('.wysiwyg-mision').trumbowyg('html')
                 + '&vision=' + $('.wysiwyg-vision').trumbowyg('html')
                 + '&ventajas=' + $('.wysiwyg-ventajas').trumbowyg('html');

  $.ajax({
    method: method,
    url: url,
    data: data + editores
  })
  .done(function (success) {
    swal({
        title: 'Excelente!',
        text: 'Hemos actualizado la información correctamente',
        type: 'success',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary'
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    if (status === 422) {
      errorAjax(frmAboutus, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    swal({
        title: 'Upsss!',
        text: value[0],
        type: 'warning',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary'
    });
  });
}
</script>
@endsection
