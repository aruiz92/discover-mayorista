@extends('admin.layouts.app')

@section('title', 'Editar Provincia')

@section('styles')
<link rel="stylesheet" href="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection

@section('content')
<div class="content__inner content__inner--sm">
  <header class="content__title">
    <h1>Editar Provincia</h1>

    <div class="actions">
      <a href="{{ url('dashboard/provinces') }}" class="actions__item zmdi zmdi-arrow-left"></a>
    </div>
  </header>

  <div class="card">
    <div class="card-body">
      <form id="frmProvince" action="{{ url('dashboard/provinces/'.$province->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">

        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label>País</label>
              <select id="selectCountry" class="form-control" name="pais">
                <option value="">Seleccionar</option>
                @foreach($countries as $country)
                  <option value="{{ $country->id }}"{{ (int) $province->country_id === (int) $country->id ? ' selected':'' }}>{{ $country->nombre }}</option>
                @endforeach
              </select>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="form-group form-group--float">
              <input type="text" class="form-control" name="nombre" value="{{ $province->nombre }}">
              <label>Nombre</label>
              <i class="form-group__bar"></i>
              <div class="invalid-feedback"></div>
            </div>
          </div>
        </div>

        <button type="submit" class="btn btn-primary">Actualizar</button>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js') }}"></script>

<script type="text/javascript">

const site_url = '{{ url('/') }}/';
const frmProvince = '#frmProvince';

$(frmProvince).submit(function (event) {
  const method = $(this).attr('method');
  const url = $(this).attr('action');
  const data = $(this).serialize();

  $.ajax({
    method: method,
    url: url,
    data: data
  })
  .done(function (success) {
    clearErrorAjax(frmProvince);

    swal({
      title: 'Excelente!',
      text: 'Hemos actualizado la información correctamente',
      type: 'success',
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-primary'
    });
  })
  .fail(function (error) {
    const status = error.status;
    const errors = error.responseJSON;

    clearErrorAjax(frmProvince);

    if (status === 422) {
      errorAjax(frmProvince, errors);
    }
  });

  event.preventDefault();
});

function errorAjax(frm, errors) {
  $.each(errors, function (item, value) {
    const input = $(frm + ' *[name="' + item + '"]');
    const parent = $(input).parent()[0];
    const msg = $(parent).children()[3];

    $(input).addClass('is-invalid');
    $(msg).html(value[0]);
  });
}

function clearErrorAjax(frm) {
  $(frm + ' input, ' + frm + ' select, ' + frm + ' textarea').removeClass('is-invalid');
}
</script>
@endsection
