<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>@yield('title')</title>
@include('admin.partials.head')
@yield('styles')
<!-- App styles -->
<link rel="stylesheet" href="{{ asset('admin/css/app.min.css') }}">
</head>

<body data-ma-theme="blue">
<main class="main">
  <div class="page-loader">
    <div class="page-loader__spinner">
      <svg viewBox="25 25 50 50">
        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
      </svg>
    </div>
  </div>

  @include('admin.partials.header')
  @include('admin.partials.sidebar')
  <!-- @include('admin.partials.chat') -->

  <section class="content">
    @yield('content')

    @include('admin.partials.footer')
  </section>
</main>

<!-- Older IE warning message -->
<!--[if IE]>
  <div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade to any of the following web browsers to access this website.</p>

    <div class="ie-warning__downloads">
      <a href="http://www.google.com/chrome">
        <img src="img/browsers/chrome.png" alt="">
      </a>

      <a href="https://www.mozilla.org/en-US/firefox/new">
        <img src="img/browsers/firefox.png" alt="">
      </a>

      <a href="http://www.opera.com">
        <img src="img/browsers/opera.png" alt="">
      </a>

      <a href="https://support.apple.com/downloads/safari">
        <img src="img/browsers/safari.png" alt="">
      </a>

      <a href="https://www.microsoft.com/en-us/windows/microsoft-edge">
        <img src="img/browsers/edge.png" alt="">
      </a>

      <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
        <img src="img/browsers/ie.png" alt="">
      </a>
    </div>
    <p>Sorry for the inconvenience!</p>
  </div>
<![endif]-->

<!-- Javascript -->
@include('admin.partials.script')
@yield('scripts')
<!-- App functions and actions -->
<script src="{{ asset('admin/js/app.js') }}"></script>
</body>
</html>
