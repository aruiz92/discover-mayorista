@extends('frontend.layout.layout-internal') @section('title', 'Ingreso de agentes') @section('description', 'Discover')

@section('fusion-style')
<link rel="stylesheet" href="{{ asset('css/fusion-419.css')}}" />
@stop

@section('custom-style')
<link rel="stylesheet" href="{{ asset('css/ingreso-agentes.custom.css')}}" />
@stop

@section('custom-script')
<script type='text/javascript' src="{{ url('js/contact-script.js') }}"></script>
@stop

@section('style')

@stop

@section('body-style')
page-template-default page page-id-419 top-parent-419 fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-totop no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text
@stop


@section('content')

@include('frontend.partials.header')
<div id="sliders-container"></div>

<div id="main" role="main" class="clearfix " style="">
  <div class="fusion-row" style="">
    <div id="content" style="width: 100%;">
      <div id="post-423" class="post-423 page type-page status-publish hentry">
        <span class="entry-title rich-snippet-hidden">Perfil</span>
        <span class="vcard rich-snippet-hidden">
          <span class="fn">
            <a href="{{ url('') }}" title="Entradas de Discover Mayorista" rel="author">Discover Mayorista</a>
          </span>
        </span>
        <span class="updated rich-snippet-hidden">2017-05-21T23:25:28+00:00</span>
        <div class="post-content">
          <div class="fusion-fullwidth fullwidth-box fusion-blend-mode nonhundred-percent-fullwidth" style="background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;">
            <div class="fusion-builder-row fusion-row ">
              <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-first 1_5" style="margin-top:0px;margin-bottom:0px;width:16.8%; margin-right: 4%;">
                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                  <div class="fusion-clearfix"></div>
                </div>
              </div>
              <div class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth 3_5" style="margin-top:0px;margin-bottom:0px;width:58.4%; margin-right: 4%;">
                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                  <div id="wpmem_login">
                    <div class="successFrm" style="text-align: center; display:none;">
                      <h1>Excelente!</h1>
                      <p>Hemos enviado un enlace de restablecimiento de contraseña a tu correo.</p>
                    </div>
                    <form method="POST" action="{{ url('/password/reset') }}" class="form">
                      {{ csrf_field() }}
                      <input type="hidden" name="token" value="{{ $token }}">

                      <fieldset>
                        <legend>Cambiar contraseña</legend>
                        <div class="div-wrapper-field">
                          <label for="email">Correo electrónico</label>
                          <div class="div_text">
                            <input id="email" type="email" class="password{{ $errors->has('email') ? ' has-error' : '' }}" name="email" value="{{ $email or old('email') }}">
                          </div>
                        </div>

                        <div class="div-wrapper-field">
                          <label for="email">Contraseña</label>
                          <div class="div_text">
                            <input id="password" type="password" name="password" class="password{{ $errors->has('password') ? ' has-error' : '' }}">
                          </div>
                        </div>

                        <div class="div-wrapper-field">
                          <label for="email">Repetir Contraseña</label>
                          <div class="div_text">
                            <input id="password-confirm" type="password" name="password_confirmation" class="password{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                          </div>
                        </div>

                        <div class="button_div">
                          <input id="btnSubmit" type="submit" name="Submit" value="Cambiar contraseña" class="buttons">
                        </div>
                      </fieldset>
                    </form>
                  </div>
                  <div class="fusion-clearfix"></div>
                </div>
              </div>
              <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-last 1_5" style="margin-top:0px;margin-bottom:0px;width:16.8%">
                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                  <div class="fusion-clearfix"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- fusion-row -->
</div>
@stop

@section('script')
@stop
