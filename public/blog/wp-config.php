<?php

// BEGIN iThemes Security - No modifiques ni borres esta línea
// iThemes Security Config Details: 2
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'C:\xampp563\htdocs\www\discover.test\public\blog\wp-content\plugins\wp-super-cache/' );
define( 'DISALLOW_FILE_EDIT', true ); // Desactivar editor de archivos - Seguridad > Ajustes > Ajustes WordPress > Editor de archivos
// END iThemes Security - No modifiques ni borres esta línea

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'discover_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wffjscrtauhiv7pplhi5cd14rwxud2au2yf2dxufjsevk7azzwam09u88rmwrfil');
define('SECURE_AUTH_KEY',  'ex17n1xx4sl63taqnyia1nzdfqwwtzbv072xnymsyyk0t6evaxdockkjqvuzja2z');
define('LOGGED_IN_KEY',    'gn2pq97mhfozlgzr18hsxdyggbcte91z5xghqzccoorimexhrckmc4fkcsd33q2z');
define('NONCE_KEY',        'nukx3f2rrjijrdxmgvd4iq86vjdnplatp18dogkubxu7jix1db0qhcrxkzwrwvho');
define('AUTH_SALT',        '95hfydfxw49g0w30duhmg9faorhgctbg0md1zn6rugp5uzmm59vijadzzwmkthlb');
define('SECURE_AUTH_SALT', 'id1kgzeokmnewbxiknu2bujkbolzn5gdzivsifdgffzdbrmhbocruihzzcsw7rnm');
define('LOGGED_IN_SALT',   'ptrneknhzgwj55ed8feibf9g9fvv6hqr7qsmdgnumrhnuwurbcdw1hlxirnu2s1j');
define('NONCE_SALT',       'tqqnjnoz51pidiuiliwlbmqf8ijjxfnheapad27zql3xqv8xrlbefer6qjb2n4vs');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpdc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_AUTO_UPDATE_CORE', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
