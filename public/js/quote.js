jQuery.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
  }
});

jQuery('#selectPais').change(function () {
  var value = jQuery(this).val();

  if (value == 1) {
    jQuery('.group-provincia').addClass('d-block');
    var form = {
      method: 'GET',
      action: site_url + 'api/province/' + value,
      data: {}
    }

    ajaxLocation(form, 'selectProvincia');

  } else {
    jQuery('.group-provincia').removeClass('d-block');
    jQuery('.group-distrito').removeClass('d-block');
    jQuery('.group-agencia').removeClass('d-block');
  }
});

jQuery('#selectProvincia').change(function () {
  var value = jQuery(this).val();
  jQuery('.group-distrito').addClass('d-block');

  var form = {
    method: 'GET',
    action: site_url + 'api/district/' + value,
    data: {}
  }

  ajaxLocation(form, 'selectDistrito');
});

jQuery('#selectDistrito').change(function () {
  var value = jQuery(this).val();
  jQuery('.group-agencia').addClass('d-block');

  var form = {
    method: 'GET',
    action: site_url + 'api/agencies/district/' + value,
    data: {}
  }

  ajaxLocation(form, 'selectAgencia');
});

jQuery('.btnSolicitar').click(function () {
    var id = jQuery(this).attr('data-id');

    var paquete = packages.filter(function (item) {
      return item.id === parseInt(id);
    })[0];

    jQuery('#modalQuote .modal-title-quote').html(paquete.nombre);
    jQuery('#frmQuote input[name="package_id"]').val(paquete.id);
});


jQuery('#frmQuote').submit(function( event ) {
  var action = jQuery(this).attr('action');
  var method = jQuery(this).attr('method');
  var data = jQuery(this).serialize();

  jQuery('#btnSubmit').prop('disabled', true);
  jQuery('#btnSubmit').val('Enviando...');

  jQuery.ajax({
    method: method,
    url: action,
    data: data,
    success: function (response) {
      removeClassError();
      clearForm();
      jQuery('#btnSubmit').prop('disabled', false);
      jQuery('.group-provincia').removeClass('d-block');
      jQuery('.group-distrito').removeClass('d-block');
      jQuery('.group-agencia').removeClass('d-block');

      jQuery('#btnSubmit').val('Enviar');

      jQuery('#frmQuote').hide();
      jQuery('.frm-header').hide();
      jQuery('.frm-response h2').html('Excelente! Tenemos su cotización, muy pronto estaremos en contacto contigo.');
    }
  })
  .fail(function(error) {
    var status = error.status;
    var data = error.responseJSON;

    jQuery('#btnSubmit').prop('disabled', false);
    jQuery('#btnSubmit').val('Enviar');

    if (status === 422) {
      removeClassError();
      jQuery.each(data, function(key, value) {
        var input = jQuery('#frmQuote *[name="' + key + '"]').addClass('has-error');
      });
    }
  });

  event.preventDefault();
});

jQuery('.Form_Cotizar, .Form_Cotizar_1184').on('hidden.bs.modal', function (e) {
  jQuery('#frmQuote').show();
  jQuery('.frm-header').show();
  jQuery('.wpcf7-response-output').hide();
  jQuery('.frm-response h2').html('');
});

function clearForm() {
  document.getElementById('frmQuote').reset();
}

function removeClassError () {
  jQuery('#frmQuote input, #frmQuote select').removeClass('has-error');
}

function removeOption (select) {
  jQuery('#' + select).empty();
  jQuery('#' + select).append(jQuery('<option>', {
    value: "",
    text : 'Seleccionar'
  }));
}

function ajaxLocation (form, select) {
  removeOption(select);

  jQuery.ajax({
    method: form.method,
    url: form.action,
    data: form.data,
    success: function (response) {
      jQuery.each(response, function (key, item) {
        if (select === 'selectAgencia') {
          jQuery('#' + select).append(jQuery('<option>', {
            value: item.id,
            text : item.nombre_comercial
          }));
        } else {
          jQuery('#' + select).append(jQuery('<option>', {
            value: item.id,
            text : item.nombre
          }));
        }
      });
    }
  });
}
