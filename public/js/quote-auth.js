jQuery.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
  }
});

jQuery('.btnSolicitar').click(function () {
    var id = jQuery(this).attr('data-id');

    var paquete = packages.filter(function (item) {
      return item.id === parseInt(id);
    })[0];

    jQuery('#modalQuote .modal-title-quote').html(paquete.nombre);
    jQuery('#frmQuote input[name="package_id"]').val(paquete.id);
});

jQuery('#frmQuote').submit(function( event ) {
  var action = jQuery(this).attr('action');
  var method = jQuery(this).attr('method');
  var data = jQuery(this).serialize();

  jQuery('#btnSubmit').prop('disabled', true);
  jQuery('#btnSubmit').val('Enviando...');

  jQuery.ajax({
    method: method,
    url: action,
    data: data,
    success: function (response) {
      removeClassError();
      clearForm();

      jQuery('#btnSubmit').val('Enviar');

      jQuery('#frmQuote').hide();
      jQuery('.frm-header').hide();
      jQuery('.frm-response h2').html('Excelente! Tenemos su cotización, muy pronto estaremos en contacto contigo.');
    }
  })
  .fail(function(error) {
    var status = error.status;
    var data = error.responseJSON;

    jQuery('#btnSubmit').prop('disabled', false);
    jQuery('#btnSubmit').val('Enviar');

    if (status === 422) {
      removeClassError();
      jQuery.each(data, function(key, value) {
        var input = jQuery('#frmQuote *[name="' + key + '"]').addClass('has-error');
      });
    }
  });

  event.preventDefault();
});

jQuery('.Form_Cotizar, .Form_Cotizar_1184').on('hidden.bs.modal', function (e) {
  jQuery('#btnSubmit').prop('disabled', false);
  jQuery('#btnSubmit').val('Enviar');
  jQuery('#frmQuote').show();
  jQuery('.frm-header').show();
  jQuery('.wpcf7-response-output').hide();
  jQuery('.frm-response h2').html('');
});

function clearForm() {
  document.getElementById('frmQuote').reset();
}

function removeClassError () {
  jQuery('#frmQuote input, #frmQuote select').removeClass('has-error');
}
