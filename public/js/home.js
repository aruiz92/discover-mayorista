
jQuery('.lista').slick({
  infinite: true,
  slidesToShow: 6,
  slidesToScroll: 6,
  prevArrow: '<div class="slick-prev slick-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
  nextArrow: '<div class="slick-next slick-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
  adaptiveHeight: true,
  responsive: [
    {
      breakpoint: 1500,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 5,
        infinite: true,
      }
    },
    {
      breakpoint: 1300,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
      }
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
      }
    }
  ]
});

jQuery('#fusion-tab-reservas, #fusion-tab-paquetes, #mobile-fusion-tab-paquetes, #mobile-fusion-tab-reservas').on('shown.bs.tab', function (e) {
  jQuery('.lista').slick('setPosition');
});


jQuery('.home-slider').slick({
  dots: false,
  autoplay: true,
  autoplaySpeed: 7000,
  speed: 500,
  fade: true,
  infinite: true,
  arrows: true,
  prevArrow: '<div class="slick-prev slick-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
  nextArrow: '<div class="slick-next slick-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>',
});
