var now = moment()
moment.locale('es')
// var a = moment('2018-07').format('MMMM YYYY');
// console.log(a);
function fun_fecha_salidas_nrodias() {
    var id = $('#listpackages').val();
    var namepackage = $("#listpackages option:selected" ).text();
    if (namepackage != "" || namepackage != "Buscar Destino") {
        $('#fecha_salida').empty();
        $.ajax({
            async: false,
            type: 'GET',
            url: 'fecha_salida_nrodias/' + namepackage,
            data: {},
            dataType: 'json',
            success: function(data) {
              console.log(data);

                $.each(data, function(i, value) {
                if(!value.mes){
                $('#fecha_salida').append('<option value="">::Sin Fechas::</option>');
                }

                        $('#fecha_salida').append('<option value="' + value.mes + '">'  + moment(value.mes).format('MMM')+ value.anno + '</option>');



                });
            },
            error: function(jqXHR, status, err) {
              //  alert("Local error callback.");
                $('#fecha_salida').empty();
              $('#fecha_salida').append('<option value="">::Fecha::</option>');

            }
        });
    } else {
        $('#fecha_salida').empty();
        $('#fecha_salida').append('<option value="">::Fecha::</option>');
    }


    if (namepackage != "" || namepackage != "Buscar Destino") {
        $('#cant_dias').empty();
        $.ajax({
            async: false,
            type: 'GET',
            url: 'cantidaddias/' + namepackage,
            data: {},
            dataType: 'json',
            success: function(data) {
                $('#cant_dias').append('<option value="">::Días::</option>');
                $.each(data, function(i, value) {
                    $('#cant_dias').append('<option value="' + value.nro_dias + '">' + value.nro_dias + '</option>');
                });
            },
            error: function(jqXHR, status, err) {
                //alert("Local error callback.");
                $('#cant_dias').empty();
                $('#cant_dias').append('<option value="">::Días::</option>');
            }
        });
    } else {
        $('#cant_dias').empty();
        $('#cant_dias').append('<option value="">::Días::</option>');
    }
}
