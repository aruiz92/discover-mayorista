<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->tinyInteger('orden')->default(1);
            $table->string('archivo')->nullable();
            $table->boolean('estado')->default(TRUE);
            $table->integer('package_id')->unsigned();
            $table->timestamps();

            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('package_files');
    }
}
