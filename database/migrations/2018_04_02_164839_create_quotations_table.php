<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres')->nullable();
            $table->string('correo')->nullable();
            $table->string('asunto')->nullable();
            $table->mediumText('mensaje')->nullable();
            $table->boolean('estado')->default(TRUE);
            $table->timestamps();

            $table->integer('package_id')->unsigned();
            $table->integer('agency_id')->unsigned();

            $table->foreign('package_id')->references('id')->on('packages');
            $table->foreign('agency_id')->references('id')->on('agencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quotations');
    }
}
