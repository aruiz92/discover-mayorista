<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aboutus', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('quienes_somos')->nullable();
            $table->mediumText('vision')->nullable();
            $table->mediumText('mision')->nullable();
            $table->mediumText('ventajas')->nullable();
            $table->string('url_regulacion')->nullable();
            $table->boolean('estado')->default(TRUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aboutus');
    }
}
