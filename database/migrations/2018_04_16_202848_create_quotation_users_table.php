<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_users', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('mensaje');
            $table->boolean('estado')->default(TRUE);
            $table->timestamps();

            $table->integer('package_id')->unsigned();
            $table->integer('agency_user_id')->unsigned();

            $table->foreign('package_id')->references('id')->on('packages');
            $table->foreign('agency_user_id')->references('id')->on('agency_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quotation_users');
    }
}
