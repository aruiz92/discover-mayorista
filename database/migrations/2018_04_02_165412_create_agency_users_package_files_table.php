<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyUsersPackageFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_users_package_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agency_user_id')->unsigned();
            $table->integer('package_file_id')->unsigned();
            $table->timestamps();

            $table->foreign('agency_user_id')->references('id')->on('agency_users');
            $table->foreign('package_file_id')->references('id')->on('package_files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agency_users_package_files');
    }
}
