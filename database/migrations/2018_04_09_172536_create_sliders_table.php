<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagen');
            $table->string('texto_1')->nullable();
            $table->string('texto_2')->nullable();
            $table->string('texto_3')->nullable();
            $table->string('url')->default('#');
            $table->string('seccion')->default('home');
            $table->smallInteger('orden')->default(1);
            $table->boolean('state')->default(TRUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sliders');
    }
}
