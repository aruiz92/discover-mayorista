<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destination_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable();
            $table->string('subtitulo')->nullable();
            $table->mediumText('descripcion')->nullable();
            $table->string('imagen')->nullable();
            $table->string('href')->default('#');
            $table->tinyInteger('orden')->default(1);
            $table->boolean('estado')->default(TRUE);
            $table->integer('destination_id')->unsigned();
            $table->timestamps();

            $table->foreign('destination_id')->references('id')->on('destinations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destination_banners');
    }
}
