<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod_discover')->nullable();
            $table->string('nombre')->nullable();
            $table->mediumText('descripcion')->nullable();
            $table->decimal('precio_min', 11, 2)->nullable();
            $table->decimal('precio_max', 11, 2)->nullable();
            $table->decimal('precio_ser', 11, 2)->nullable();
            $table->decimal('precio_bol', 11, 2)->nullable();
            $table->smallInteger('nro_dias')->nullable();
            $table->smallInteger('nro_noches')->nullable();
            $table->string('imagen_1')->nullable();
            $table->string('imagen_2')->nullable();
            $table->string('flyers')->nullable();
            $table->date('fecha_salida')->nullable();
            $table->date('fecha_retorno')->nullable();
            $table->date('vigencia')->nullable();
            $table->boolean('promocion')->default(FALSE);
            $table->boolean('destacado')->default(FALSE);
            $table->tinyInteger('orden_destacado')->default(0);
            $table->boolean('estado')->default(TRUE);
            $table->integer('destination_id')->unsigned();
            $table->integer('package_subtype_id')->unsigned();
            $table->timestamps();

            $table->foreign('destination_id')->references('id')->on('destinations');
            $table->foreign('package_subtype_id')->references('id')->on('package_subtypes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('packages');
    }
}
