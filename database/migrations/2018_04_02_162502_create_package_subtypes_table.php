<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageSubtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_subtypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->tinyInteger('orden')->default(1);
            $table->boolean('estado')->default(TRUE);
            $table->integer('package_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('package_type_id')->references('id')->on('package_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('package_subtypes');
    }
}
