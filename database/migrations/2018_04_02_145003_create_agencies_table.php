<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario')->unique()->nullable();
            $table->string('correo')->unique()->nullable();
            $table->string('ruc')->unique()->nullable();
            $table->string('razon_social')->nullable();
            $table->string('nombre_comercial')->nullable();
            $table->string('nombres')->nullable();
            $table->string('telefono')->nullable();
            $table->string('celular')->nullable();
            $table->string('direccion_1')->nullable();
            $table->string('direccion_2')->nullable();
            $table->string('logo')->nullable();
            $table->integer('district_id')->unsigned();
            $table->boolean('estado')->default(TRUE);
            $table->timestamps();

            $table->foreign('district_id')->references('id')->on('districts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agencies');
    }
}
