<?php

use Illuminate\Database\Seeder;

class AboutusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('aboutus')->delete();
        
        \DB::table('aboutus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'quienes_somos' => '<p class="MsoNormal"><span style="font-size: 10pt; line-height: 107%; font-family: Arial, sans-serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Somos una Empresa joven,
dinámica, agresiva en el actuar, enfocados en proyectos basados en altos
valores humanos y éticos comerciales, dedicados a la creación de paquetes de
vacaciones, servicios personalizados, representaciones de operadores y
productos exclusivos, ofreciendo propuestas diferentes e innovadoras. Conformamos un grupo de profesionales de turismo de destacada labor en el medio, enfocados en
proyectos de alto valor.<o:p></o:p></span></p>',
                'vision' => 'Convertirnos en socios estratégicos de todas las agencias de viajes del país. Destacando por nuestra calidad de servicio, una propuesta renovada y eficiencia en todos nuestros procesos hasta lograr la solución más idónea.',
                'mision' => 'Nuestra principal misión es respaldar a las agencias de viajes para que puedan otorgar un mejor servicio a todos sus pasajeros.',
                'ventajas' => '<ul><li><strong>Integridad:</strong> Entregamos lo que prometemos y prometemos lo que podemos ofrecer. Hacemos lo que decimos con calidez y mucho pensamiento.</li><li><strong>Responsabilidad:</strong> Nos comprometemos a satisfacer las necesidades de nuestros clientes y el medio ambiente a través de nuestro trabajo, maximizando los recursos de la empresa.</li><li><strong>Innovación:</strong> Con un grupo genuinamente talentoso de personas, excelentes servicios, mentes abiertas y clientes con retos complejos y únicos, simplemente no podemos dejar de reinventarnos.</li><li><strong>Excelencia:</strong> Nos distinguimos a través de nuestro fuerte compromiso de mejorar e inspirar.</li><li><strong>Confianza:</strong> Generar seguridad a nuestros clientes, no solo al brindarles un servicio de calidad, sino también al darles alcance de soluciones.</li></ul>',
                'url_regulacion' => NULL,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-14 16:11:47',
            ),
        ));
        
        
    }
}
