<?php

use Illuminate\Database\Seeder;

class DestinationBannersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('destination_banners')->delete();
        
        \DB::table('destination_banners')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Caribe',
                'subtitulo' => 'Disfruta de las mejores playas del Caribe.',
                'descripcion' => NULL,
                'imagen' => '1.jpg',
                'href' => '#',
                'orden' => 2,
                'estado' => 1,
                'destination_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'titulo' => 'La marinera',
                'subtitulo' => 'Es uno de los bailes más reconocidos del Perú, que, al son de la música y los aplausos, deleita al público en cualquier momento.',
                'descripcion' => NULL,
                'imagen' => '2.jpeg',
                'href' => '#',
                'orden' => 2,
                'estado' => 1,
                'destination_id' => 2,
                'created_at' => NULL,
                'updated_at' => '2018-04-27 21:40:14',
            ),
            2 => 
            array (
                'id' => 3,
                'titulo' => 'Caribe',
                'subtitulo' => 'Disfruta de las mejores playas del Caribe.',
                'descripcion' => NULL,
                'imagen' => '3.jpg',
                'href' => '#',
                'orden' => 1,
                'estado' => 1,
                'destination_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'titulo' => 'Machu Picchu',
                'subtitulo' => 'Conozca Machu Picchu con nosotros y recargue sus energías en esta mágica ciudad de los Incas.',
                'descripcion' => NULL,
                'imagen' => '4.jpg',
                'href' => '#',
                'orden' => 1,
                'estado' => 1,
                'destination_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
