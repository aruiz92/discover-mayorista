<?php

use Illuminate\Database\Seeder;

class CompanyTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('company')->delete();
        
        \DB::table('company')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Discover Mayorista de Turismo ',
                'direccion_1' => NULL,
                'direccion_2' => NULL,
                'telefono_fijo' => '+51 1 444 4949',
                'telefono_movil' => '',
                'correo_informacion' => 'viajes@discovermayorista.com',
                'correo_agente' => 'internacional@discovermayorista.com',
                'correo_visitante' => 'viajes@discovermayorista.com',
                'facebook' => 'https://www.facebook.com/DiscoverMayorista/',
                'twitter' => '#',
                'instagram' => 'https://www.instagram.com/discover_mt/',
                'youtube' => 'https://www.youtube.com/channel/UCEA0vdI1IHjvnRHL8396pag?view_as=subscriber',
                'linkedin' => '#',
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 16:07:32',
            ),
        ));
        
        
    }
}
