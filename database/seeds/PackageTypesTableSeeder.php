<?php

use Illuminate\Database\Seeder;

class PackageTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('package_types')->delete();
        
        \DB::table('package_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Salidas Confirmadas',
                'orden' => 1,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 09:44:47',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Vacaciones',
                'orden' => 1,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 09:45:10',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Premium',
                'orden' => 1,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
