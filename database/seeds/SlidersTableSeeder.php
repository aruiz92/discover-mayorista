<?php

use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sliders')->delete();
        
        \DB::table('sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'imagen' => '1.jpg',
                'texto_1' => 'Una ruta. Varios destinos',
                'texto_2' => '¡Viaja sin límites!',
                'texto_3' => 'Disfruta de tus vacaciones, en los mejores lugares.',
                'url' => '#',
                'seccion' => 'home',
                'orden' => 1,
                'state' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-15 16:32:56',
            ),
            1 => 
            array (
                'id' => 2,
                'imagen' => '2.jpg',
                'texto_1' => 'Una ruta. Varios destinos',
                'texto_2' => 'Naturaleza, Playa y Sol',
                'texto_3' => 'Disfruta de tus vacaciones en los mejores lugares.',
                'url' => '#',
                'seccion' => 'home',
                'orden' => 1,
                'state' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-14 17:56:56',
            ),
            2 => 
            array (
                'id' => 4,
                'imagen' => '4.jpeg',
                'texto_1' => NULL,
                'texto_2' => 'Cancún',
                'texto_3' => 'Cancún es una verdadera joya del Caribe, situada en el estado de Quintana Roo en la región sureste de México, en la punta noreste de la Península de Yucatán, al norte de la Riviera Maya.',
                'url' => '#',
                'seccion' => 'promociones',
                'orden' => 1,
                'state' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5,
                'imagen' => '5.jpg',
                'texto_1' => 'Una ruta. Varios destinos',
                'texto_2' => 'Conoce lugares increibles',
                'texto_3' => 'Disfruta de tus vacaciones, en lugares magníficos.',
                'url' => '#',
                'seccion' => 'home',
                'orden' => 1,
                'state' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-14 16:28:55',
            ),
            4 => 
            array (
                'id' => 11,
                'imagen' => '11.jpeg',
                'texto_1' => NULL,
                'texto_2' => 'Villa de Leyva',
                'texto_3' => 'Es reconocida en el país y fuera de él como un lugar ideal para el descanso, el encuentro con la historia, la ciencia, el arte y la cultura, además de tener un gran campo para el encuentro con la naturaleza.',
                'url' => '#',
                'seccion' => 'bloqueos',
                'orden' => 1,
                'state' => 1,
                'created_at' => '2018-05-18 09:44:24',
                'updated_at' => '2018-05-18 09:44:24',
            ),
        ));
        
        
    }
}
