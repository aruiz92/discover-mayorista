<?php

use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('partners')->delete();
        
        \DB::table('partners')->insert(array (
            0 => 
            array (
                'id' => 26,
                'nombre' => 'COPA AIRLINES',
                'imagen' => '26.png',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-05-15 16:40:41',
                'updated_at' => '2018-05-15 16:41:09',
            ),
            1 => 
            array (
                'id' => 27,
                'nombre' => 'LATAM ',
                'imagen' => '27.png',
                'orden' => 2,
                'estado' => 1,
                'created_at' => '2018-05-15 16:40:57',
                'updated_at' => '2018-05-15 16:40:57',
            ),
            2 => 
            array (
                'id' => 28,
                'nombre' => 'AVIANCA',
                'imagen' => '28.png',
                'orden' => 3,
                'estado' => 1,
                'created_at' => '2018-05-15 16:41:40',
                'updated_at' => '2018-05-15 16:41:40',
            ),
            3 => 
            array (
                'id' => 29,
                'nombre' => 'ASSIST CARD',
                'imagen' => '29.png',
                'orden' => 4,
                'estado' => 1,
                'created_at' => '2018-05-15 16:41:58',
                'updated_at' => '2018-05-15 16:41:58',
            ),
            4 => 
            array (
                'id' => 30,
                'nombre' => 'PLUSULTRA',
                'imagen' => '30.png',
                'orden' => 5,
                'estado' => 1,
                'created_at' => '2018-05-15 16:43:00',
                'updated_at' => '2018-05-15 16:43:00',
            ),
        ));
        
        
    }
}
