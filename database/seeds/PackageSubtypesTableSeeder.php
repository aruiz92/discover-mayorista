<?php

use Illuminate\Database\Seeder;

class PackageSubtypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('package_subtypes')->delete();
        
        \DB::table('package_subtypes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Culturales',
                'orden' => 1,
                'estado' => 1,
                'package_type_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Europa',
                'orden' => 1,
                'estado' => 1,
                'package_type_id' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 09:48:04',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Caribe',
                'orden' => 1,
                'estado' => 1,
                'package_type_id' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-15 12:55:53',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Caribe',
                'orden' => 1,
                'estado' => 1,
                'package_type_id' => 2,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 09:46:51',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Sudamerica',
                'orden' => 1,
                'estado' => 1,
                'package_type_id' => 2,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 09:46:33',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Norteamerica',
                'orden' => 1,
                'estado' => 1,
                'package_type_id' => 2,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 09:47:10',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Lujo',
                'orden' => 1,
                'estado' => 1,
                'package_type_id' => 3,
                'created_at' => NULL,
                'updated_at' => '2018-05-16 11:59:55',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Cruceros',
                'orden' => 1,
                'estado' => 1,
                'package_type_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'Vacaciones',
                'orden' => 3,
                'estado' => 1,
                'package_type_id' => 3,
                'created_at' => '2018-05-16 11:47:53',
                'updated_at' => '2018-05-16 11:47:53',
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'Europa',
                'orden' => 4,
                'estado' => 1,
                'package_type_id' => 2,
                'created_at' => '2018-05-21 09:47:33',
                'updated_at' => '2018-05-21 09:47:33',
            ),
        ));
        
        
    }
}
