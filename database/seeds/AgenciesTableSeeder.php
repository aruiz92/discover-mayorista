<?php

use Illuminate\Database\Seeder;

class AgenciesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('agencies')->delete();
        
        \DB::table('agencies')->insert(array (
            0 => 
            array (
                'id' => 1,
                'usuario' => 'discovermayorista',
                'correo' => 'info@discoverperu.com',
                'ruc' => '20601402506',
                'razon_social' => 'Discover Mayorista de Turismo S.A.C',
                'nombre_comercial' => 'Discover Mayorista',
                'nombres' => 'Luis Garcia',
                'telefono' => '+51 1 411 8111',
                'celular' => NULL,
                'direccion_1' => 'Av. Alfredo Benavides 264 Dpto. 703 Urb. Cercado de Miraflores',
                'direccion_2' => NULL,
                'logo' => '1.png',
                'district_id' => 16,
                'estado' => 1,
                'created_at' => '2018-04-06 20:17:33',
                'updated_at' => '2018-05-21 15:11:02',
            ),
            1 => 
            array (
                'id' => 2,
                'usuario' => 'mateo',
                'correo' => 'jzuniga@webtilia.com',
                'ruc' => '10263074100',
                'razon_social' => 'Nextel',
                'nombre_comercial' => 'Nextel SRL',
                'nombres' => 'Mateo Javier',
                'telefono' => '2620013',
                'celular' => '999278426',
                'direccion_1' => 'A. Jose Barrio Nuevo 320',
                'direccion_2' => NULL,
                'logo' => '2.jpg',
                'district_id' => 45,
                'estado' => 1,
                'created_at' => '2018-05-15 10:07:33',
                'updated_at' => '2018-05-15 13:15:27',
            ),
            2 => 
            array (
                'id' => 3,
                'usuario' => 'raymitravel',
                'correo' => 'ventas@raymi-travel.com',
                'ruc' => '74920321000',
                'razon_social' => 'Raymi Travel Sac',
                'nombre_comercial' => 'Raymi Travel',
                'nombres' => 'Raymi Gracia ',
                'telefono' => '958162797',
                'celular' => '',
                'direccion_1' => 'Av. Alfredo Benavides 264',
                'direccion_2' => '',
                'logo' => '3.png',
                'district_id' => 22,
                'estado' => 1,
                'created_at' => '2018-05-21 15:38:10',
                'updated_at' => '2018-05-21 15:47:36',
            ),
        ));
        
        
    }
}
