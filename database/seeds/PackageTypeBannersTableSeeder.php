<?php

use Illuminate\Database\Seeder;

class PackageTypeBannersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('package_type_banners')->delete();
        
        \DB::table('package_type_banners')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Acapulco de Juárez',
                'subtitulo' => 'El viento cálido pegándote en todo el cuerpo, mientras escuchas debajo de ti el agua rugiendo y siente el sol en tu cara.',
                'descripcion' => NULL,
                'imagen' => '1.jpg',
                'orden' => 1,
                'estado' => 1,
                'destination_id' => 1,
                'package_type_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'titulo' => 'Colombia de Compras',
                'subtitulo' => 'Las Tiendas de Bogotá, Colombia te sorprenderán.',
                'descripcion' => NULL,
                'imagen' => '2.jpg',
                'orden' => 1,
                'estado' => 1,
                'destination_id' => 1,
                'package_type_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'titulo' => 'Caribe',
                'subtitulo' => 'Disfruta de las mejores playes del Caribe.',
                'descripcion' => NULL,
                'imagen' => '3.jpg',
                'orden' => 1,
                'estado' => 1,
                'destination_id' => 1,
                'package_type_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
