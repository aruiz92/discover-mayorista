<?php

use Illuminate\Database\Seeder;

class DestinationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('destinations')->delete();
        
        \DB::table('destinations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Mundo',
                'descripcion' => NULL,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Perú',
                'descripcion' => NULL,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
