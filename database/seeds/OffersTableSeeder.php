<?php

use Illuminate\Database\Seeder;

class OffersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('offers')->delete();
        
        \DB::table('offers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Fiestas Patrias Punta Cana',
                'precio_min' => '1055.00',
                'url_accion' => 'http://www.discovermayorista.com/package/61-fiestas-patrias-en-punta-cana',
                'imagen_bg' => '1.jpg',
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 16:05:48',
            ),
        ));
        
        
    }
}
