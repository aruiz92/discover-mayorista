<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('employees')->delete();
        
        \DB::table('employees')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombres' => 'Cesar',
                'apellidos' => 'Meza',
                'correo' => 'cesar@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Administración',
                'foto' => '1.jpeg',
                'orden' => 3,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-14 15:33:10',
            ),
            1 => 
            array (
                'id' => 2,
                'nombres' => 'Ana',
                'apellidos' => 'Cumpa',
                'correo' => 'europa1@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Ventas Europa y Destinos Exóticos',
                'foto' => '2.jpeg',
                'orden' => 11,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-14 15:45:53',
            ),
            2 => 
            array (
                'id' => 3,
                'nombres' => 'Ivette',
                'apellidos' => 'Díaz',
                'correo' => 'ivette@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Producto & Desarrollo',
                'foto' => '3.jpeg',
                'orden' => 7,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 10:20:47',
            ),
            3 => 
            array (
                'id' => 4,
                'nombres' => 'Jackeline',
                'apellidos' => 'Zorrilla',
                'correo' => 'jackeline@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Producto & Desarrollo',
                'foto' => '4.jpeg',
                'orden' => 8,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 10:21:12',
            ),
            4 => 
            array (
                'id' => 5,
                'nombres' => 'Marcelo',
                'apellidos' => 'Barreto',
                'correo' => 'marcelo@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Administración',
                'foto' => '5.jpeg',
                'orden' => 5,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-04-27 21:29:37',
            ),
            5 => 
            array (
                'id' => 6,
                'nombres' => 'Rafael',
                'apellidos' => 'Huanca',
                'correo' => 'rafael@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Ventas América',
                'foto' => '6.jpeg',
                'orden' => 14,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-21 10:12:52',
            ),
            6 => 
            array (
                'id' => 7,
                'nombres' => 'Tania',
                'apellidos' => 'Garcia Bedoya',
                'correo' => 'asist.adm@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Administración',
                'foto' => '7.jpeg',
                'orden' => 4,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-14 16:02:04',
            ),
            7 => 
            array (
                'id' => 8,
                'nombres' => 'Yonathan',
                'apellidos' => 'De Paz Gallardo',
                'correo' => 'ejecutivo@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Promotor de Campo',
                'foto' => '8.jpeg',
                'orden' => 9,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-14 15:48:27',
            ),
            8 => 
            array (
                'id' => 9,
                'nombres' => 'Zhurik',
                'apellidos' => 'Garay',
                'correo' => 'zhurik@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Ventas Europa y Destinos Exóticos',
                'foto' => '9.jpeg',
                'orden' => 10,
                'estado' => 1,
                'created_at' => '2018-04-27 21:29:11',
                'updated_at' => '2018-05-14 15:56:12',
            ),
            9 => 
            array (
                'id' => 10,
                'nombres' => 'Diana                    ',
                'apellidos' => 'Verano      ',
                'correo' => 'diana@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Ventas Amércia',
                'foto' => '10.jpeg',
                'orden' => 13,
                'estado' => 1,
                'created_at' => '2018-05-02 20:29:31',
                'updated_at' => '2018-05-14 15:56:40',
            ),
            10 => 
            array (
                'id' => 11,
                'nombres' => 'Jorge ',
                'apellidos' => 'Achamisa ',
                'correo' => 'jorge@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Ventas América',
                'foto' => '11.jpeg',
                'orden' => 12,
                'estado' => 1,
                'created_at' => '2018-05-02 20:32:16',
                'updated_at' => '2018-05-21 10:12:40',
            ),
            11 => 
            array (
                'id' => 12,
                'nombres' => 'Raymi',
                'apellidos' => 'Garcia',
                'correo' => 'administracion@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Jefe de administración y RRHH',
                'foto' => '12.jpeg',
                'orden' => 2,
                'estado' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-14 15:31:51',
            ),
            12 => 
            array (
                'id' => 13,
                'nombres' => 'Luis Ignacio ',
                'apellidos' => 'Garcia Mailhe ',
                'correo' => 'Ignacio@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Gerente General',
                'foto' => '13.jpeg',
                'orden' => 1,
                'estado' => 1,
                'created_at' => '2018-05-14 13:12:10',
                'updated_at' => '2018-05-21 10:03:15',
            ),
            13 => 
            array (
                'id' => 14,
                'nombres' => 'Ignacio  ',
                'apellidos' => 'Garcia ',
                'correo' => 'i.garcia@discovermayorista.com',
                'telefono' => NULL,
                'skype' => NULL,
                'cargo' => 'Área de  Marketing',
                'foto' => '14.jpeg',
                'orden' => 6,
                'estado' => 1,
                'created_at' => '2018-05-14 13:13:55',
                'updated_at' => '2018-05-14 15:42:37',
            ),
        ));
        
        
    }
}
