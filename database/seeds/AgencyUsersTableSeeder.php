<?php

use Illuminate\Database\Seeder;

class AgencyUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('agency_users')->delete();
        
        \DB::table('agency_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Ignacio Garcia G. Bedoya ',
                'correo' => 'i.garcia@discovermayorista.com',
                'contrasena' => '$2y$10$h/rWucneVT6dJyNmHHpFdO1izuEkQMFN9IzkW.r08i0rQKxDml1DK',
                'estado' => 1,
                'remember_token' => 'STtuhklkhKRWw2yBd4Nb1S0OFbcraffxVqmhjxzRFYfn41JCkcGGpuBQH8eI',
                'agency_id' => 1,
                'role_id' => 1,
                'created_at' => '2018-04-06 20:17:34',
                'updated_at' => '2018-05-21 16:26:44',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Mateo Javier',
                'correo' => 'jzuniga@webtilia.com',
                'contrasena' => '$2y$10$HV9lT3MNfy.oYIbgl9PmoeB5BlloKI7iw2zKsWo24kyqlvW6G7UCa',
                'estado' => 1,
                'remember_token' => NULL,
                'agency_id' => 2,
                'role_id' => 1,
                'created_at' => '2018-05-15 10:07:33',
                'updated_at' => '2018-05-15 13:15:27',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Juancito Zevallos',
                'correo' => 'jzevallos@gmail.com',
                'contrasena' => '$2y$10$PeTX58XbWTSShO7UgaYToeRrAi24nCir6ggfGP9h2RAsm1GT/61aS',
                'estado' => 1,
                'remember_token' => NULL,
                'agency_id' => 1,
                'role_id' => 2,
                'created_at' => '2018-05-21 14:31:12',
                'updated_at' => '2018-05-21 14:31:12',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Pedrito Zuñiga',
                'correo' => 'josealberto@gmail.com',
                'contrasena' => '$2y$10$Xy40bkaqs9s0KsfPZgyYxO2P/FP2qIFVtPO1rytpFkj69om0Mi0qS',
                'estado' => 1,
                'remember_token' => NULL,
                'agency_id' => 1,
                'role_id' => 2,
                'created_at' => '2018-05-21 14:45:29',
                'updated_at' => '2018-05-21 14:45:29',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Raymi Gracia ',
                'correo' => 'ventas@raymi-travel.com',
                'contrasena' => '$2y$10$iCxsw63NTNjsdkeW/wxh7eJGFoEgJZhw24mwt4oOcouiZu1GU.dQK',
                'estado' => 1,
                'remember_token' => 'FwrEbewh4R3aI4wWC4u2nTEDWq6CF4nzeRhtMjgyWpA2D1NnwWzsLSZu9SIU',
                'agency_id' => 3,
                'role_id' => 1,
                'created_at' => '2018-05-21 15:38:10',
                'updated_at' => '2018-05-21 15:57:26',
            ),
        ));
        
        
    }
}
