<?php

use Illuminate\Database\Seeder;

class PackageTabsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('package_tabs')->delete();
        
        \DB::table('package_tabs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima &ndash; Panam&aacute; - Punta Cana &ndash; Panam&aacute; - Lima v&iacute;a Copa Airlines</li>
<li>Traslado de ingreso y salida</li>
<li>05 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n <strong>TODO INCLUIDO</strong></li>
<li>Excursi&oacute;n Catamar&aacute;n y Buggies</li>
<li>Shopping Tour en la zona de B&aacute;varo</li>
<li>Seguro de asistencia por 06 d&iacute;as</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>RESERVAS: Contra pre-pago de $ 900 por persona el cual es NO REEMBOLSABLE, adicionalmente deber&aacute;n adjuntar. copia de los pasaportes de cada pasajero (requisito indispensable).</li>
<li>PAGO DEL SALDO: 15 d&iacute;as despu&eacute;s realizado el prepago.</li>
<li>Consultar por tarifa de INF (0-2 a&ntilde;os).</li>
<li>Reservas que ingresen dentro de los 30 d&iacute;as previos a la fecha de inicio de viaje, se solicitar&aacute; PAGO TOTAL y con un tiempo l&iacute;mite especial.</li>
<li>Pasajeros deben presentarse en Counter con 3 horas de anticipaci&oacute;n.</li>
<li>Entrega de los boletos y vouchers se realizar&aacute;n 48 horas antes de la fecha de salida.</li>
<li>Anulaciones y cancelaciones se penalizar&aacute; con el 100% una vez realizado el pago final. NO SHOW: Se penalizar&aacute; al 100%.</li>
<li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad</li>
<li>Itinerarios y vuelos est&aacute;n sujetos a variaci&oacute;n seg&uacute;n disposici&oacute;n de la l&iacute;nea a&eacute;rea.</li>
<li>Tarifas, queues e impuestos est&aacute;n sujetos a cambio sin previo aviso hasta el momento de la emisi&oacute;n de los mismos.</li>
<li>Capacidad m&aacute;xima por habitaci&oacute;n: 03 ADT+ 1 CHD &oacute; 02 ADT + 02 CHD</li>
<li>M&aacute;ximo 02 ni&ntilde;os por habitaci&oacute;n, siempre que est&eacute;n acompa&ntilde;ados de 02 adultos pagando en la misma habitaci&oacute;n.</li>
<li>Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) est&aacute;n sujetas a las regulaciones aeron&aacute;uticas internacionales vigentes, quedando bajo las mismas la aplicaci&oacute;n de la normativa. El operador que act&uacute;a en la intermediaci&oacute;n, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informar&aacute; en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir seg&uacute;n apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerol&iacute;neas, operadores terrestres etc, deber&aacute;n ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y soluci&oacute;n deber&aacute; presentar reclamo v&iacute;a carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se gener&oacute; la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias act&uacute;an siempre como asesores o intermediarios de la operaci&oacute;n entre los proveedores locales e internacionales y el usuario, somos responsables &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima-Punta Cana-Lima v&iacute;a Copa Airlines</li>
<li>Traslados de ingreso y salida</li>
<li>05 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tour a Isla Saona</li>
<li>Seguro de asistencia</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
<li>GRATIS: City Tour + Tour de Compras</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>RESERVAS: Contra pre-pago de $ 900 por persona el cual es NO REEMBOLSABLE, adicionalmente deber&aacute;n adjuntar. copia de los pasaportes de cada pasajero (requisito indispensable).</li>
<li>PAGO DEL SALDO: 15 d&iacute;as despu&eacute;s realizado el prepago.</li>
<li>Consultar por tarifa de INF (0-2 a&ntilde;os).</li>
<li>Reservas que ingresen dentro de los 30 d&iacute;as previos a la fecha de inicio de viaje, se solicitar&aacute; PAGO TOTAL y con un tiempo l&iacute;mite especial.</li>
<li>Pasajeros deben presentarse en Counter con 3 horas de anticipaci&oacute;n.</li>
<li>Entrega de los boletos y vouchers se realizar&aacute;n 48 horas antes de la fecha de salida.</li>
<li>Anulaciones y cancelaciones se penalizar&aacute; con el 100% una vez realizado el pago final. NO SHOW: Se penalizar&aacute; al 100%.</li>
<li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad</li>
<li>Itinerarios y vuelos est&aacute;n sujetos a variaci&oacute;n seg&uacute;n disposici&oacute;n de la l&iacute;nea a&eacute;rea.</li>
<li>Tarifas, queues e impuestos est&aacute;n sujetos a cambio sin previo aviso hasta el momento de la emisi&oacute;n de los mismos.</li>
<li>Hoteles sujetos a disponibilidad.</li>
<li>Capacidad m&aacute;xima por habitaci&oacute;n: 03 ADT+ 1 CHD &oacute; 02 ADT + 02 CHD</li>
<li>M&aacute;ximo 02 ni&ntilde;os por habitaci&oacute;n, siempre que est&eacute;n acompa&ntilde;ados de 02 adultos pagando en la misma habitaci&oacute;n.</li>
<li>Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) est&aacute;n sujetas a las regulaciones aeron&aacute;uticas internacionales vigentes, quedando bajo las mismas la aplicaci&oacute;n de la normativa. El operador que act&uacute;a en la intermediaci&oacute;n, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informar&aacute; en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir seg&uacute;n apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerol&iacute;neas, operadores terrestres etc, deber&aacute;n ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y soluci&oacute;n deber&aacute; presentar reclamo v&iacute;a carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se gener&oacute; la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias act&uacute;an siempre como asesores o intermediarios de la operaci&oacute;n entre los proveedores locales e internacionales y el usuario, somos responsables &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima-Punta Cana-Lima v&iacute;a Copa Airlines</li>
<li>Traslados de ingreso y salida</li>
<li>05 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tour a Isla Saona</li>
<li>Seguro de asistencia</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
<li>GRATIS: City Tour + Tour de Compras</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>RESERVAS: Contra pre-pago de $ 900 por persona el cual es NO REEMBOLSABLE, adicionalmente deberán adjuntar. copia de los pasaportes de cada pasajero (requisito indispensable).</li>
<li>PAGO DEL SALDO: 15 días después realizado el prepago.</li>
<li>Consultar por tarifa de INF (0-2 años).</li>
<li>Reservas que ingresen dentro de los 30 días previos a la fecha de inicio de viaje, se solicitará PAGO TOTAL y con un tiempo límite especial.</li>
<li>Pasajeros deben presentarse en Counter con 3 horas de anticipación.</li>
<li>Entrega de los boletos y vouchers se realizarán 48 horas antes de la fecha de salida.</li>
<li>Anulaciones y cancelaciones se penalizará con el 100% una vez realizado el pago final. NO SHOW: Se penalizará al 100%.</li>
<li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad.</li>
<li>Itinerarios y vuelos están sujetos a variación según disposición de la línea aérea.</li>
<li>Tarifas, queues e impuestos están sujetos a cambio sin previo aviso hasta el momento de la emisión de los mismos.</li>
<li>Hoteles sujetos a disponibilidad.</li>
<li>Capacidad máxima por habitación: 03 ADT+ 1 CHD ó 02 ADT + 02 CHD.</li>
<li>Máximo 02 niños por habitación, siempre que estén acompañados de 02 adultos pagando en la misma habitación.</li>
<li>Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) están sujetas a las regulaciones aeronáuticas internacionales vigentes, quedando bajo las mismas la aplicación de la normativa. El operador que actúa en la intermediación, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informará en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir según apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerolíneas, operadores terrestres etc, deberán ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y solución deberá presentar reclamo vía carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se generó la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias actúan siempre como asesores o intermediarios de la operación entre los proveedores locales e internacionales y el usuario, somos responsables únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima &ndash; Foz de Iguaz&uacute; &ndash; Lima v&iacute;a Latam.</li>
<li>Traslados de ingreso y salida en servicio regular.</li>
<li>04 Noches de alojamiento en hotel seleccionado.</li>
<li>Desayunos.</li>
<li>Visita a las cataratas lado brasilero (sin entradas).</li>
<li>Visita a las cataratas lado argentino (sin entradas).</li>
<li>Visita al parque Dreamland: Valle de los dinosaurios con entradas + Bar de Hielo (-11&deg;C, trajes especiales, open bar y tiempo ilimitado de permanencia).</li>
<li>Cortes&iacute;as: Visita parque de aves (sin entradas &ndash; mismo d&iacute;a de visita cataratas lado brasilero). Visita al Hito de las Tres Fronteras (mismo d&iacute;a de visita a las cataratas lado argentino), parada en el Centro de artesan&iacute;as de Foz de Iguaz&uacute;, visita al Duty Free Shop.</li>
<li>Seguro de asistencia.</li>
<li>Impuestos hoteleros y a&eacute;reos.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>RESERVAS: Contra pre-pago de $ 700 por persona el cual es NO REEMBOLSABLE, adicionalmente deber&aacute;n adjuntar. copia de los DNI o pasaportes de cada pasajero (requisito indispensable).</li>
<li>PAGO DEL SALDO: A los 15 d&iacute;as despu&eacute;s de realizado el prepago.</li>
<li>Reservas que ingresen dentro de los 30 d&iacute;as previos a la fecha de inicio de viaje, se solicitar&aacute; PAGO TOTAL y con un tiempo l&iacute;mite especial.</li>
<li>Pasajeros deben presentarse en Counter con 4 horas de anticipaci&oacute;n.</li>
<li>Entrega de los boletos y vouchers se realizar&aacute;n 48 horas antes de la fecha de salida.</li>
<li>Anulaciones y cancelaciones se penalizar&aacute; con el 100% una vez realizado el pago final. NO SHOW: Se penalizar&aacute; al 100%.</li>
<li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad.</li>
<li>Itinerarios y vuelos est&aacute;n sujetos a variaci&oacute;n seg&uacute;n disposici&oacute;n de la l&iacute;nea a&eacute;rea.</li>
<li>Tarifas, queues e impuestos est&aacute;n sujetos a cambio sin previo aviso hasta el momento de la emisi&oacute;n de los mismos.</li>
<li>Hoteles sujetos a disponibilidad.</li>
<li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) est&aacute;n sujetas a las regulaciones aeron&aacute;uticas internacionales vigentes, quedando bajo las mismas la aplicaci&oacute;n de la normativa. El operador que act&uacute;a en la intermediaci&oacute;n, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informar&aacute; en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir seg&uacute;n apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerol&iacute;neas, operadores terrestres etc, deber&aacute;n ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y soluci&oacute;n deber&aacute; presentar reclamo v&iacute;a carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se gener&oacute; la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias act&uacute;an siempre como asesores o intermediarios de la operaci&oacute;n entre los proveedores locales e internacionales y el usuario, somos responsables &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima-Canc&uacute;n-Lima v&iacute;a Copa Airlines</li>
<li>Traslados de ingreso y salida</li>
<li>05 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Entradas Gratis e ilimitadas: Xcaret, Xel-H&aacute;, Xplor, Xplor Fuego, Xenses, Xoximilco y los tours de Xenotes, Xichen, Tulum y Cob&aacute;</li>
<li>Seguro de asistencia</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>PAGO DEL SALDO: A los 15 d&iacute;as despu&eacute;s realizado el prepago.</li>
<li>Consultar por tarifa de INF (0-2 a&ntilde;os) y CHD (2 &ndash; 5 a&ntilde;os).</li>
<li>Tarifas v&aacute;lidas para comprar hasta el 30 Mayo o agotar stock.</li>
<li>Reservas que ingresen dentro de los 30 d&iacute;as previos a la fecha de inicio de viaje, se solicitar&aacute; PAGO TOTAL y con un tiempo l&iacute;mite especial.</li>
<li>Pasajeros deben presentarse en Counter con 4 horas de anticipaci&oacute;n.</li>
<li>Entrega de los boletos y vouchers se realizar&aacute;n 48 horas antes de la fecha de salida.</li>
<li>Anulaciones y cancelaciones se penalizar&aacute; con el 100% una vez realizado el pago final. NO SHOW: Se penalizar&aacute; al 100%.</li>
<li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad</li>
<li>Itinerarios y vuelos est&aacute;n sujetos a variaci&oacute;n seg&uacute;n disposici&oacute;n de la l&iacute;nea a&eacute;rea.</li>
<li>Tarifas, queues e impuestos est&aacute;n sujetos a cambio sin previo aviso hasta el momento de la emisi&oacute;n de los mismos.</li>
<li>Hoteles sujetos a disponibilidad.</li>
<li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) est&aacute;n sujetas a las regulaciones aeron&aacute;uticas internacionales vigentes, quedando bajo las mismas la aplicaci&oacute;n de la normativa. El operador que act&uacute;a en la intermediaci&oacute;n, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informar&aacute; en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir seg&uacute;n apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerol&iacute;neas, operadores terrestres etc, deber&aacute;n ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y soluci&oacute;n deber&aacute; presentar reclamo v&iacute;a carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se gener&oacute; la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias act&uacute;an siempre como asesores o intermediarios de la operaci&oacute;n entre los proveedores locales e internacionales y el usuario, somos responsables &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 5,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima-Panam&aacute;-Canc&uacute;n-Panam&aacute;-Lima v&iacute;a Copa Airlines</li>
<li>Traslados de ingreso y salida</li>
<li>05 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Seguro de asistencia al viajero</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>RESERVAS: Contra pre-pago de $ 700 por persona el cual es NO REEMBOLSABLE, adicionalmente deber&aacute;n adjuntar. copia de los pasaportes de cada pasajero (requisito indispensable).</li>
<li>PAGO DEL SALDO: A los 15 d&iacute;as despu&eacute;s realizado el prepago.</li>
<li>Consultar por tarifa de INF (0-2 a&ntilde;os) y CHD (2 &ndash; 5 a&ntilde;os).</li>
<li>Tarifas v&aacute;lidas para comprar hasta el 30 Mayo o agotar stock.</li>
<li>Reservas que ingresen dentro de los 30 d&iacute;as previos a la fecha de inicio de viaje, se solicitar&aacute; PAGO TOTAL y con un tiempo l&iacute;mite especial.</li>
<li>Pasajeros deben presentarse en Counter con 4 horas de anticipaci&oacute;n.</li>
<li>Entrega de los boletos y vouchers se realizar&aacute;n 48 horas antes de la fecha de salida.</li>
<li>Anulaciones y cancelaciones se penalizar&aacute; con el 100% una vez realizado el pago final. NO SHOW: Se penalizar&aacute; al 100%.</li>
<li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad</li>
<li>Itinerarios y vuelos est&aacute;n sujetos a variaci&oacute;n seg&uacute;n disposici&oacute;n de la l&iacute;nea a&eacute;rea.</li>
<li>Tarifas, queues e impuestos est&aacute;n sujetos a cambio sin previo aviso hasta el momento de la emisi&oacute;n de los mismos.</li>
<li>Hoteles sujetos a disponibilidad.</li>
<li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) est&aacute;n sujetas a las regulaciones aeron&aacute;uticas internacionales vigentes, quedando bajo las mismas la aplicaci&oacute;n de la normativa. El operador que act&uacute;a en la intermediaci&oacute;n, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informar&aacute; en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir seg&uacute;n apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerol&iacute;neas, operadores terrestres etc, deber&aacute;n ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y soluci&oacute;n deber&aacute; presentar reclamo v&iacute;a carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se gener&oacute; la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias act&uacute;an siempre como asesores o intermediarios de la operaci&oacute;n entre los proveedores locales e internacionales y el usuario, somos responsables &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima-Panam&aacute;-Canc&uacute;n-Panam&aacute;-Lima v&iacute;a Copa Airlines</li>
<li>Traslados de ingreso y salida</li>
<li>05 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Seguro de asistencia al viajero</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>RESERVAS: Contra pre-pago de $ 700 por persona el cual es NO REEMBOLSABLE, adicionalmente deber&aacute;n adjuntar. copia de los pasaportes de cada pasajero (requisito indispensable).</li>
<li>PAGO DEL SALDO: A los 15 d&iacute;as despu&eacute;s realizado el prepago.</li>
<li>Consultar por tarifa de INF (0-2 a&ntilde;os) y CHD (2 &ndash; 5 a&ntilde;os).</li>
<li>Tarifas v&aacute;lidas para comprar hasta el 30 Mayo o agotar stock.</li>
<li>Reservas que ingresen dentro de los 30 d&iacute;as previos a la fecha de inicio de viaje, se solicitar&aacute; PAGO TOTAL y con un tiempo l&iacute;mite especial.</li>
<li>Pasajeros deben presentarse en Counter con 4 horas de anticipaci&oacute;n.</li>
<li>Entrega de los boletos y vouchers se realizar&aacute;n 48 horas antes de la fecha de salida.</li>
<li>Anulaciones y cancelaciones se penalizar&aacute; con el 100% una vez realizado el pago final. NO SHOW: Se penalizar&aacute; al 100%.</li>
<li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad</li>
<li>Itinerarios y vuelos est&aacute;n sujetos a variaci&oacute;n seg&uacute;n disposici&oacute;n de la l&iacute;nea a&eacute;rea.</li>
<li>Tarifas, queues e impuestos est&aacute;n sujetos a cambio sin previo aviso hasta el momento de la emisi&oacute;n de los mismos.</li>
<li>Hoteles sujetos a disponibilidad.</li>
<li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) est&aacute;n sujetas a las regulaciones aeron&aacute;uticas internacionales vigentes, quedando bajo las mismas la aplicaci&oacute;n de la normativa. El operador que act&uacute;a en la intermediaci&oacute;n, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informar&aacute; en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir seg&uacute;n apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerol&iacute;neas, operadores terrestres etc, deber&aacute;n ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y soluci&oacute;n deber&aacute; presentar reclamo v&iacute;a carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se gener&oacute; la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias act&uacute;an siempre como asesores o intermediarios de la operaci&oacute;n entre los proveedores locales e internacionales y el usuario, somos responsables &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima-Panam&aacute;-Canc&uacute;n-Panam&aacute;-Lima v&iacute;a Copa Airlines</li>
<li>Traslados de ingreso y salida</li>
<li>05 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Seguro de asistencia al viajero</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>RESERVAS: Contra pre-pago de $ 700 por persona el cual es NO REEMBOLSABLE, adicionalmente deber&aacute;n adjuntar. copia de los pasaportes de cada pasajero (requisito indispensable).</li>
<li>PAGO DEL SALDO: A los 15 d&iacute;as despu&eacute;s realizado el prepago.</li>
<li>Consultar por tarifa de INF (0-2 a&ntilde;os) y CHD (2 &ndash; 5 a&ntilde;os).</li>
<li>Tarifas v&aacute;lidas para comprar hasta el 30 Mayo o agotar stock.</li>
<li>Reservas que ingresen dentro de los 30 d&iacute;as previos a la fecha de inicio de viaje, se solicitar&aacute; PAGO TOTAL y con un tiempo l&iacute;mite especial.</li>
<li>Pasajeros deben presentarse en Counter con 4 horas de anticipaci&oacute;n.</li>
<li>Entrega de los boletos y vouchers se realizar&aacute;n 48 horas antes de la fecha de salida.</li>
<li>Anulaciones y cancelaciones se penalizar&aacute; con el 100% una vez realizado el pago final. NO SHOW: Se penalizar&aacute; al 100%.</li>
<li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad</li>
<li>Itinerarios y vuelos est&aacute;n sujetos a variaci&oacute;n seg&uacute;n disposici&oacute;n de la l&iacute;nea a&eacute;rea.</li>
<li>Tarifas, queues e impuestos est&aacute;n sujetos a cambio sin previo aviso hasta el momento de la emisi&oacute;n de los mismos.</li>
<li>Hoteles sujetos a disponibilidad.</li>
<li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) est&aacute;n sujetas a las regulaciones aeron&aacute;uticas internacionales vigentes, quedando bajo las mismas la aplicaci&oacute;n de la normativa. El operador que act&uacute;a en la intermediaci&oacute;n, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informar&aacute; en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir seg&uacute;n apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerol&iacute;neas, operadores terrestres etc, deber&aacute;n ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y soluci&oacute;n deber&aacute; presentar reclamo v&iacute;a carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se gener&oacute; la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias act&uacute;an siempre como asesores o intermediarios de la operaci&oacute;n entre los proveedores locales e internacionales y el usuario, somos responsables &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima-Panam&aacute;-Canc&uacute;n-Panam&aacute;-Lima v&iacute;a Copa Airlines</li>
<li>Traslados de ingreso y salida</li>
<li>05 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Seguro de asistencia al viajero</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>RESERVAS: Contra pre-pago de $ 700 por persona el cual es NO REEMBOLSABLE, adicionalmente deber&aacute;n adjuntar. copia de los pasaportes de cada pasajero (requisito indispensable).</li>
<li>PAGO DEL SALDO: A los 15 d&iacute;as despu&eacute;s realizado el prepago.</li>
<li>Consultar por tarifa de INF (0-2 a&ntilde;os) y CHD (2 &ndash; 5 a&ntilde;os).</li>
<li>Tarifas v&aacute;lidas para comprar hasta el 30 mayo o agotar stock.</li>
<li>Reservas que ingresen dentro de los 30 d&iacute;as previos a la fecha de inicio de viaje, se solicitar&aacute; PAGO TOTAL y con un tiempo l&iacute;mite especial.</li>
<li>Pasajeros deben presentarse en Counter con 4 horas de anticipaci&oacute;n.</li>
<li>Entrega de los boletos y vouchers se realizar&aacute;n 48 horas antes de la fecha de salida.</li>
<li>Anulaciones y cancelaciones se penalizar&aacute; con el 100% una vez realizado el pago final. NO SHOW: Se penalizar&aacute; al 100%.</li>
<li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad</li>
<li>Itinerarios y vuelos est&aacute;n sujetos a variaci&oacute;n seg&uacute;n disposici&oacute;n de la l&iacute;nea a&eacute;rea.</li>
<li>Tarifas, queues e impuestos est&aacute;n sujetos a cambio sin previo aviso hasta el momento de la emisi&oacute;n de los mismos.</li>
<li>Hoteles sujetos a disponibilidad.</li>
<li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) est&aacute;n sujetas a las regulaciones aeron&aacute;uticas internacionales vigentes, quedando bajo las mismas la aplicaci&oacute;n de la normativa. El operador que act&uacute;a en la intermediaci&oacute;n, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informar&aacute; en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir seg&uacute;n apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerol&iacute;neas, operadores terrestres etc, deber&aacute;n ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y soluci&oacute;n deber&aacute; presentar reclamo v&iacute;a carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se gener&oacute; la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias act&uacute;an siempre como asesores o intermediarios de la operaci&oacute;n entre los proveedores locales e internacionales y el usuario, somos responsables &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Buenos Aires / Lima v&iacute;a Aerol&iacute;neas Argentinas</li>
<li>Traslados In/Out en servicio privado</li>
<li>Desayunos</li>
<li>3 noches de alojamiento en Hotel Seleccionado</li>
<li>City Tour en servicio regular</li>
<li>Seguro de Asistencia</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto Aereo Lima &ndash; Buenos Aires &ndash; Lima Via Avianca</li>
<li>Traslados In/Out</li>
<li>4 noches de alojamiento en Hotel Seleccionado</li>
<li>Entrada a Funci&oacute;n Regular (A Ciegas Gourmet)</li>
<li>Tarjeta de Asistencia</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Cualquier incidencia presentada en el hotel o en el transporte es importante dejarlo registrado en el destino. Luego a su retorno informar a la agencia para proceder con las averiguaciones respectivas.</li>
<li>Discover Mayorista de Turismo SAC actua como intermediario entre los proveedores locales e internacionales y el usuario, y son responsables &uacute;nicamente por la organizaci&oacute;n de los tours adquiridos. El usuario no puede imputarnos responsabilidad por causas que est&aacute;n fuera de nuestro alcance. No somos responsables del perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control (ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de tercero o a la imprudencia del propio usuario afectado). Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino. En el caso de la l&iacute;nea a&eacute;rea, el usuario debe registrar el reclamo directamente con ellos. En nuestra condici&oacute;n de intermediarios gestionaremos su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Cordoba / Lima v&iacute;a LATAM</li>
<li>Traslados In/Out en servicio privado</li>
<li>Desayunos</li>
<li>3 noches de alojamiento en Hotel Seleccionado</li>
<li>City Tour en servicio regular</li>
<li>Seguro de Asistencia</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Medonza / Lima v&iacute;a Avianca</li>
<li>Traslados In/Out en servicio privado</li>
<li>Desayunos</li>
<li>2 noches de alojamiento en Hotel Seleccionado</li>
<li>City Tour en servicio regular</li>
<li>Seguro de Asistencia</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Buenos Aires / Salta / Buenos Aires / Lima v&iacute;a Aerol&iacute;neas Argentinas</li>
<li>Traslados In/Out en servicio privado</li>
<li>Desayunos</li>
<li>3 noches de alojamiento en Hotel Seleccionado</li>
<li>City Tour en servicio regular</li>
<li>Seguro de Asistencia</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima-Sao Paulo-Salvador de Bah&iacute;a &ndash; Sao Paulo-Lima v&iacute;a Latam</li>
<li>Traslados de ingreso y salida</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Seguro de asistencia</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Santiago / Lima v&iacute;a Latam</li>
<li>Desayuno buffet</li>
<li>02 noches de alojamiento en Santiago</li>
<li>Traslados de llegada y salida</li>
<li>City tour. Servicio Regular</li>
<li>Seguro de Asistencia</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Bogot&aacute; / Lima v&iacute;a Avianca</li>
<li>1 noche de Alojamiento en Villa de Leyva, Pueblo Patrimonio de Colombia</li>
<li>2 noches de Alojamiento en Paipa</li>
<li>Desayunos diarios</li>
<li>Visita y entrada a la Catedral de Sal</li>
<li>Traslados y entrada al Spa Termal en Paipa: cama de burbujas, jacuzzi, piscina termal, sauna</li>
<li>Visita al Vi&ntilde;edo de Punta Larga</li>
<li>Visita a f&aacute;brica de queso Paipa, uno de los quesos con denominaci&oacute;n de origen en Colombia.</li>
<li>Recorrido por pueblos t&iacute;picos, visitando R&aacute;quira (pueblo de artesan&iacute;as en barro), Nobsa (tejidos en lana), Tibasosa (pueblo de frutas y feijoa), Mongu&iacute; (Pueblo patrimonio de Colombia, f&aacute;bricas de balones y cueros).</li>
<li>Transporte todo el programa desde Bogot&aacute; (Servicio Puerta - Puerta), excepto en los d&iacute;as libres, con guianza y atenci&oacute;n personalizada seg&uacute;n el itinerario.</li>
<li>Entrada a pueblito boyacense y a distintos atractivos durante el recorrido</li>
<li>Toures en servicio regular (grupo) o privado seg&uacute;n disponibilidad.</li>
<li>Gu&iacute;a acompa&ntilde;ante en los toures. (En espa&ntilde;ol)</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Bogot&aacute; / Lima v&iacute;a Avianca</li>
<li>2 Noches de alojamiento</li>
<li>Desayunos diarios</li>
<li>Traslados IN/OUT</li>
<li>City tour Panor&aacute;mico</li>
<li>Tour de Compras</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 18,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 18,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Bogot&aacute; / Lima v&iacute;a Avianca</li>
<li>3 Noches de Alojamiento</li>
<li>Desayunos diarios</li>
<li>Traslados IN/OUT</li>
<li>City tour Panor&aacute;mico</li>
<li>Entrada a Parque Diversiones SALITRE MAGICO</li>
<li>1 chip celular por habitaci&oacute;n con datos.</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 19,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 19,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Bogot&aacute; / Lima v&iacute;a Avianca</li>
<li>3 Noches de Alojamiento</li>
<li>Desayunos diarios</li>
<li>Traslados IN/OUT</li>
<li>City tour Panor&aacute;mico</li>
<li>Entrada a Disco GAIRA con Traslado IN</li>
<li>City de noche con Restaurante Andr&eacute;s Carne de Res con trf in Bono de Consumo de USD 18</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 20,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 20,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Bogot&aacute; / Lima v&iacute;a Avianca</li>
<li>Desayunos diarios</li>
<li>Traslados IN/OUT</li>
<li>City tour Panor&aacute;mico</li>
<li>2 noches de alojamiento</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 21,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.  </li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li>
<li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 21,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto Aéreo Lima / Bogotá / Cartagena / Lima vía Avianca</li>
<li>2 Noches de alojamiento en Bogotá con desayunos</li>
<li>City tour panorámico Bogotá</li>
<li>Traslados IN/OUT</li>
<li>4 Noches de alojamiento en Cartagena con desayunos</li>
<li>City tour panorámico Cartagena</li>
<li>Paseo en carruaje tirado con Caballo</li>
<li>Traslados IN/OUT</li>
<li>2 Noches de alojamiento en Isla del Encanto</li>
<li>Comidas incluidas</li>
<li>1 chip de celular por habitación con datos.</li>
<li>Tarjeta de asistencia médica</li>
<li>Impuestos Aéreos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 22,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 22,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Bogot&aacute; / Cartagena / Lima v&iacute;a Avianca</li>
<li>2 Noches de alojamiento en Bogot&aacute; con desayunos</li>
<li>City tour panor&aacute;mico Bogot&aacute;</li>
<li>3 Noches de alojamiento en Cartagena con desayunos</li>
<li>City tour panor&aacute;mico Cartagena</li>
<li>Full day alojamiento en Isla del Encanto con almuerzo</li>
<li>Traslados IN/OUT CTG y BOG</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 23,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 23,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Bogot&aacute; / Lima v&iacute;a Avianca</li>
<li>3 Noches de alojamiento</li>
<li>Desayunos diarios</li>
<li>Traslados IN/OUT</li>
<li>City tour con Monserrate</li>
<li>City tour Zipaquira con entrada</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 24,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 24,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Cali / Lima v&iacute;a Avianca</li>
<li>3 Noches de alojamiento</li>
<li>Desayunos diarios</li>
<li>Traslados IN/OUT</li>
<li>Clase de Salsa (2hrs con traslados)</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 25,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.  </li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li>
<li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 25,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>&bull; Boleto A&eacute;reo Lima / Medell&iacute;n / Lima v&iacute;a Avianca</li>
<li>&bull; 2 Noches de alojamiento</li>
<li>&bull; Desayunos diarios</li>
<li>&bull; Traslados IN/OUT</li>
<li>&bull; City tour Panor&aacute;mico</li>
<li>&bull; Tour de Compras</li>
<li>&bull; Tarjeta de asistencia m&eacute;dica</li>
<li>&bull; Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 26,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 26,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Medell&iacute;n / Lima v&iacute;a Avianca</li>
<li>2 Noches de alojamiento</li>
<li>Desayunos diarios</li>
<li>Traslados IN/OUT</li>
<li>City tour Panor&aacute;mico</li>
<li>Tour de Pablo Escobar</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 27,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 27,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto A&eacute;reo Lima / Medell&iacute;n / Santa Marta / Lima v&iacute;a Avianca</li>
<li>2 Noches de alojamiento en Medell&iacute;n</li>
<li>Desayunos diarios</li>
<li>Traslados IN/OUT</li>
<li>City tour Panor&aacute;mico</li>
<li>2 Noches de alojamiento en Santa Marta</li>
<li>Desayunos</li>
<li>City Tour Historico</li>
<li>Tarjeta de asistencia m&eacute;dica</li>
<li>Impuestos A&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 28,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 28,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto Aéreo Lima / Panama / Orlando / Panama / Lima vía Copa Airlines </li>
<li>Traslados In/Out </li>
<li>Desayunos continentales </li>
<li>4 noches de Alojamiento </li>
<li>1 día de shopping Premium Outlet Mall</li>
<li>1 dia de shopping Walmart</li>
<li>Seguro de Asistencia</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.  </li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li>
<li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima-San Salvador-Roat&aacute;n-San Salvador-Lima v&iacute;a Avianca</li>
<li>Traslados de ingreso y salida</li>
<li>07 Noches de alojamiento</li>
<li>Seguro de asistencia</li>
<li>Sistema de alimentacion y bebidas Todo incluido</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 30,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 30,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto Aereo Lima / Panamá / Montego Bay / Panamá / Lima vía Copa Airlines</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>03 Noches de alojamiento</li>
<li>Sistema de alimentación TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 días</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 31,
                'created_at' => NULL,
                'updated_at' => '2018-05-18 13:36:17',
            ),
            61 => 
            array (
                'id' => 62,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li>
<li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 31,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto aéreo Lima-Panamá-Montego Bay–Panamá-Lima vía Copa Airlines</li>
<li>Traslados de ingreso y salida </li>
<li>04 Noches de alojamiento </li>
<li>Sistema de alimentación TODO INCLUIDO</li>
<li>Seguro de asistencia</li>
<li>Impuestos hoteleros y aéreos</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 32,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.  </li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li>
<li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 32,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 33,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 33,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 34,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 34,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 35,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 35,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto aéreo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>03 Noches de alojamiento</li>
<li>Sistema de alimentación TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 días</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 36,
                'created_at' => NULL,
                'updated_at' => '2018-05-18 13:35:59',
            ),
            71 => 
            array (
                'id' => 72,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 36,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 37,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 37,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 38,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 38,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 39,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 39,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 40,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 40,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto aéreo </li>
<li>Traslados de ingreso y salida en servicio regular </li>
<li>04 Noches de alojamiento </li>
<li>Sistema de alimentación TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 días</li>
<li>Impuestos hoteleros </li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 41,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 41,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 42,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 42,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 43,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 43,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 44,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.  </li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li>
<li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 44,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 45,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 45,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto aéreo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentación TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 días</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 46,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.  </li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li>
<li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 46,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 47,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 47,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 48,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 48,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 49,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 49,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo Lima - M&eacute;xico D.F-Canc&uacute;n - M&eacute;xico D.F - Lima v&iacute;a Aerom&eacute;xico</li>
<li>Traslados de ingreso y salida</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Servicio ALL FUN INCLUSIVE</li>
<li>Seguro de asistencia</li>
<li>Impuestos hoteleros y a&eacute;reos</li>
</ul>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 50,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 101,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 50,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 102,
                'nombre' => 'Detalle',
                'descripcion' => 'Vivir y disfrutar lo mejor de México nunca fue tan fácil como en Hotel Xcaret México, donde tienes acceso ilimitado a los parques y tours de Xcaret. Xcaret, Xel-Há, Xplor, Xplor Fuego, Xoximilco, Xenses, Xenotes y Xichén, con transportación incluida y el plan de alimentos que cada atracción ofrece. Además de transportación aeropuerto-hotel-aeropuerto. Es por ello que los huéspedes no tendrán que preocuparse de absolutamente nada. Nuestra misión y mayor recompensa será, que en todo momento se lleven a México en la piel.',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 50,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 103,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 51,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 104,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 51,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 105,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto a&eacute;reo</li>
<li>Traslados de ingreso y salida en servicio regular</li>
<li>04 Noches de alojamiento</li>
<li>Sistema de alimentaci&oacute;n TODO INCLUIDO</li>
<li>Tarjeta de asistencia por 05 d&iacute;as</li>
<li>Impuestos hoteleros</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 52,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 106,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayor&iacute;a de Hoteles que ofrecen acomodaci&oacute;n TRIPLE y CUADRUPLE. La habitaci&oacute;n consta de 1 &oacute; 2 camas dobles, las cuales ser&aacute;n asignadas de acuerdo a disponibilidad. Consultar siempre por el m&aacute;ximo de personas permitidas por cada tipo de habitaci&oacute;n requerida.</li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicar&aacute; un suplemento que ser&aacute; indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocer&aacute; derecho de devoluci&oacute;n alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no est&aacute;n incluidas en ning&uacute;n servicio que ofrecemos. Al requerir servicios de maleteros &oacute; cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deber&aacute; de esperar al transportista, en el lugar indicado y horario establecido (la informaci&oacute;n de horarios se les comunicar&aacute; en el destino final).</li>
<li>Discover Mayorista de Turismo solo act&uacute;a como intermediario entre los proveedores locales e internacionales y el usuario, es responsable &uacute;nicamente por la organizaci&oacute;n de los tours, servicios y boletos a&eacute;reos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que est&eacute;n fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier p&eacute;rdida, da&ntilde;o, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deber&aacute; registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condici&oacute;n de intermediario, gestionar&aacute; su tramitaci&oacute;n hasta la respuesta del proveedor, agotando la reconsideraci&oacute;n de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 52,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 107,
                'nombre' => 'Incluye',
                'descripcion' => '<ul>
<li>Boleto Aéreo Lima / Montevideo / Lima vía Avianca </li>
<li>Traslados In/Out en servicio privado</li>
<li>Desayunos</li>
<li>2 noches de alojamiento en Hotel Seleccionado</li>
<li>City Tour en servicio regular </li>
<li>Seguro de Asistencia</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 53,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 108,
                'nombre' => 'Importante',
                'descripcion' => '<ul>
<li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.  </li>
<li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li>
<li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li>
<li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li>
<li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li>
<li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li>
<li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li>
</ul>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 53,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 109,
                'nombre' => 'Itinerario',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border: none;">
<tbody><tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;mso-border-alt:solid #538135 .5pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;background:#538135;
mso-background-themecolor:accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">FECHAS<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="174" colspan="2" style="width:130.2pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">SALIDA<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">LLEGADA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">25 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">338<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">02:35<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">06:11<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">25 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">150<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PUNTA CANA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">09:34<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">13:18<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">30 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">109<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PUNTA CANA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">12:41<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">14:16<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">30 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">493<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">15:46<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">19:19<o:p></o:p></span></strong></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 1,
                'created_at' => '2018-05-17 17:09:53',
                'updated_at' => '2018-05-17 17:09:53',
            ),
            108 => 
            array (
                'id' => 110,
                'nombre' => 'Itinerario ',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border: none;">
<tbody><tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;mso-border-alt:solid #538135 .5pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;background:#538135;
mso-background-themecolor:accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">FECHAS<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="174" colspan="2" style="width:130.2pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">SALIDA<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">LLEGADA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">25 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">338<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">02:35<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">06:11<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">25 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">150<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PUNTA CANA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">09:34<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">13:18<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">30 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">109<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PUNTA CANA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">12:41<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">14:16<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">30 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">493<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">15:46<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">19:19<o:p></o:p></span></strong></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 9,
                'created_at' => '2018-05-17 17:37:42',
                'updated_at' => '2018-05-17 17:37:42',
            ),
            109 => 
            array (
                'id' => 111,
                'nombre' => 'Itinerario ',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border: none;">
<tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:13.65pt">
<td width="108" style="width:81.15pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;mso-border-alt:solid #538135 .5pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;background:#538135;
mso-background-themecolor:accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;
height:13.65pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:yellow">FECHAS<o:p></o:p></span></strong></p>
</td>
<td width="77" valign="top" style="width:57.9pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:
13.65pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:yellow">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="181" colspan="2" style="width:135.6pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:
13.65pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:yellow">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="79" style="width:59.6pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt;height:13.65pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:yellow">SALIDA<o:p></o:p></span></strong></p>
</td>
<td width="95" style="width:71.3pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt;height:13.65pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:yellow">LLEGADA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr style="mso-yfti-irow:1;height:13.25pt">
<td width="108" style="width:81.15pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%">27 JUL<o:p></o:p></span></strong></p>
</td>
<td width="77" valign="top" style="width:57.9pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">LA 2443<o:p></o:p></span></strong></p>
</td>
<td width="90" style="width:67.8pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="90" style="width:67.8pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">IGUAZU<o:p></o:p></span></strong></p>
</td>
<td width="79" style="width:59.6pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">12:35 HRS<o:p></o:p></span></strong></p>
</td>
<td width="95" style="width:71.3pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">18:30 HRS<o:p></o:p></span></strong></p>
</td>
</tr>
<tr style="mso-yfti-irow:2;mso-yfti-lastrow:yes;height:13.25pt">
<td width="108" style="width:81.15pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%">31 JUL<o:p></o:p></span></strong></p>
</td>
<td width="77" valign="top" style="width:57.9pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">LA 2442<o:p></o:p></span></strong></p>
</td>
<td width="90" style="width:67.8pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">IGUAZU<o:p></o:p></span></strong></p>
</td>
<td width="90" style="width:67.8pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="79" style="width:59.6pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">19:45 HRS<o:p></o:p></span></strong></p>
</td>
<td width="95" style="width:71.3pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt;height:13.25pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:10.0pt;line-height:105%;color:black">22:10 HRS<o:p></o:p></span></strong></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 4,
                'created_at' => '2018-05-17 18:24:15',
                'updated_at' => '2018-05-17 18:24:15',
            ),
            110 => 
            array (
                'id' => 112,
                'nombre' => 'Itinerario ',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border: none;">
<tbody><tr>
<td width="104" style="width:77.95pt;border:solid windowtext 1.0pt;mso-border-alt:
solid windowtext .5pt;background:#5B9BD5;mso-background-themecolor:accent1;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">FECHAS<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border:solid windowtext 1.0pt;
border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;background:#5B9BD5;mso-background-themecolor:accent1;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="174" colspan="2" style="width:130.2pt;border:solid windowtext 1.0pt;
border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;background:#5B9BD5;mso-background-themecolor:accent1;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border:solid windowtext 1.0pt;border-left:
none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
background:#5B9BD5;mso-background-themecolor:accent1;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">SALIDA<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border:solid windowtext 1.0pt;border-left:
none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
background:#5B9BD5;mso-background-themecolor:accent1;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">LLEGADA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid windowtext 1.0pt;border-top:
none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">25/26 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">492<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">04:45<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">08:26<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid windowtext 1.0pt;border-top:
none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">25/26 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">325<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">CANCÚN<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">09:52<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">12:37<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid windowtext 1.0pt;border-top:
none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">30/31 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">271<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">CANCÚN<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">16:48<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">19:23<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid windowtext 1.0pt;border-top:
none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">30/31 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">337<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">21:37<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;
mso-fareast-language:EN-US">01:14<o:p></o:p></span></strong></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 5,
                'created_at' => '2018-05-18 09:55:54',
                'updated_at' => '2018-05-18 09:55:54',
            ),
            111 => 
            array (
                'id' => 113,
                'nombre' => 'Servicios Incluidos',
                'descripcion' => '<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:red;
background:yellow;mso-highlight:yellow">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif;
color:red;background:yellow;mso-highlight:yellow">Todos los parques incluidos
(Xcaret, Xplor, Xplor Fuego, Xenses, Xoximilco, Xel-Ha, Xenotes, Xichen)<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">Shuttle
Aeropuerto - Hotel - Aeropuerto cada 30 minutos<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">Transportación
cada 30 minutos entre Hotel y Parque Xcaret, Xplor, Xplor Fuego, Xenses<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">Servicio
de Concierge<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">WIFI
(gratis)<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">Actividades
diarias Eco-Friendly<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">SPA
- Clases Yoga (sujeta a disponibilidad)<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">Servicio
de computadora e impresora / El Business center se mueve contigo<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">10
Restaurates, 3 de ellos solo para Adultos, HA’ ($), La Cantina &amp; Fuego<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">Gimnasio<o:p></o:p></span></p>

<p class="MsoNoSpacing" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:
l0 level1 lfo1"><!--[if !supportLists]--><span lang="ES" style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-family:&quot;Arial Narrow&quot;,sans-serif">Servicio
a habitación 24 Hrs.<o:p></o:p></span></p>',
                'orden' => 4,
                'estado' => 1,
                'package_id' => 5,
                'created_at' => '2018-05-18 09:55:54',
                'updated_at' => '2018-05-18 09:55:54',
            ),
            112 => 
            array (
                'id' => 114,
                'nombre' => 'Incluye',
                'descripcion' => '<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm"><!--[if !supportLists]--><span style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Arial Narrow&quot;;mso-bidi-font-weight:bold">Boleto aéreo Lima-Punta Cana-Lima vía
Copa Airlines</span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm"><!--[if !supportLists]--><span style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Arial Narrow&quot;;mso-no-proof:yes">Traslados de ingreso y salida </span><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;;
mso-bidi-font-weight:bold"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Arial Narrow&quot;">05 Noches de alojamiento </span><span style="font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Arial Narrow&quot;">Sistema de alimentación <strong><em>TODO INCLUIDO</em></strong></span><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
Arial;mso-no-proof:yes">Seguro de asistencia<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Arial Narrow&quot;">Impuestos hoteleros y aéreos<o:p></o:p></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 54,
                'created_at' => '2018-05-18 10:05:51',
                'updated_at' => '2018-05-18 10:05:51',
            ),
            113 => 
            array (
                'id' => 115,
                'nombre' => 'Importante',
                'descripcion' => '<p class="MsoListParagraph" style="margin-top:0cm;margin-right:0cm;margin-bottom:
8.0pt;margin-left:18.0pt;mso-add-space:auto;text-align:justify;text-indent:
-18.0pt;mso-line-height-alt:5.0pt;mso-list:l0 level1 lfo1;mso-hyphenate:none"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;">Reprogramaciones y/o cancelaciones de vuelos (comerciales
y/o privados) están sujetas a las regulaciones aeronáuticas internacionales
vigentes, quedando bajo las mismas la aplicación de la normativa. El operador
que actúa en la intermediación, Discover Mayorista de Turismo SAC NO es
responsable de las acciones fortuitas e involuntarias de los prestadores de
servicios, siendo este un intermediario al igual que las agencias de viajes.
Discover Mayorista de Turismo SAC Informará en las regulaciones de los
programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir
según apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en
cualquiera de las situaciones generadas por un tercero, aerolíneas, operadores
terrestres etc, deberán ser reclamados en primera instancia directamente al
operador en destino, de no tener respuesta y solución deberá presentar reclamo
vía carta formal del implicado a la agencia para que Discover Mayorista y/o la
agencia de viajes lo eleve al prestador de servicios involucrado, de la misma
manera que se generó la venta, respetando dicho canal comercial. Discover
Mayorista y/o las agencias actúan siempre como asesores o intermediarios de la
operación <span style="color:black">entre los proveedores locales e
internacionales y el usuario, somos responsables únicamente por la organización
de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario
no puede imputarnos responsabilidad alguna por causas que estén fuera de
nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado
de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza
mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad
causada al usuario por hecho de terceros o por la imprudencia del propio
usuario afectado.&nbsp;</span><o:p></o:p></span></p><p class="MsoListParagraph" style="margin-top:0cm;margin-right:0cm;margin-bottom:
8.0pt;margin-left:18.0pt;mso-add-space:auto;text-align:justify;text-indent:
-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:
10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;color:black">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black">Los traslados brindados son en servicio regular
teniendo las siguientes condiciones: El operador que ofrece el servicio esta
detallado en el voucher entregado al cliente, es muy importante que reconozcan
al operador por el cartel y el uniforme con el nombre del operador. Al regreso
deben reconfirmar el servicio y la hora del recojo dos días antes de salida,
esto lo hace en las mismas oficinas del operador dentro del hotel o por el
teléfono detallado en el voucher.<o:p></o:p></span></p>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 54,
                'created_at' => '2018-05-18 10:05:51',
                'updated_at' => '2018-05-18 10:05:51',
            ),
            114 => 
            array (
                'id' => 116,
                'nombre' => 'Itinerario ',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border: none;">
<tbody><tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;mso-border-alt:solid #538135 .5pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;background:#538135;
mso-background-themecolor:accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">FECHAS<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="174" colspan="2" style="width:130.2pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">SALIDA<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">LLEGADA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">25 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">338<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">02:35<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">06:11<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">25 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">150<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PUNTA CANA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">09:34<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">13:18<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">30 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">109<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PUNTA CANA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">12:41<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">14:16<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">30 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">493<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">15:46<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">19:19<o:p></o:p></span></strong></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 54,
                'created_at' => '2018-05-18 10:05:51',
                'updated_at' => '2018-05-18 10:05:51',
            ),
            115 => 
            array (
                'id' => 117,
                'nombre' => 'Incluye',
                'descripcion' => '<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm"><!--[if !supportLists]--><span style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;;
mso-bidi-font-weight:bold">Boleto aéreo Lima-Panamá-Cancún-Panamá-Lima vía Copa
Airlines</span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm"><!--[if !supportLists]--><span style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;;
mso-no-proof:yes">Traslados de ingreso y salida </span><span style="font-size:
11.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;;
mso-bidi-font-weight:bold"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;">05 Noches
de alojamiento</span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:Arial;mso-no-proof:yes">Seguro
de asistencia al viajero<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;">Impuestos
hoteleros y aéreos<o:p></o:p></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 56,
                'created_at' => '2018-05-18 12:17:30',
                'updated_at' => '2018-05-18 12:17:30',
            ),
            116 => 
            array (
                'id' => 118,
                'nombre' => 'Importante',
            'descripcion' => '<ul style="font-size: 17px; color: rgb(116, 116, 116);"><li>PAGO DEL SALDO: A los 15 días después realizado el prepago.</li><li>Consultar por tarifa de INF (0-2 años) y CHD (2 – 5 años).</li><li>Tarifas válidas para comprar hasta el 30 Mayo o agotar stock.</li><li>Reservas que ingresen dentro de los 30 días previos a la fecha de inicio de viaje, se solicitará PAGO TOTAL y con un tiempo límite especial.</li><li>Pasajeros deben presentarse en Counter con 4 horas de anticipación.</li><li>Entrega de los boletos y vouchers se realizarán 48 horas antes de la fecha de salida.</li><li>Anulaciones y cancelaciones se penalizará con el 100% una vez realizado el pago final. NO SHOW: Se penalizará al 100%.</li><li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad</li><li>Itinerarios y vuelos están sujetos a variación según disposición de la línea aérea.</li><li>Tarifas, queues e impuestos están sujetos a cambio sin previo aviso hasta el momento de la emisión de los mismos.</li><li>Hoteles sujetos a disponibilidad.</li><li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) están sujetas a las regulaciones aeronáuticas internacionales vigentes, quedando bajo las mismas la aplicación de la normativa. El operador que actúa en la intermediación, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informará en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir según apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerolíneas, operadores terrestres etc, deberán ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y solución deberá presentar reclamo vía carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se generó la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias actúan siempre como asesores o intermediarios de la operación entre los proveedores locales e internacionales y el usuario, somos responsables únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li></ul>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 56,
                'created_at' => '2018-05-18 12:17:30',
                'updated_at' => '2018-05-18 12:17:30',
            ),
            117 => 
            array (
                'id' => 119,
                'nombre' => 'Itinerario',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border: none;">
<tbody><tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;mso-border-alt:solid #538135 .5pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;background:#538135;
mso-background-themecolor:accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">FECHAS<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="174" colspan="2" style="width:130.2pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">SALIDA<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">LLEGADA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">25/26 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">492<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">04:45<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">08:26<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">25/26 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">325<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">CANCÚN<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">09:52<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">12:37<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">30/31 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">271<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">CANCÚN<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">16:48<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">19:23<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">30/31 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">337<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">21:37<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">01:14<o:p></o:p></span></strong></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 56,
                'created_at' => '2018-05-18 12:17:31',
                'updated_at' => '2018-05-18 12:17:31',
            ),
            118 => 
            array (
                'id' => 120,
                'nombre' => 'Incluye',
                'descripcion' => '<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm"><!--[if !supportLists]--><span style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;;
mso-bidi-font-weight:bold">Boleto aéreo Lima-Panamá-Cancún-Panamá-Lima vía Copa
Airlines</span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm"><!--[if !supportLists]--><span style="font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;mso-bidi-font-weight:
bold">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;;
mso-no-proof:yes">Traslados de ingreso y salida </span><span style="font-size:
11.0pt;mso-bidi-font-size:12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;;
mso-bidi-font-weight:bold"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;">05 Noches
de alojamiento</span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:Arial;mso-no-proof:yes">Seguro
de asistencia al viajero<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo1;
tab-stops:list 0cm;mso-layout-grid-align:none;text-autospace:none"><!--[if !supportLists]--><span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:
Symbol;mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
12.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Arial Narrow&quot;">Impuestos
hoteleros y aéreos<o:p></o:p></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 58,
                'created_at' => '2018-05-18 12:21:36',
                'updated_at' => '2018-05-18 12:21:36',
            ),
            119 => 
            array (
                'id' => 121,
                'nombre' => 'Importante',
            'descripcion' => '<ul style="font-size: 17px; color: rgb(116, 116, 116);"><li>PAGO DEL SALDO: A los 15 días después realizado el prepago.</li><li>Consultar por tarifa de INF (0-2 años) y CHD (2 – 5 años).</li><li>Tarifas válidas para comprar hasta el 30 Mayo o agotar stock.</li><li>Reservas que ingresen dentro de los 30 días previos a la fecha de inicio de viaje, se solicitará PAGO TOTAL y con un tiempo límite especial.</li><li>Pasajeros deben presentarse en Counter con 4 horas de anticipación.</li><li>Entrega de los boletos y vouchers se realizarán 48 horas antes de la fecha de salida.</li><li>Anulaciones y cancelaciones se penalizará con el 100% una vez realizado el pago final. NO SHOW: Se penalizará al 100%.</li><li>No se permite cambio de fechas por tratarse de una salida en grupo. Hoteles sujetos a disponibilidad</li><li>Itinerarios y vuelos están sujetos a variación según disposición de la línea aérea.</li><li>Tarifas, queues e impuestos están sujetos a cambio sin previo aviso hasta el momento de la emisión de los mismos.</li><li>Hoteles sujetos a disponibilidad.</li><li>Reprogramaciones y/o cancelaciones de vuelos (comerciales y/o privados) están sujetas a las regulaciones aeronáuticas internacionales vigentes, quedando bajo las mismas la aplicación de la normativa. El operador que actúa en la intermediación, Discover Mayorista de Turismo SAC NO es responsable de las acciones fortuitas e involuntarias de los prestadores de servicios, siendo este un intermediario al igual que las agencias de viajes. Discover Mayorista de Turismo SAC Informará en las regulaciones de los programas ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir según apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en cualquiera de las situaciones generadas por un tercero, aerolíneas, operadores terrestres etc, deberán ser reclamados en primera instancia directamente al operador en destino, de no tener respuesta y solución deberá presentar reclamo vía carta formal del implicado a la agencia para que Discover Mayorista y/o la agencia de viajes lo eleve al prestador de servicios involucrado, de la misma manera que se generó la venta, respetando dicho canal comercial. Discover Mayorista y/o las agencias actúan siempre como asesores o intermediarios de la operación entre los proveedores locales e internacionales y el usuario, somos responsables únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado.</li></ul>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 58,
                'created_at' => '2018-05-18 12:21:36',
                'updated_at' => '2018-05-18 12:21:36',
            ),
            120 => 
            array (
                'id' => 122,
                'nombre' => 'Itinerario',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border: none;">
<tbody><tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;mso-border-alt:solid #538135 .5pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;background:#538135;
mso-background-themecolor:accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">FECHAS<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="174" colspan="2" style="width:130.2pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">SALIDA<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow;mso-ansi-language:ES;mso-fareast-language:EN-US">LLEGADA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">25/26 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">492<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">04:45<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">08:26<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">25/26 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">325<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">CANCÚN<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">09:52<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">12:37<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">30/31 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">271<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">CANCÚN<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">16:48<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">19:23<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="104" style="width:77.95pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;
line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:ES;mso-fareast-language:EN-US">30/31 JUL<o:p></o:p></span></strong></p>
</td>
<td width="74" valign="top" style="width:55.6pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">337<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">21:37<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center;line-height:105%"><strong><span lang="ES" style="font-size:9.5pt;line-height:105%;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:ES;mso-fareast-language:EN-US">01:14<o:p></o:p></span></strong></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 58,
                'created_at' => '2018-05-18 12:21:36',
                'updated_at' => '2018-05-18 12:21:36',
            ),
            121 => 
            array (
                'id' => 123,
                'nombre' => 'Incluye ',
                'descripcion' => '<p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]-->•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp; </span><span lang="ES" style="">Boleto aéreo Lima / Madrid / Lima vía PLUS
ULTRA</span><o:p></o:p></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp;</span></span><span lang="ES" style="">Traslado de llegada y de salida<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp;</span></span><!--[endif]--><span lang="ES" style="">01 noche a bordo<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•</span><span lang="ES" style="">15 noches de alojamiento en hoteles categoría
turista con desayuno incluido<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;</span></span><!--[endif]--><span lang="ES" style="">Guía acompañante durante todo el recorrido<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">• Vi</span><span lang="ES" style="">sitas en las ciudades de: Madrid, París,
Venecia, Florencia, Roma, todas con expertos guías locales<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;</span></span><!--[endif]--><span lang="ES" style="">Audio guías <o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><a name="_Hlk495319765"><!--[if !supportLists]--><span lang="ES" style="">•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;</span></span><!--[endif]--><span lang="ES" style="">Wifi gratuito en el autobús</span></a><span lang="ES" style=""><o:p></o:p></span></p>

<p class="MsoListParagraphCxSpLast" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp;</span></span><!--[endif]--><span lang="ES" style="">Tarjeta de asistencia ASSIST
CARD&nbsp; -&nbsp; cobertura mínima&nbsp; Euros&nbsp; 30000</span><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:black;
mso-themecolor:text1"><o:p></o:p></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 59,
                'created_at' => '2018-05-21 11:07:41',
                'updated_at' => '2018-05-21 11:07:41',
            ),
            122 => 
            array (
                'id' => 124,
                'nombre' => 'Itinerario ',
                'descripcion' => '<p class="MsoNoSpacing" align="center" style="text-align:center"><strong><span lang="ES" style="font-size:10.0pt;
font-family:&quot;Tahoma&quot;,sans-serif;color:#2E74B5;mso-themecolor:accent1;
mso-themeshade:191">Madrid / Burdeos / Valle del Loira / París / Lucerna /
Zürich / Verona / Venecia / Florencia / Roma / Pisa / Niza / Costa Azul /
Barcelona / Zaragoza / Madrid<o:p></o:p></span></strong></p>

<p class="MsoNoSpacing" align="center" style="text-align:center"><strong><span lang="ES" style="font-size:10.0pt;
font-family:&quot;Tahoma&quot;,sans-serif;color:#C45911;mso-themecolor:accent2;
mso-themeshade:191">17 días &nbsp;/ &nbsp;16 noches</span></strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#C45911;
mso-themecolor:accent2;mso-themeshade:191"><o:p></o:p></span></p>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 59,
                'created_at' => '2018-05-21 11:07:41',
                'updated_at' => '2018-05-21 11:07:41',
            ),
            123 => 
            array (
                'id' => 125,
                'nombre' => 'Itinerario Aéreo ',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="463" style="width: 346.9pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">
<tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:9.75pt">
<td width="62" style="width:46.45pt;background:#5B9BD5;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="150" valign="top" style="width:112.65pt;background:#5B9BD5;padding:
0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">FECHA<o:p></o:p></span></strong></p>
</td>
<td width="112" style="width:84.15pt;background:#5B9BD5;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="53" style="width:39.85pt;background:#5B9BD5;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">SALE<o:p></o:p></span></strong></p>
</td>
<td width="85" style="width:63.8pt;background:#5B9BD5;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">LLEGA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr style="mso-yfti-irow:1;height:9.75pt">
<td width="62" style="width:46.45pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">PU&nbsp; 302</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE"><o:p></o:p></span></p>
</td>
<td width="150" valign="top" style="width:112.65pt;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE">04&nbsp; JULIO<o:p></o:p></span></p>
</td>
<td width="112" valign="top" style="width:84.15pt;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE">LIMA / MADRID<o:p></o:p></span></p>
</td>
<td width="53" style="width:39.85pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">18:30</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;
mso-fareast-font-family:SimSun;color:#1F4E79;mso-themecolor:accent1;
mso-themeshade:128;mso-font-kerning:1.0pt;mso-ansi-language:EN-US;mso-fareast-language:
ES-PE"><o:p></o:p></span></p>
</td>
<td width="85" style="width:63.8pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">13:35 +1<o:p></o:p></span></p>
</td>
</tr>
<tr style="mso-yfti-irow:2;mso-yfti-lastrow:yes;height:9.75pt">
<td width="62" style="width:46.45pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">PU&nbsp; 301</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE"><o:p></o:p></span></p>
</td>
<td width="150" valign="top" style="width:112.65pt;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE">20&nbsp; JULIO<o:p></o:p></span></p>
</td>
<td width="112" valign="top" style="width:84.15pt;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE">MADRID / LIMA<o:p></o:p></span></p>
</td>
<td width="53" style="width:39.85pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">11:00</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;
mso-fareast-font-family:SimSun;color:#1F4E79;mso-themecolor:accent1;
mso-themeshade:128;mso-font-kerning:1.0pt;mso-ansi-language:EN-US;mso-fareast-language:
ES-PE"><o:p></o:p></span></p>
</td>
<td width="85" style="width:63.8pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">16:15</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;
mso-fareast-font-family:SimSun;color:#1F4E79;mso-themecolor:accent1;
mso-themeshade:128;mso-font-kerning:1.0pt;mso-ansi-language:EN-US;mso-fareast-language:
ES-PE"><o:p></o:p></span></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 59,
                'created_at' => '2018-05-21 11:07:41',
                'updated_at' => '2018-05-21 11:07:41',
            ),
            124 => 
            array (
                'id' => 126,
                'nombre' => 'Importante ',
                'descripcion' => '<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 36pt; line-height: normal;"><br><p><ul><li>Precios por persona, sujetos a
variación sin previo aviso y disponibilidad de espacios.<br></li><li>Los hoteles se reconfirman <span style="font-size: 1rem;">&nbsp;</span><span style="font-size: 1rem;">10 días antes de la llegada de los pasajeros.</span><br></li><li><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp; S</span><span lang="ES" style="font-size: 1rem;">e permite sólo 1 maleta de
bodega de (20 kg) además del equipaje de mano.</span><br></li><li>Habitación Triple: La
acomodación es de 1 cama de dos plazas y/o 2 camas de 1 plaza + 1 cama
plegable.<br></li><li><span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp;P</span><span lang="ES" style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-size: 1rem;">re pago NO REEMBOLSABLE de US$&nbsp; 1600.00
para reservar por persona.</span><br></li><li>PAGO FINAL<span style="font-size: 1rem;">&nbsp; </span><span style="font-size: 1rem;">:</span><span style="font-size: 1rem;">&nbsp; </span><span style="font-size: 1rem;">&nbsp;&nbsp; </span><span style="font-size: 1rem;">25
Mayo 2018.&nbsp;</span><br></li></ul></p></p>













',
                'orden' => 4,
                'estado' => 1,
                'package_id' => 59,
                'created_at' => '2018-05-21 11:07:41',
                'updated_at' => '2018-05-21 11:07:41',
            ),
            125 => 
            array (
                'id' => 127,
                'nombre' => 'Incluye ',
                'descripcion' => '<p class="MsoListParagraphCxSpFirst" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]-->•<span lang="ES" style="">Boleto aéreo Lima / Madrid / Lima vía PLUS
ULTRA</span><o:p></o:p></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">T</span></span><span lang="ES" style="">raslado de llegada y de salida<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•</span><span lang="ES" style="">01 noche a bordo<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•</span><span lang="ES" style="">15 noches de alojamiento en hoteles categoría
turista con desayuno incluido<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp;</span></span><span lang="ES" style="">Guía acompañante durante todo el recorrido<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal;">&nbsp;</span></span><span lang="ES" style="">Visitas en las ciudades de: Madrid, París,
Venecia, Florencia, Roma, todas con expertos guías locales<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•</span><span lang="ES" style="">Audio guías <o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><a name="_Hlk495319765"><!--[if !supportLists]--><span lang="ES" style="">•</span><span lang="ES" style="">Wifi gratuito en el autobús</span></a><span lang="ES" style=""><o:p></o:p></span></p>

<p class="MsoListParagraphCxSpLast" style="margin: 0cm 0cm 0.0001pt; line-height: normal;"><!--[if !supportLists]--><span lang="ES" style="">•</span><span lang="ES" style="">Tarjeta de asistencia ASSIST
CARD&nbsp; -&nbsp; cobertura mínima&nbsp; Euros&nbsp; 30000</span><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:black;
mso-themecolor:text1"><o:p></o:p></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 60,
                'created_at' => '2018-05-21 12:12:03',
                'updated_at' => '2018-05-21 12:12:03',
            ),
            126 => 
            array (
                'id' => 128,
                'nombre' => 'Itinerario ',
                'descripcion' => '<p class="MsoNoSpacing" align="center" style="text-align:center"><strong><span lang="ES" style="font-size:10.0pt;
font-family:&quot;Tahoma&quot;,sans-serif;color:#2E74B5;mso-themecolor:accent1;
mso-themeshade:191">Madrid / Burdeos / Valle del Loira / París / Lucerna /
Zürich / Verona / Venecia / Florencia / Roma / Pisa / Niza / Costa Azul /
Barcelona / Zaragoza / Madrid</span></strong></p>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 60,
                'created_at' => '2018-05-21 12:12:03',
                'updated_at' => '2018-05-21 12:12:03',
            ),
            127 => 
            array (
                'id' => 129,
                'nombre' => 'Itinerario Aéreo ',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="463" style="width: 346.9pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">
<tbody><tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;height:9.75pt">
<td width="62" style="width:46.45pt;background:#5B9BD5;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="150" valign="top" style="width:112.65pt;background:#5B9BD5;padding:
0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">FECHA<o:p></o:p></span></strong></p>
</td>
<td width="112" style="width:84.15pt;background:#5B9BD5;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="53" style="width:39.85pt;background:#5B9BD5;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">SALE<o:p></o:p></span></strong></p>
</td>
<td width="85" style="width:63.8pt;background:#5B9BD5;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal"><strong><span lang="ES" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
Calibri;color:white;mso-themecolor:background1">LLEGA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr style="mso-yfti-irow:1;height:9.75pt">
<td width="62" style="width:46.45pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">PU&nbsp; 302</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE"><o:p></o:p></span></p>
</td>
<td width="150" valign="top" style="width:112.65pt;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE">04&nbsp; JULIO<o:p></o:p></span></p>
</td>
<td width="112" valign="top" style="width:84.15pt;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE">LIMA / MADRID<o:p></o:p></span></p>
</td>
<td width="53" style="width:39.85pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">18:30</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;
mso-fareast-font-family:SimSun;color:#1F4E79;mso-themecolor:accent1;
mso-themeshade:128;mso-font-kerning:1.0pt;mso-ansi-language:EN-US;mso-fareast-language:
ES-PE"><o:p></o:p></span></p>
</td>
<td width="85" style="width:63.8pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">13:35 +1<o:p></o:p></span></p>
</td>
</tr>
<tr style="mso-yfti-irow:2;mso-yfti-lastrow:yes;height:9.75pt">
<td width="62" style="width:46.45pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">PU&nbsp; 301</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE"><o:p></o:p></span></p>
</td>
<td width="150" valign="top" style="width:112.65pt;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE">20&nbsp; JULIO<o:p></o:p></span></p>
</td>
<td width="112" valign="top" style="width:84.15pt;padding:0cm 0cm 0cm 0cm;
height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:
SimSun;color:#1F4E79;mso-themecolor:accent1;mso-themeshade:128;mso-font-kerning:
1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ES-PE">MADRID / LIMA<o:p></o:p></span></p>
</td>
<td width="53" style="width:39.85pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">11:00</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;
mso-fareast-font-family:SimSun;color:#1F4E79;mso-themecolor:accent1;
mso-themeshade:128;mso-font-kerning:1.0pt;mso-ansi-language:EN-US;mso-fareast-language:
ES-PE"><o:p></o:p></span></p>
</td>
<td width="85" style="width:63.8pt;padding:0cm 0cm 0cm 0cm;height:9.75pt">
<p class="MsoNormal" align="center" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;mso-pagination:none"><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;color:#1F4E79;
mso-themecolor:accent1;mso-themeshade:128;mso-ansi-language:EN-US">16:15</span><span lang="EN-US" style="font-size:10.0pt;font-family:&quot;Tahoma&quot;,sans-serif;
mso-fareast-font-family:SimSun;color:#1F4E79;mso-themecolor:accent1;
mso-themeshade:128;mso-font-kerning:1.0pt;mso-ansi-language:EN-US;mso-fareast-language:
ES-PE"><o:p></o:p></span></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 60,
                'created_at' => '2018-05-21 12:12:03',
                'updated_at' => '2018-05-21 12:12:03',
            ),
            128 => 
            array (
                'id' => 130,
                'nombre' => 'Importante',
                'descripcion' => '<p class="MsoNormalCxSpFirst" style="margin: 0cm 0cm 0.0001pt 36pt; line-height: normal;"><ul><li>Tarifas sujetas a variación y
cambios sin previo aviso.<br></li><li>Precios no aplican en fechas de
feriados calendarios del destino, eventos, convenciones etc., para lo cual
aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li><span lang="ES" style="font-size: 1rem;">Tarifas aplican solo para
peruanos residentes en Perú y extranjeros que no visiten su país de nacimiento.</span></li><li><span lang="ES" style="font-size: 1rem;">Al
momento de emitir los boletos deberán presentar: Copia pasaporte, sin
excepciones.</span></li><li><span lang="ES" style="font-size: 1rem;">Antes de la emisión del boleto
aéreo, se deberán reconfirmar los impuestos del boleto y los locales, debido a
que los precios de los impuestos están sujetos a constantes variaciones.</span></li><li><span lang="ES" style="font-size: 1rem;">En caso de que el vuelo se
retrase o reprograme, se deberá dar la información para poder reprogramar su
recojo o caso contrario se deberá llamar al número de emergencias indicado en el
voucher de servicio.</span></li><li><span lang="ES" style="font-size: 1rem;">Los traslados incluidos en los
programas son en base a servicio privado. El pasajero debe de tener en cuenta
que, para el traslado de llegada, el transportista estará esperándolo con un
cartel a nombre del grupo en el lugar indicado y horario establecido (la
información de horarios será proporcionada por la agencia de viajes).</span></li><li><span lang="ES" style="font-size: 1rem;">Las propinas no están incluidas
en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó
cualquier servicio adicional, las propinas son obligatorias.</span></li><li><span lang="ES" style="font-size: 1rem;">La empresa no reconocerá derecho
de devolución alguno, por el uso de servicios de terceros ajenos al servicio
contratado, que no hayan sido autorizados previamente por escrito por la
empresa.</span></li><li><span lang="ES" style="font-size: 1rem;">Precios válidos para grupos, no
reembolsables, no endosables ni transferibles.</span></li></ul></p>

<br>',
                'orden' => 4,
                'estado' => 1,
                'package_id' => 60,
                'created_at' => '2018-05-21 12:12:03',
                'updated_at' => '2018-05-21 12:12:03',
            ),
            129 => 
            array (
                'id' => 131,
                'nombre' => 'Incluye',
                'descripcion' => '<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-family: Symbol;">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family: Calibri, sans-serif;">Boleto aéreo Lima-Punta Cana-Lima vía
Copa Airlines</span></p>

<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-family: Symbol;">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-size: 7pt; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family: Calibri, sans-serif;">Traslados de ingreso y salida </span><span style="font-family: Calibri, sans-serif;"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-family: Symbol;">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-size: 7pt; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family: Calibri, sans-serif;">05 Noches de alojamiento </span><span style="font-family: Calibri, sans-serif;"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-family: Symbol;">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-size: 7pt; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family: Calibri, sans-serif;">Sistema de alimentación <strong><em>TODO INCLUIDO</em></strong></span><span style="font-family: Calibri, sans-serif;"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-family: Symbol;">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-size: 7pt; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family: Calibri, sans-serif;">Seguro de asistencia<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-family: Symbol;">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; line-height: normal; font-size: 7pt; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-family: Calibri, sans-serif;">Impuestos hoteleros y aéreos<o:p></o:p></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 61,
                'created_at' => '2018-05-21 12:35:18',
                'updated_at' => '2018-05-21 12:35:18',
            ),
            130 => 
            array (
                'id' => 132,
                'nombre' => 'Itinerario ',
                'descripcion' => '<p align="center">

<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" style="border: none;">
<tbody><tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;mso-border-alt:solid #538135 .5pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;background:#538135;
mso-background-themecolor:accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">FECHAS<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">VUELOS<o:p></o:p></span></strong></p>
</td>
<td width="174" colspan="2" style="width:130.2pt;border:solid #538135 1.0pt;
mso-border-themecolor:accent6;mso-border-themeshade:191;border-left:none;
mso-border-left-alt:solid #538135 .5pt;mso-border-left-themecolor:accent6;
mso-border-left-themeshade:191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;background:#538135;mso-background-themecolor:
accent6;mso-background-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">RUTA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">SALIDA<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-left:none;mso-border-left-alt:solid #538135 .5pt;
mso-border-left-themecolor:accent6;mso-border-left-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
background:#538135;mso-background-themecolor:accent6;mso-background-themeshade:
191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;
mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:yellow">LLEGADA<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">25 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">338<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">02:35<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">06:11<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">25 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">150<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PUNTA CANA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">09:34<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">13:18<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">30 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">109<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PUNTA CANA<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">12:41<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">14:16<o:p></o:p></span></strong></p>
</td>
</tr>
<tr>
<td width="95" style="width:70.9pt;border:solid #538135 1.0pt;mso-border-themecolor:
accent6;mso-border-themeshade:191;border-top:none;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-alt:
solid #538135 .5pt;mso-border-themecolor:accent6;mso-border-themeshade:191;
padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:
&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;">30 JUL<o:p></o:p></span></strong></p>
</td>
<td width="84" valign="top" style="width:63.1pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">493<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">PANAMÁ<o:p></o:p></span></strong></p>
</td>
<td width="87" style="width:65.1pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">LIMA<o:p></o:p></span></strong></p>
</td>
<td width="76" style="width:57.25pt;border-top:none;border-left:none;
border-bottom:solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;
mso-border-bottom-themeshade:191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:
accent6;mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">15:46<o:p></o:p></span></strong></p>
</td>
<td width="91" style="width:68.5pt;border-top:none;border-left:none;border-bottom:
solid #538135 1.0pt;mso-border-bottom-themecolor:accent6;mso-border-bottom-themeshade:
191;border-right:solid #538135 1.0pt;mso-border-right-themecolor:accent6;
mso-border-right-themeshade:191;mso-border-top-alt:solid #538135 .5pt;
mso-border-top-themecolor:accent6;mso-border-top-themeshade:191;mso-border-left-alt:
solid #538135 .5pt;mso-border-left-themecolor:accent6;mso-border-left-themeshade:
191;mso-border-alt:solid #538135 .5pt;mso-border-themecolor:accent6;
mso-border-themeshade:191;padding:0cm 5.4pt 0cm 5.4pt">
<p class="MsoNormal" align="center" style="text-align:center"><strong><span style="font-size:9.5pt;font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:
minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black">19:19<o:p></o:p></span></strong></p>
</td>
</tr>
</tbody></table>

</p>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 61,
                'created_at' => '2018-05-21 12:35:18',
                'updated_at' => '2018-05-21 12:35:18',
            ),
            131 => 
            array (
                'id' => 133,
                'nombre' => 'Importante ',
                'descripcion' => '<p class="MsoListParagraphCxSpFirst" style="margin-top:0cm;margin-right:0cm;
margin-bottom:8.0pt;margin-left:18.0pt;mso-add-space:auto;text-align:justify;
text-indent:-18.0pt;mso-line-height-alt:5.0pt;mso-list:l0 level1 lfo1;
mso-hyphenate:none"><!--[if !supportLists]--><span style="font-size:10.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;">Reprogramaciones y/o cancelaciones de vuelos (comerciales
y/o privados) están sujetas a las regulaciones aeronáuticas internacionales
vigentes, quedando bajo las mismas la aplicación de la normativa. El operador
que actúa en la intermediación, Discover Mayorista de Turismo SAC NO es
responsable de las acciones fortuitas e involuntarias de los prestadores de
servicios, siendo este un intermediario al igual que las agencias de viajes.
Discover Mayorista de Turismo SAC Informará en las regulaciones de los programas
ofrecidos a los agentes y/o pasajeros de los procedimientos a seguir según
apliquen dichas regulaciones. Cualquier perjuicio que afecte al pasajero en
cualquiera de las situaciones generadas por un tercero, aerolíneas, operadores
terrestres etc, deberán ser reclamados en primera instancia directamente al
operador en destino, de no tener respuesta y solución deberá presentar reclamo
vía carta formal del implicado a la agencia para que Discover Mayorista y/o la
agencia de viajes lo eleve al prestador de servicios involucrado, de la misma
manera que se generó la venta, respetando dicho canal comercial. Discover
Mayorista y/o las agencias actúan siempre como asesores o intermediarios de la
operación <span style="color:black">entre los proveedores locales e
internacionales y el usuario, somos responsables únicamente por la organización
de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario
no puede imputarnos responsabilidad alguna por causas que estén fuera de
nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado
de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza
mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad
causada al usuario por hecho de terceros o por la imprudencia del propio usuario
afectado. </span><o:p></o:p></span></p>

<p class="MsoListParagraphCxSpLast" style="margin-left:18.0pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;mso-line-height-alt:5.0pt;mso-list:l0 level1 lfo1;
mso-hyphenate:none"><!--[if !supportLists]--><span style="font-size:10.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;">Sugerimos recomendar a agentes y pasajeros la compra
de:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:18.0pt;text-align:justify;mso-line-height-alt:
5.0pt;mso-hyphenate:none"><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;">Seguros de asistencia al viajero con beneficios de
cancelaciones.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:18.0pt;text-align:justify;mso-line-height-alt:
5.0pt;mso-hyphenate:none"><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;">Seguros de asistencia médica.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:18.0pt;text-align:justify;mso-line-height-alt:
5.0pt;mso-hyphenate:none"><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;">Excursiones y traslados desde el punto de origen en
Peru.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<o:p></o:p></span></p>

<p class="MsoNormal" style="margin-left:18.0pt;text-align:justify;mso-line-height-alt:
5.0pt;mso-hyphenate:none"><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;">Seguros de cancelación en varias opciones.<strong><o:p></o:p></strong></span></p>

<p class="MsoNoSpacing" style="margin-left:18.0pt;text-align:justify;text-indent:
-18.0pt;line-height:normal;mso-list:l0 level1 lfo1;mso-hyphenate:auto"><!--[if !supportLists]--><span lang="ES" style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol;color:black">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES" style="font-size:10.0pt;
mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:
Calibri;mso-hansi-theme-font:minor-latin;color:black">Tarjeta de Asistencia
incluida es válida para personas menores de 74 años de edad.<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpFirst" style="margin-top:0cm;margin-right:0cm;
margin-bottom:8.0pt;margin-left:18.0pt;mso-add-space:auto;text-align:justify;
text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:black">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size: 10pt; font-family: Calibri, sans-serif; color: black; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Debido a los múltiples cambios
que ocurren diariamente en turismo estos precios deben ser confirmados a la
hora de hacer la reserva.</span><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black"><o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;
margin-bottom:8.0pt;margin-left:18.0pt;mso-add-space:auto;text-align:justify;
text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:black">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black">Los asientos serán solicitados a la línea aérea
después de la emisión del grupo, quedando sujeto a disponibilidad del mapa de
asientos y condiciones que tenga la Línea. Para temporada alta y en los casos
que no tengan asientos asignados les pedimos que le informen a sus clientes que
deben presentarse 4:00 horas antes en el aeropuerto. Por política de las líneas
aéreas en caso de sobreventa, éstas no garantizan los asientos asignados, la
única forma de garantizarlo es realizando el pre-chequeo.<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;
margin-bottom:8.0pt;margin-left:18.0pt;mso-add-space:auto;text-align:justify;
text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:black">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black">Es muy importante que las solicitudes especiales
(sillas de ruedas, habitaciones Handicap, luna de miel, aniversario,
cumpleaños, habitaciones comunicantes o contiguas, etc.), deban ser Ingresadas
al momento de solicitar la reserva.<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;
margin-bottom:8.0pt;margin-left:18.0pt;mso-add-space:auto;text-align:justify;
text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:black">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black">Los traslados brindados son en servicio regular
teniendo las siguientes condiciones: El operador que ofrece el servicio esta
detallado en el voucher entregado al cliente, es muy importante que reconozcan
al operador por el cartel y el uniforme con el nombre del operador. Al regreso
deben reconfirmar el servicio y la hora del recojo dos días antes de salida,
esto lo hace en las mismas oficinas del operador dentro del hotel o por el
teléfono detallado en el voucher.<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="margin-top:0cm;margin-right:0cm;
margin-bottom:8.0pt;margin-left:18.0pt;mso-add-space:auto;text-align:justify;
text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:black">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black">Debe de tener en cuenta que todos los traslados
de llegada, salida del aeropuerto, hotel y las excursiones, deberá de esperar
al transportista, en el lugar indicado y horario establecido (la información de
horarios se les comunicará en el destino final). Si esto no sucediera el
transportista no está en la obligación de esperar y continuará con su ruta
programada. Por lo tanto, si no cumple con los horarios establecidos y no
accede a su servicio, no es responsabilidad del transportista; ni está sujeto a
reclamaciones o reembolsos hacia la entidad prestadora del servicio.&nbsp;<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpLast" style="margin-top:0cm;margin-right:0cm;
margin-bottom:8.0pt;margin-left:18.0pt;mso-add-space:auto;text-align:justify;
text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:10.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:black">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;
mso-ascii-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black">Precios sujetos a cambio sin previo aviso.&nbsp;<o:p></o:p></span></p>',
                'orden' => 3,
                'estado' => 1,
                'package_id' => 61,
                'created_at' => '2018-05-21 12:35:18',
                'updated_at' => '2018-05-21 12:35:18',
            ),
            132 => 
            array (
                'id' => 134,
                'nombre' => 'Incluye ',
                'descripcion' => '<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">Boleto aéreo Lima / Buenos Aires /
Lima <o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><span lang="ES" style="text-indent: -18pt; font-size: 12pt; font-family: Symbol;">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang="ES" style="text-indent: -18pt; font-size: 1rem;">Traslados de ingreso y salida en servicio privado</span><br></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">03 Noches de alojamiento </span><span lang="ES" style="font-size:12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">Sistema de alimentación: &nbsp;DESAYUNO</span><span lang="ES" style="font-size:
12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">City Tour por la ciudad</span><span lang="ES" style="font-size:12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">Tarjeta de asistencia por 04 días</span><span lang="ES" style="font-size:12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">Impuestos hoteleros&nbsp;</span><span lang="ES" style="font-size:12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 62,
                'created_at' => '2018-05-21 12:51:21',
                'updated_at' => '2018-05-21 12:51:21',
            ),
            133 => 
            array (
                'id' => 135,
                'nombre' => 'Importante ',
            'descripcion' => '<ul style="font-size: 17px; color: rgb(116, 116, 116);"><li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li><li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li></ul>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 62,
                'created_at' => '2018-05-21 12:51:21',
                'updated_at' => '2018-05-21 12:51:21',
            ),
            134 => 
            array (
                'id' => 136,
                'nombre' => 'Incluye',
                'descripcion' => '<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">Boleto aéreo Lima / Buenos Aires /
Bariloche / Buenos Aires / Lima</span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">Traslados de ingreso y salida en servicio regular</span><span lang="ES" style="font-size:12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">03 Noches de alojamiento </span><span lang="ES" style="font-size:12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">Sistema de alimentación: &nbsp;DESAYUNO</span><span lang="ES" style="font-size:
12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">HD Circuito Chico</span><span lang="ES" style="font-size:12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">Tarjeta de asistencia por 04 días</span><span lang="ES" style="font-size:12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:36.0pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:normal;
mso-list:l0 level1 lfo1;tab-stops:list 0cm;mso-layout-grid-align:none;
text-autospace:none"><!--[if !supportLists]--><span lang="ES" style="font-size:12.0pt;
font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;
mso-no-proof:yes">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span lang="ES">Impuestos hoteleros&nbsp;</span><span lang="ES" style="font-size:12.0pt;mso-bidi-font-family:Arial;mso-no-proof:yes"><o:p></o:p></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 63,
                'created_at' => '2018-05-21 12:56:14',
                'updated_at' => '2018-05-21 12:56:14',
            ),
            135 => 
            array (
                'id' => 137,
                'nombre' => 'Importante',
            'descripcion' => '<ul style="font-size: 17px; color: rgb(116, 116, 116);"><li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li><li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li></ul>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 63,
                'created_at' => '2018-05-21 12:56:15',
                'updated_at' => '2018-05-21 12:56:15',
            ),
            136 => 
            array (
                'id' => 138,
                'nombre' => 'Incluye',
                'descripcion' => '<p class="MsoListParagraphCxSpFirst" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">Boleto Aéreo Lima / Miami / Lima vía Avianca <strong><o:p></o:p></strong></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><span style="text-indent: -18pt; font-size: 11pt; font-family: Symbol;">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="text-indent: -18pt; font-size: 11pt;">Traslado Miami Aeropuerto - Hotel Miami Beach</span><br></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">3 Noches de Alojamiento - Hotel a Elección<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">WiFi Gratuito<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">1 Día de Shopping Sawgrass Mall<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">Cupones de Descuentos<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">Traslado Hotel Miami Beach - Miami Aeropuerto<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpLast" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">Seguro de Asistencia<strong><o:p></o:p></strong></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 64,
                'created_at' => '2018-05-21 13:27:35',
                'updated_at' => '2018-05-21 13:27:35',
            ),
            137 => 
            array (
                'id' => 139,
                'nombre' => 'Importante ',
            'descripcion' => '<ul style="font-size: 17px; color: rgb(116, 116, 116);"><li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li><li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li></ul>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 64,
                'created_at' => '2018-05-21 13:27:35',
                'updated_at' => '2018-05-21 13:27:35',
            ),
            138 => 
            array (
                'id' => 140,
                'nombre' => 'Inculye',
                'descripcion' => '<p class="MsoListParagraphCxSpFirst" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">Boleto Aéreo Lima / El Salvador / Nueva York / El Salvador / Lima vía Avianca
<strong><o:p></o:p></strong></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><span style="text-indent: -18pt; font-size: 11pt; font-family: Symbol;">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="text-indent: -18pt; font-size: 11pt;">Traslado aeropuerto JFK/LGA - Hotel en Manhattan</span><br></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">3 Noches de Alojamiento en hotel San Carlos<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">1 Día de Shopping en Jersey Garden’s Mall<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">Cupones de Descuentos<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpMiddle" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">Traslado Hotel en Manhattan – aeropuerto JFK/LGA<o:p></o:p></span></p>

<p class="MsoListParagraphCxSpLast" style="text-indent:-18.0pt;mso-list:l0 level1 lfo1"><!--[if !supportLists]--><span style="font-size:11.0pt;mso-bidi-font-size:10.0pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><!--[endif]--><span style="font-size:11.0pt;mso-bidi-font-size:
10.0pt">Seguro de Asistencia<strong><o:p></o:p></strong></span></p>',
                'orden' => 1,
                'estado' => 1,
                'package_id' => 65,
                'created_at' => '2018-05-21 15:10:11',
                'updated_at' => '2018-05-21 15:10:11',
            ),
            139 => 
            array (
                'id' => 141,
                'nombre' => 'Importante ',
            'descripcion' => '<ul style="font-size: 17px; color: rgb(116, 116, 116);"><li>En la mayoría de Hoteles que ofrecen acomodación TRIPLE y CUADRUPLE. La habitación consta de 1 ó 2 camas dobles, las cuales serán asignadas de acuerdo a disponibilidad. Consultar siempre por el máximo de personas permitidas por cada tipo de habitación requerida.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Generalmente el Check-in: 15:00 horas- Check-out: 12:00 horas, esta regla los hoteles la cumplen estrictamente.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados incluidos en los programas son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li><li>Discover Mayorista de Turismo solo actúa como intermediario entre los proveedores locales e internacionales y el usuario, es responsable únicamente por la organización de los tours, servicios y boletos aéreos adquiridos. Por lo tanto, el usuario no puede imputarnos responsabilidad alguna por causas que estén fuera de nuestro alcance. No somos responsables de perjuicio o retraso alguno derivado de circunstancias ajenas a nuestro control ya sean causas fortuitas, de fuerza mayor y a cualquier pérdida, daño, accidente o alguna otra irregularidad causada al usuario por hecho de terceros o por la imprudencia del propio usuario afectado. Cualquier reclamo del usuario respecto a los servicios deberá registrarlo directamente con el proveedor en destino Discover Mayorista de Turismo, en condición de intermediario, gestionará su tramitación hasta la respuesta del proveedor, agotando la reconsideración de ser necesario.</li></ul>',
                'orden' => 2,
                'estado' => 1,
                'package_id' => 65,
                'created_at' => '2018-05-21 15:10:11',
                'updated_at' => '2018-05-21 15:10:11',
            ),
        ));
        
        
    }
}
