<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'nombres' => 'William',
                'correo' => 'wromero@webtilia.com',
                'contrasena' => '$2y$10$eDYiwNQKBWXicwXOmN6U1e4JN6IEO0E5cnNlHfJlfedPsOykn6Omy',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));


    }
}
