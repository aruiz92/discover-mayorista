<?php

use Illuminate\Database\Seeder;

class PackageFilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('package_files')->delete();
        
        \DB::table('package_files')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '1.pdf',
                'estado' => 1,
                'package_id' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-05-17 17:30:49',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '2.pdf',
                'estado' => 1,
                'package_id' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '3.pdf',
                'estado' => 1,
                'package_id' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 6,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '6.pdf',
                'estado' => 1,
                'package_id' => 6,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 7,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '7.pdf',
                'estado' => 1,
                'package_id' => 7,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 8,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '8.pdf',
                'estado' => 1,
                'package_id' => 8,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 9,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '9.pdf',
                'estado' => 1,
                'package_id' => 9,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 10,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '10.pdf',
                'estado' => 1,
                'package_id' => 10,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 11,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '11.pdf',
                'estado' => 1,
                'package_id' => 11,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 12,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '12.pdf',
                'estado' => 1,
                'package_id' => 12,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 13,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '13.pdf',
                'estado' => 1,
                'package_id' => 13,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 14,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '14.pdf',
                'estado' => 1,
                'package_id' => 14,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 15,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '15.pdf',
                'estado' => 1,
                'package_id' => 15,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 16,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '16.pdf',
                'estado' => 1,
                'package_id' => 16,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 17,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '17.pdf',
                'estado' => 1,
                'package_id' => 17,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 18,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '18.pdf',
                'estado' => 1,
                'package_id' => 18,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 19,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '19.pdf',
                'estado' => 1,
                'package_id' => 19,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 20,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '20.pdf',
                'estado' => 1,
                'package_id' => 20,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 21,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '21.pdf',
                'estado' => 1,
                'package_id' => 21,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 22,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '22.pdf',
                'estado' => 1,
                'package_id' => 22,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 23,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '23.pdf',
                'estado' => 1,
                'package_id' => 23,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 24,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '24.pdf',
                'estado' => 1,
                'package_id' => 24,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 25,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '25.pdf',
                'estado' => 1,
                'package_id' => 25,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 26,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '26.pdf',
                'estado' => 1,
                'package_id' => 26,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 27,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '27.pdf',
                'estado' => 1,
                'package_id' => 27,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 28,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '28.pdf',
                'estado' => 1,
                'package_id' => 28,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 29,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '29.pdf',
                'estado' => 1,
                'package_id' => 29,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 30,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '30.pdf',
                'estado' => 1,
                'package_id' => 30,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 31,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '31.pdf',
                'estado' => 1,
                'package_id' => 31,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 32,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '32.pdf',
                'estado' => 1,
                'package_id' => 32,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 33,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '33.pdf',
                'estado' => 1,
                'package_id' => 33,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 34,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '34.pdf',
                'estado' => 1,
                'package_id' => 34,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 35,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '35.pdf',
                'estado' => 1,
                'package_id' => 35,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 36,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '36.pdf',
                'estado' => 1,
                'package_id' => 36,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 37,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '37.pdf',
                'estado' => 1,
                'package_id' => 37,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 38,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '38.pdf',
                'estado' => 1,
                'package_id' => 38,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 39,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '39.pdf',
                'estado' => 1,
                'package_id' => 39,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 40,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '40.pdf',
                'estado' => 1,
                'package_id' => 40,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 41,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '41.pdf',
                'estado' => 1,
                'package_id' => 41,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 42,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '42.pdf',
                'estado' => 1,
                'package_id' => 42,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 43,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '43.pdf',
                'estado' => 1,
                'package_id' => 43,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 44,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '44.pdf',
                'estado' => 1,
                'package_id' => 44,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 45,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '45.pdf',
                'estado' => 1,
                'package_id' => 45,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 46,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '46.pdf',
                'estado' => 1,
                'package_id' => 46,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 47,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '47.pdf',
                'estado' => 1,
                'package_id' => 47,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 48,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '48.pdf',
                'estado' => 1,
                'package_id' => 48,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 49,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '49.pdf',
                'estado' => 1,
                'package_id' => 49,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 50,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '50.pdf',
                'estado' => 1,
                'package_id' => 50,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 51,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '51.pdf',
                'estado' => 1,
                'package_id' => 51,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 52,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '52.pdf',
                'estado' => 1,
                'package_id' => 52,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 53,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '53.pdf',
                'estado' => 1,
                'package_id' => 53,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 54,
                'nombre' => '',
                'orden' => 2,
                'archivo' => '7noFwPTDW0cf6Au1mMBV.pdf',
                'estado' => 1,
                'package_id' => 9,
                'created_at' => '2018-05-17 17:37:42',
                'updated_at' => '2018-05-17 17:37:42',
            ),
            52 => 
            array (
                'id' => 55,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '.pdf',
                'estado' => 1,
                'package_id' => 4,
                'created_at' => '2018-05-17 18:24:15',
                'updated_at' => '2018-05-17 18:39:00',
            ),
            53 => 
            array (
                'id' => 56,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 2,
                'archivo' => '.pdf',
                'estado' => 1,
                'package_id' => 5,
                'created_at' => '2018-05-18 09:55:54',
                'updated_at' => '2018-05-18 09:55:54',
            ),
            54 => 
            array (
                'id' => 57,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '57.pdf',
                'estado' => 1,
                'package_id' => 54,
                'created_at' => '2018-05-18 10:05:51',
                'updated_at' => '2018-05-18 10:05:51',
            ),
            55 => 
            array (
                'id' => 58,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '58.pdf',
                'estado' => 1,
                'package_id' => 56,
                'created_at' => '2018-05-18 12:17:32',
                'updated_at' => '2018-05-18 12:17:32',
            ),
            56 => 
            array (
                'id' => 59,
                'nombre' => 'Descargar Informacón Detallada',
                'orden' => 1,
                'archivo' => '59.pdf',
                'estado' => 1,
                'package_id' => 58,
                'created_at' => '2018-05-18 12:21:37',
                'updated_at' => '2018-05-18 12:21:37',
            ),
            57 => 
            array (
                'id' => 60,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '60.pdf',
                'estado' => 1,
                'package_id' => 59,
                'created_at' => '2018-05-21 11:07:41',
                'updated_at' => '2018-05-21 11:07:41',
            ),
            58 => 
            array (
                'id' => 61,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '61.pdf',
                'estado' => 1,
                'package_id' => 60,
                'created_at' => '2018-05-21 12:12:03',
                'updated_at' => '2018-05-21 12:12:03',
            ),
            59 => 
            array (
                'id' => 62,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '62.pdf',
                'estado' => 1,
                'package_id' => 62,
                'created_at' => '2018-05-21 12:51:21',
                'updated_at' => '2018-05-21 12:51:21',
            ),
            60 => 
            array (
                'id' => 63,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '63.pdf',
                'estado' => 1,
                'package_id' => 63,
                'created_at' => '2018-05-21 12:56:15',
                'updated_at' => '2018-05-21 12:56:15',
            ),
            61 => 
            array (
                'id' => 64,
                'nombre' => 'Descargar Información Detallada ',
                'orden' => 1,
                'archivo' => '64.pdf',
                'estado' => 1,
                'package_id' => 64,
                'created_at' => '2018-05-21 13:27:35',
                'updated_at' => '2018-05-21 13:27:35',
            ),
            62 => 
            array (
                'id' => 65,
                'nombre' => 'Descargar Información Detallada',
                'orden' => 1,
                'archivo' => '65.pdf',
                'estado' => 1,
                'package_id' => 65,
                'created_at' => '2018-05-21 15:10:11',
                'updated_at' => '2018-05-21 15:10:11',
            ),
        ));
        
        
    }
}
