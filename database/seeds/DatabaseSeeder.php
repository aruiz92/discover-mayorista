<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('PartnersTableSeeder');
        $this->call('EmployeesTableSeeder');
        $this->call('CompanyTableSeeder');
        $this->call('AboutusTableSeeder');
        $this->call('DestinationsTableSeeder');
        $this->call('DestinationBannersTableSeeder');
        $this->call('PackageTypesTableSeeder');
        $this->call('PackageSubtypesTableSeeder');
        $this->call('PackagesTableSeeder');
        $this->call('PackageTypeBannersTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('CountriesTableSeeder');
        $this->call('ProvincesTableSeeder');
        $this->call('DistrictsTableSeeder');
        $this->call('AgenciesTableSeeder');
        $this->call('AgencyUsersTableSeeder');
        $this->call('PackageFilesTableSeeder');
        $this->call('SlidersTableSeeder');
        $this->call('PackageTabsTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('OffersTableSeeder');
    }
}
