<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteUser extends Model
{
    protected $table = 'quotation_users';

    public function package()
    {
      return $this->belongsTo('App\Package');
    }

    public function agency_user()
    {
      return $this->belongsTo('App\AgencyUser');
    }
}
