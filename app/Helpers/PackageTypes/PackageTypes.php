<?php
namespace App\Helpers\PackageTypes;

use Illuminate\Support\Facades\DB;
use Image;

class PackageTypes {

  public static function wpArticle()
  {
    $articulo = DB::connection('wp')->table('wpdc_posts')
                                                  ->where('post_type', 'post')
                                                  ->where('post_status', 'publish')
                                                  ->orderBy('ID', 'DESC')
                                                  ->first();
    $wpImagen = DB::connection('wp')->table('wpdc_postmeta')
                                          ->where('meta_key', '_thumbnail_id')
                                          ->where('post_id', $articulo->ID)
                                          ->first();
    $wpImagen = DB::connection('wp')->table('wpdc_posts')
                                    ->where('post_type', 'attachment')
                                    ->where('ID', $wpImagen->meta_value)
                                    ->first();
    $img = Image::make($wpImagen->guid);
    $img->fit(150, 150);
    $articulo->imagen = $img->encode('data-url');

    return $articulo;
  }

    public static function parentPackageTypes($destination, $packages) {
      $package_types = [];

      foreach ($packages as $key => $package) {
        $mainPackageType = \App\DetailPackageType::where('main', true)->where('package_id', $package->id)->first();

        if ($mainPackageType) {
          $packageType = \App\PackageType::find($mainPackageType->package_type_id);

          if ($packageType->padre_id !== 0) {
            $parentPackageType = (new self)->parentPackageType($packageType->padre_id);

            if (count($package_types) > 0) {
              if (!(new self)->existsPackageType($package_types, $parentPackageType)) {
                $package_types[] = $parentPackageType;
              }
            } else {
              $package_types[] = $parentPackageType;
            }
          } else {
            if (!(new self)->existsPackageType($package_types, $packageType)) {
              $package_types[] = $packageType;
            }
          }
        }
      }

      return $package_types;
    }


    public static function getForType($idType, $limit, $orderBy, $destacado)
    {
      $packages = DB::table(DB::raw('packages p'))
                    ->join(DB::raw('destinations d'), 'p.destination_id', '=', 'd.id')
                    ->join(DB::raw('package_subtypes ps'), 'p.package_subtype_id', '=', 'ps.id')
                    ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                    ->select('p.*')
                    ->when($idType, function ($query) use ($idType) {
                      return $query->where('pt.id', $idType);
                    })
                    ->when($destacado, function ($query) use ($destacado) {
                      return $query->where('p.destacado', $destacado);
                    })
                    ->where('pt.estado', TRUE)
                    ->where('p.estado', TRUE)
                  //  ->where('p.vigencia', '>=', date('Y-m-d'))
                    ->orderBy('p.orden_destacado', $orderBy)
                    ->limit($limit)
                    ->get();

      return $packages;

    }

    private function existsPackageType($package_types, $parentPackageType)
    {
      $collection = collect($package_types);

      return $collection->contains('id', $parentPackageType->id);
    }

    private function parentPackageType($padre_id)
    {
      $packageType = \App\PackageType::find($padre_id);
      if ($packageType) {
          $this->parentPackageType($packageType->padre_id);
      }

      return $packageType;
    }
}
