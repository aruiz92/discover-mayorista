<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageFile extends Model
{
    protected $table = 'package_files';

    /**
     * Get the package that owns the file.
     */
    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
