<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'packages';

    /**
     * Get the images for the package.
     */
    public function images()
    {
        return $this->hasMany('App\PackageImage');
    }

    /**
     * Get the files for the package post.
     */
    public function files()
    {
        return $this->hasMany('App\PackageFile');
    }

    // HasMany
    public function itineraries()
    {
        return $this->hasMany('App\Itinerary');
    }

    public function quotes()
    {
      return $this->hasMany('App\Quote');
    }

    public function quote_users()
    {
      return $this->hasMany('App\QuoteUser');
    }

    public function quotation_users()
    {
      return $this->hasMany('App\QuotationUser');
    }

    public function package_tabs()
    {
      return $this->hasMany('App\PackageTab');
    }

    public function quotations()
    {
        return $this->hasMany('App\Quotation');
    }

    // BelongsTo
    public function package_type()
    {
      return $this->belongsTo('App\PackageType');
    }

    public function destination()
    {
      return $this->belongsTo('App\Destination');
    }
}
