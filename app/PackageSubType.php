<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageSubType extends Model
{
    protected $table = 'package_subtypes';
}
