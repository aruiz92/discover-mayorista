<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DestinationBanner extends Model
{
    protected $table = 'destination_banners';

    public function destination()
    {
        return $this->belongsTo('App\Destination');
    }
}
