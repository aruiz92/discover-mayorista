<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageType extends Model
{
    protected $table = 'package_types';

    public function packages()
    {
        return $this->hasMany('App\Package');
    }

    public function package_type_banners()
    {
        return $this->hasMany('App\PackageTypeBanner');
    }
}
