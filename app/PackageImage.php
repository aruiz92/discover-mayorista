<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageImage extends Model
{
    protected $table = 'package_images';


    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
