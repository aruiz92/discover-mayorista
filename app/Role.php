<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    public function agency_users()
    {
      return $this->hasMany('App\AgencyUser');
    }
}
