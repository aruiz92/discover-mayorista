<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $table = 'quotations';

    public function agency()
    {
      return $this->belongsTo('App\Agency');
    }

    public function package()
    {
      return $this->belongsTo('App\Package');
    }
}
