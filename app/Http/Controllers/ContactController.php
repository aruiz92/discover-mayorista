<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Discover;

class ContactController extends Controller
{
    private $articulo;

    public function __construct()
    {
      $this->articulo = Discover::wpArticle();
    }

    public function index()
    {
      $company      = \App\Company::find(1);
      $destinations = \App\Destination::where('estado', true)->get();
      $countries    = \App\Country::where('estado', TRUE)->get();

      return view('frontend.contact.index', [
        'destinations' => $destinations,
        'company'      => $company,
        'countries'    => $countries,
        'articulo'     => $this->articulo
      ]);
    }
}
