<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Discover;

class WholesalerController extends Controller
{
    public function index()
    {
      $company      = \App\Company::find(1);
      $destinations = \App\Destination::where('estado', true)->get();

      $articulo = Discover::wpArticle();

      return view('frontend.wholesaler.index', [
        'destinations' => $destinations,
        'company'      => $company,
        'articulo'     => $articulo
      ]);
    }
}
