<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use Route;
use Discover;

class LockController extends Controller
{
    private $articulo;

    public function __construct()
    {
      $this->articulo = Discover::wpArticle();
    }

    public function index()
    {
      $modo = Route::current()->uri();

      if ($modo !== 'bloqueos' && $modo !== 'promociones') {
        return redirect('/');
      }

      $company      = \App\Company::find(1);
      $destinations = \App\Destination::where('estado', true)->get();
      $countries    = \App\Country::where('estado', TRUE)->get();

      if ($modo === 'bloqueos') {
        $packages = \App\Package::join(DB::raw('package_subtypes ps'), 'packages.package_subtype_id', '=', 'ps.id')
                      ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                      ->select('packages.*')
                      ->where('pt.id', 1)
                    //  ->where('packages.vigencia', '>=', date('Y-m-d'))
                      ->where('packages.estado', TRUE)
                      ->orderBy('packages.id', 'desc')
                      ->paginate(2);
      } else {
        $packages = \App\Package::where('promocion', TRUE)
                    //  ->where('vigencia', '>=', date('Y-m-d'))
                      ->where('estado', TRUE)
                      ->orderBy('id', 'desc')
                      ->paginate(2);
      }


      $sliders = \App\Slider::where('seccion', $modo)->where('state', TRUE)->get();

      return view('frontend.lock.index', [
        'modo'         => $modo,
        'destinations' => $destinations,
        'company'      => $company,
        'packages'     => $packages,
        'sliders'      => $sliders,
        'countries'    => $countries,
        'articulo'     => $this->articulo
      ]);
    }

    public function show()
    {
      $company      = \App\Company::find(1);
      $destinations = \App\Destination::where('estado', true)->get();

      return view('frontend.lock.show', [
        'destinations' => $destinations,
        'company'      => $company,
        'articulo'     => $this->articulo
      ]);
    }


    public function search_package(Request $request)
    {

      $listpackages = $request->get('listpackages');
      $fecha_salida = $request->get('fecha_salida');
      $cant_dias    = $request->get('cant_dias');

      if($listpackages == ""){
        return redirect()->route('bloqueos');
      }

      $modo = Route::current()->uri();
      //
      if ($modo !== 'bloqueos' && $modo !== 'promociones'  && $modo !== 'resultado-filtro') {
        return redirect('/');
      }

      $packages = $this->queryallsearch($listpackages,$fecha_salida,$cant_dias);
      if(!count($packages)){
        return "NOHAY";
      }

      $company      = \App\Company::find(1);
      $destinations = \App\Destination::where('estado', true)->get();
      $countries    = \App\Country::where('estado', TRUE)->get();
      $sliders      = \App\Slider::where('seccion', $modo)->where('state', TRUE)->get();


      return view('frontend.lock.show_alones_filter', [
        'modo'           => $modo,
        'destinations'   => $destinations,
        'company'        => $company,
        'packages'       => $packages,
        'sliders'        => $sliders,
        'countries'      => $countries,
        'articulo'       => $this->articulo,
        'listpackages'   => $listpackages,
        'mes'            => $fecha_salida,
        'dias'           => $cant_dias

      ]);

    }

    public function queryallsearch($listpackages,$fecha_salida,$cant_dias){
      if($listpackages != "" && $fecha_salida != "" && $cant_dias != ""){
        $packages = \App\Package::join(DB::raw('package_subtypes ps'), 'packages.package_subtype_id', '=', 'ps.id')
                      ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                      ->where('packages.nombre','like','%'.$listpackages.'%')
                    //  ->whereMonth('packages.fecha_salida','=', $fecha_salida)
                      ->where('nro_dias', $cant_dias)
                      ->select('packages.*')
                    //  ->where('pt.id', 1)
                     ->where('packages.estado', TRUE)
                      ->orderBy('packages.id', 'desc')
                      ->paginate(2);
        return $packages;
      }  else if(!empty($listpackages) && $fecha_salida != ""){
          $packages = \App\Package::join(DB::raw('package_subtypes ps'), 'packages.package_subtype_id', '=', 'ps.id')
                        ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                        ->select('packages.*')
                        ->where('packages.nombre','like','%'.$listpackages.'%')
                      //  ->whereMonth('packages.fecha_salida','=', $fecha_salida)
                      //  ->where('pt.id', 1)
                       ->where('packages.estado', TRUE)
                        ->orderBy('packages.id', 'desc')
                        ->paginate(2);
          return $packages;
        }
        else if(!empty($listpackages) && $cant_dias != ""){
            $packages = \App\Package::join(DB::raw('package_subtypes ps'), 'packages.package_subtype_id', '=', 'ps.id')
                          ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                          ->select('packages.*')
                          ->where('packages.nombre','like','%'.$listpackages.'%')
                          ->where('nro_dias', $cant_dias)
                        //  ->where('pt.id', 1)
                         ->where('packages.estado', TRUE)
                          ->orderBy('packages.id', 'desc')
                          ->paginate(2);
            return $packages;
          }

      else if(!empty($listpackages)){
        $packages = \App\Package::join(DB::raw('package_subtypes ps'), 'packages.package_subtype_id', '=', 'ps.id')
                      ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                      ->select('packages.*')
                      ->where('packages.nombre','like','%'.$listpackages.'%')
                      //->where('pt.id', 1)
                      ->where('packages.estado', TRUE)
                      ->orderBy('packages.id', 'desc')
                      ->paginate(2);
        return $packages;
      }

    }
}
