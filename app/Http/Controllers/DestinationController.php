<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Discover;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $nombre)
    {
        $destination = \App\Destination::where('estado', TRUE)
                                       ->where('id', $id)
                                       ->first();

        if (!$destination) {
            return view('errors.503');
        }

        $articulo = Discover::wpArticle();
        $offer = \App\Offer::where('estado', TRUE)->orderBy('id', 'DESC')->first();
        $company = \App\Company::find(1);
        $destinations = \App\Destination::where('estado', TRUE)->get();

        $destination_banners = \App\DestinationBanner::where('destination_id', $destination->id)->where('estado', TRUE)->orderBy('orden', 'ASC')->get();
        $packageTypes = DB::table(DB::raw('packages p'))
                      ->join(DB::raw('package_subtypes ps'), 'p.package_subtype_id', '=', 'ps.id')
                      ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                      ->select('pt.*')
                      ->where('p.destination_id', $destination->id)
                      ->where('p.vigencia', '>=', date('Y-m-d'))
                      ->where('pt.estado', TRUE)
                      ->groupBy('pt.id')
                      ->get();

        foreach ($packageTypes as $key => $packageType) {
            $packageTypes[$key]->packages = DB::table(DB::raw('packages p'))
                          ->join(DB::raw('package_subtypes ps'), 'p.package_subtype_id', '=', 'ps.id')
                          ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                          ->select('p.*', 'pt.nombre as nombre_pt', 'pt.id as id_pt')
                          ->where('p.destination_id', $destination->id)
                          ->where('pt.id', $packageType->id)
                          ->where('p.vigencia', '>=', date('Y-m-d'))
                          ->where('p.estado', TRUE)
                          ->orderBy('p.id', 'DESC')
                          ->get();
        }

        return view('frontend.destination.index', [
            'company'             => $company,
            'destinations'        => $destinations,
            'destination'         => $destination,
            'destination_banners' => $destination_banners,
            'package_types'       => $packageTypes,
            'articulo'            => $articulo,
            'offer'               => $offer
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
