<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Discover;

class PackageTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idD = NULL, $nameD = NULL, $idT = NULL, $nameT = NULL, $idST = NULL, $nameST = NULL)
    {
        $destination  = \App\Destination::where('id', $idD)->where('estado', true)->first();
        $destinations = \App\Destination::where('estado', TRUE)->get();
        $packageType  = \App\PackageType::find($idT);

        if (!$destination || !$packageType) {
          return view('errors.503');
        }

        $limit = 4;

        $countries = \App\Country::where('estado', TRUE)->get();
        $company   = \App\Company::find(1);
        $subTypes  = DB::table(DB::raw('packages p'))
                       ->join(DB::raw('package_subtypes ps'), 'p.package_subtype_id', '=', 'ps.id')
                       ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                       ->select('ps.*')
                       ->where('p.destination_id', $destination->id)
                       ->where('ps.package_type_id', $packageType->id)
                       ->where('p.vigencia', '>=', date('Y-m-d'))
                       ->where('p.estado', TRUE)
                       ->where('pt.estado', TRUE)
                       ->groupBy('ps.id')
                       ->get();

        $packageTypes = DB::table(DB::raw('package_types pt'))
                          ->join(DB::raw('package_subtypes ps'), 'ps.package_type_id', '=', 'pt.id')
                          ->join(DB::raw('packages p'), 'p.package_subtype_id', '=', 'ps.id')
                          ->select('pt.*')
                          ->where('p.destination_id', $destination->id)
                          ->where('p.vigencia', '>=', date('Y-m-d'))
                          ->where('pt.estado', TRUE)
                          ->groupBy('pt.id')
                          ->get();

        $packageSubType     = \App\PackageSubType::find($idST);
        $packageTypeBanners = \App\PackageTypeBanner::where('package_type_id', $packageType->id)
                                                    ->where('destination_id', $destination->id)
                                                    ->where('estado', true)
                                                    ->orderBy('orden', 'ASC')
                                                    ->get();

        if (!$packageSubType) {
          $packages = DB::table(DB::raw('packages p'))
                        ->join(DB::raw('package_subtypes ps'), 'p.package_subtype_id', '=', 'ps.id')
                        ->select('p.*')
                        ->where('p.destination_id', $destination->id)
                        ->where('ps.id', $subTypes[0]->id)
                        ->where('p.vigencia', '>=', date('Y-m-d'))
                        ->where('p.estado', TRUE)
                        ->orderBy('p.id', 'DESC')
                        ->limit($limit)
                        ->get();
          $packageSubType = $subTypes[0];
        } else {
          $packages = DB::table(DB::raw('packages p'))
                        ->join(DB::raw('package_subtypes ps'), 'p.package_subtype_id', '=', 'ps.id')
                        ->select('p.*')
                        ->where('p.destination_id', $destination->id)
                        ->where('ps.id', $packageSubType->id)
                        ->where('p.vigencia', '>=', date('Y-m-d'))
                        ->where('p.estado', TRUE)
                        ->orderBy('p.id', 'DESC')
                        ->limit($limit)
                        ->get();
        }

        $articulo = Discover::wpArticle();

        return view('frontend.package_type.show', [
           'company'              => $company,
           'destinations'         => $destinations,
           'destination'          => $destination,
           'subTypes'             => $subTypes,
           'packageTypes'         => $packageTypes,
           'package_type'         => $packageType,
           'package_type_child'   => $packageSubType,
           'packages'             => $packages,
           'package_type_banners' => $packageTypeBanners,
           'limit'                => $limit,
           'countries'            => $countries,
           'articulo'             => $articulo
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
