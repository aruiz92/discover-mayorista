<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Discover;
use DB;
use Image;

class HomeController extends Controller
{
  public function index()
  {
    $destinations = \App\Destination::where('estado', true)->get();
    
    if (!$destinations) {
      return view('errors.503');
    }

    // dd(date('Y-m-d'));

    $sliders                = \App\Slider::where('seccion', 'home')->where('state', TRUE)->get();
    $company                = \App\Company::find(1);
    $international_packages = \App\Package::where('destination_id', 1)
                                          ->where('estado', TRUE)
                                        //  ->where('vigencia', '>=', date('Y-m-d'))
                                          ->orderBy('id', 'desc')
                                          ->limit(12)
                                          ->get();

    $national_packages      = \App\Package::where('destination_id', 2)
                                          ->where('estado', TRUE)
                                        //  ->where('vigencia', '>=', date('Y-m-d'))
                                          ->orderBy('id', 'desc')
                                          ->limit(12)
                                          ->get();

    $locks = Discover::getForType(1, 12, 'desc', TRUE); // typeID, Limit, orberBY, Destacado
    $holidays = Discover::getForType(NULL, 12, 'desc', TRUE); // typeID, Limit, orberBY, Destacado

    $articulo = Discover::wpArticle();

    return view('frontend.home.index', [
      'company'                => $company,
      'destinations'           => $destinations,
      'international_packages' => $international_packages,
      'national_packages'      => $national_packages,
      'locks'                  => $locks,
      'holidays'               => $holidays,
      'sliders'                => $sliders,
      'articulo'               => $articulo
    ]);
  }
}
