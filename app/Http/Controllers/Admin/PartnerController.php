<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StorePartnerRequest;
use App\Http\Requests\UpdatePartnerRequest;
use App\Http\Controllers\Controller;

use App\Partner;
use Storage;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::get();

        return view('admin.partner.index', [
            'active'   => 'socios',
            'partners' => $partners
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partner.create', [
            'active' => 'socios'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePartnerRequest $request)
    {
        $partner = new Partner;
        $partner->nombre = $request->input('nombre');
        $partner->orden  = $this->_cleanRequest($request->input('orden'), 1);
        $partner->save();

        $partner->imagen = $this->_moveFilePartner($request->input('imagen'), $partner);
        $partner->save();

        return response()->json($partner, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $partner = Partner::find($id);

      return view('admin.partner.edit', [
          'active'  => 'socios',
          'partner' => $partner
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePartnerRequest $request, $id)
    {
      $partner = Partner::find($id);
      $partner->nombre = $request->input('nombre');
      $partner->orden  = $this->_cleanRequest($request->input('orden'), 1);
      $partner->save();

      if ($request->input('imagen') !== $partner->imagen) {
        $partner->imagen = $this->_moveFilePartner($request->input('imagen'), $partner);
        $partner->save();
      }

      return response()->json($partner, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Partner::find($id);
        $partner->delete();

        return response()->json($partner, 201);
    }

    // Funciones Ajax
    public function ajaxFilePartner(Request $request)
    {
      $file = $request->file('file');
      $path = $file->getRealPath();
      $ext  = $file->guessExtension();
      $newName = str_random(20).'.'.$ext;

      $exists = Storage::disk('public')->exists('temporal/'.$newName);

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$newName);
      }

      Storage::disk('public')->put('temporal/'.$newName, file_get_contents($path));

      return response()->json(array('fileName' => $newName), 201);
    }

    // Funciones Privadas
    private function _moveFilePartner($fileTem, $package)
    {
      $exists  = Storage::disk('public')->exists('temporal/'.$fileTem);
      $path    = Storage::disk('public')->get('temporal/'.$fileTem);
      $ext     = pathinfo(storage_path('/app/public/temporal/'.$fileTem), PATHINFO_EXTENSION);
      $newName = $package->id.'.'.$ext;

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$fileTem);
      }

      Storage::disk('discover')->put('partners/'.$newName, $path);

      return $newName;
    }

    private function _cleanRequest($request, $default)
    {
      $response = trim($request);
      $response = empty($response) ? $default: $response;

      return $response;
    }
}
