<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Admin\UpdateAgencyRequest;
use App\Http\Requests\Admin\StoreAgencyRequest;
use App\Http\Controllers\Controller;

use App\Agency;
use App\AgencyUser;
use App\Country;
use App\Province;
use App\District;
use App\Role;

use Storage;
use DB;
use Mail;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agencies = Agency::where('estado', TRUE)->orWhere('estado', FALSE)->get();

        return view('admin.agency.index', [
            'active'  => 'agencies',
            'agencies' => $agencies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::get();
        $roles = Role::where('estado', TRUE)->get();

        return view('admin.agency.create', [
            'active'    => 'agencies',
            'countries' => $countries,
            'roles'     => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgencyRequest $request)
    {
        $agency = new Agency;
        $agency->usuario          = $request->input('usuario');
        $agency->correo           = $request->input('correo');
        $agency->ruc              = $request->input('ruc');
        $agency->razon_social     = $request->input('razon_social');
        $agency->nombre_comercial = $request->input('nombre_comercial');
        $agency->nombres          = $request->input('nombres');
        $agency->telefono         = $request->input('telefono');
        $agency->celular          = $this->_cleanRequest($request->input('celular'), NULL);
        $agency->direccion_1      = $request->input('direccion_1');
        $agency->direccion_2      = $this->_cleanRequest($request->input('direccion_2'), NULL);
        $agency->district_id      = $request->input('distrito');
        $agency->save();

        if ($request->input('logo') !== $agency->logo) {
            $agency->logo = $this->_moveFilePackage($request->input('logo'), $agency);
            $agency->save();
        }

        $contrasena = str_random(10);

        $user = new AgencyUser;
        $user->nombre     = $agency->nombres;
        $user->correo     = $agency->correo;
        $user->contrasena = bcrypt($contrasena);
        $user->agency_id  = $agency->id;
        $user->role_id    = 1;
        $user->save();

        $this->_correoWelcome($user, $contrasena);

        return response()->json($agency, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agency = Agency::join(DB::raw('districts d'), 'agencies.district_id', '=', 'd.id')
                        ->join(DB::raw('provinces p'), 'd.province_id', '=', 'p.id')
                        ->join(DB::raw('countries c'), 'p.country_id', '=', 'c.id')
                        ->select('agencies.*', 'p.id as p_id', 'c.id as c_id')
                        ->where('agencies.id', $id)
                        ->first();

        $countries = Country::where('estado', TRUE)->get();
        $provinces = Province::where('country_id', $agency->c_id)->where('estado', TRUE)->get();
        $districts = District::where('province_id', $agency->p_id)->where('estado', TRUE)->get();
        $agencyUsers = AgencyUser::where('agency_id', $agency->id)->where('estado', TRUE)->get();
        $roles = Role::where('estado', TRUE)->get();

        $agency->size = Storage::disk('discover')->size('logos/'.$agency->logo);
        $agency->type = Storage::disk('discover')->mimeType('logos/'.$agency->logo);

        return view('admin.agency.edit', [
            'active'    => 'agencies',
            'agency'    => $agency,
            'countries' => $countries,
            'provinces' => $provinces,
            'districts' => $districts,
            'agencyUsers' => $agencyUsers,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAgencyRequest $request, $id)
    {
        $agency = Agency::find($id);
        $agency->usuario          = $request->input('usuario');
        $agency->correo           = $request->input('correo');
        $agency->ruc              = $request->input('ruc');
        $agency->razon_social     = $request->input('razon_social');
        $agency->nombre_comercial = $request->input('nombre_comercial');
        $agency->nombres          = $request->input('nombres');
        $agency->telefono         = $request->input('telefono');
        $agency->celular          = $this->_cleanRequest($request->input('celular'), NULL);
        $agency->direccion_1      = $request->input('direccion_1');
        $agency->direccion_2      = $this->_cleanRequest($request->input('direccion_2'), NULL);
        $agency->district_id      = $request->input('distrito');
        $agency->estado           = $request->input('estado');
        $agency->save();

        if ($request->input('logo') !== $agency->logo) {
            $agency->logo = $this->_moveFilePackage($request->input('logo'), $agency);
            $agency->save();
        }

        return response()->json($agency, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agency = Agency::find($id);
        $agency->estado = 3;
        $agency->save();

        return response()->json($agency, 201);
    }

    // Funciones ajax
    public function ajaxActivar($id)
    {
        $agency = Agency::find($id);
        $agency->estado = TRUE;
        $agency->save();

        $contrasena = str_random(10);

        $user = AgencyUser::where('agency_id', $agency->id)->first();
        $user->contrasena = bcrypt($contrasena);
        $user->estado = TRUE;
        $user->save();

        $this->_correoWelcome($user, $contrasena);


        return response()->json($agency, 201);
    }

    // Funciones Privadas
    private function _moveFilePackage($fileTem, $package)
    {
      $exists  = Storage::disk('public')->exists('temporal/'.$fileTem);
      $path    = Storage::disk('public')->get('temporal/'.$fileTem);
      $ext     = pathinfo(storage_path('/app/public/temporal/'.$fileTem), PATHINFO_EXTENSION);
      $newName = $package->id.'.'.$ext;

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$fileTem);
      }

      Storage::disk('discover')->put('logos/'.$newName, $path);

      return $newName;
    }

    private function _cleanRequest($request, $default)
    {
      $response = trim($request);
      $response = empty($response) ? $default: $response;

      return $response;
    }

    private function _correoWelcome($user, $contrasena)
    {
        Mail::send('emails.welcome', ['user' => $user, 'contrasena' => $contrasena], function ($m) use ($user) {
            $m->from('no-responder@discovermayorista.com', 'No Responder');
            $m->to($user->correo, $user->nombre)->subject('Bienvenido! Hemos activado tu cuenta');
        });
    }
}
