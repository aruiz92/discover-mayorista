<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\QuotationUser;

use DB;

class QuotationUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotation_users = QuotationUser::join(DB::raw('agency_users au'), 'quotation_users.agency_user_id', '=', 'au.id')
                                        ->join(DB::raw('agencies a'), 'au.agency_id', '=', 'a.id')
                                        ->select('quotation_users.*', 'au.nombre as au_nombre', 'au.correo as au_correo', 'a.nombre_comercial')
                                        ->get();

        return view('admin.quotation_user.index', [
            'active'          => 'quotation_users',
            'quotation_users' => $quotation_users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quotation_user = QuotationUser::find($id);

        return view('admin.quotation_user.show', [
            'active'         => 'quotation_users',
            'quotation_user' => $quotation_user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quotation_user = QuotationUser::find($id);
        $quotation_user->delete();

        return response()->json($quotation_user, 201);
    }
}
