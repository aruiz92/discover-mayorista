<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreDestinationRequest;
use App\Http\Controllers\Controller;

use App\Destination;
use App\DestinationBanner;

use DB;
use Storage;
use Image;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $destinations = Destination::get();

        return view('admin.destination.index', [
            'active'       => 'destinations',
            'destinations' => $destinations
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $destinations = Destination::get();

        return view('admin.destination.create', [
            'active'       => 'destinations',
            'destinations' => $destinations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDestinationRequest $request)
    {
        $destination = new Destination;
        $destination->nombre      = $request->input('nombre');
        $destination->descripcion = $this->_cleanRequest($request->input('descripcion'), NULL);
        $destination->save();

        $banners = (object) json_decode($request->input('sliders'));

        foreach ($banners as $banner) {
            $destinationBanner = new DestinationBanner;

            $destinationBanner->titulo         = $this->_cleanRequest($banner->titulo, NULL);
            $destinationBanner->subtitulo      = $this->_cleanRequest($banner->subtitulo, NULL);
            $destinationBanner->descripcion    = $this->_cleanRequest($banner->descripcion, NULL);
            $destinationBanner->orden          = $this->_cleanRequest($banner->orden, 1);
            $destinationBanner->destination_id = $destination->id;
            $destinationBanner->save();

            $destinationBanner->imagen = $this->_moveFilePackage($banner->imagen, $destinationBanner);
            $destinationBanner->save();
        }

        return response()->json($destination, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $destination = Destination::find($id);
        $destination_banners = DestinationBanner::join(DB::raw('destinations d'), 'destination_banners.destination_id', '=', 'd.id')
                                                ->select('destination_banners.*', 'd.nombre as d_nombre', 'd.id as d_id')
                                                ->where('destination_banners.destination_id', $id)
                                                ->get();

        foreach ($destination_banners as $key => $destination_banner) {
            $destination_banners[$key]->size = Storage::disk('discover')->size('destination_banners/'.$destination_banner->imagen);
            $destination_banners[$key]->type = Storage::disk('discover')->mimeType('destination_banners/'.$destination_banner->imagen);
        }

        return view('admin.destination.edit', [
            'active'      => 'destinations',
            'destination' => $destination,
            'destination_banners' => $destination_banners
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDestinationRequest $request, $id)
    {
        $destination = Destination::find($id);
        $destination->nombre      = $request->input('nombre');
        $destination->descripcion = $this->_cleanRequest($request->input('descripcion'), NULL);
        $destination->save();

        $banners = (object) json_decode($request->input('sliders'));

        foreach ($banners as $banner) {
            if ($banner->modo === 'editar' || $banner->modo === 'eliminar') {
                if (!is_null($banner->idd)) {
                    $destinationBanner = DestinationBanner::find($banner->idd);
                }
            } else {
                $destinationBanner = new DestinationBanner;
            }

            if ($banner->modo === 'eliminar') {
                if (!is_null($banner->idd)) {
                    $destinationBanner->delete();
                }
            } else {
                $destinationBanner->titulo         = $this->_cleanRequest($banner->titulo, NULL);
                $destinationBanner->subtitulo      = $this->_cleanRequest($banner->subtitulo, NULL);
                $destinationBanner->descripcion    = $this->_cleanRequest($banner->descripcion, NULL);
                $destinationBanner->orden          = $this->_cleanRequest($banner->orden, 1);
                $destinationBanner->destination_id = $destination->id;
                $destinationBanner->save();

                if ($banner->imagen !== $destinationBanner->imagen) {
                    $destinationBanner->imagen = $this->_moveFilePackage($banner->imagen, $destinationBanner);
                    $destinationBanner->save();
                }
            }
        }

        return response()->json($destination, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destination = Destination::find($id);
        $destination->delete();

        return response()->json($destination, 201);
    }

    // Funciones Privadas
    private function _moveFilePackage($fileTem, $package)
    {
      $exists  = Storage::disk('public')->exists('temporal/'.$fileTem);
      $path    = Storage::disk('public')->get('temporal/'.$fileTem);
      $ext     = pathinfo(storage_path('/app/public/temporal/'.$fileTem), PATHINFO_EXTENSION);
      $newName = $package->id.'.'.$ext;

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$fileTem);
      }

      Storage::disk('discover')->put('destination_banners/'.$newName, $path);

      // open an image file
      $img = Image::make($path);
      // now you are able to resize the instance
      $img->fit(180, 110);
      $path_thumbs = public_path('files/destination_banners/thumbs/'.$newName);
      // finally we save the image as a new file
      $img->save($path_thumbs);

      return $newName;
    }

    private function _cleanRequest($request, $default)
    {
      $response = trim($request);
      $response = empty($response) ? $default: $response;

      return $response;
    }
}
