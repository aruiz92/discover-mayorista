<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Admin\UpdateOfferRequest;
use App\Http\Controllers\Controller;

use App\Offer;
use Storage;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offer = Offer::find(1);

        return view('admin.offer.index', [
          'active' => 'offers',
          'offer'  => $offer
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOfferRequest $request, $id)
    {
        $offer = Offer::find($id);
        $offer->titulo     = $request->input('titulo');
        $offer->precio_min = $request->input('precio_min');
        $offer->url_accion = $request->input('url_accion');
        $offer->save();

        if ($offer->imagen_bg !== $request->input('imagen_bg')) {
            $offer->imagen_bg = $this->_moveFile($request->input('imagen_bg'), $offer);
            $offer->save();
        }

        return response()->json($offer, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Funciones Privadas
    private function _moveFile($file, $item)
    {
      $exists  = Storage::disk('public')->exists('temporal/'.$file);
      $path    = Storage::disk('public')->get('temporal/'.$file);
      $ext     = pathinfo(storage_path('/app/public/temporal/'.$file), PATHINFO_EXTENSION);
      $newName = $item->id.'.'.$ext;

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$file);
      }

      Storage::disk('discover')->put('offers/'.$newName, $path);

      return $newName;
    }

    private function _cleanRequest($request, $default)
    {
      $response = trim($request);
      $response = empty($response) ? $default: $response;

      return $response;
    }
}
