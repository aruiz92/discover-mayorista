<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Storage;
use App\Http\Requests;
use App\Http\Requests\StoreSliderRequest;
use App\Http\Controllers\Controller;

use App\Slider;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sliders = Slider::get();

      return view('admin.slider.index', [
          'active'  => 'sliders',
          'sliders' => $sliders
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create', [
          'active' => 'sliders'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSliderRequest $request)
    {
        $slider = new Slider;
        $slider->texto_1 = $this->_cleanRequest($request->input('texto_1'), NULL);
        $slider->texto_2 = $request->input('texto_2');
        $slider->texto_3 = $this->_cleanRequest($request->input('texto_3'), NULL);
        $slider->url     = $this->_cleanRequest($request->input('url'), '#');
        $slider->orden   = $this->_cleanRequest($request->input('orden'), 1);
        $slider->imagen  = $request->input('imagen');
        $slider->seccion = $request->input('seccion');
        $slider->save();

        $slider->imagen = $this->_moveFile($request->input('imagen'), $slider);
        $slider->save();

        return response()->json($slider, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $slider = Slider::find($id);

      return view('admin.slider.edit', [
        'active' => 'sliders',
        'slider' => $slider
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSliderRequest $request, $id)
    {
      $slider = Slider::find($id);
      $slider->texto_1 = $this->_cleanRequest($request->input('texto_1'), NULL);
      $slider->texto_2 = $request->input('texto_2');
      $slider->texto_3 = $this->_cleanRequest($request->input('texto_3'), NULL);
      $slider->url     = $this->_cleanRequest($request->input('url'), '#');
      $slider->orden   = $this->_cleanRequest($request->input('orden'), 1);
      $slider->seccion = $request->input('seccion');
      $slider->save();

      if ($slider->imagen !== $request->input('imagen')) {
        $slider->imagen = $this->_moveFile($request->input('imagen'), $slider);
        $slider->save();
      }

      return response()->json($slider, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $slider = Slider::find($id);
      $slider->delete();

      $exists = Storage::disk('discover')->exists('sliders/'.$slider->imagen);

      if ($exists) {
        Storage::disk('discover')->delete('sliders/'.$slider->imagen);
      }

      return response()->json($slider, 201);
    }

    // Funciones Privadas
    private function _moveFile($file, $item)
    {
      $exists  = Storage::disk('public')->exists('temporal/'.$file);
      $path    = Storage::disk('public')->get('temporal/'.$file);
      $ext     = pathinfo(storage_path('/app/public/temporal/'.$file), PATHINFO_EXTENSION);
      $newName = $item->id.'.'.$ext;

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$file);
      }

      Storage::disk('discover')->put('sliders/'.$newName, $path);

      return $newName;
    }

    private function _cleanRequest($request, $default)
    {
      $response = trim($request);
      $response = empty($response) ? $default: $response;

      return $response;
    }
}
