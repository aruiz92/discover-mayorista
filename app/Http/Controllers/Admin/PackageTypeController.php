<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StorePackageTypeRequest;
use App\Http\Controllers\Controller;

use App\PackageType;
use App\PackageTypeBanner;
use App\Destination;

use DB;
use Storage;

class PackageTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package_types = PackageType::get();

        return view('admin.package_type.index', [
            'active'        => 'package_types',
            'package_types' => $package_types
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $destinations = Destination::get();

        return view('admin.package_type.create', [
            'active'       => 'package_types',
            'destinations' => $destinations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackageTypeRequest $request)
    {
        $packageType = new PackageType;
        $packageType->nombre = $request->input('nombre');
        $packageType->orden  = $this->_cleanRequest($request->input('orden'), 1);
        $packageType->save();

        $banners = (object) json_decode($request->input('sliders'));

        foreach ($banners as $banner) {
            $packageTypeBanner = new PackageTypeBanner;

            $packageTypeBanner->titulo          = $this->_cleanRequest($banner->titulo, NULL);
            $packageTypeBanner->subtitulo       = $this->_cleanRequest($banner->subtitulo, NULL);
            $packageTypeBanner->descripcion     = $this->_cleanRequest($banner->descripcion, NULL);
            $packageTypeBanner->orden           = $this->_cleanRequest($banner->orden, 1);
            $packageTypeBanner->destination_id  = $banner->d_id;
            $packageTypeBanner->package_type_id = $packageType->id;
            $packageTypeBanner->save();

            $packageTypeBanner->imagen = $this->_moveFilePackage($banner->imagen, $packageTypeBanner);
            $packageTypeBanner->save();
        }

        return response()->json($packageType, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package_type = PackageType::find($id);
        $destinations = Destination::get();
        $package_type_banners = PackageTypeBanner::join(DB::raw('destinations d'), 'package_type_banners.destination_id', '=', 'd.id')
                                                 ->select('package_type_banners.*', 'd.nombre as d_nombre', 'd.id as d_id')
                                                 ->where('package_type_banners.package_type_id', $id)
                                                 ->get();

        foreach ($package_type_banners as $key => $package_type_banner) {
            $package_type_banners[$key]->size = Storage::disk('discover')->size('package_type_banners/'.$package_type_banner->imagen);
            $package_type_banners[$key]->type = Storage::disk('discover')->mimeType('package_type_banners/'.$package_type_banner->imagen);
        }

        return view('admin.package_type.edit', [
            'active'        => 'package_types',
            'destinations'  => $destinations,
            'package_type'  => $package_type,
            'package_type_banners' => $package_type_banners
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePackageTypeRequest $request, $id)
    {
        $packageType = PackageType::find($id);
        $packageType->nombre = $request->input('nombre');
        $packageType->orden  = $this->_cleanRequest($request->input('orden'), 1);
        $packageType->save();

        $banners = (object) json_decode($request->input('sliders'));

        foreach ($banners as $banner) {
            if ($banner->modo === 'editar' || $banner->modo === 'eliminar') {
                if (!is_null($banner->idd)) {
                    $packageTypeBanner = PackageTypeBanner::find($banner->idd);
                }
            } else {
                $packageTypeBanner = new PackageTypeBanner;
            }

            if ($banner->modo === 'eliminar') {
                if (!is_null($banner->idd)) {
                    $packageTypeBanner->delete();
                }
            } else {
                $packageTypeBanner->titulo          = $this->_cleanRequest($banner->titulo, NULL);
                $packageTypeBanner->subtitulo       = $this->_cleanRequest($banner->subtitulo, NULL);
                $packageTypeBanner->descripcion     = $this->_cleanRequest($banner->descripcion, NULL);
                $packageTypeBanner->orden           = $this->_cleanRequest($banner->orden, 1);
                $packageTypeBanner->destination_id  = $banner->d_id;
                $packageTypeBanner->package_type_id = $packageType->id;
                $packageTypeBanner->save();

                if ($banner->imagen !== $packageTypeBanner->imagen) {
                    $packageTypeBanner->imagen = $this->_moveFilePackage($banner->imagen, $packageTypeBanner);
                    $packageTypeBanner->save();
                }
            }
        }

        return response()->json($packageType, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $packageType = PackageType::find($id);
        $packageType->delete();

        return response()->json($packageType, 201);
    }

    // Funciones Privadas
    private function _moveFilePackage($fileTem, $package)
    {
      $exists  = Storage::disk('public')->exists('temporal/'.$fileTem);
      $path    = Storage::disk('public')->get('temporal/'.$fileTem);
      $ext     = pathinfo(storage_path('/app/public/temporal/'.$fileTem), PATHINFO_EXTENSION);
      $newName = $package->id.'.'.$ext;

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$fileTem);
      }

      Storage::disk('discover')->put('package_type_banners/'.$newName, $path);

      return $newName;
    }

    private function _cleanRequest($request, $default)
    {
      $response = trim($request);
      $response = empty($response) ? $default: $response;

      return $response;
    }
}
