<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StorePackageSubTypeRequest;
use App\Http\Controllers\Controller;

use App\PackageType;
use App\PackageSubType;
use App\PackageTypeBanner;
use App\Destination;

use DB;
use Storage;

class PackageSubTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package_subtypes = PackageSubType::join(DB::raw('package_types pt'), 'package_subtypes.package_type_id', '=', 'pt.id')
                                          ->select('package_subtypes.*', 'pt.nombre as pt_nombre')
                                          ->get();

        return view('admin.package_subtype.index', [
            'active'           => 'package_subtypes',
            'package_subtypes' => $package_subtypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $package_types = PackageType::get();

        return view('admin.package_subtype.create', [
            'active'        => 'package_subtypes',
            'package_types' => $package_types
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackageSubTypeRequest $request)
    {
        $packageSubType = new PackageSubType;
        $packageSubType->nombre = $request->input('nombre');
        $packageSubType->orden  = $this->_cleanRequest($request->input('orden'), 1);
        $packageSubType->package_type_id = $request->input('categoria');
        $packageSubType->save();

        return response()->json($packageSubType, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package_subtype = PackageSubType::find($id);
        $package_types   = PackageType::get();

        return view('admin.package_subtype.edit', [
            'active'          => 'package_subtypes',
            'package_types'   => $package_types,
            'package_subtype' => $package_subtype
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePackageSubTypeRequest $request, $id)
    {
        $packageSubType = PackageSubType::find($id);
        $packageSubType->nombre = $request->input('nombre');
        $packageSubType->orden  = $this->_cleanRequest($request->input('orden'), 1);
        $packageSubType->package_type_id = $request->input('categoria');
        $packageSubType->save();

        return response()->json($packageSubType, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $packageSubType = PackageSubType::find($id);
        $packageSubType->delete();

        return response()->json($packageSubType, 201);
    }

    public function ajaxSubTypes($pt_id)
    {
        $subtypes = PackageSubType::where('package_type_id', $pt_id)->get();

        return response()->json($subtypes, 200);
    }

    // Funciones privadas
    private function _cleanRequest($request, $default)
    {
      $response = trim($request);
      $response = empty($response) ? $default: $response;

      return $response;
    }
}
