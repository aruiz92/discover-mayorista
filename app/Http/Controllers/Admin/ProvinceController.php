<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreProvinceRequest;
use App\Http\Controllers\Controller;

use App\Province;
use App\Country;

use DB;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::join(DB::raw('countries c'), 'provinces.country_id', '=', 'c.id')
                             ->select('provinces.*', 'c.nombre as pais')
                             ->get();

        return view('admin.province.index', [
            'active'    => 'provinces',
            'provinces' => $provinces
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::get();

        return view('admin.province.create', [
            'active'    => 'provinces',
            'countries' => $countries
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProvinceRequest $request)
    {
        $province = new Province;
        $province->nombre     = $request->input('nombre');
        $province->country_id = $request->input('pais');
        $province->save();

        return response()->json($province, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $province  = Province::find($id);
        $countries = Country::get();

        return view('admin.province.edit', [
            'active'    => 'provinces',
            'province'  => $province,
            'countries' => $countries
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProvinceRequest $request, $id)
    {
        $province = Province::find($id);
        $province->nombre     = $request->input('nombre');
        $province->country_id = $request->input('pais');
        $province->save();

        return response()->json($province, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $province = Province::find($id);
        $province->delete();

        return response()->json($province, 201);
    }

    public function ajaxProvinces($country_id)
    {
        $provinces = Province::where('country_id', $country_id)->get();

        return response()->json($provinces, 200);
    }
}
