<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreDistrictRequest;
use App\Http\Controllers\Controller;

use App\District;
use App\Province;
use App\Country;

use DB;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $districts = District::join(DB::raw('provinces p'), 'districts.province_id', '=', 'p.id')
                             ->join(DB::raw('countries c'), 'p.country_id', '=', 'c.id')
                             ->select('districts.*', 'p.nombre as provincia', 'c.nombre as pais')
                             ->get();

        return view('admin.district.index', [
            'active'    => 'districts',
            'districts' => $districts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::get();

        return view('admin.district.create', [
            'active'    => 'districts',
            'countries' => $countries
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDistrictRequest $request)
    {
        $district = new District;
        $district->nombre      = $request->input('nombre');
        $district->province_id = $request->input('provincia');
        $district->save();

        return response()->json($district, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $district = District::join(DB::raw('provinces p'), 'districts.province_id', '=', 'p.id')
                             ->join(DB::raw('countries c'), 'p.country_id', '=', 'c.id')
                             ->select('districts.*', 'p.nombre as provincia', 'c.nombre as pais', 'c.id as c_id')
                             ->where('districts.id', $id)
                             ->first();
        $countries = Country::get();
        $provinces = Province::where('country_id', $district->c_id)->get();

        return view('admin.district.edit', [
            'active'    => 'districts',
            'district'  => $district,
            'countries' => $countries,
            'provinces' => $provinces
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDistrictRequest $request, $id)
    {
        $district = District::find($id);
        $district->nombre      = $request->input('nombre');
        $district->province_id = $request->input('provincia');
        $district->save();

        return response()->json($district, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $district = District::find($id);
        $district->delete();

        return response()->json($district, 201);
    }

    public function ajaxDistricts($district_id)
    {
        $districts = District::where('province_id', $district_id)->get();

        return response()->json($districts, 200);
    }
}
