<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Storage;
use App\Http\Requests;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Http\Requests\AjaxFotoRequest;
use App\Http\Controllers\Controller;

use App\Employee;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::orderBy('orden', 'asc')->get();

        return view('admin.employee.index', [
            'active'    => 'colaboradores',
            'employees' => $employees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.employee.create', [
            'active' => 'colaboradores'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployeeRequest $request)
    {
        $employee = new Employee;
        $employee->nombres   = $request->input('nombres');
        $employee->apellidos = $request->input('apellidos');
        $employee->correo    = $request->input('correo');
        $employee->telefono  = $this->_cleanRequest($request->input('telefono'), NULL);
        $employee->skype     = $this->_cleanRequest($request->input('skype'), NULL);
        $employee->cargo     = $request->input('cargo');
        $employee->orden     = $this->_cleanRequest($request->input('orden'), 1);
        $employee->save();

        $employee->foto = $this->_moveFile($request->input('foto'), $employee);
        $employee->save();

        return response()->json($employee, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        return view('admin.employee.edit', [
            'active'   => 'colaboradores',
            'employee' => $employee
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEmployeeRequest $request, $id)
    {
        $employee = Employee::find($id);
        $employee->nombres   = $request->input('nombres');
        $employee->apellidos = $request->input('apellidos');
        $employee->correo    = $request->input('correo');
        $employee->telefono  = $this->_cleanRequest($request->input('telefono'), NULL);
        $employee->skype     = $this->_cleanRequest($request->input('skype'), NULL);
        $employee->cargo     = $request->input('cargo');
        $employee->orden     = $this->_cleanRequest($request->input('orden'), 1);
        $employee->save();

        if ($employee->foto !== $request->input('foto')) {
            $employee->foto = $this->_moveFile($request->input('foto'), $employee);
            $employee->save();
        }

        return response()->json($employee, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();

        return response()->json($employee, 201);
    }

    private function _moveFile($file, $item)
    {
      $exists  = Storage::disk('public')->exists('temporal/'.$file);
      $path    = Storage::disk('public')->get('temporal/'.$file);
      $ext     = pathinfo(storage_path('/app/public/temporal/'.$file), PATHINFO_EXTENSION);
      $newName = $item->id.'.'.$ext;

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$file);
      }

      Storage::disk('discover')->put('employees/'.$newName, $path);

      return $newName;
    }

    private function _cleanRequest($request, $default)
    {
      $response = trim($request);
      $response = empty($response) ? $default: $response;

      return $response;
    }
}
