<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use DB;
use Storage;
use App\Http\Requests;
use App\Http\Requests\StorePackageRequest;
use App\Http\Controllers\Controller;

use App\Package;
use App\Destination;
use App\PackageType;
use App\PackageTab;
use App\PackageFile;
use App\PackageSubType;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $packages = Package::join(DB::raw('package_subtypes ps'), 'packages.package_subtype_id', '=', 'ps.id')
                         ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                         ->select('packages.*', 'ps.nombre as ps_nombre', 'pt.nombre as pt_nombre')
                         ->where('packages.estado', TRUE)
                         ->orderBy('packages.id', 'desc')
                         ->get();

      return view('admin.package.index', [
        'active'   => 'packages',
        'packages' => $packages
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $destinations = Destination::where('estado', TRUE)->get();
      $types        = PackageType::where('estado', TRUE)->get();

      return view('admin.package.create', [
        'active'       => 'packages',
        'destinations' => $destinations,
        'types'        => $types
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackageRequest $request)
    {


      $package = new Package;
      $package->cod_discover          = $request->input('cod_discover');
      $package->nombre                = $request->input('nombre');
      $package->descripcion           = $this->_cleanRequest($request->input('descripcion'), NULL);
      $package->precio_min            = $this->_cleanRequest($request->input('precio_min'), NULL);
      $package->precio_max            = $this->_cleanRequest($request->input('precio_max'), NULL);
      $package->precio_ser            = $this->_cleanRequest($request->input('precio_ser'), NULL);
      $package->precio_bol            = $this->_cleanRequest($request->input('precio_bol'), NULL);
      $package->nro_dias              = $request->input('nro_dias');
      $package->nro_noches            = $request->input('nro_noches');
      $package->imagen_1              = $request->input('imagen_1');
      $package->imagen_2              = $request->input('imagen_2');
      $package->flyers                = $request->input('flyers');
      $package->vigencia              = $this->_cleanRequest($request->input('vigencia'), NULL);
      $package->fecha_salida          = $this->_cleanRequest($request->input('salida'), NULL);
      $package->fecha_retorno         = $this->_cleanRequest($request->input('regreso'), NULL);
      $package->promocion             = $request->input('promocion');
      $package->destination_id        = $request->input('destino');
      $package->package_subtype_id    = $request->input('subcategoria');
      $package->destacado             = $request->input('destacado');
      $package->orden_destacado       = $request->input('orden_destacado');
      $package->save();





      $package->imagen_1 = $this->_moveImagePackage($request->input('imagen_1'), $package, 'principal');
      $package->imagen_2 = $this->_moveImagePackage($request->input('imagen_2'), $package, 'secundario');


      if($request->input('flyers')){
        $package->flyers   = $this->_moveImagePackage($request->input('flyers'), $package, 'flyers');
      }

      $package->save();


      $tabs = (object) json_decode($request->input('tabs'));

      foreach ($tabs as $tab) {
        $packageTab = new PackageTab;
        $packageTab->nombre      = $tab->nombre;
        $packageTab->descripcion = $tab->descripcion;
        $packageTab->orden       = $tab->orden;
        $packageTab->package_id  = $package->id;
        $packageTab->save();
      }

      $docs = (object) json_decode($request->input('docs'));

      foreach ($docs as $doc) {
        $packageFile = new PackageFile;
        $packageFile->nombre     = $doc->nombre;
        $packageFile->archivo    = $doc->archivo;
        $packageFile->orden      = $doc->orden;
        $packageFile->package_id = $package->id;
        $packageFile->save();

        $packageFile->archivo = $this->_moveFilePackage($doc->archivo, $packageFile);

        $packageFile->save();
      }

      return response()->json($package, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $destinations = Destination::where('estado', TRUE)->get();
      $types        = PackageType::where('estado', TRUE)->get();

      $package = Package::join(DB::raw('package_subtypes ps'), 'packages.package_subtype_id', '=', 'ps.id')
                         ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                         ->select('packages.*', 'ps.nombre as ps_nombre', 'ps.id as ps_id', 'pt.nombre as pt_nombre', 'pt.id as pt_id')
                         ->where('packages.id', $id)
                         ->where('packages.estado', TRUE)
                         ->first();

      $subtypes = PackageSubType::where('package_type_id', $package->pt_id)->get();
      $tabs     = PackageTab::where('package_id', $package->id)->get();
      $docs     = PackageFile::where('package_id', $package->id)->get();

      foreach ($docs as $key => $doc) {
        $docs[$key]->size = Storage::disk('discover')->size('packages/documents/'.$doc->archivo);
        $docs[$key]->type = Storage::disk('discover')->mimeType('packages/documents/'.$doc->archivo);
      }

      $package->size_1 = Storage::disk('discover')->size('packages/'.$package->imagen_1);
      $package->type_1 = Storage::disk('discover')->mimeType('packages/'.$package->imagen_1);
      $package->size_2 = Storage::disk('discover')->size('packages/secundario/'.$package->imagen_2);
      $package->type_2 = Storage::disk('discover')->mimeType('packages/secundario/'.$package->imagen_2);

      return view('admin.package.edit', [
        'active'       => 'packages',
        'destinations' => $destinations,
        'types'        => $types,
        'subtypes'     => $subtypes,
        'package'      => $package,
        'tabs'         => $tabs,
        'docs'         => $docs
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePackageRequest $request, $id)
    {

      $package = Package::find($id);

      $imagen1 = $request->input('imagen_1');
      $imagen2 = $request->input('imagen_2');
      $flyers = $request->input('flyers');

      if($request->input('imagen_1') != $package->imagen_1) {
        $imagen1 = $this->_moveImagePackage($request->input('imagen_1'), $package, 'principal');
      }
      if ($request->input('imagen_2') != $package->imagen_2) {
        $imagen2 = $this->_moveImagePackage($request->input('imagen_2'), $package, 'secundario');
      }
      if ( $request->input('flyers') != $package->flyers) {
          if($request->input('flyers') == ""){
            $flyers = "";
          }else{
            $flyers = $this->_moveImagePackage($request->input('flyers'), $package, 'flyers');
          }

      }

      $package->cod_discover  = $request->input('cod_discover');
      $package->nombre        = $request->input('nombre');
      $package->descripcion   = $request->input('descripcion');
      $package->precio_min    = $request->input('precio_min');
      $package->precio_max    = $request->input('precio_max');
      $package->precio_ser    = $request->input('precio_ser');
      $package->precio_bol    = $request->input('precio_bol');
      $package->nro_dias      = $request->input('nro_dias');
      $package->nro_noches    = $request->input('nro_noches');
      $package->imagen_1      = $imagen1;
      $package->imagen_2      = $imagen2;
      $package->flyers        = $flyers;
      $package->vigencia      = $request->input('vigencia');
      $package->fecha_salida  = $request->input('salida');
      $package->fecha_retorno = $request->input('regreso');
      $package->promocion     = $request->input('promocion');
      $package->destination_id = $request->input('destino');
      $package->package_subtype_id = $request->input('subcategoria');

      $package->destacado          = $request->input('destacado');
      $package->orden_destacado    = $request->input('orden_destacado');


      $package->save();



      $tabs = (object) json_decode($request->input('tabs'));

      foreach ($tabs as $tab) {
        if ($tab->modo === 'editar' || $tab->modo === 'eliminar') {
          $packageTab = PackageTab::find($tab->idd);
        } else {
          $packageTab = new PackageTab;
        }

        if ($tab->modo === 'eliminar') {
          $packageTab->delete();
        } else {
          $packageTab->nombre      = $tab->nombre;
          $packageTab->descripcion = $tab->descripcion;
          $packageTab->orden       = $tab->orden;
          $packageTab->package_id  = $package->id;
          $packageTab->save();
        }
      }

      $docs = (object) json_decode($request->input('docs'));

      foreach ($docs as $doc) {
        if ($doc->modo === 'editar' || $doc->modo === 'eliminar') {
          $packageFile = PackageFile::find($doc->idd);
        } else {
          $packageFile = new PackageFile;
        }

        if ($doc->modo === 'eliminar') {
          $packageFile->delete();
          $this->_deleteFile($packageFile->archivo, 'packages/documents/');
        } else {
          $packageFile->nombre     = $doc->nombre;
          $packageFile->orden      = $doc->orden;
          $packageFile->package_id = $package->id;

          $packageFile->save();

          if ($doc->archivo !== $packageFile->archivo) {
            $packageFile->archivo = $this->_moveFilePackage($doc->archivo, $packageFile);
          }

          $packageFile->save();
        }
      }

      return response()->json($package, 201);
    }

    public function destroy($id)
    {
      $package = Package::find($id);
      $package->estado = FALSE;
      $package->save();

      return response()->json($package, 201);
    }

    // Funciones Ajax
    public function ajaxFilesPackage(Request $request)
    {
      $file = $request->file('file');
      $path = $file->getRealPath();
      $ext  = $file->guessExtension();
      $newName = str_random(20).'.'.$ext;

      $exists = Storage::disk('public')->exists('temporal/'.$newName);

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$newName);
      }

      Storage::disk('public')->put('temporal/'.$newName, file_get_contents($path));

      return response()->json(array('fileName' => $newName), 201);
    }

    // Funciones Privadas
    private function _moveImagePackage($fileTem, $package, $modo)
    {
      $exists  = Storage::disk('public')->exists('temporal/'.$fileTem);
      $path    = Storage::disk('public')->get('temporal/'.$fileTem);
      $ext     = pathinfo(storage_path('/app/public/temporal/'.$fileTem), PATHINFO_EXTENSION);
      $newName = $package->id.'.'.$ext;

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$fileTem);
      }

      if ($modo === 'secundario') {
        Storage::disk('discover')->put('packages/secundario/'.$newName, $path);
      } elseif($modo === 'flyers'){
        Storage::disk('discover')->put('flyers/'.$newName, $path);
      }
        else {
        Storage::disk('discover')->put('packages/'.$newName, $path);
      }

      return $newName;
    }

    // Funciones Privadas
    private function _moveFilePackage($fileTem, $package)
    {
      $exists  = Storage::disk('public')->exists('temporal/'.$fileTem);
      $path    = Storage::disk('public')->get('temporal/'.$fileTem);
      $ext     = pathinfo(storage_path('/app/public/temporal/'.$fileTem), PATHINFO_EXTENSION);
      $newName = $package->id.'.'.$ext;

      if ($exists) {
        Storage::disk('public')->delete('temporal/'.$fileTem);
      }

      Storage::disk('discover')->put('packages/documents/'.$newName, $path);

      return $newName;
    }

    private function _deleteFile($file, $dir)
    {
      $exists  = Storage::disk('discover')->exists($dir.$file);

      if ($exists) {
        Storage::disk('discover')->delete($dir.$file);
      }

      return TRUE;
    }

    private function _cleanRequest($request, $default)
    {
      $response = trim($request);
      $response = empty($response) ? $default: $response;

      return $response;
    }
    public function estadopublico($id){
      $package = Package::find($id);
      if($package->publico){
        $package->publico    = 0;
      }else{
        $package->publico    = 1;
      }

      $package->save();
      return response()->json($package, 201);
    }
}
