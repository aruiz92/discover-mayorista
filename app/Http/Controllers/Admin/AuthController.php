<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AuthUserRequest;
use App\Http\Controllers\Controller;

use Hash;
use Auth;

class AuthController extends Controller
{
    public function login()
    {
      return view('admin.auth.login');
    }

    public function loginAjax(AuthUserRequest $request)
    {
      $user = \App\AgencyUser::where('correo', $request->input('correo'))->where('estado', TRUE)->first();

      if (Hash::check($request->input('contrasena'), $user->contrasena)) {
        Auth::login($user);
      } else {
        Auth::logout();
      }

      if (Auth::check()) {
        return response()->json(array('auth' => TRUE), 200);
      }

      return response()->json(array('contrasena' => array('La contraseña no coincide')), 422);
    }
}
