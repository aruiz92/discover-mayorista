<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UpdateCompanyRequest;
use App\Http\Controllers\Controller;

use App\Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::find(1);

        return view('admin.company.index', [
          'active' => 'discover',
          'company' => $company
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        $company = Company::find($id);

        $company->nombre             = $request->input('nombre');
        $company->telefono_fijo      = $request->input('telefono_fijo');
        $company->telefono_movil     = $request->input('telefono_movil');
        $company->correo_informacion = $request->input('correo_informacion');
        $company->correo_visitante   = $request->input('correo_visitante');
        $company->correo_agente      = $request->input('correo_agente');
        $company->facebook           = $request->input('facebook');
        $company->instagram          = $request->input('instagram');
        $company->youtube            = $request->input('youtube');

        $company->save();

        return response()->json($company, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
