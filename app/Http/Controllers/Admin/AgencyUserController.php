<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreAgencyUserRequest;
use App\Http\Requests\Admin\StoreAgencyUserRequest as AdminStoreAgencyUserRequest;
use App\Http\Controllers\Controller;

use App\AgencyUser;

use Mail;

class AgencyUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminStoreAgencyUserRequest $request)
    {
        $agencyUser = new AgencyUser;
        $agencyUser->nombre    = $request->input('nombre');
        $agencyUser->correo    = $request->input('correo');
        $agencyUser->agency_id = $request->input('agency_id');
        $agencyUser->role_id   = $request->input('role');
        $agencyUser->save();

        $contrasena = str_random(8);
        $agencyUser->contrasena = bcrypt($contrasena);
        $agencyUser->save();

        $this->_correoWelcome($agencyUser, $contrasena);

        return response()->json($agencyUser, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAgencyUserRequest $request, $id)
    {
        $agencyUser = AgencyUser::find($id);
        $agencyUser->nombre    = $request->input('nombre');
        $agencyUser->correo    = $request->input('correo');
        $agencyUser->agency_id = $request->input('agency_id');
        $agencyUser->role_id   = $request->input('role');
        $agencyUser->save();

        return response()->json($agencyUser, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agencyUser = AgencyUser::find($id);

        if (!$agencyUser) {
          return response()->json('Document not found', 404);
        }

        if($agencyUser->delete());

        return response()->json("Eliminado", 201);
    }

    // Funciones ajax
    public function resetContrasena($id)
    {
      $agencyUser = AgencyUser::find($id);
      $contrasena = str_random(8);
      $agencyUser->contrasena = bcrypt($contrasena);
      $agencyUser->save();

      $this->_correoReset($agencyUser, $contrasena);

      return response()->json($agencyUser, 200);
    }

    public function ajaxDeleteAgencyUser(Request $request)
    {
      $agencyUser = AgencyUser::find($request->input('id'));

      if (!$agencyUser) {
        return response()->json('Document not found', 404);
      }

      $agencyUser->estado = FALSE;
      $agencyUser->save();

      return response()->json($agencyUser, 201);
    }

    // Funciones provadas
    private function _correoReset($user, $contrasena)
    {
        Mail::send('emails.reset', ['user' => $user, 'contrasena' => $contrasena], function ($m) use ($user) {
            $m->from('viajes@discovermayorista.com', 'Discover Mayorista');
            $m->to($user->correo, $user->nombre)->subject('Hemos actualizado sus datos de acceso');
        });
    }

    private function _correoWelcome($user, $contrasena)
    {
        Mail::send('emails.welcome', ['user' => $user, 'contrasena' => $contrasena], function ($m) use ($user) {
            $m->from('viajes@discovermayorista.com', 'Discover Mayorista');
            $m->to($user->correo, $user->nombre)->subject('Bienvenido! Hemos activado tu cuenta');
        });
    }
}
