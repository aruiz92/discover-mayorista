<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Discover;

class SubscriberController extends Controller
{
    public function index()
    {
      $company      = \App\Company::find(1);
      $destinations = \App\Destination::where('estado', TRUE)->get();
      $countries    = \App\Country::where('estado', TRUE)->get();

      $articulo = Discover::wpArticle();

      return view('frontend.subscriber.index', [
        'destinations' => $destinations,
        'company'      => $company,
        'countries'    => $countries,
        'articulo'     => $articulo
      ]);
    }
}
