<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreAgencyRequest;
use App\Http\Requests\UpdateAgencyRequest;
use App\Http\Requests\UpdateLogoAgencyRequest;
use App\Http\Controllers\Controller;
use Hash;
use Storage;
use DB;
use Auth;
use Mail;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgencyRequest $request)
    {
      $agency = new \App\Agency;
      $agency->usuario          = str_slug($request->input('usuario'));
      $agency->correo           = $request->input('correo');
      $agency->ruc              = $request->input('ruc');
      $agency->razon_social     = $request->input('razon_social');
      $agency->nombre_comercial = $request->input('nombre_comercial');
      $agency->nombres          = $request->input('nombres');
      $agency->telefono         = $request->input('telefono');
      $agency->celular          = (empty(trim($request->input('celular')))) ? NULL: $request->input('celular');
      $agency->direccion_1      = $request->input('direccion_1');
      $agency->direccion_2      = (empty(trim($request->input('direccion_2')))) ? NULL: $request->input('direccion_2');
      $agency->district_id      = $request->input('distrito');
      $agency->estado           = FALSE;
      $agency->save();

      $file = $request->file('logo');
      $nombreLogo = $agency->id.'.'.$file->getClientOriginalExtension();
      Storage::disk('discover')->put('logos/'.$nombreLogo, file_get_contents($file->getRealPath()));

      $agency->logo = $nombreLogo;
      $agency->save();

      $user = new \App\AgencyUser;
      $user->nombre     = $agency->nombres;
      $user->correo     = $agency->correo;
      $user->contrasena = Hash::make('12345678');
      $user->agency_id  = $agency->id;
      $user->role_id    = 1;
      $user->estado     = FALSE;
      $user->save();

      $this->_correo($agency);

      return response()->json($agency, 201);
    }

    public function updateLogo(UpdateLogoAgencyRequest $request)
    {
      $agency = \App\Agency::find(Auth::user()->agency->id);

      $file = $request->file('logo');
      $nombreLogo = $agency->id.'.'.$file->getClientOriginalExtension();
      Storage::disk('discover')->put('logos/'.$nombreLogo, file_get_contents($file->getRealPath()));
      $agency->logo = $nombreLogo;
      $agency->save();

      return response()->json($agency, 201);
    }

    public function showDistrict($id)
    {
      $agencies = \App\Agency::where('district_id', $id)->where('estado', TRUE)->get();
      if (count($agencies) === 0) {
        $agencies = \App\Agency::where('id', 1)->get();
      }

      return response()->json($agencies, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAgencyRequest $request, $id)
    {
      if (!Auth::check() || Auth::user()->agency->id !== (int) $id) {
        return response()->json(array('error' => 'No seas pendejo'));
      }

      $agency = \App\Agency::find(Auth::user()->agency->id);
      $agency->usuario          = str_slug($request->input('usuario'));
      $agency->correo           = $request->input('correo');
      $agency->ruc              = $request->input('ruc');
      $agency->razon_social     = $request->input('razon_social');
      $agency->nombre_comercial = $request->input('nombre_comercial');
      $agency->nombres          = $request->input('nombres');
      $agency->telefono         = $request->input('telefono');
      $agency->celular          = $request->input('celular');
      $agency->direccion_1      = $request->input('direccion_1');
      $agency->direccion_2      = $request->input('direccion_2');
      $agency->district_id      = $request->input('distrito');
      $agency->save();

      return response()->json($agency, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function agencies(Request $request)
    {
      $idP   = $request->input('province');
      $idD   = $request->input('district');
      $limit = $request->input('limit');
      $page  = $request->input('page');

      $offset = $limit * $page;

      $agencies = DB::table(DB::raw('agencies a'))
                    ->join(DB::raw('districts d'), 'a.district_id', '=', 'd.id')
                    ->join(DB::raw('provinces p'), 'd.province_id', '=', 'p.id')
                    ->select('a.*')
                    ->when($idP, function ($query) use ($idP) {
                      return $query->where('p.id', $idP);
                    })
                    ->when($idD, function ($query) use ($idD) {
                      return $query->where('d.id', $idD);
                    })
                    ->where('a.estado', TRUE)
                    ->orderBy('a.id', 'DESC')
                    ->offset($offset)
                    ->limit($limit)
                    ->get();

       $btnLoad = TRUE;

       if (!$agencies) {
         $btnLoad = FALSE;
       }

       $view = view('frontend.agency.ajax.index')->with('agencies', $agencies)->render();

       return response()->json(array('load' => $btnLoad, 'view' => $view), 200);
    }

    // Funciones privadas
    private function _correo($agency)
    {
      $discover = \App\Company::find(1);

      Mail::send('emails.agency', ['agency' => $agency], function ($m) use ($discover, $agency) {

        $m->from('no-responder@discovermayorista.com', 'No Responder');
        $m->to('agencias@discovermayorista.com', 'Discover Mayorista')->subject('Agencia #'.$agency->id);
      });
    }
}
