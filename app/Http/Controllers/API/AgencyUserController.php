<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UpdatePasswordAgencyUser;
use App\Http\Controllers\Controller;

use App\AgencyUser;
use Auth;
use Hash;

class AgencyUserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }

  public function updatePassword(UpdatePasswordAgencyUser $request)
  {

    $agencyUser = AgencyUser::find(Auth::user()->id);

    if (!$agencyUser) {
      return response()->json(array('Document not found'), 404);
    }

    if (Hash::check($request->input('passwordOld'), $agencyUser->contrasena))
    {
      $agencyUser->contrasena = Hash::make($request->input('password'));
      $agencyUser->save();

      return response()->json($agencyUser, 201);
    }

    return response()->json(array('currentPassword' => 'La contraseña actual no coincide'), 422);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }
}
