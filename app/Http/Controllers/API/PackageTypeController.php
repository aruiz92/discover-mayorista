<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class PackageTypeController extends Controller
{

    public function loadajax(Request $request)
    {
      $idD   = $request->input('destination');
      $idT   = $request->input('type');
      $idST  = $request->input('sub');
      $limit = $request->input('limit');
      $page  = $request->input('page');

      $offset = $limit * $page;

      $destination    = \App\Destination::where('id', $idD)->where('estado', true)->first();
      $packageType    = \App\PackageType::find($idT);
      $packageSubType = \App\PackageSubType::find($idST);

      $packages = DB::table(DB::raw('packages p'))
                    ->join(DB::raw('package_subtypes ps'), 'p.package_subtype_id', '=', 'ps.id')
                    ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                    ->select('p.*')
                    ->where('p.destination_id', $destination->id)
                    ->where('ps.id', $packageSubType->id)
                    ->where('pt.id', $packageType->id)
                    ->where('p.estado', TRUE)
                    ->orderBy('p.id', 'DESC')
                    ->offset($offset)
                    ->limit($limit)
                    ->get();

      $btnLoad = TRUE;

      if (!$packages) {
        $btnLoad = FALSE;
      }

      $view = view('frontend.package_type.ajax.show')->with('packages', $packages)->render();

      return response()->json(array('load' => $btnLoad, 'view' => $view), 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
