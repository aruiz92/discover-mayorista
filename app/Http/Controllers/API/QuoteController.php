<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreQuoteRequest;
use App\Http\Requests\StoreQuoteUserRequest;

use Auth;
use Mail;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQuoteRequest $request)
    {
      $quote = new \App\Quote;
      $quote->nombres    = $request->input('nombres');
      $quote->correo     = $request->input('correo');
      // $quote->asunto     = $request->input('asunto');
      $quote->mensaje    = $request->input('mensaje');
      $quote->package_id = $request->input('package_id');

      $agency = $request->input('agency_id');

      if ($request->input('pais') === 2) { // Chile
        $quote->agency_id = 1; // Agencia por defecto
      } else {
        if (empty($agency) || is_null($agency)) {
          $quote->agency_id = 1; // Agencia por defecto
        } else {
          $quote->agency_id = $request->input('agency_id');
        }
      }

      $quote->save();

      // Enviar correo
      $this->_sendEmail($quote->id);

      return response()->json(array($quote), 201);
    }

    public function storeAuth(StoreQuoteUserRequest $request)
    {
      $quoteUser = new \App\QuoteUser;
      $quoteUser->mensaje        = $request->input('mensaje');
      $quoteUser->package_id     = $request->input('package_id');
      $quoteUser->agency_user_id = Auth::user()->id;
      $quoteUser->save();

      // Enviar correo
      $this->_sendEmail($quoteUser->id);

      return response()->json($quoteUser);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Funciones privadas
    private function _sendEmail($idQuote)
    {
      $company = \App\Company::find(1);
      $company->correo_mail = $company->correo_visitante;
      $vista = 'emails.quote';
      $tipo = 'Cliente';

      if (Auth::check()) {
        $quote = \App\QuoteUser::find($idQuote);
        $company->correo_mail = $company->correo_agente;
        $vista = 'emails.quote-auth';
        $tipo = 'Agente';
      } else {
        $quote = \App\Quote::find($idQuote);
      }

      // Visitante
      if (!Auth::check()) {
        Mail::send($vista, ['quote' => $quote], function ($m) use ($quote) {
          $m->from('no-responder@discovermayorista.com', 'No Responder');
          $m->to($quote->agency->correo, $quote->agency->nombre_comercial)->subject('Cotización #'.$quote->id);
        });
      }

      // Mail correo Discover
      Mail::send($vista, ['quote' => $quote], function ($m) use ($company, $quote, $tipo) {
        $m->from('no-responder@discovermayorista.com', 'No Responder');
        $m->to($company->correo_mail, 'Discover Mayorista')->subject('Cotización #'.$quote->id.' ['.$tipo.']');
      });
    }
}
