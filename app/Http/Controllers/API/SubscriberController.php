<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreSubscriberRequest;
use App\Http\Controllers\Controller;

use Mail;

class SubscriberController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreSubscriberRequest $request)
  {
    $subscriber = new \App\Subscriber;
    $subscriber->nombres    = $request->input('nombres');
    $subscriber->apellidos  = $request->input('apellidos');
    $subscriber->fecnac     = $request->input('fecnac');
    $subscriber->telefono   = $request->input('telefono');
    $subscriber->direccion  = $request->input('direccion');
    $subscriber->correo     = $request->input('correo');

    if ($request->input('pais') === 1) {
      $subscriber->district_id = $request->input('district_id');
    } else {
      $subscriber->district_id = 5;
    }

    $subscriber->save();

    $this->_correo($subscriber);

    return response()->json($subscriber, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }

  // Funciones privadas
  private function _correo($subscriber)
  {
    $discover = \App\Company::find(1);

    Mail::send('emails.subscriber', ['subscriber' => $subscriber], function ($m) use ($discover, $subscriber) {

      $m->from('no-responder@discovermayorista.com', 'No Responder');
      $m->to($discover->correo_visitante, 'Discover Mayorista')->subject('Suscriptor #'.$subscriber->id);
    });
  }
}
