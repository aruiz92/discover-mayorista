<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreContactRequest;
use App\Http\Controllers\Controller;

use Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContactRequest $request)
    {
        $agencia = trim($request->input('agencia'));

        $contact = new \App\Contact;
        $contact->nombres   = $request->input('nombres');
        $contact->apellidos = $request->input('apellidos');
        $contact->telefono  = $request->input('telefono');
        $contact->correo    = $request->input('correo');
        $contact->consulta  = $request->input('consulta');
        $contact->agency_id = $agencia;

        if (empty($agencia) || is_null($agencia)) {
            $contact->agency_id = 1;
        }

        $contact->save();

        $this->_correo($contact);

        return response()->json($contact, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Funciones privadas
    private function _correo($contact)
    {
        $discover = \App\Company::find(1);

        Mail::send('emails.contact', ['contact' => $contact], function ($m) use ($discover, $contact) {

        $m->from('no-responder@discovermayorista.com', 'No Responder');
        $m->to($discover->correo_visitante, 'Discover Mayorista')->subject('Contacto #'.$contact->id);
        });
    }
}
