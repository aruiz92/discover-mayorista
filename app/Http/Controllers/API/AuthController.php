<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthAgencyUserRequest;
use App\Http\Requests\ResetPasswordRequest;

use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;

use Auth;
use Hash;

class AuthController extends Controller
{
  public function auth(AuthAgencyUserRequest $request)
  {
    $user = \App\AgencyUser::where('correo', $request->input('correo'))->where('estado', TRUE)->first();

    if (Hash::check($request->input('contrasena'), $user->contrasena)) {
      Auth::login($user);
    } else {
      Auth::logout();
    }

    if (Auth::check()) {
      return response()->json(array('auth' => TRUE), 200);
    }

    return response()->json(array('auth' => FALSE), 200);
  }

  public function resetPassword(ResetPasswordRequest $request)
  {
    $credentials = ['correo' => $request->input('correo')];

    $response = Password::sendResetLink($credentials, function (Message $message) {
      $message->subject('Restablecer contraseña');
    });

    switch ($response) {
      case Password::RESET_LINK_SENT:
        return response()->json(array('success' => trans($response)), 201);
      case Password::INVALID_USER:
        return response()->json(array('error' => trans($response)), 422);
    }
  }
}
