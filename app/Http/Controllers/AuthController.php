<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Discover;

class AuthController extends Controller
{
    private $articulo;

    public function __construct()
    {
      $this->articulo = Discover::wpArticle();
    }

    public function index()
    {
      $company      = \App\Company::find(1);
      $destinations = \App\Destination::where('estado', true)->get();

      return view('frontend.auth.index', [
        'destinations' => $destinations,
        'company'      => $company,
        'articulo'     => $this->articulo
      ]);
    }

    public function store()
    {
      $company      = \App\Company::find(1);
      $destinations = \App\Destination::where('estado', true)->get();
      $country      = \App\Country::find(1);
      $provinces    = \App\Province::where('country_id', $country->id)->where('estado', TRUE)->get();

      return view('frontend.auth.store', [
        'destinations' => $destinations,
        'company'      => $company,
        'country'      => $country,
        'provinces'    => $provinces,
        'articulo'     => $this->articulo
      ]);
    }

    public function reset()
    {
      $company      = \App\Company::find(1);
      $destinations = \App\Destination::where('estado', true)->get();

      return view('frontend.auth.reset', [
        'destinations' => $destinations,
        'company'      => $company,
        'articulo'     => $this->articulo
      ]);
    }
}
