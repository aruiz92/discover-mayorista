<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use Auth;
use Discover;

class AgencyController extends Controller
{
    private $company;
    private $destinations;
    private $distritos;
    private $provincias;
    private $limit;
    private $articulo;

    public function __construct()
    {
      $this->limit        = 8;
      $this->articulo     = Discover::wpArticle();
      $this->company      = \App\Company::find(1);
      $this->destinations = \App\Destination::where('estado', true)->get();

      $this->provincias   = \App\Province::where('estado', TRUE)
                                 ->where('nombre', '<>', 'Lima')
                                 ->get();

       $this->distritos    = \App\District::where('estado', TRUE)
                                  ->where('province_id', 1)
                                  ->get();
    }

    public function index()
    {
      $agencies = \App\Agency::where('estado', TRUE)->orderBy('id', 'DESC')->limit($this->limit)->get();

      return view('frontend.agency.index', [
        'destinations' => $this->destinations,
        'company'      => $this->company,
        'distritos'    => $this->distritos,
        'provincias'   => $this->provincias,
        'agencies'     => $agencies,
        'province'     => NULL,
        'district'     => NULL,
        'limit'        => $this->limit,
        'articulo'     => $this->articulo
      ]);
    }

    public function districts($pro_id = NULL, $pro_url = NULL, $dis_id = NULL, $dis_url = NULL)
    {
      if ($pro_id === NULL && $pro_url === NULL || $dis_id === NULL && $dis_url === NULL) {
        return redirect('agencias');
      }


      $agencies = DB::table(DB::raw('agencies a'))
                    ->join(DB::raw('districts d'), 'a.district_id', '=', 'd.id')
                    ->join(DB::raw('provinces p'), 'd.province_id', '=', 'p.id')
                    ->select('a.*')
                    ->where('a.district_id', $dis_id)
                    ->where('a.estado', TRUE)
                    ->orderBy('a.id', 'DESC')
                    ->limit($this->limit)
                    ->get();

      return view('frontend.agency.index', [
        'destinations' => $this->destinations,
        'company'      => $this->company,
        'distritos'    => $this->distritos,
        'provincias'   => $this->provincias,
        'agencies'     => $agencies,
        'province'     => $pro_id,
        'district'     => $dis_id,
        'limit'        => $this->limit,
        'articulo'     => $this->articulo
      ]);
    }

    public function provinces($pro_id = NULL, $pro_url = NULL)
    {
      if ($pro_id === NULL && $pro_url === NULL) {
        return redirect('agentes');
      }

      $agencies = DB::table(DB::raw('agencies a'))
                    ->join(DB::raw('districts d'), 'd.id', '=', 'a.district_id')
                    ->join(DB::raw('provinces p'), 'p.id', '=', 'd.province_id')
                    ->where('d.province_id', $pro_id)
                    ->where('a.estado', TRUE)
                    ->orderBy('a.id', 'DESC')
                    ->limit($this->limit)
                    ->get();

      return view('frontend.agency.index', [
        'destinations' => $this->destinations,
        'company'      => $this->company,
        'distritos'    => $this->distritos,
        'provincias'   => $this->provincias,
        'agencies'     => $agencies,
        'province'     => $pro_id,
        'district'     => NULL,
        'limit'        => $this->limit,
        'articulo'     => $this->articulo
      ]);
    }

    public function show($usuario)
    {
      if (!Auth::check() || Auth::user()->agency->usuario !== $usuario) {
        return redirect('/');
      }

      $agency = \App\Agency::join(DB::raw('districts d'), 'agencies.district_id', '=', 'd.id')
                           ->join(DB::raw('provinces p'), 'd.province_id', '=', 'p.id')
                           ->select('agencies.*', 'p.id as p_id')
                           ->where('agencies.usuario', Auth::user()->agency->usuario)->first();

      $country = \App\Country::find(1);

      $provinces = \App\Province::where('country_id', $country->id)
                                 ->where('estado', TRUE)
                                 ->orderBy('nombre', 'ASC')
                                 ->get();

     $districts = \App\District::where('province_id', $agency->p_id)
                               ->where('estado', TRUE)
                               ->orderBy('nombre', 'ASC')
                               ->get();

     $agencyUsers = \App\AgencyUser::where('estado', TRUE)->where('agency_id', $agency->id)->get();
     $roles       = \App\Role::get();
      
      return view('frontend.agency.show', [
        'destinations' => $this->destinations,
        'company'      => $this->company,
        'agency'       => $agency,
        'country'      => $country,
        'provinces'    => $provinces,
        'districts'    => $districts,
        'articulo'     => $this->articulo,
        'agencyUsers'  => $agencyUsers,
        'roles'        => $roles
      ]);

    }
}
