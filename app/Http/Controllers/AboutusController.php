<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Discover;

class AboutusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $destinations = \App\Destination::where('estado', true)->get();
        $company      = \App\Company::find(1);
        $aboutus      = \App\Aboutus::find(1);
        $employees    = \App\Employee::where('estado', TRUE)->orderBy('orden', 'asc')->get();
        $partners     = \App\Partner::where('estado', TRUE)->orderBy('orden', 'asc')->get();
        $articulo     = Discover::wpArticle();

        return view('frontend.aboutus.index', [
          'aboutus'      => $aboutus,
          'company'      => $company,
          'destinations' => $destinations,
          'employees'    => $employees,
          'partners'     => $partners,
          'articulo'     => $articulo
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
