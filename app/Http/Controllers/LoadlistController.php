<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use App\Http\Requests;
use App\Package;
use Illuminate\Support\Facades\DB;

class LoadlistController extends Controller
{
    // function loadlistpackages($id){
    //   $package = Package::where('destination_id', $id)->get();
    //   return $package;
    // }
    function allpackage(){
      $allpackage = Package::select('id','nombre')->where('estado',1)->where('publico',1)->get();
      echo json_encode($allpackage);
    }
    function loadlistpackages($id){
      $listpackage = DB::table('packages')->select('id', 'nombre')->where('destination_id', '=', $id)->where('estado',1)->get();
       echo json_encode($listpackage);
    }
    function loadlistdate_departure_nro_dias($id){

      $fecha_departure = DB::table('packages')
      ->select('fecha_salida','fecha_retorno')
      ->where('nombre', '=', $id)
      ->get();
      $fecha_inicial = substr($fecha_departure[0]->fecha_salida,0,-3);
      $fecha_final =  substr($fecha_departure[0]->fecha_retorno,0,-3);


      while (strtotime($fecha_inicial) <= strtotime($fecha_final)) {
        $data[] = ["fechas" => $fecha_inicial,"mes" => substr($fecha_inicial, -2),"anno" => substr($fecha_inicial, 2,-3)];
        $fecha_inicial=date("Y-m", strtotime("$fecha_inicial +1 month"));
      }
         echo json_encode($data);




    }
    function loadlistcant_dias($id){
      $nro_dias = DB::table('packages')
                ->select( DB::raw('COUNT(*) as cant_nro_dias'),'nro_dias')
                ->where('nombre', '=', $id)
                ->groupBy('nro_dias')
                ->orderBy('nro_dias','asc')
                ->get();
      echo json_encode($nro_dias);
    }
}
