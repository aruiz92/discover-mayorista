<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use Discover;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = \App\Package::join(DB::raw('package_subtypes ps'), 'packages.package_subtype_id', '=', 'ps.id')
                               ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                               ->join(DB::raw('destinations d'), 'packages.destination_id', '=', 'd.id')
                               ->select('packages.*', 'ps.nombre as ps_nombre', 'ps.id as ps_id', 'pt.nombre as pt_nombre', 'pt.id as pt_id', 'd.nombre as d_nombre', 'd.id as d_id')
                               //->where('packages.vigencia', '>=', date('Y-m-d'))
                               ->where('packages.id', $id)
                               ->first();

        if (!$package) {
          return view('errors.503');
        }

        $articulo     = Discover::wpArticle();
        $countries    = \App\Country::where('estado', TRUE)->get();
        $company      = \App\Company::find(1);
        $destinations = \App\Destination::where('estado', true)->get();
        $bloqueos     = \App\Package::join(DB::raw('package_subtypes ps'), 'packages.package_subtype_id', '=', 'ps.id')
                                    ->join(DB::raw('package_types pt'), 'ps.package_type_id', '=', 'pt.id')
                                    ->select('packages.*')
                                    ->where('pt.id', 1)
                                    ->where('packages.vigencia', '>=', date('Y-m-d'))
                                    //->where('packages.estado', TRUE)
                                    ->orderBy('packages.id', 'DESC')
                                    ->limit(12)
                                    ->get();

        return view('frontend.package.show', [
          'destinations' => $destinations,
          'company'      => $company,
          'package'      => $package,
          'countries'    => $countries,
          'bloqueos'     => $bloqueos,
          'articulo'     => $articulo
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
