<?php

//rutas aruiz

Route::get('/allpackage',['as' => 'allpackage', 'uses'=> 'LoadlistController@allpackage']);
Route::get('/packages/{id}',['as' => 'loadlistpackages', 'uses'=> 'LoadlistController@loadlistpackages']);
Route::get('/fecha_salida_nrodias/{id}',['as' => 'loadlistdate_departure', 'uses'=> 'LoadlistController@loadlistdate_departure_nro_dias']);
Route::get('/cantidaddias/{id}',['as' => 'loadlistcant_dias', 'uses'=> 'LoadlistController@loadlistcant_dias']);
Route::get('/resultado-filtro',['as' => 'filtrospackage', 'uses'=> 'LockController@search_package']);


Route::get('/', 		['as' => 'home' , 'uses' => 'HomeController@index']);

// Nuevas rutas
Route::resource('/package', 'PackageController');

Route::get('/nosotros', 'AboutusController@index');
Route::get('/contacto', 'ContactController@index');
Route::get('/registro-suscriptores', 'SubscriberController@index');
Route::get('/ingreso-agencias', 'AuthController@index');
Route::get('/ingreso-agencias/reset', 'AuthController@reset');
Route::get('/registro-agencias', 'AuthController@store');
// Route::get('/blog', 'BlogController@index');
Route::get('/mayorista', 'WholesalerController@index');
Route::get('/promociones', 'LockController@index');
Route::get('/bloqueos', 'LockController@index');

Route::get('{id}-destination-{name}', [
  'as'   => 'destination',
  'uses' => 'DestinationController@show'
]);

Route::get('{idD}-destination-{nameD}/{idT}-{nameT}', [
  'as'   => 'packageType',
  'uses' => 'PackageTypeController@show'
]);

Route::get('{idD}-destination-{nameD}/{idT}-{nameT}/{idS?}-{nameS?}', [
  'as'   => 'packageSubType',
  'uses' => 'PackageTypeController@show'
]);

Route::get('package/{idPackage}-{urlPakage}', ['as'   => 'package','uses' => 'PackageController@show']);

// Controlador: AGENCY
Route::get('/agencias', 'AgencyController@index');

Route::get('/agencias/{pro_id}-{pro_url}/{dis_id}-{dis_url}', [
  'as'   => 'district',
  'uses' => 'AgencyController@districts'
]);

Route::get('/agencias/{pro_id}-{pro_url}', [
  'as'   => 'province',
  'uses' => 'AgencyController@provinces'
]);

Route::get('agency/{id}', 'AgencyController@show');

// Rutas Dashboard
Route::auth();

Route::group(['middleware' => ['auth', 'authDashboard']], function () {
  Route::group(['prefix' => 'dashboard'], function () {
    Route::resource('discover', 'Admin\CompanyController');
    Route::resource('aboutus', 'Admin\AboutusController');
    Route::resource('employees', 'Admin\EmployeeController');
    Route::resource('partners', 'Admin\PartnerController');
    Route::resource('newsletters', 'Admin\NewsletterController');
    Route::resource('sliders', 'Admin\SliderController');
    Route::resource('packages', 'Admin\PackageController');
    Route::resource('agencies', 'Admin\AgencyController');
    Route::resource('countries', 'Admin\CountryController');
    Route::resource('provinces', 'Admin\ProvinceController');
    Route::resource('districts', 'Admin\DistrictController');
    Route::resource('quotations', 'Admin\QuotationController');
    Route::resource('quotation_users', 'Admin\QuotationUserController');
    Route::resource('package_types', 'Admin\PackageTypeController');
    Route::resource('package_subtypes', 'Admin\PackageSubTypeController');
    Route::resource('destinations', 'Admin\DestinationController');
    Route::resource('agency_users', 'Admin\AgencyUserController');
    Route::resource('offers', 'Admin\OfferController');
    Route::resource('subscribers', 'Admin\SubscriberController');
    Route::resource('contacts', 'Admin\ContactController');
    Route::get('/', 'Admin\DashboardController@index');
    Route::get('ajaxResetContrasena/{id}', 'Admin\AgencyUserController@resetContrasena');
    Route::get('ajaxActivar/{id}', 'Admin\AgencyController@ajaxActivar');
    Route::post('ajaxFoto', 'Admin\EmployeeController@ajaxFoto');
    Route::post('ajaxSlider', 'Admin\SliderController@ajaxSlider');
    Route::post('ajaxFilesPackage', 'Admin\PackageController@ajaxFilesPackage');
    Route::post('ajaxFilePartner', 'Admin\PartnerController@ajaxFilePartner');
    Route::post('ajaxDeleteAgencyUser', 'Admin\AgencyUserController@ajaxDeleteAgencyUser');
    Route::post('cambiarestadopublico/{id}', 'Admin\PackageController@estadopublico');
  });
});

Route::group(['prefix' => 'dashboard'], function () {
  Route::get('login', 'Admin\AuthController@login');
  Route::get('ajaxSubTypes/{pt_id}', 'Admin\PackageSubTypeController@ajaxSubTypes');
  Route::get('ajaxProvinces/{c_id}', 'Admin\ProvinceController@ajaxProvinces');
  Route::get('ajaxDistricts/{p_id}', 'Admin\DistrictController@ajaxDistricts');
  Route::post('loginAjax', 'Admin\AuthController@loginAjax');
});

// Rutas API
Route::group(['prefix' => 'api'], function () {
  Route::resource('newsletters', 'API\NewsletterController');
  Route::resource('quote', 'API\QuoteController');
  Route::resource('subscriber', 'API\SubscriberController');
  Route::resource('province', 'API\ProvinceController');
  Route::resource('district', 'API\DistrictController');
  Route::resource('agency', 'API\AgencyController');
  Route::resource('contact', 'API\ContactController');
  Route::resource('agencyuser', 'API\AgencyUserController');
  Route::post('updatelogo', 'API\AgencyController@updateLogo');
  Route::post('quoteAuth', 'API\QuoteController@storeAuth');
  Route::get('agencies/district/{id}', 'API\AgencyController@showDistrict');
  Route::post('agencies', 'API\AgencyController@agencies');
  Route::post('packagesubtypes', 'API\PackageTypeController@loadajax');
  Route::post('auth', 'API\AuthController@auth');
  Route::post('auth-reset', 'API\AuthController@resetPassword');
  Route::put('password/{id}', 'API\AgencyUserController@updatePassword');
});
