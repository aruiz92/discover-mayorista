<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreQuoteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres'   => 'required|min:3|max:100',
            'correo'    => 'required|email',
            // 'asunto'    => 'required|min:3|max:200',
            'mensaje'   => 'required|min:3',
            'pais'      => 'required|digits_between:1,3',
            'provincia' => 'required_if:pais,1|digits_between:1,3',
            'distrito'  => 'required_if:pais,1|digits_between:1,3',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombres.required' => 'El nombres es requerido.',
            'correo.required'  => 'El correo es requerido.',
            'correo.email'  => 'El correo es invalido.',
            'asunto.required'  => 'El asunto es requerido.',
            'mensaje.required' => 'El mensaje es requerido.',
        ];
    }
}
