<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateAboutusRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quienes_somos' => 'required',
            'mision'        => 'required',
            'vision'        => 'required',
            'ventajas'      => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'quienes_somos.required' => 'El campo Quienes Somos es requerido',
            'mision.required'        => 'El campo Misión es requerido',
            'vision.required'        => 'El campo Visión es requerido',
            'ventajas.required'      => 'El campo Ventajas Corporativas es requerido',
        ];
    }
}
