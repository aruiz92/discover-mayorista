<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StorePackageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cod_discover' => '',
            'nombre'       => 'required|min:3|max:100',
            'descripcion'  => 'min:10|max:200',
            'precio_min'   => '',
            'precio_max'   => '',
            'precio_ser'   => '',
            'precio_bol'   => '',
            'nro_dias'     => 'required|digits_between:1,2',
            'nro_noches'   => 'required|digits_between:1,2',
            'imagen_1'     => 'required',
            'imagen_2'     => 'required',
            'vigencia'     => 'date',
            'salida'       => '',
            'regreso'      => '',
            'promocion'    => 'required',
            'destino'      => 'required|digits_between:1,2',
            'categoria'    => 'required|digits_between:1,2',
            'subcategoria' => 'required|digits_between:1,2',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required'       => 'El campo :attribute es requerido',
            '*.digits_between' => 'El campo :attribute debe ser un numero',
            '*.date'           => 'El campo :attribute no es una fecha',
            '*.min'            => 'El campo :attribute debe tener mínimo :min caracteres',
            '*.max'            => 'El campo :attribute debe tener mínimo :max caracteres',
            '*.regex'          => 'El campo :attribute no es valido',
        ];
    }
}
