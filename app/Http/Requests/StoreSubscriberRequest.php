<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreSubscriberRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'nombres'   => 'required|min:3|max:100',
          'apellidos' => 'required|min:3|max:100',
          'fecnac'    => 'required|date_format:Y-m-d',
          'telefono'  => 'required|digits_between:7,9',
          'direccion' => 'required',
          'correo'    => 'required|email',
          'pais'      => 'required',
          'provincia' => 'required_if:pais,1',
          'distrito'  => 'required_if:pais,1'
      ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombres.required'      => 'El nombres es requerido.',
            'apellidos.required'    => 'El apellido es requerido.',
            'correo.required'       => 'El correo es requerido.',
            'correo.email'          => 'El correo es invalido.',
            'telefono.required'     => 'El telefono es requerido.',
            'fecnac.required'       => 'La fecha de nacimiento es requerido.',
            'direccion.required'    => 'La dirección es requerido.',
            'pais.required'         => 'El páis es requerido.',
            'provincia.required_if' => 'La provincia es requerido.',
            'distrito.required_if'  => 'El distrito es requerido.',
        ];
    }
}
