<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreSliderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'texto_1' => '',
            'texto_2' => 'required',
            'texto_3' => '',
            'orden'   => 'digits_between:1,2',
            'seccion' => 'required|alpha',
            'imagen'  => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required'       => 'El campo :attribute es requerido',
            '*.digits_between' => 'El campo :attribute no es un numero',
        ];
    }
}
