<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres'   => 'required|min:3|max:20',
            'apellidos' => 'required|min:3|max:20',
            'telefono'  => 'required|digits_between:8,11',
            'correo'    => 'required|email',
            'consulta'  => 'required|min:4',
            'pais'      => 'required|digits_between:1,3',
            'provincia' => 'required_if:pais,1|digits_between:1,3',
            'distrito'  => 'required_if:pais,1|digits_between:1,3',
            'agencia'   => 'digits_between:1,4',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required'       => 'El campo :attribute es requerido.',
            '*.min'            => 'El campo :attribute debe tener mínimo 3 letras.',
            '*.max'            => 'El campo :attribute debe tener máximo 20 letras.',
            '*.email'          => 'El campo :attribute es invalido.',
            '*.digits_between' => 'El campo :attribute debe contener solo numeros.',
            '*.required_if'    => 'El campo :attribute es requerido.'
        ];
    }
}
