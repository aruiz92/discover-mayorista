<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreAgencyUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $agencyUserId = $_POST['agency_user_id'];

        return [
            'correo'    => 'required|email|unique:agency_users,correo,'.$agencyUserId,
            'nombre'    => 'required',
            'agency_id' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => 'El campo :attribute es requerido.',
            '*.email'    => 'El campo :attribute es invalido.',
        ];
    }
}
