<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class StoreAgencyUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'nombre'    => 'required',
          'correo'    => 'required|email|unique:agency_users,correo',
          'agency_id' => 'required|exists:agencies,id',
          'role'      => 'required|exists:roles,id'
      ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required'    => 'Ingresa un nombre',
            'correo.required'    => 'Ingresa un correo',
            'correo.email'       => 'Ingresa un correo valido',
            'correo.unique'      => 'El correo esta en uso',
            'agency_id.required' => 'Ingresa una agencia',
            'agency_id.exists'   => 'La agencia no existe',
            'role.required'      => 'Selecciona un rol',
            'role.exists'        => 'El rol no existe'
        ];
    }
}
