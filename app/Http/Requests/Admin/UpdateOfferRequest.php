<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class UpdateOfferRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'     => 'required',
            'precio_min' => 'required',
            'url_accion' => 'required|url',
            'imagen_bg'  => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => 'El campo :attribute es requerido',
            '*.url'      => 'El campo :attribute no es una URL valida'
        ];
    }
}
