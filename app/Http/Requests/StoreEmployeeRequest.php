<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreEmployeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres'   => 'required',
            'apellidos' => 'required',
            'correo'    => 'required|email',
            'telefono'  => '',
            'skype'     => '',
            'cargo'     => 'required',
            'foto'      => 'required',
            'orden'     => 'digits_between:1,2',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required'       => 'El campo :attribute es requerido.',
            '*.email'          => 'El campo :attribute es invalido.',
            '*.digits_between' => 'El campo :attribute debe contener solo numeros.',
        ];
    }
}
