<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuthUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'correo'     => 'required|email|exists:agency_users',
            'contrasena' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'correo.required'     => 'El correo es requerido',
            'correo.email'        => 'El correo no es valido',
            'email.exists'        => 'El correo no existe',
            'contrasena.required' => 'La contraseña es requerido',
        ];
    }
}
