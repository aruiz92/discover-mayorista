<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateLogoAgencyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'file|mimes:png|dimensions:min_width=100,min_height=100',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.mimes'          => 'El campo :attribute solo acepta: PNG.',
            '*.file'           => 'El campo :attribute es requerido.',
            '*.dimensions'     => 'El campo :attribute no es del tamaño correcto.',
        ];
    }
}
