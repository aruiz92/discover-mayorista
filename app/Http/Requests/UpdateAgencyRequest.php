<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class UpdateAgencyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::user()->agency->id;

        return [
            'usuario'          => 'required|unique:agencies,usuario,'.$id.'|alpha|min:3|max:20',
            'correo'           => 'required|email|unique:agencies,correo,'.$id,
            'ruc'              => 'required|unique:agencies,ruc,'.$id.'|digits_between:8,11',
            'razon_social'     => 'required|min:3|max:100',
            'nombre_comercial' => 'required|min:3|max:100',
            'nombres'          => 'required|min:3|max:100',
            'telefono'         => 'required',
            'celular'          => '',
            'direccion_1'      => 'required|min:3|max:100',
            'direccion_2'      => 'min:3|max:100',
            'pais'             => 'required|digits_between:1,3',
            'provincia'        => 'required_if:pais,1|digits_between:1,3',
            'distrito'         => 'required_if:pais,1|digits_between:1,3',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required'       => 'El campo :attribute es requerido.',
            '*.unique'         => 'El campo :attribute ya existe.',
            '*.alpha'          => 'El campo :attribute debe contener solo letras.',
            '*.min'            => 'El campo :attribute debe tener mínimo 3 letras.',
            '*.max'            => 'El campo :attribute debe tener máximo 20 letras.',
            '*.email'          => 'El campo :attribute es invalido.',
            '*.digits_between' => 'El campo :attribute debe contener solo numeros.',
            '*.required_if'    => 'El campo :attribute es requerido.',
        ];
    }
}
