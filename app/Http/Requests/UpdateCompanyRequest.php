<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateCompanyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'             => 'required',
            'telefono_fijo'      => 'required',
            'telefono_movil'     => '',
            'correo_informacion' => 'required|email',
            'correo_visitante'   => 'required|email',
            'correo_agente'      => 'required|email',
            'facebook'           => 'required|url',
            'instagram'          => 'required|url',
            'youtube'            => 'required|url',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => 'El campo es requerido',
            '*.email'    => 'Ingresa un correo valido',
            '*.url'      => 'Ingresa una URL valida',
        ];
    }
}
