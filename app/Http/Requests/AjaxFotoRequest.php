<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AjaxFotoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'image|max:3000'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file.image' => 'Ingrese una fotografía',
            'file.size'  => 'Tamaño máximo 3 MB'
        ];
    }
}
