<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateEmployeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres'   => 'required',
            'apellidos' => 'required',
            'correo'    => 'required|email',
            'cargo'     => 'required',
            'orden'     => 'required|digits_between:1,2'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => 'El campo :attribute es requerido',
            '*.email'    => 'El correo no es valido',
            '*.digits'   => 'El campo :attribute debe ser un numero',
        ];
    }
}
