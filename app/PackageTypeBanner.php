<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageTypeBanner extends Model
{
    protected $table = 'package_type_banners';

    public function package_type()
    {
        return $this->belongsTo('App\PackageType');
    }

    public function destination()
    {
        return $this->belongsTo('App\Destination');
    }
}
