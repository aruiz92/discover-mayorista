<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function districts()
    {
        return $this->hasMany('App\District');
    }
}
