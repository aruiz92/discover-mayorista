<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPackageType extends Model
{
    protected $table = 'detail_package_types';
}
