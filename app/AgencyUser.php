<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

// class AgencyUser extends Model
class AgencyUser extends Authenticatable
{
    protected $table = 'agency_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
          'nombre', 'correo', 'contrasena', 'estado',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'contrasena',
    ];

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->contrasena;
    }

    public function getEmailForPasswordReset()
    {
        return $this->correo;
    }

    // hasMany
    public function quote_users()
    {
      return $this->hasMany('App\QuoteUser');
    }

    public function quotation_users()
    {
      return $this->hasMany('App\QuotationUser');
    }

    // BelongsTo
    public function agency()
    {
      return $this->belongsTo('App\Agency');
    }

    public function role()
    {
      return $this->belongsTo('App\Role');
    }
}
