<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $table = 'agencies';

    /**
     * Get the files for the package post.
     */
    public function agency_users()
    {
        return $this->hasMany('App\AgencyUser');
    }

    public function quotes()
    {
        return $this->hasMany('App\Quote');
    }

    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }

    public function quotations()
    {
        return $this->hasMany('App\Quotation');
    }
}
