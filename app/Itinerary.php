<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itinerary extends Model
{
    protected $table = 'itineraries';

    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
