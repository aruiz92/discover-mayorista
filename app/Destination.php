<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $table = 'destinations';

    public function destination_banners()
    {
        return $this->hasMany('App\DestinationBanner');
    }

    public function package_type_banners()
    {
        return $this->hasMany('App\PackageTypeBanner');
    }

    public function packages()
    {
        return $this->hasMany('App\Package');
    }
}
