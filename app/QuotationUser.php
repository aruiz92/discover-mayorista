<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationUser extends Model
{
    protected $table = 'quotation_users';

    public function agency_user()
    {
      return $this->belongsTo('App\AgencyUser');
    }

    public function package()
    {
      return $this->belongsTo('App\Package');
    }
}
