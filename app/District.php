<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function subscribers()
    {
        return $this->hasMany('App\Subscriber');
    }
}
