<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageTab extends Model
{
    protected $table = 'package_tabs';

    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
