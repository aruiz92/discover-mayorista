<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $table = 'quotations';

    public function package()
    {
      return $this->belongsTo('App\Package');
    }

    public function agency()
    {
      return $this->belongsTo('App\Agency');
    }
}
