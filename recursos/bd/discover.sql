-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: discover
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aboutus`
--

DROP TABLE IF EXISTS `aboutus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aboutus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quienes_somos` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `mision` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `vision` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `ventajas` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aboutus`
--

LOCK TABLES `aboutus` WRITE;
/*!40000 ALTER TABLE `aboutus` DISABLE KEYS */;
INSERT INTO `aboutus` VALUES (1,'<p>Dedicados a la creación de paquetes de vacaciones, representaciones de operadores y productos exclusivos, innovadores y diferentes, somos una empresa joven, dinámica y agresiva en el actuar. Conformada por un grupo de profesionales del turismo de destacadas labores en el medio. Enfocados en un proyecto basado en altos valores humanos y éticos comerciales, servicios personalizados más propuestas diferentes e innovadoras en nuestras redes de agencias de viajes.</p>','<p>Nuestra principal misión es respaldar a las agencias de viajes para que puedan otorgar un mejor servicio a todos sus pasajeros.</p>','<p>Convertirnos en socios estratégicos de todas las agencias de viajes del país. Destacando por nuestra calidad de servicio, una propuesta renovada y eficiencia<br> en todos nuestros procesos hasta lograr la solución más idónea.</p>','<ul><li><b>Integridad:</b> Entregamos lo que prometemos y prometemos lo que podemos ofrecer. Hacemos lo que decimos con calidez y mucho pensamiento.</li><li><b>Responsabilidad:</b> Nos comprometemos a satisfacer las necesidades de nuestros clientes y el medio ambiente a través de nuestro trabajo, maximizando los recursos de la empresa.</li><li><b>Innovación:</b> Con un grupo genuinamente talentoso de personas, excelentes servicios, mentes abiertas y clientes con retos complejos y únicos, simplemente no podemos dejar de reinventarnos.</li><li><b>Excelencia:</b> Nos distinguimos a través de nuestro fuerte compromiso de mejorar e inspirar.</li><li><b>Confianza:</b> Generar seguridad a nuestros clientes, no solo al brindarles un servicio de calidad, sino también al darles alcance de soluciones.</li></ul>',1,NULL,NULL);
/*!40000 ALTER TABLE `aboutus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agencies`
--

DROP TABLE IF EXISTS `agencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ruc` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `razon_social` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_comercial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `celular` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom_contacto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district_id` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `agencies_ruc_unique` (`ruc`),
  UNIQUE KEY `agencies_email_unique` (`email`),
  KEY `agencies_district_id_foreign` (`district_id`),
  CONSTRAINT `agencies_district_id_foreign` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agencies`
--

LOCK TABLES `agencies` WRITE;
/*!40000 ALTER TABLE `agencies` DISABLE KEYS */;
INSERT INTO `agencies` VALUES (1,'10472869774','info@disney.com',NULL,'Disney','015425169',NULL,NULL,NULL,NULL,NULL,NULL,NULL,15,1,NULL,NULL),(9,'10472869775','name@ejemplo.com',NULL,'Agencia 1','015425169',NULL,NULL,NULL,NULL,NULL,NULL,NULL,44,1,NULL,NULL),(10,'10472869776','name_2@ejemplo.com',NULL,'Agencia 2','015425169',NULL,NULL,NULL,NULL,NULL,NULL,NULL,45,1,NULL,NULL),(11,'10472869777','name_3@ejemplo.com',NULL,'Agencia 3','015425169',NULL,NULL,NULL,NULL,NULL,NULL,NULL,46,1,NULL,NULL),(12,'10472869778','name_4@ejemplo.com',NULL,'Agencia 4','015425169',NULL,NULL,NULL,NULL,NULL,NULL,NULL,47,1,NULL,NULL);
/*!40000 ALTER TABLE `agencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono_fijo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono_movil` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo_informacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `linkedin` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'Discover','Av. Benavides N° 264 - Of. 703.','Miraflores - Lima, Perú','+51 1 444 4949',NULL,'info@discovermayorista.com','#','#','#','#','#',1,NULL,NULL);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destination_banners`
--

DROP TABLE IF EXISTS `destination_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destination_banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` mediumtext COLLATE utf8_unicode_ci,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#',
  `orden` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `destination_id` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `destination_banners_destination_id_foreign` (`destination_id`),
  CONSTRAINT `destination_banners_destination_id_foreign` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destination_banners`
--

LOCK TABLES `destination_banners` WRITE;
/*!40000 ALTER TABLE `destination_banners` DISABLE KEYS */;
INSERT INTO `destination_banners` VALUES (1,'Cuzco','Lorem ipsum dolor sit amet donec consectetur diam ac mi cursus.',NULL,'banner_1.jpg','#','0',1,1,NULL,NULL),(2,'Cuzco','Lorem ipsum dolor sit amet donec consectetur diam ac mi cursus.',NULL,'banner_1.jpg','#','0',2,1,NULL,NULL);
/*!40000 ALTER TABLE `destination_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destinations`
--

DROP TABLE IF EXISTS `destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destinations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` mediumtext COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `destinations_url_unique` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destinations`
--

LOCK TABLES `destinations` WRITE;
/*!40000 ALTER TABLE `destinations` DISABLE KEYS */;
INSERT INTO `destinations` VALUES (1,'Mundo',NULL,'mundo',1,NULL,NULL),(2,'Perú',NULL,'peru',1,NULL,NULL);
/*!40000 ALTER TABLE `destinations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detail_package_types`
--

DROP TABLE IF EXISTS `detail_package_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detail_package_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package_type_id` int(10) unsigned NOT NULL,
  `package_id` int(10) unsigned NOT NULL,
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detail_package_types_package_type_id_foreign` (`package_type_id`),
  KEY `detail_package_types_package_id_foreign` (`package_id`),
  CONSTRAINT `detail_package_types_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`),
  CONSTRAINT `detail_package_types_package_type_id_foreign` FOREIGN KEY (`package_type_id`) REFERENCES `package_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detail_package_types`
--

LOCK TABLES `detail_package_types` WRITE;
/*!40000 ALTER TABLE `detail_package_types` DISABLE KEYS */;
INSERT INTO `detail_package_types` VALUES (1,1,1,0,NULL,NULL),(2,1,2,0,NULL,NULL),(3,1,3,0,NULL,NULL),(4,1,4,0,NULL,NULL),(6,1,5,0,NULL,NULL),(8,1,6,0,NULL,NULL),(10,1,7,0,NULL,NULL),(12,5,8,0,NULL,NULL),(13,6,8,1,NULL,NULL),(14,1,9,0,NULL,NULL),(16,1,10,0,NULL,NULL),(17,5,11,0,NULL,NULL),(18,6,11,1,NULL,NULL),(19,5,12,0,NULL,NULL),(20,7,12,1,NULL,NULL),(21,5,13,0,NULL,NULL),(22,7,13,1,NULL,NULL),(23,5,14,0,NULL,NULL),(24,7,14,1,NULL,NULL),(25,1,15,0,NULL,NULL),(26,1,16,0,NULL,NULL),(27,2,17,0,NULL,NULL),(28,10,17,1,NULL,NULL),(29,2,18,0,NULL,NULL),(30,9,18,1,NULL,NULL),(31,2,19,0,NULL,NULL),(32,9,19,1,NULL,NULL),(63,14,1,1,NULL,NULL),(64,14,2,1,NULL,NULL),(65,14,3,1,NULL,NULL),(66,13,4,1,NULL,NULL),(67,13,5,1,NULL,NULL),(68,13,6,1,NULL,NULL),(69,13,7,1,NULL,NULL),(70,12,9,1,NULL,NULL),(71,12,10,1,NULL,NULL),(72,12,15,1,NULL,NULL),(73,12,16,1,NULL,NULL),(74,1,20,0,NULL,NULL),(75,12,20,1,NULL,NULL),(76,1,21,0,NULL,NULL),(77,12,21,1,NULL,NULL),(78,1,22,0,NULL,NULL),(79,13,22,1,NULL,NULL),(80,1,23,0,NULL,NULL),(81,13,23,1,NULL,NULL),(82,1,24,0,NULL,NULL),(83,14,24,1,NULL,NULL),(84,1,25,0,NULL,NULL),(85,14,25,1,NULL,NULL),(86,2,26,0,NULL,NULL),(87,9,26,1,NULL,NULL),(88,2,27,0,NULL,NULL),(89,10,27,1,NULL,NULL),(90,2,28,0,NULL,NULL),(91,11,28,1,NULL,NULL),(92,4,29,0,NULL,NULL),(93,15,29,1,NULL,NULL),(94,4,30,0,NULL,NULL),(95,16,30,1,NULL,NULL),(96,4,31,0,NULL,NULL),(97,17,31,1,NULL,NULL),(98,5,32,0,NULL,NULL),(99,6,32,1,NULL,NULL),(100,5,33,0,NULL,NULL),(101,7,33,1,NULL,NULL),(102,5,34,0,NULL,NULL),(103,8,34,1,NULL,NULL),(104,5,35,0,NULL,NULL),(105,8,35,1,NULL,NULL),(106,2,36,0,NULL,NULL),(107,11,36,1,NULL,NULL),(108,4,37,0,NULL,NULL),(109,15,37,1,NULL,NULL),(110,4,38,0,NULL,NULL),(111,16,38,1,NULL,NULL),(112,4,39,0,NULL,NULL),(113,17,39,1,NULL,NULL);
/*!40000 ALTER TABLE `detail_package_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `districts`
--

DROP TABLE IF EXISTS `districts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `districts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `districts_province_id_foreign` (`province_id`),
  CONSTRAINT `districts_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `districts`
--

LOCK TABLES `districts` WRITE;
/*!40000 ALTER TABLE `districts` DISABLE KEYS */;
INSERT INTO `districts` VALUES (1,'Ancón',1,1,NULL,NULL),(2,'Ate',1,1,NULL,NULL),(3,'Barranco',1,1,NULL,NULL),(4,'Breña',1,1,NULL,NULL),(5,'Carabayllo',1,1,NULL,NULL),(6,'Chaclacayo',1,1,NULL,NULL),(7,'Chorrillos',1,1,NULL,NULL),(8,'Cieneguilla',1,1,NULL,NULL),(9,'Comas',1,1,NULL,NULL),(10,'El Agustino',1,1,NULL,NULL),(11,'Independencia',1,1,NULL,NULL),(12,'Jesús María',1,1,NULL,NULL),(13,'La Molina',1,1,NULL,NULL),(14,'La Victoria',1,1,NULL,NULL),(15,'Lima',1,1,NULL,NULL),(16,'Lince',1,1,NULL,NULL),(17,'Los Olivos',1,1,NULL,NULL),(18,'Lurigancho',1,1,NULL,NULL),(19,'Lurín',1,1,NULL,NULL),(20,'Magdalena del Mar',1,1,NULL,NULL),(21,'Miraflores',1,1,NULL,NULL),(22,'Pachacamac',1,1,NULL,NULL),(23,'Pucusana',1,1,NULL,NULL),(24,'Pueblo Libre',1,1,NULL,NULL),(25,'Puente Piedra',1,1,NULL,NULL),(26,'Punta Hermosa',1,1,NULL,NULL),(27,'Punta Negra',1,1,NULL,NULL),(28,'Rímac',1,1,NULL,NULL),(29,'San Bartolo',1,1,NULL,NULL),(30,'San Borja',1,1,NULL,NULL),(31,'San Isidro',1,1,NULL,NULL),(32,'San Juan de Lurigancho',1,1,NULL,NULL),(33,'San Juan de Miraflores',1,1,NULL,NULL),(34,'San Luis',1,1,NULL,NULL),(35,'San Martín de Porres',1,1,NULL,NULL),(36,'San Miguel',1,1,NULL,NULL),(37,'Santa Anita',1,1,NULL,NULL),(38,'Santa María del Mar',1,1,NULL,NULL),(39,'Santa Rosa',1,1,NULL,NULL),(40,'Santiago de Surco',1,1,NULL,NULL),(41,'Surquillo',1,1,NULL,NULL),(42,'Villa El Salvador',1,1,NULL,NULL),(43,'Villa María del Triunfo',1,1,NULL,NULL),(44,'Arequipa',2,1,NULL,NULL),(45,'Cuzco',3,1,NULL,NULL),(46,'Huancayo',4,1,NULL,NULL),(47,'Trujillo',5,1,NULL,NULL);
/*!40000 ALTER TABLE `districts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL),(2,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL),(3,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL),(4,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL),(5,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL),(6,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL),(7,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL),(8,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL),(9,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL),(10,'Raymi Garcia','Garcia-Bedoya','ignacio@discovermayorista.com','+51 952 902 418','nachogarcia67','Administracion &amp; RR.HH',1,NULL,NULL);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itineraries`
--

DROP TABLE IF EXISTS `itineraries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itineraries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha_salida` datetime NOT NULL,
  `fecha_llegada` datetime NOT NULL,
  `vuelo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruta_salida` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruta_llegada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `package_id` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `itineraries_package_id_foreign` (`package_id`),
  CONSTRAINT `itineraries_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itineraries`
--

LOCK TABLES `itineraries` WRITE;
/*!40000 ALTER TABLE `itineraries` DISABLE KEYS */;
INSERT INTO `itineraries` VALUES (1,'2018-03-20 08:00:00','2018-03-20 12:00:00','LA 1245','Lima','Quito',1,1,NULL,NULL),(2,'2018-03-23 08:00:00','2018-03-23 12:00:00','LA 1234','Quito','Lima',1,1,NULL,NULL);
/*!40000 ALTER TABLE `itineraries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2018_02_26_135790_create_destinations_table',1),('2018_02_26_135791_create_provinces_table',1),('2018_02_26_135792_create_districts_table',1),('2018_02_26_135795_create_agencies_table',1),('2018_02_26_135796_create_package_types_table',1),('2018_02_26_135801_create_packages_table',1),('2018_02_26_141909_create_itineraries_table',1),('2018_02_27_200415_create_quotes_table',1),('2018_02_28_143642_create_package_tabs_table',1),('2018_02_28_202523_create_detail_package_types_table',1),('2018_03_01_201012_create_package_type_banners_table',1),('2018_03_01_211426_create_destination_banners_table',1),('2018_03_02_172128_create_company_table',1),('2018_03_02_204854_create_aboutus_table',1),('2018_03_02_212744_create_employees_table',1),('2018_03_07_152752_create_subscribers_table',1),('2018_03_07_203344_create_packages_agencies_table',1),('2018_03_08_194252_create_partners_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_tabs`
--

DROP TABLE IF EXISTS `package_tabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_tabs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `package_id` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `package_tabs_package_id_foreign` (`package_id`),
  CONSTRAINT `package_tabs_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_tabs`
--

LOCK TABLES `package_tabs` WRITE;
/*!40000 ALTER TABLE `package_tabs` DISABLE KEYS */;
INSERT INTO `package_tabs` VALUES (1,'Incluye','<ul><li>Boleto aéreo en VUELO CHARTER en la ruta LIMA – LISBOA – MOSCU – LISBOA – LIMA</li><li>04 (CUATRO) noches de alojamiento con desayunoss incluidos (HOTELES EN IZMAILOVO).</li><li>Traslados Aeropuerto – Hotel – Aeropuerto en bus privado con guía en español.</li><li>PAQUETE OFICIAL MATCH HOSPITALITY EN CATEGORIA MATCH CLUB, incluye entrada y servicios.</li><li>Traslado aéreo a ciudad sede de JUEGO, traslado al estadio, parqueadero, recepción, bebidas, alimentación y regalo. PERU VS DINAMARCA 16 DE JUNIO EN SARANSK</li><li>City tour panorámico en Moscú con visita al centro comercial y guía en español.</li><li>Kit de viaje MUNDIALISTA.</li><li>Tarjeta de asistencia de CORTESIA.</li></ul>',1,1,NULL,NULL),(2,'No incluye','<ul><li>Otros servicios no especificados dentro del programa.</li><li>Gastos de índole personal.</li></ul>',1,1,NULL,NULL),(4,'Itinerario','<table><thead><tr><td>Ruta</td><td>Fecha</td><td>Compañía</td><td>Vuelo</td><td>Salida</td><td>Llegada</td></tr></thead><tbody><tr><td>Lima / Lisboa</td><td>12 Junio</td><td>EURO ATLANTIC AIRWAYS</td><td>479</td><td>13:00 pm</td><td>06:15 am +1</td></tr><tr><td>Lisboa / Moscú</td><td>13 Junio</td><td>CHARTER *</td><td>001</td><td>08:15 am</td><td>15:00 pm</td></tr><tr><td>Moscú / Lisboa</td><td>17 Junio</td><td>CHARTER *</td><td>002</td><td>11:00 am</td><td>13:45 pm</td></tr><tr><td>Lisboa / Lima</td><td>17 Junio</td><td>EURO ATLANTIC AIRWAYS</td><td>480</td><td>15:45 pm</td><td>21:30 pm</td></tr></tbody></table>',1,1,NULL,NULL),(5,'Incluye','<ul><li>Boleto aéreo en VUELO CHARTER & VUELO COMERCIAL en la ruta LIMA – MADRID- MOSCU – MADRID– LIMA con EURO ATLANTIC AIRWAYS.</li><li>09 (NUEVE) noches de alojamiento con desayunos en Moscú.</li><li>Traslado Aeropuerto – Hotel – Aeropuerto en bus privado en Moscú con guía en español.</li><li>Boleto aéreo en VUELO CHARTER & VUELO COMERCIAL en la ruta LIMA – MADRID- MOSCU – MADRID– LIMA con EURO ATLANTIC AIRWAYS.</li><li>09 (NUEVE) noches de alojamiento con desayunos en Moscú.</li><li>Traslado Aeropuerto – Hotel – Aeropuerto en bus privado en Moscú con guía en español.</li><li>02 Traslados Hotel – Aeropuerto – Hotel en Moscú para tomar el vuelo charter MATCH DAY PROGRAMME en bus privado con asistencia en español.</li><li>PAQUETE OFICIAL MATCH HOSPITALITY EN CATEGORIA MATCH CLUB.</li><li>MATCH DAY PROGRAMME: Traslado aéreo a ciudad sede de JUEGO, traslado al estadio, parqueadero, recepción, bebidas, alimentación y regalo.<br>PERU VS FRANCIA JUE 21 DE JUNIO EN EKATERIMBURGO<br>PERU VS AUSTRALIA MAR 26 DE JUNIO EN SOCHI</li><li>City tour panorámico en Moscú con visita al centro comercial y guía en español.</li><li>Kit de viaje MUNDIALISTA (Chip, casaca, gorra, bufanda y canguro)</li><li>Tarjeta de asistencia de CORTESIA.</li><li>Tour líder desde LIMA.</li></ul>',2,1,NULL,NULL),(6,'No incluye','<ul><li>Otros servicios no especificados dentro del programa.</li><li>Gastos de índole personal.</li></ul>',2,1,NULL,NULL),(8,'Itinerario','<table><thead><tr><td>Ruta</td><td>Fecha</td><td>Compañía</td><td>Vuelo</td><td>Salida</td><td>Llegada</td></tr></thead><tbody><tr><td>LIMA / MADRID</td><td>18 JUNIO</td><td>EURO ATLANTIC AIRWAYS</td><td>001</td><td>13:00 PM</td><td>06:15 AM +1</td></tr><tr><td>MADRID / MOSCÚ</td><td>19 JUNIO</td><td>EURO ATLANTIC AIRWAYS</td><td>002</td><td>08:15 AM</td><td>15:00 PM</td></tr><tr><td>MOSCÚ / MADRID</td><td>28 JUNIO</td><td>EURO ATLANTIC AIRWAYS</td><td>003</td><td>11:00 AM</td><td>13:45 PM</td></tr><tr><td>MADRID / LIMA</td><td>28 JUNIO</td><td>EURO ATLANTIC AIRWAYS</td><td>004</td><td>15:45 PM</td><td>21:30 PM</td></tr></tbody></table>',2,1,NULL,NULL),(9,'Incluye','<ul><li>Boleto aéreo LIMA – MADRID - MOSCU – MADRID - LIMA</li><li>10 (DIEZ) noches de alojamiento EN MOSCU con desayunos.</li><li>03 (TRES) noches de alojamiento EN SAN PETERSBURGO con desayunos.</li><li>Traslados Aeropuerto – Hotel – Aeropuerto en bus privado con guía en español.</li><li>PAQUETE OFICIAL MATCH HOSPITALITY EN CATEGORIA VIP MATCH CLUB, incluye entrada y servicios.</li><li>Traslado aéreo a cada ciudad sede de JUEGO, traslado al estadio, parqueadero, recepción, bebidas, alimentación y regalo.<br>PERU VS DINAMARCA 16 JUNIO EN SARANSK<br>PERU VS FRANCIA 21 DE JUNIO EN EKATERIMBURGO<br>PERU VS AUSTRALIA 26 DE JUNIO EN SOCHI</li><li>City tour panorámico en Moscú.</li><li>Kit de viaje MUNDIALISTA. </li><li>Tarjeta de asistencia de CORTESIA.</li><li>Tour líder desde LIMA. </li></ul>',3,1,NULL,NULL),(10,'No incluye','<ul><li>Otros servicios no especificados dentro del programa.</li><li>Gastos de índole personal.</li></ul>',3,1,NULL,NULL),(12,'Itinerario','<table><thead><tr><td>Ruta</td><td>Fecha</td><td>Compañía</td><td>Vuelo</td><td>Salida</td><td>Llegada</td></tr></thead><tbody><tr><td>Lima / Madrid</td><td>13 Junio</td><td>AIR EUROPA</td><td>176</td><td>10:40 am</td><td>05:10 am +1</td></tr><tr><td>Madrid / Moscú</td><td>14 Junio</td><td>CHARTER *</td><td>001</td><td>11:00 am</td><td>16:50 pm</td></tr><tr><td>Moscú / Madrid</td><td>27 Junio</td><td>CHARTER *</td><td>002</td><td>10:30 am</td><td>13:00 pm</td></tr><tr><td>Madrid / Lima</td><td>27 Junio</td><td>AIR EUROPA</td><td>175</td><td>23:55 pm</td><td>04:30 am +1</td></tr></tbody></table>',3,1,NULL,NULL),(13,'Incluye','<ul><li>Boleto aéreo Lima-Panamá-Cancún-Panamá-Lima vía Copa Airlines</li><li>IGV del boleto, $68.94 Queue, $15.00 DY, $30.75HW, $51.31 XT</li><li>Traslados de ingreso y salida </li><li>06 Noches de alojamiento </li><li>Sistema de alimentación TODO INCLUIDO</li><li>Seguro de asistencia al viajero</li><li>Impuestos hoteleros y aéreos</li></ul>',4,1,NULL,NULL),(14,'Itinerario','<table><thead><tr><th>FECHAS</th><th colspan=\"2\">RUTA</th><th>SALIDA</th><th>LLEGADA</th></tr></thead><tbody><tr><td>25/26 JUL</td><td>LIMA</td><td>PANAMÁ</td><td>04:45</td><td>08:26</td></tr><tr><td>25/26 JUL</td><td>PANAMÁ</td><td>CANCÚN</td><td>09:52</td><td>12:37</td></tr><tr><td>30/31 JUL</td><td>CANCÚN</td><td>PANAMÁ</td><td>16:48</td><td>19:23</td></tr><tr><td>30/31 JUL</td><td>PANAMÁ</td><td>LIMA</td><td>21:37</td><td>01:14</td></tr></tbody></table>',4,1,NULL,NULL),(16,'Incluye','<ul><li>Boleto aéreo Lima-Panamá-Cancún-Panamá-Lima vía Copa Airlines</li><li>IGV del boleto, $68.94 Queue, $15.00 DY, $30.75HW, $51.31 XT</li><li>Traslados de ingreso y salida </li><li>06 Noches de alojamiento </li><li>Sistema de alimentación TODO INCLUIDO</li><li>Seguro de asistencia al viajero</li><li>Impuestos hoteleros y aéreos</li></ul>',5,1,NULL,NULL),(17,'Itinerario','<table><thead><tr><th>FECHAS</th><th colspan=\"2\">RUTA</th><th>SALIDA</th><th>LLEGADA</th></tr></thead><tbody><tr><td>25/26 JUL</td><td>LIMA</td><td>PANAMÁ</td><td>04:45</td><td>08:26</td></tr><tr><td>25/26 JUL</td><td>PANAMÁ</td><td>CANCÚN</td><td>09:52</td><td>12:37</td></tr><tr><td>30/31 JUL</td><td>CANCÚN</td><td>PANAMÁ</td><td>16:48</td><td>19:23</td></tr><tr><td>30/31 JUL</td><td>PANAMÁ</td><td>LIMA</td><td>21:37</td><td>01:14</td></tr></tbody></table>',5,1,NULL,NULL),(19,'Incluye','<ul><li>Boleto aéreo Lima-Cancún-Lima vía Copa Airlines</li><li>IGV del boleto, $68.94 Queue, $15.00 DY, $30.75HW, $51.31 XT</li><li>Traslados de ingreso y salida </li><li>06 Noches de alojamiento </li><li>Sistema de alimentación TODO INCLUIDO</li><li>Seguro de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',6,1,NULL,NULL),(20,'Itinerario','<table><thead><tr><th>FECHAS</th><th colspan=\"2\">RUTA</th><th>SALIDA</th><th>LLEGADA</th></tr></thead><tbody><tr><td>25/26 JUL</td><td>LIMA</td><td>PANAMÁ</td><td>04:45</td><td>08:26</td></tr><tr><td>25/26 JUL</td><td>PANAMÁ</td><td>CANCÚN</td><td>09:52</td><td>12:37</td></tr><tr><td>30/31 JUL</td><td>CANCÚN</td><td>PANAMÁ</td><td>16:48</td><td>19:23</td></tr><tr><td>30/31 JUL</td><td>PANAMÁ</td><td>LIMA</td><td>21:37</td><td>01:14</td></tr></tbody></table>',6,1,NULL,NULL),(22,'Incluye','<ul><li>Boleto aéreo Lima-Cancún-Lima vía Copa Airlines</li><li>IGV del boleto, $68.94 Queue, $15.00 DY, $30.75HW, $51.31 XT</li><li>Traslados de ingreso y salida</li><li>06 Noches de alojamiento</li><li>Sistema de alimentación TODO INCLUIDO</li><li>Seguro de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',7,1,NULL,NULL),(23,'Itinerario','<table><thead><tr><th>FECHAS</th><th colspan=\"2\">RUTA</th><th>SALIDA</th><th>LLEGADA</th></tr></thead><tbody><tr><td>25/26 JUL</td><td>LIMA</td><td>PANAMÁ</td><td>04:45</td><td>08:26</td></tr><tr><td>25/26 JUL</td><td>PANAMÁ</td><td>CANCÚN</td><td>09:52</td><td>12:37</td></tr><tr><td>30/31 JUL</td><td>CANCÚN</td><td>PANAMÁ</td><td>16:48</td><td>19:23</td></tr><tr><td>30/31 JUL</td><td>PANAMÁ</td><td>LIMA</td><td>21:37</td><td>01:14</td></tr></tbody></table>',7,1,NULL,NULL),(25,'Incluye','<ul><li>Boleto Aéreo Lima / Bogotá / Lima vía Avianca</li><li>IGV del Boleto, Queue del boleto, USD$ 15.00 DY, USD$30.75 HW, USD$112.58 XT.</li><li>1 noche de Alojamiento en Villa de Leyva, Pueblo Patrimonio de Colombia</li><li>2 noches de Alojamiento en Paipa</li><li>Desayunos diarios</li><li>Visita y entrada a la Catedral de Sal</li><li>Traslados y entrada al Spa Termal en Paipa: cama de burbujas, jacuzzi, piscina termal, sauna</li><li>Visita al Viñedo de Punta Larga</li><li>Visita a fábrica de queso Paipa, uno de los quesos con denominación de origen en Colombia.</li><li>Recorrido por pueblos típicos, visitando Ráquira (pueblo de artesanías en barro), Nobsa (tejidos en lana), Tibasosa (pueblo de frutas y feijoa), Monguí (Pueblo patrimonio de Colombia, fábricas de balones y cueros).</li><li>Transporte todo el programa desde Bogotá (Servicio Puerta - Puerta), excepto en los días libres, con guianza y atención personalizada según el itinerario.</li><li>Entrada a pueblito boyacense y a distintos atractivos durante el recorrido</li><li>Toures en servicio regular (grupo) o privado según disponibilidad.</li><li>Guía acompañante en los toures. (En español)</li><li>Tarjeta de asistencia médica</li><li>Impuestos Aéreos</li></ul>',8,1,NULL,NULL),(27,'Incluye','<ul><li>Boleto aéreo Lima-Cancún-Lima vía Copa Airlines</li><li>IGV del boleto, $68.94 Queue, $15.00 DY, $30.75HW, $51.31 XT</li><li>Traslados de ingreso y salida</li><li>06 Noches de alojamiento</li><li>Sistema de alimentación TODO INCLUIDO</li><li>Entradas Gratis e ilimitadas: Xcaret, Xel-Há, Xplor, Xplor Fuego, Xenses, Xoximilco y los tours de Xenotes, Xichen, Tulum y Cobá</li><li>Seguro de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',9,1,NULL,NULL),(28,'Itinerario','<table><thead><tr><th>FECHAS</th><th colspan=\"2\">RUTA</th><th>SALIDA</th><th>LLEGADA</th></tr></thead><tbody><tr><td>25/26 JUL</td><td>LIMA</td><td>PANAMÁ</td><td>04:45</td><td>08:26</td></tr><tr><td>25/26 JUL</td><td>PANAMÁ</td><td>CANCÚN</td><td>09:52</td><td>12:37</td></tr><tr><td>30/31 JUL</td><td>CANCÚN</td><td>PANAMÁ</td><td>16:48</td><td>19:23</td></tr><tr><td>30/31 JUL</td><td>PANAMÁ</td><td>LIMA</td><td>21:37</td><td>01:14</td></tr></tbody></table>',9,1,NULL,NULL),(30,'Incluye','<ul><li>Boleto aéreo Lima – Foz de Iguazú – Lima vía Latam</li><li>IGV del boleto, $15.00 DY, $30.75HW, $107.05 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>04 Noches de alojamiento en hotel seleccionado</li><li>Desayunos</li><li>Visita a las cataratas lado brasilero (sin entradas)</li><li>Visita a las cataratas lado argentino (sin entradas)</li><li>Visita al parque Dreamland: Valle de los dinosaurios con entradas + Bar de Hielo (-11°C, trajes especiales, open bar y tiempo ilimitado de permanencia).</li><li>Cortesías: Visita parque de aves (sin entradas – mismo día de visita cataratas lado brasilero),</li>Visita al Hito de las Tres Fronteras (mismo día de visita a las cataratas lado argentino), parada en el Centro de artesanías de Foz de Iguazú, visita al Duty Free Shop.<li>Seguro de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',10,1,NULL,NULL),(31,'Itinerario','<table><thead><tr><td>FECHAS</td><td>VUELO</td><td>RUTA</td><td>SALIDA</td><td>LLEGADA</td></tr></thead><tbody><tr><td>27 JUL</td><td>LA 2443</td><td>LIM</td><td>IGU</td><td>12:35</td><td>18:30</td></tr><tr><td>31 JUL</td><td>LA 2442</td><td>IGU</td><td>LIM</td><td>19:45</td><td>22:10</td></tr></tbody></table>',10,1,NULL,NULL),(33,'Con boletos aéreos','<p><strong>Incluye</strong></p><ul><li>Boletos Aéreos.</li><li>Impuestos TKT DY $15 HW $30.75 XT $102.81</li><li>Traslados de ingreso y salida en servicio regular</li><li>04 Noches de alojamiento</li><li>Sistema de alimentación TODO INCLUIDO</li><li>Tarjeta de asistencia por 05 días</li><li>Impuestos hoteleros</li></ul',11,1,NULL,NULL),(34,'Sin boletos aéreos','<p><strong>Incluye:</strong></p><ul><li>Traslados de ingreso y salida en servicio regular</li><li>04 Noches de alojamiento</li><li>Sistema de alimentación TODO INCLUIDO</li><li>Tarjeta de asistencia por 05 días</li><li>Impuestos hoteleros</li></ul>',11,1,NULL,NULL),(35,'Incluye','<ul><li>Traslado de llegada y salida en Buenos Aires</li><li>City Tour de mediodía</li><li>04 noches de alojamiento</li><li>Desayunos</li><li>Impuestos hoteleros</li><li>Seguro de asistencia.</li></ul>',12,1,NULL,NULL),(36,'Importante','<ul><li>Tarifas sujetas a cambios sin previo aviso, paquete válido para comprar hasta el 31/01/18.</li><li>Debido a los múltiples cambios que ocurren diariamente en turismo estos precios deben ser confirmados a la hora de hacer la reserva.</li><li>Tarifas comisionables al 10% .</li><li>Incentivo al counter: $10 por pasajero.</li></ul>',12,1,NULL,NULL),(37,'Incluye','<ul><li>Traslado de llegada y salida en aeropuerto Internacional de São Paulo-Guarulhos</li><li>Desayuno</li><li>Entrada para los 3 días del evento en sector Lola Pass (pista)</li><li>Tarjeta de Asistencia</li><li>04 Noches de alojamiento en hotel seleccionado</li><li>Impuestos hoteleros</li></ul>',13,1,NULL,NULL),(38,'Importante','<ul><li>Tarifas sujetas a cambios sin previo aviso, paquete válido para comprar hasta el 30/01/17.</li><li>Debido a los múltiples cambios que ocurren diariamente en turismo estos precios deben ser confirmados a la hora de hacer la reserva.</li><li>Tarifas comisionables al 10% (descontando IGV e impuestos aéreos).</li><li>Incentivo al counter: $10 por pasajero.</li></ul>',13,1,NULL,NULL),(39,'Incluye','<ul><li>Traslado de llegada y salida en Santiago</li><li>Traslados al parque para todos los días del evento</li><li>Entrada a elegir (por los 3 días de concierto).</li><li>Tarjeta de Asistencia</li><li>03 Noches de alojamiento en hotel seleccionado</li><li>Impuestos hoteleros</li></ul>',14,1,NULL,NULL),(40,'Importante','<ul><li>Tarifas sujetas a cambios sin previo aviso, paquete válido para comprar hasta el 31/01/18.</li><li>Debido a los múltiples cambios que ocurren diariamente en turismo estos precios deben ser confirmados a la hora de hacer la reserva.</li><li>Tarifas comisionables al 10%.</li><li>Incentivo al counter: $10 por pasajero.</li></ul>',14,1,NULL,NULL),(41,'Incluye','<ul><li>Boleto aéreo Lima-Punta Cana-Lima vía Copa Airlines</li><li>IGV del boleto, $80.28 Queue, $15.00 DY, $30.75HW, $85.10 XT</li><li>Traslados de ingreso y salida</li><li>05 Noches de alojamiento</li><li>Sistema de alimentación TODO INCLUIDO</li><li>Seguro de asistencia</li><li>Impuestos hoteleros y aéreos</li><li>GRATIS: City Tour + Tour de Compras</li></ul>',15,1,NULL,NULL),(42,'Importante','<ul><li>Tarifas con promoción válida para comprar hasta el 31/01/18.</li><li>Precios válidos para viajar hasta el 31 de Julio 2018.</li><li>Debido a los múltiples cambios que ocurren diariamente en turismo estos precios deben ser confirmados a la hora de hacer la reserva.</li><li>Entrega de los boletos y vouchers se realizarán 48 horas antes de la fecha de salida.</li><li>Anulaciones y cancelaciones se penalizará con el 100% una vez realizado el pago final. NO SHOW: Se penalizará al 100%.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Precios solo aplica para salida específica de Fiestas Patrias del 25 al 31 de Julio 2018.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',15,1,NULL,NULL),(43,'Incluye','<ul><li>Boleto aéreo Lima-Punta Cana-Lima vía Copa Airlines</li><li>IGV del boleto, $80.28 Queue, $15.00 DY, $30.75HW, $85.10 XT</li><li>Traslados de ingreso y salida</li><li>05 Noches de alojamiento</li><li>Sistema de alimentación TODO INCLUIDO</li><li>Seguro de asistencia</li><li>Impuestos hoteleros y aéreos</li><li>GRATIS: City Tour + Tour de Compras</li></ul>',16,1,NULL,NULL),(44,'Importante','<ul><li>Tarifas con promoción válida para comprar hasta el 31/01/18.</li><li>Precios válidos para viajar hasta el 31 de Julio 2018.</li><li>Debido a los múltiples cambios que ocurren diariamente en turismo estos precios deben ser confirmados a la hora de hacer la reserva.</li><li>Entrega de los boletos y vouchers se realizarán 48 horas antes de la fecha de salida.</li><li>Anulaciones y cancelaciones se penalizará con el 100% una vez realizado el pago final. NO SHOW: Se penalizará al 100%.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Precios solo aplica para salida específica de Fiestas Patrias del 25 al 31 de Julio 2018.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',16,1,NULL,NULL),(45,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona - Porlamar – Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.75HW, $102.81 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>04 Noches de alojamiento</li><li>Sistema de alimentación TODO INCLUIDO</li><li>Full Day “Isla Cubagua”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',17,1,NULL,NULL),(46,'Importante','<ul><li>Precios válidos para reservar hasta el 15 de Enero 2018.</li><li>Precios válidos para viajar hasta el 31 de Marzo 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',17,1,NULL,NULL),(47,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.75HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>04 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',18,1,NULL,NULL),(48,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',18,1,NULL,NULL),(49,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',19,1,NULL,NULL),(50,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',19,1,NULL,NULL),(51,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',20,1,NULL,NULL),(52,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',20,1,NULL,NULL),(53,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',21,1,NULL,NULL),(54,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',21,1,NULL,NULL),(55,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',22,1,NULL,NULL),(56,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',22,1,NULL,NULL),(57,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',23,1,NULL,NULL),(58,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',23,1,NULL,NULL),(59,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',24,1,NULL,NULL),(60,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',24,1,NULL,NULL),(61,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',25,1,NULL,NULL),(62,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',25,1,NULL,NULL),(63,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',26,1,NULL,NULL),(64,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',26,1,NULL,NULL),(65,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',27,1,NULL,NULL),(66,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',27,1,NULL,NULL),(67,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',28,1,NULL,NULL),(68,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',28,1,NULL,NULL),(69,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',29,1,NULL,NULL),(70,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',29,1,NULL,NULL),(71,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',30,1,NULL,NULL),(72,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',30,1,NULL,NULL),(73,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',31,1,NULL,NULL),(74,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',31,1,NULL,NULL),(75,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',32,1,NULL,NULL),(76,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',32,1,NULL,NULL),(77,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',33,1,NULL,NULL),(78,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',33,1,NULL,NULL),(79,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',34,1,NULL,NULL),(80,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',34,1,NULL,NULL),(81,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',35,1,NULL,NULL),(82,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',35,1,NULL,NULL),(83,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',36,1,NULL,NULL),(84,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',36,1,NULL,NULL),(85,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',37,1,NULL,NULL),(86,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',37,1,NULL,NULL),(87,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',38,1,NULL,NULL),(88,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',38,1,NULL,NULL),(89,'Incluye','<ul><li>Boleto aéreo Lima – Barcelona – Porlamar - Barcelona - Lima vía Avior</li><li>IGV del boleto, $15.00 DY, $30.74HW, $119.90 XT.</li><li>Traslados de ingreso y salida en servicio regular</li><li>06 Noches de alojamiento en hotel seleccionado</li><li>Sistema de alimentación “TODO INCLUIDO”</li><li>Tarjeta de asistencia</li><li>Impuestos hoteleros y aéreos</li></ul>',39,1,NULL,NULL),(90,'Importante','<ul><li>Precios válidos para reservar hasta el 05/02/2018.</li><li>Precios válidos para viajar hasta el 20/12/ 2018.</li><li>Precios sujetos a cambios sin previo aviso.</li><li>Tarifas comisionables al 10%.</li><li>Precios no aplican en fechas de feriados calendarios del destino, eventos, convenciones etc, para lo cual aplicará un suplemento que será indicado al momento de solicitar la reserva.</li><li>La empresa no reconocerá derecho de devolución alguno, por el uso de servicios de terceros, ajenos al servicio contratado y que no hayan sido autorizados previamente por escrito por la empresa.</li><li>Las propinas no están incluidas en ningún servicio que ofrecemos. Al requerir servicios de maleteros ó cualquier servicio adicional, las propinas son obligatorias.</li><li>Los traslados y excursiones son en base a servicio regular, es decir en base a grupos de pasajeros por destino. El pasajero debe de tener en cuenta que todos los traslados de llegada y salida del aeropuerto, hotel y las excursiones, deberá de esperar al transportista, en el lugar indicado y horario establecido (la información de horarios se les comunicará en el destino final).</li></ul>',39,1,NULL,NULL);
/*!40000 ALTER TABLE `package_tabs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_type_banners`
--

DROP TABLE IF EXISTS `package_type_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_type_banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` mediumtext COLLATE utf8_unicode_ci,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#',
  `orden` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `package_type_id` int(10) unsigned NOT NULL,
  `destination_id` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `package_type_banners_package_type_id_foreign` (`package_type_id`),
  KEY `package_type_banners_destination_id_foreign` (`destination_id`),
  CONSTRAINT `package_type_banners_destination_id_foreign` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`),
  CONSTRAINT `package_type_banners_package_type_id_foreign` FOREIGN KEY (`package_type_id`) REFERENCES `package_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_type_banners`
--

LOCK TABLES `package_type_banners` WRITE;
/*!40000 ALTER TABLE `package_type_banners` DISABLE KEYS */;
INSERT INTO `package_type_banners` VALUES (1,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',5,1,1,NULL,NULL),(2,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',5,1,1,NULL,NULL),(3,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',5,2,1,NULL,NULL),(4,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',5,2,1,NULL,NULL),(5,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',2,1,1,NULL,NULL),(6,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',2,1,1,NULL,NULL),(7,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',2,2,1,NULL,NULL),(8,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',2,2,1,NULL,NULL),(9,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',3,1,1,NULL,NULL),(10,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',3,1,1,NULL,NULL),(11,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',3,2,1,NULL,NULL),(12,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',3,2,1,NULL,NULL),(13,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',1,1,1,NULL,NULL),(14,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',1,1,1,NULL,NULL),(15,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',1,2,1,NULL,NULL),(16,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',1,2,1,NULL,NULL),(17,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',4,1,1,NULL,NULL),(18,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',4,1,1,NULL,NULL),(19,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',4,2,1,NULL,NULL),(20,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',4,2,1,NULL,NULL);
/*!40000 ALTER TABLE `package_type_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_types`
--

DROP TABLE IF EXISTS `package_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` mediumtext COLLATE utf8_unicode_ci,
  `orden` smallint(6) NOT NULL DEFAULT '0',
  `padre_id` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_types`
--

LOCK TABLES `package_types` WRITE;
/*!40000 ALTER TABLE `package_types` DISABLE KEYS */;
INSERT INTO `package_types` VALUES (1,'Bloqueos',NULL,1,0,'bloqueo',1,NULL,NULL),(2,'Temáticos',NULL,3,0,'tematicos',1,NULL,NULL),(3,'Grupales',NULL,4,0,'grupales',1,NULL,NULL),(4,'Premium',NULL,5,0,'premium',1,NULL,NULL),(5,'Vacaciones',NULL,2,0,'vacaciones',1,NULL,NULL),(6,'Culturales',NULL,0,5,'culturales',1,NULL,NULL),(7,'Festivales/Conciertos',NULL,1,5,'festivales-conciertos',1,NULL,NULL),(8,'Deportivos',NULL,2,5,'deportivos',1,NULL,NULL),(9,'Escolares',NULL,0,2,'culturales',1,NULL,NULL),(10,'Incentivos',NULL,1,2,'festivales-conciertos',1,NULL,NULL),(11,'Diversion',NULL,2,2,'deportivos',1,NULL,NULL),(12,'Culturales',NULL,0,1,'culturales',1,NULL,NULL),(13,'Festivales/Conciertos',NULL,1,1,'festivales-conciertos',1,NULL,NULL),(14,'Deportivos',NULL,2,1,'deportivos',1,NULL,NULL),(15,'Mundial',NULL,0,4,'culturales',1,NULL,NULL),(16,'Cruceros',NULL,1,4,'festivales-conciertos',1,NULL,NULL),(17,'Deportivos',NULL,2,4,'deportivos',1,NULL,NULL);
/*!40000 ALTER TABLE `package_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` mediumtext COLLATE utf8_unicode_ci,
  `precio` decimal(11,2) DEFAULT NULL,
  `nro_dias` smallint(6) DEFAULT NULL,
  `nro_noches` smallint(6) DEFAULT NULL,
  `imagen_detalle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen_miniatura` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `archivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promocion` tinyint(1) NOT NULL DEFAULT '0',
  `destination_id` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `packages_destination_id_foreign` (`destination_id`),
  CONSTRAINT `packages_destination_id_foreign` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
INSERT INTO `packages` VALUES (1,'1er Partido PERU VS DINAMARCA ','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,5,4,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(2,'2 Partidos: PERU vs FRANCIA &  PERU vs AUSTRALIA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,10,9,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(3,'3 Partidos: PERU VS DINAMARCA & FRANCIA & AUSTRALIA ','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,14,13,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(4,'FIESTAS PATRIAS CANCÚN','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,6,5,'1.jpg','min_1.jpg','1.pdf',1,1,NULL,1,NULL,NULL),(5,'FIESTAS PATRIAS PLAYA DEL CARMEN','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,6,5,'1.jpg','min_1.jpg','1.pdf',1,1,NULL,1,NULL,NULL),(6,'FIESTAS PATRIAS RIVIERA MAYA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,6,5,'1.jpg','min_1.jpg','1.pdf',1,1,NULL,1,NULL,NULL),(7,'FIESTAS PATRIAS CANCÚN O RIVIERA MAYA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,6,5,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(8,'BOYACÁ & VILLA DE LEYVA COLOMBIA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(9,'XCARET MÉXICO ALL FUN INCLUSIVE FIESTAS PATRIAS','<p>Vivir y disfrutar lo mejor de México nunca fue tan fácil como en Hotel Xcaret México, donde tienes acceso ilimitado a los parques y tours de Xcaret. Xcaret, Xel-Há, Xplor, Xplor Fuego, Xoximilco, Xenses, Xenotes y Xichén, con transportación incluida y el plan de alimentos que cada atracción ofrece. Además de transportación aeropuerto-hotel-aeropuerto. Es por ello que los huéspedes no tendrán que preocuparse de absolutamente nada. Nuestra misión y mayor recompensa será, que en todo momento se lleven a México en la piel.</p>',NULL,6,5,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(10,'FIESTAS PATRIAS EN FOZ DE IGUAZÚ','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,5,4,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(11,'ISLA MARGARITA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,5,4,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(12,'LOLLAPALOOZA BUENOS AIRES','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,5,4,'1.jpg','min_1.jpg','1.pdf',1,1,NULL,1,NULL,NULL),(13,'LOLLAPALOOZA SAO PAULO','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,5,4,'1.jpg','min_1.jpg','1.pdf',1,1,NULL,1,NULL,NULL),(14,'LOLLAPALOOZA SANTIAGO DE CHILE','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(15,'FIESTAS PATRIAS PUNTA CANA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,6,5,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(16,'FIESTAS PATRIAS PUNTA CANA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,6,5,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(17,'SAN VALENTIN 2018 ISLA MARGARITA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,5,4,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(18,'SEMANA SANTA ISLA MARGARITA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,5,4,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(19,'SEMANA SANTA ISLA MARGARITA','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,7,6,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(20,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(21,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(22,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(23,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(24,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(25,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(26,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(27,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(28,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(29,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(30,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(31,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(32,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(33,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(34,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,2,NULL,1,NULL,NULL),(35,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(36,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(37,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(38,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL),(39,'Lorem Ipsum','<p>Lorem ipsum dolor sit amet, ei eos zril intellegat. Lorem partem necessitatibus mel te. Vis enim apeirian facilisi no, modus legere delicata nec ut. Zril inciderint nam et. At pri quas dicat, albucius evertitur incorrupte pri te. Liber nostrud constituto pri an, ei dicant delectus nec.</p>',NULL,4,3,'1.jpg','min_1.jpg','1.pdf',0,1,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages_agencies`
--

DROP TABLE IF EXISTS `packages_agencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages_agencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `package_id` int(10) unsigned NOT NULL,
  `agency_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `packages_agencies_package_id_foreign` (`package_id`),
  KEY `packages_agencies_agency_id_foreign` (`agency_id`),
  CONSTRAINT `packages_agencies_agency_id_foreign` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`),
  CONSTRAINT `packages_agencies_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages_agencies`
--

LOCK TABLES `packages_agencies` WRITE;
/*!40000 ALTER TABLE `packages_agencies` DISABLE KEYS */;
INSERT INTO `packages_agencies` VALUES (1,1,1,NULL,NULL),(2,2,1,NULL,NULL),(3,3,1,NULL,NULL),(4,4,1,NULL,NULL),(5,5,1,NULL,NULL),(6,6,1,NULL,NULL),(7,7,1,NULL,NULL),(8,8,1,NULL,NULL),(9,9,1,NULL,NULL),(10,10,1,NULL,NULL),(11,11,1,NULL,NULL),(12,12,1,NULL,NULL),(13,13,1,NULL,NULL),(14,14,1,NULL,NULL),(15,15,1,NULL,NULL),(16,16,1,NULL,NULL),(17,17,1,NULL,NULL),(18,18,1,NULL,NULL),(19,19,1,NULL,NULL);
/*!40000 ALTER TABLE `packages_agencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (1,'360 Experience',1,NULL,NULL),(2,'Abreu',1,NULL,NULL),(3,'Aquarel',1,NULL,NULL),(4,'CL Mundo',1,NULL,NULL),(5,'Collibri',1,NULL,NULL),(6,'Convencional',1,NULL,NULL),(7,'Destinos Costa Rica',1,NULL,NULL),(8,'DMC',1,NULL,NULL),(9,'Global Assist',1,NULL,NULL),(10,'Grayline',1,NULL,NULL),(11,'Hover Tours',1,NULL,NULL),(12,'Infinitas',1,NULL,NULL),(13,'Life Destionation',1,NULL,NULL),(14,'MMC',1,NULL,NULL),(15,'Nexus',1,NULL,NULL),(16,'Otium',1,NULL,NULL),(17,'Panamericana de viajes',1,NULL,NULL),(18,'Rge Style',1,NULL,NULL),(19,'Seven Tours',1,NULL,NULL),(20,'Sistema Moderno',1,NULL,NULL),(21,'Surland',1,NULL,NULL),(22,'Trapsatur',1,NULL,NULL),(23,'Travelplan',1,NULL,NULL),(24,'Turar',1,NULL,NULL),(25,'Welcomebeds',1,NULL,NULL);
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinces` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provinces`
--

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` VALUES (1,'Lima',1,NULL,NULL),(2,'Arequipa',1,NULL,NULL),(3,'Cuzco',1,NULL,NULL),(4,'Huancayo',1,NULL,NULL),(5,'Trujillo',1,NULL,NULL);
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotes`
--

DROP TABLE IF EXISTS `quotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `asunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `package_id` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quotes_package_id_foreign` (`package_id`),
  CONSTRAINT `quotes_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotes`
--

LOCK TABLES `quotes` WRITE;
/*!40000 ALTER TABLE `quotes` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecnac` date NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `envio` tinyint(1) NOT NULL DEFAULT '1',
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscribers_correo_unique` (`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-09  9:23:25
