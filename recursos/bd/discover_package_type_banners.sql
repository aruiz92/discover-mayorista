-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: discover
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `package_type_banners`
--

DROP TABLE IF EXISTS `package_type_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_type_banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` mediumtext COLLATE utf8_unicode_ci,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#',
  `orden` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `package_type_id` int(10) unsigned NOT NULL,
  `destination_id` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `package_type_banners_package_type_id_foreign` (`package_type_id`),
  KEY `package_type_banners_destination_id_foreign` (`destination_id`),
  CONSTRAINT `package_type_banners_destination_id_foreign` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`),
  CONSTRAINT `package_type_banners_package_type_id_foreign` FOREIGN KEY (`package_type_id`) REFERENCES `package_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_type_banners`
--

LOCK TABLES `package_type_banners` WRITE;
/*!40000 ALTER TABLE `package_type_banners` DISABLE KEYS */;
INSERT INTO `package_type_banners` VALUES (1,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',5,1,1,NULL,NULL),(2,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',5,1,1,NULL,NULL),(3,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',5,2,1,NULL,NULL),(4,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',5,2,1,NULL,NULL),(5,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',2,1,1,NULL,NULL),(6,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',2,1,1,NULL,NULL),(7,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',2,2,1,NULL,NULL),(8,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',2,2,1,NULL,NULL),(9,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',3,1,1,NULL,NULL),(10,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',3,1,1,NULL,NULL),(11,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',3,2,1,NULL,NULL),(12,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',3,2,1,NULL,NULL),(13,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',1,1,1,NULL,NULL),(14,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',1,1,1,NULL,NULL),(15,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',1,2,1,NULL,NULL),(16,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',1,2,1,NULL,NULL),(17,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',4,1,1,NULL,NULL),(18,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',4,1,1,NULL,NULL),(19,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','0',4,2,1,NULL,NULL),(20,'Paquetes perfectos para disfrutar sus vacaciones','Muchas experiencias te esperan en distintos destinos',NULL,'banner_1.jpg','#','1',4,2,1,NULL,NULL);
/*!40000 ALTER TABLE `package_type_banners` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-09  7:01:31
