-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: discover
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `package_types`
--

DROP TABLE IF EXISTS `package_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` mediumtext COLLATE utf8_unicode_ci,
  `orden` smallint(6) NOT NULL DEFAULT '0',
  `padre_id` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_types`
--

LOCK TABLES `package_types` WRITE;
/*!40000 ALTER TABLE `package_types` DISABLE KEYS */;
INSERT INTO `package_types` VALUES (1,'Bloqueos',NULL,1,0,'bloqueo',1,NULL,NULL),(2,'Temáticos',NULL,3,0,'tematicos',1,NULL,NULL),(3,'Grupales',NULL,4,0,'grupales',1,NULL,NULL),(4,'Premium',NULL,5,0,'premium',1,NULL,NULL),(5,'Vacaciones',NULL,2,0,'vacaciones',1,NULL,NULL),(6,'Culturales',NULL,0,5,'culturales',1,NULL,NULL),(7,'Festivales/Conciertos',NULL,1,5,'festivales-conciertos',1,NULL,NULL),(8,'Deportivos',NULL,2,5,'deportivos',1,NULL,NULL),(9,'Culturales',NULL,0,2,'culturales',1,NULL,NULL),(10,'Festivales/Conciertos',NULL,1,2,'festivales-conciertos',1,NULL,NULL),(11,'Deportivos',NULL,2,2,'deportivos',1,NULL,NULL),(12,'Culturales',NULL,0,1,'culturales',1,NULL,NULL),(13,'Festivales/Conciertos',NULL,1,1,'festivales-conciertos',1,NULL,NULL),(14,'Deportivos',NULL,2,1,'deportivos',1,NULL,NULL),(15,'Culturales',NULL,0,4,'culturales',1,NULL,NULL),(16,'Festivales/Conciertos',NULL,1,4,'festivales-conciertos',1,NULL,NULL),(17,'Deportivos',NULL,2,4,'deportivos',1,NULL,NULL);
/*!40000 ALTER TABLE `package_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-09  7:01:29
