-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: discover
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aboutus`
--

DROP TABLE IF EXISTS `aboutus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aboutus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quienes_somos` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `mision` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `vision` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `ventajas` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aboutus`
--

LOCK TABLES `aboutus` WRITE;
/*!40000 ALTER TABLE `aboutus` DISABLE KEYS */;
INSERT INTO `aboutus` VALUES (1,'<p>Dedicados a la creación de paquetes de vacaciones, representaciones de operadores y productos exclusivos, innovadores y diferentes, somos una empresa joven, dinámica y agresiva en el actuar. Conformada por un grupo de profesionales del turismo de destacadas labores en el medio. Enfocados en un proyecto basado en altos valores humanos y éticos comerciales, servicios personalizados más propuestas diferentes e innovadoras en nuestras redes de agencias de viajes.</p>','<p>Nuestra principal misión es respaldar a las agencias de viajes para que puedan otorgar un mejor servicio a todos sus pasajeros.</p>','<p>Convertirnos en socios estratégicos de todas las agencias de viajes del país. Destacando por nuestra calidad de servicio, una propuesta renovada y eficiencia<br> en todos nuestros procesos hasta lograr la solución más idónea.</p>','<ul><li><b>Integridad:</b> Entregamos lo que prometemos y prometemos lo que podemos ofrecer. Hacemos lo que decimos con calidez y mucho pensamiento.</li><li><b>Responsabilidad:</b> Nos comprometemos a satisfacer las necesidades de nuestros clientes y el medio ambiente a través de nuestro trabajo, maximizando los recursos de la empresa.</li><li><b>Innovación:</b> Con un grupo genuinamente talentoso de personas, excelentes servicios, mentes abiertas y clientes con retos complejos y únicos, simplemente no podemos dejar de reinventarnos.</li><li><b>Excelencia:</b> Nos distinguimos a través de nuestro fuerte compromiso de mejorar e inspirar.</li><li><b>Confianza:</b> Generar seguridad a nuestros clientes, no solo al brindarles un servicio de calidad, sino también al darles alcance de soluciones.</li></ul>',1,NULL,NULL);
/*!40000 ALTER TABLE `aboutus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-09  7:01:31
