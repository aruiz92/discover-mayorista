-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: discover
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `districts`
--

DROP TABLE IF EXISTS `districts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `districts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `districts_province_id_foreign` (`province_id`),
  CONSTRAINT `districts_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `districts`
--

LOCK TABLES `districts` WRITE;
/*!40000 ALTER TABLE `districts` DISABLE KEYS */;
INSERT INTO `districts` VALUES (1,'Ancón',1,1,NULL,NULL),(2,'Ate',1,1,NULL,NULL),(3,'Barranco',1,1,NULL,NULL),(4,'Breña',1,1,NULL,NULL),(5,'Carabayllo',1,1,NULL,NULL),(6,'Chaclacayo',1,1,NULL,NULL),(7,'Chorrillos',1,1,NULL,NULL),(8,'Cieneguilla',1,1,NULL,NULL),(9,'Comas',1,1,NULL,NULL),(10,'El Agustino',1,1,NULL,NULL),(11,'Independencia',1,1,NULL,NULL),(12,'Jesús María',1,1,NULL,NULL),(13,'La Molina',1,1,NULL,NULL),(14,'La Victoria',1,1,NULL,NULL),(15,'Lima',1,1,NULL,NULL),(16,'Lince',1,1,NULL,NULL),(17,'Los Olivos',1,1,NULL,NULL),(18,'Lurigancho',1,1,NULL,NULL),(19,'Lurín',1,1,NULL,NULL),(20,'Magdalena del Mar',1,1,NULL,NULL),(21,'Miraflores',1,1,NULL,NULL),(22,'Pachacamac',1,1,NULL,NULL),(23,'Pucusana',1,1,NULL,NULL),(24,'Pueblo Libre',1,1,NULL,NULL),(25,'Puente Piedra',1,1,NULL,NULL),(26,'Punta Hermosa',1,1,NULL,NULL),(27,'Punta Negra',1,1,NULL,NULL),(28,'Rímac',1,1,NULL,NULL),(29,'San Bartolo',1,1,NULL,NULL),(30,'San Borja',1,1,NULL,NULL),(31,'San Isidro',1,1,NULL,NULL),(32,'San Juan de Lurigancho',1,1,NULL,NULL),(33,'San Juan de Miraflores',1,1,NULL,NULL),(34,'San Luis',1,1,NULL,NULL),(35,'San Martín de Porres',1,1,NULL,NULL),(36,'San Miguel',1,1,NULL,NULL),(37,'Santa Anita',1,1,NULL,NULL),(38,'Santa María del Mar',1,1,NULL,NULL),(39,'Santa Rosa',1,1,NULL,NULL),(40,'Santiago de Surco',1,1,NULL,NULL),(41,'Surquillo',1,1,NULL,NULL),(42,'Villa El Salvador',1,1,NULL,NULL),(43,'Villa María del Triunfo',1,1,NULL,NULL),(44,'Arequipa',2,1,NULL,NULL),(45,'Cuzco',3,1,NULL,NULL),(46,'Huancayo',4,1,NULL,NULL),(47,'Trujillo',5,1,NULL,NULL);
/*!40000 ALTER TABLE `districts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-09  7:01:31
